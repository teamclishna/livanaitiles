﻿using System.Runtime.Serialization;

using Avalonia.Media;
using LivanaiTiles.Utils;

namespace LivanaiTiles.Controllers;

/// <summary>
/// アプリケーションの設定情報
/// </summary>
[DataContract]
public class AppConfigDeserialize
{
    /// <summary>
    /// アプリケーション全体の設定を保存するファイル名
    /// </summary>
    public static readonly string CONF_FILENAME = "app.conf";

    /// <summary>
    /// グラス効果を使用する時のエフェクト方法
    /// </summary>
    public enum AeroGlassType
    {
        /// <summary>未設定</summary>
        UNDEFINED = 0,
        /// <summary>エフェクト無</summary>
        NONE = 1,
        /// <summary>DWMのブラー効果</summary>
        BLUR_BEHIND = 3,
        /// <summary>Fluent Designのアクリル効果</summary>
        ACRYLIC = 4,
    }

    /// <summary>
    /// このオブジェクトの内容を保存するファイルのフルパス
    /// </summary>
    public static string FilePath
    {
        get
        {
            return Path.Combine(
                FileUtils.GetConfigDirectory(true).FullName,
                CONF_FILENAME);
        }
    }

    /// <summary>
    /// グラス効果を使用するかどうか.
    /// true に設定した場合は BLUR_BEHIND を適用する.
    /// </summary>
    [DataMember]
    public bool UseAeroGlass { get; set; }

    /// <summary>
    /// グラス効果を使用する場合のエフェクト方法.
    /// このプロパティが設定されていない場合は UseAeroGlass を参照し、
    /// そちらの設定値を使用する.
    /// </summary>
    [DataMember]
    public AeroGlassType AeroglassEffectType { get; set; }

    /// <summary>
    /// タイルの設定ボタンをマウスオーバー時のみ表示するかどうか.
    /// </summary>
    [DataMember]
    public bool ShowButtonMouseoverOnly { get; set; }

    /// <summary>
    /// タイルウィンドウのデフォルト前景色.
    /// タイルにデフォルトの背景色を使う場合、プラグインはラベルにこの色を適用すること.
    /// </summary>
    [DataMember]
    public Dictionary<string, int> ForeGroundColor { get; set; }

    /// <summary>
    /// タイルウィンドウのデフォルトの背景色
    /// </summary>
    [DataMember]
    public Dictionary<string, int> BackgroundColor { get; set; }

    /// <summary>
    /// タイルの移動時に他のタイルと自動位置調整を有効にするかどうか.
    /// </summary>
    [DataMember]
    public bool UseAutoPosition { get; set; }

    /// <summary>
    /// タイルの自動位置調整を使用する場合、タイル間のマージン
    /// </summary>
    [DataMember]
    public double TileMargin { get; set; }


    public AppConfigDeserialize() {
    }
}
