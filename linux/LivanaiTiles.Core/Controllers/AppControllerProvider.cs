using System.Reflection;

using clUtils.log;

namespace LivanaiTiles.Controllers;

/// <summary>
/// アプリケーションのコントローラを保持、初期化するためのクラス.
/// プラグインから実態にアクセスさせないためにインターフェイス経由で検索、取得する
/// </summary>
public static class AppControllerProvider
{

    // とりあえず仮実装
    // 後でリソース化する
    private static readonly string ERR_CONTROLLER_ALREADY_INITIALIZED = "ERR_CONTROLLER_ALREADY_INITIALIZED";
    private static readonly string ERR_CONTROLLER_NOT_FOUND = "ERR_CONTROLLER_NOT_FOUND";
    

    /// <summary>
    /// コントローラオブジェクト.
    /// アプリケーション内で1個だけ保持する
    /// </summary>
    public static IAppController Instance { get; private set; }

    /// <summary>
    /// コントローラの初期化を行う.
    /// このメソッドはアプリケーション起動時に1回だけ呼び出されなければならない
    /// </summary>
    /// <param name="logger">アプリケーションが使用するロガーオブジェクト</param>
    /// <returns>初期化済みのコントローラ</returns>
    public static IAppController Init(ILogger logger)
    {
        if (Instance != null)
        { throw new InvalidOperationException(AppControllerProvider.ERR_CONTROLLER_ALREADY_INITIALIZED); }

        try
        {
            logger.PrintPush(LogLevel.INFO, "Starting search controller object.");

            // アセンブリ内の IAppController を実装したクラスを取得して
            // インスタンスを生成し、staticに保持する
            var t = Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .First((x) => { return (typeof(IAppController).IsAssignableFrom(x) && (x != typeof(IAppController))); });

            logger.Print(LogLevel.INFO, $"Controller class is {t.FullName}");

            Instance = (Activator.CreateInstance(t) as IAppController).Init(logger);
            return Instance;
        }
        catch (Exception eUnknown)
        {
            logger.Print(LogLevel.ERROR, $"Controller initialization failed: {eUnknown.ToString()}");
            Instance = null;
            throw new Exception(AppControllerProvider.ERR_CONTROLLER_NOT_FOUND); 
        }
        finally
        {
            logger.PrintPop();
        }
    }

    /// <summary>
    /// コントローラのシャットダウン処理を行う.
    /// </summary>
    /// <remarks>
    /// コントローラが未初期化、あるいは初期化に失敗している場合に呼び出した場合は何もしない.
    /// また、既にシャットダウン済みの場合も何もしない.
    /// アプリケーション終了時に常に呼び出すようにすればよい.
    /// </remarks>
    public static void Shutdown()
    {
        if (Instance == null)
        { return; }

        Instance.Shutdown();
        Instance = null;
    }
}
