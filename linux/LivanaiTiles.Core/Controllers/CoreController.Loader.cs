using System.Reflection;

using clUtils.log;

using LivanaiTiles.Plugins;
using LivanaiTiles.Views;

namespace LivanaiTiles.Controllers;

public partial class CoreController : IAppController
{
    /// <summary>
    /// プラグインをロードする
    /// </summary>
    /// <param name="baseDir"></param>
    /// <returns></returns>
    private IEnumerable<PluginInfo> LoadPlugins(DirectoryInfo baseDir)
    {
        this.Logger.PrintPush(LogLevel.INFO, $"scan plugins in directory [{baseDir.FullName}]");
        var result = new List<PluginInfo>();

        // このディレクトリ配下のdllファイルを検索して ITileProvider クラスのインスタンスを取得する
        foreach (var dll in baseDir.GetFiles("*.dll"))
        {
            try
            {
                // プラグインの型である ITileProvider 型のインターフェイスを取得する
                // ただし、ITileProvider そのものは取得しない
                var asm = Assembly.LoadFrom(dll.FullName);
                foreach (var plugin in asm.GetTypes().Where((x) => { return (typeof(ITileProvider).IsAssignableFrom(x) && (x != typeof(ITileProvider))); }))
                {
                    try
                    {
                        var i = Activator.CreateInstance(plugin) as ITileProvider;
                        if (i.InitProvider(this))
                        {
                            result.Add(new PluginInfo()
                            {
                                PluginLocation = dll,
                                PluginID = i.ProviderID,
                                PluginName = i.Name,
                                Provider = i,
                            });

                            this.Logger.Print(LogLevel.DEBUG, $"plugin id [{i.ProviderID}], type [{i.GetType().FullName}] added as [{i.Name}]");
                        }
                        else
                        {
                            Logger.Print(LogLevel.WARN, $"plugin id [{i.ProviderID}], type [{i.GetType().FullName}] initialize failed.");
                        }
                    }
                    catch (Exception)
                    {
                        // 初期化に失敗したインターフェイスは無視する.
                        Logger.Print(LogLevel.WARN, $"plugin [{plugin.FullName}] failed initialize");
                    }
                }
            }
            catch(Exception)
            {
                // アセンブリを取得できなかった dll は無視する.
                Logger.Print(LogLevel.WARN, $"Failed load assembly from [{dll.FullName}]");
            }
        }

        this.Logger.PrintPop();
        return result;
    }

    /// <summary>
    /// このdllのあるディレクトリを返す
    /// </summary>
    /// <returns></returns>
    private DirectoryInfo GetEntryPointDirectory()
    { return new FileInfo(Assembly.GetExecutingAssembly().Location).Directory; }
}
