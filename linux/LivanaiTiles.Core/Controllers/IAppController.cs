using Avalonia.Controls;

using clUtils.log;

using LivanaiTiles.Plugins;
using LivanaiTiles.Views;

namespace LivanaiTiles.Controllers;

/// <summary>
/// アプリケーションのコントローラを操作するためのインターフェイス
/// </summary>
public interface IAppController
{
    #region Properties

    /// <summary>
    /// コントローラのロガー.
    /// </summary>
    ILogger Logger { get; }

    /// <summary>
    /// アプリケーションの設定情報
    /// </summary>
    public AppConfig Config { get; }

    /// <summary>
    /// 使用可能なscreenの座標の範囲情報
    /// </summary>
    // public Screen[] ScreenInfoList { get; }

    /// <summary>
    /// プラグインのリスト
    /// </summary>
    public IEnumerable<PluginInfo> Providers { get; }

    #endregion

    #region Methods

    /// <summary>
    /// コントローラのインスタンスを初期化する
    /// </summary>
    /// <param name="logger">このコントローラのロガーとして渡すロガーオブジェクト</param>
    /// <returns>初期化したこのオブジェクト自身</returns>
    IAppController Init(ILogger logger);

    /// <summary>
    /// コントローラのシャットダウン処理を行う.
    /// </summary>
    void Shutdown();

    /// <summary>
    /// セッション情報の保存を行う.
    /// </summary>
    void SaveSession();

    /// <summary>
    /// セッション情報の復元を行う.
    /// </summary>
    void RestoreSession();

    /// <summary>
    /// 全てのタイルを閉じて開きなおす.
    /// 設定変更時にその設定を反映するために使用する.
    /// </summary>
    void RestartWindows();

    /// <summary>
    /// 新規タイルウィンドウを作成する.
    /// </summary>
    /// <param name="pluginID">プラグインの識別子</param>
    /// <returns>
    /// 新しく生成したウィンドウ.
    /// </returns>
    TiledWindow CreateWindow(Guid pluginID);

    /// <summary>
    /// 新規タイルウィンドウを作成する
    /// </summary>
    /// <param name="pluginID">プラグインの識別子</param>
    /// <param name="guid">ウィンドウに割り当てるGUID</param>
    /// <returns>
    /// 新しく生成したウィンドウ.
    /// </returns>
    TiledWindow CreateWindow(Guid pluginID, Guid guid);

    /// <summary>
    /// 新規タイルウィンドウを作成する
    /// </summary>
    /// <param name="pluginID">プラグインの識別子</param>
    /// <param name="arg">プラグインに渡すパラメータ文字列</param>
    /// <returns>
    /// 新しく生成したウィンドウ.
    /// </returns>
    TiledWindow CreateWindow(Guid pluginID, string arg);

    /// <summary>
    /// 新規タイルウィンドウを作成する
    /// </summary>
    /// <param name="pluginID">プラグインの識別子</param>
    /// <param name="guid">ウィンドウに割り当てるGUID</param>
    /// <param name="arg">プラグインに渡すパラメータ文字列</param>
    /// <returns>
    /// 新しく生成したウィンドウ.
    /// </returns>
    TiledWindow CreateWindow(Guid pluginID, Guid guid, string arg);

    /// <summary>
    /// 指定したIDでタイルウィンドウの復元を要求する.
    /// </summary>
    /// <<param name="guid"></param>
    /// <param name="pluginID"></param>
    /// <returns></returns>
    TiledWindow RestoreWindow(Guid pluginID, Guid guid);

    /// <summary>
    /// 現在開いているタイルウィンドウの一覧を取得する
    /// </summary>
    /// <returns>このアプリケーションで開いているタイルの Window オブジェクト</returns>
    IEnumerable<TiledWindow> GetAllWindowList();

    /// <summary>
    /// ウィンドウ情報を削除する
    /// </summary>
    /// <remarks>
    /// タイルウィンドウを閉じたときに自身の情報をコントローラから削除するために呼び出す.
    /// </remarks>
    /// <param name="guid">削除対象のウィンドウID</param>
    /// <returns>削除に成功したらtrue</returns>
    bool DeleteWindow(Guid guid);

    /// <summary>
    /// 設定ファイルを読み直す
    /// </summary>
    void ReloadConfig();

    #endregion

}
