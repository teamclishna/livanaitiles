﻿using Avalonia.Controls;

using LivanaiTiles.Controllers;

namespace LivanaiTiles.Plugins;

/// <summary>
/// プラグインがタイルを提供するためのインターフェイス.
/// </summary>
public interface ITileProvider
{
    /// <summary>
    /// このプラグインを識別する一意の識別子
    /// </summary>
    Guid ProviderID { get; }

    /// <summary>
    /// このプラグイン名
    /// </summary>
    string Name { get; }

    #region Control provider

    /// <summary>
    /// プラグインの初期化処理を行う.
    /// </summary>
    /// <param name="controller">アプリケーションのコントローラ</param>
    /// <returns>
    /// 初期化に成功した場合はtrue.
    /// 初期化に失敗した場合はfalseを返す.
    /// falseを返す場合、同一DLLが返す他のリソースまで閉じてしまわないよう注意すること.
    /// </returns>
    bool InitProvider(IAppController controller);

    /// <summary>
    /// <para>
    /// 設定情報の保存処理を行う.
    /// この処理はユーザが明示的にセッション情報の保存を行った場合にのみ呼び出される.
    /// </para><para>
    /// この処理はすべてのウィンドウに対して SaveTileSession を呼び出した後に呼ばれる.
    /// </para>
    /// </summary>
    /// <param name="controller">アプリケーションのコントローラ</param>
    void SaveProviderSession(IAppController controller);

    /// <summary>
    /// プラグインのシャットダウン処理を行う.
    /// この処理は表示している全てのウィンドウに対して ShutdownTile を呼び出した後に呼ばれる.
    /// </summary>
    /// <param name="controller">アプリケーションのコントローラ</param>
    void ShutdownProvider(IAppController controller);

    #endregion

    #region Control Tile

    /// <summary>
    /// タイルを生成する
    /// </summary>
    /// <param name="controller">アプリケーションのコントローラ</param>
    /// <param name="parameter">タイル生成時に渡すパラメータ.</param>
    /// <returns>タイルウィンドウに格納するUserControlとタイル情報</returns>
    TileInfo CreateTile(IAppController controller, string parameter);

    /// <summary>
    /// 保存したタイルを復元する.
    /// </summary>
    /// <param name="controller">アプリケーションのコントローラ</param>
    /// <param name="windowID">
    /// ウィンドウID.
    /// このウィンドウIDを元に設定ファイルを読み込み、元の状態を復元したタイルを生成して返す.
    /// </param>
    /// <returns>タイルウィンドウに格納するUserControlとタイル情報</returns>
    TileInfo RestoreTile(IAppController controller, Guid windowID);

    /// <summary>
    /// タイル固有の設定情報を保存する.
    /// この処理はユーザが明示的にセッション情報の保存を行った場合にのみ呼び出される.
    /// </summary>
    /// <param name="controller">アプリケーションのコントローラ</param>
    /// <param name="windowID">ウィンドウID.</param>
    /// <param name="tile">保存対象のタイル</param>
    void SaveTileSession(IAppController controller, Guid windowID, UserControl tile);

    /// <summary>
    /// <para>
    /// タイルのシャットダウン処理を行う.
    /// </para><para>
    /// この処理はタイルを閉じるときに呼ばれるが、ユーザの操作で (Alt+F4で) 閉じた場合、
    /// アプリケーション自体を終了するときに全タイルを閉じている場合、
    /// あるいは Windows のシャットダウン中に閉じようとしている場合を区別できない。
    /// </para><para>
    /// この処理内で開いていたタイルの情報を保存するような操作を想定すると正しく動作しない可能性があるため、
    /// リソースを閉じ以外の処理を行うべきではない.
    /// </para>
    /// </summary>
    /// <param name="controller">アプリケーションのコントローラ</param>
    /// <param name="windowID">ウィンドウID.</param>
    /// <param name="tile">シャットダウン対象のタイル</param>
    void ShutdownTile(IAppController controller, Guid windowID, UserControl tile);

    #endregion
}
