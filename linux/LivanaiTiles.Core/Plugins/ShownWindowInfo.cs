using LivanaiTiles.Views;

namespace LivanaiTiles.Plugins;

/// <summary>
/// 現在表示しているタイル情報
/// </summary>
internal class ShownWindowInfo
{
    /// <summary>
    /// このタイルのID
    /// </summary>
    public Guid WindowID { get; set; }

    /// <summary>
    /// このタイルを生成したプラグインのID
    /// </summary>
    public Guid PluginID { get; set; }

    /// <summary>
    /// 表示しているタイルのウィンドウ
    /// </summary>
    public TiledWindow  Wnd { get; set; }
}
