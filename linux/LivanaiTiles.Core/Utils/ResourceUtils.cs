using Avalonia;
using Avalonia.Controls;

namespace LivanaiTiles.Utils;

/// <summary>
/// リソースディクショナリとその中に含まれるマージされたディクショナリを操作するためのユーティリティクラス
/// </summary>
public static class ResourceUtils {

    /// <summary>
    /// 指定したリソースディクショナリから指定したキーの値を取得する.
    /// リソースディクショナリが外部xmlのリソース定義をマージしている場合はそれも検索する.
    /// </summary>
    /// <param name="dic"></param>
    /// <param name="key"></param>
    /// <returns>
    /// リソースディクショナリ内の指定したキーで定義されたリソース.
    /// リソースディクショナリ内に定義されていない場合、MergedInclude で定義されている外部xmlファイル内の指定したキーで定義されたリソース.
    /// どこにも定義されていない場合はnull
    /// </returns>
    public static T? Get<T>(
            this ResourceDictionary dic,
            string key
            ) where T : class  {
        if (dic.ContainsKey(key)) {
            return dic[key] as T;
        } else {
            foreach (var merged in dic.MergedDictionaries) {
                var included = merged as Avalonia.Markup.Xaml.MarkupExtensions.ResourceInclude;
                if (included != null && included.Loaded.ContainsKey(key)) {
                    return included.Loaded[key] as T;
                } 
            }

            return null;
        }
    }
}
