using Avalonia.Controls;
using Avalonia.Input;
using LivanaiTiles.Controllers;
using LivanaiTiles.Utils;

namespace LivanaiTiles.Views;

public partial class TiledWindow : Window
{
    /// <summary>
    /// このウィンドウに対して割り当てる GUID
    /// </summary>
    public Guid WindowID { get; set; }

    /// <summary>
    /// タイルの初期化が完了したときに実行されるイベント
    /// </summary>
    /// <param name="e"></param>
    protected override void OnOpened(EventArgs e)
    {
        base.OnOpened(e);
        
        // 設定パネルをマウスオーバーときのみ表示する設定の場合
        // ここで設定パネルを非表示にする
        if (AppControllerProvider.Instance.Config.ShowButtonMouseoverOnly) {
            this.GetTileContent().SetSettingPanelVisilirity(false);
        }
    }

    /// <summary>
    /// タイル上でマウスボタンが押された時のイベント
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPointerPressed(PointerPressedEventArgs e)
    {
        base.OnPointerPressed(e);
        // タイトルバー無しでウィンドウ表示するので
        // 余白部分をドラッグしてウィンドウを移動できるようにする
        base.BeginMoveDrag(e);
    }

    /// <summary>
    /// タイル上でマウスボタンが話された時のイベント
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPointerReleased(PointerReleasedEventArgs e)
    {
        base.OnPointerReleased(e);
    }

    /// <summary>
    /// タイルにマウスカーソルが入った時のイベント
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPointerEnter(PointerEventArgs e)
    {
        base.OnPointerEnter(e);
        if (AppControllerProvider.Instance.Config.ShowButtonMouseoverOnly) {
            this.GetTileContent().SetSettingPanelVisilirity(true);
        }
    }

    /// <summary>
    /// タイル上からマウスカーソルが離れた時のイベント
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPointerLeave(PointerEventArgs e)
    {
        base.OnPointerLeave(e);
        if (AppControllerProvider.Instance.Config.ShowButtonMouseoverOnly) {
            this.GetTileContent().SetSettingPanelVisilirity(false);
        }
    }

    public TiledWindow()
    {
        InitializeComponent();
    }
}