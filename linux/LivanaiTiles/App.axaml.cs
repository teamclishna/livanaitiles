using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;

using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.ViewModels;

namespace LivanaiTiles;

public partial class App : Application
{

    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    public override void OnFrameworkInitializationCompleted()
    {
        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            // 通常のアプリであればここでメインウィンドウを表示する

            // アプリケーションのコントローラを初期化する
            InitializeApp();

            // TrayIcon を操作するための ViewModel をここで初期化する
            var vm = new AppViewModel();
            this.DataContext = vm;

            // プラグインのメニューを追加する
            // TrayIcon は 1 個固定、その中のメニューの先頭要素の子にプラグイン毎のメニューを追加する
            var trayIconMenu = TrayIcon.GetIcons(this)[0].Menu.Items[0] as NativeMenuItem;
            var appController = AppControllerProvider.Instance;
            foreach (var provider in appController.Providers) {
                var addMenuItem = new NativeMenuItem(provider.PluginName);
                addMenuItem.Click += (sender, e) =>{
                    vm.LaunchTileCommandImpl(provider.PluginID.ToString());
                };
                trayIconMenu.Menu.Add(addMenuItem);
            }

            // タイルのリストアを行う
            appController.RestoreSession();
        }

        base.OnFrameworkInitializationCompleted();
    }

    /// <summary>
    /// アプリケーションの初期化処理を行う
    /// </summary>
    private IAppController InitializeApp() {
        return AppControllerProvider.Init(InitLogger());
    }

    /// <summary>
    /// コントローラが使用するロガーオブジェクトを初期化する
    /// </summary>
    /// <returns></returns>
    private ILogger InitLogger() {
#if DEBUG
        return new clUtils.log.ConsoleLogger()
        {
            OutputLogLevel = new clUtils.log.LogLevel[] {
                clUtils.log.LogLevel.ERROR,
                clUtils.log.LogLevel.WARN,
                clUtils.log.LogLevel.INFO,
                clUtils.log.LogLevel.DEBUG
            },
            ShowCurrentLine = false,
            ShowThreadID = true,
            ShowCallStackIndentation = true,
            IndentString = " ",
            ShowTimestamp = true,
            ShowLogLevel = true,
        };
#else
        return new clUtils.log.NullLogger();
#endif
    }
}