using Avalonia.Controls;

namespace LivanaiTiles.Utils;

/// <summary>
/// リソースディクショナリのラッパクラス
/// </summary>
public class ResourceDictionaryWrapper {

    #region constant values.

    private static readonly string RESOURCE_URL = "avares://LivanaiTiles/Assets/strings.axaml";

    #endregion

    #region properties

    private ResourceDictionary? Resources { get; set; }

    #endregion

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public ResourceDictionaryWrapper() {
        this.Resources = Avalonia.Markup.Xaml.AvaloniaXamlLoader.Load(new System.Uri(RESOURCE_URL)) as ResourceDictionary;
    }

    /// <summary>
    /// サポートページのURL
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string SUPPORT_PAGE_URL => this.Resources.Get<string>("aboutwindow.hyperlink.support_page");
}