
using System;


/// <summary>
/// アプリケーションのバージョン情報に関するユーティリティ関数群
/// </summary>
public static class VersionUtils
{
    /// <summary>
    /// バージョン情報の 「メジャー.マイナー.ビルド.リビジョン」 形式の文字列表記を生成して返す
    /// </summary>
    /// <param name="ver"></param>
    /// <returns></returns>
    public static string FormatVersionString(this Version ver)
    { return  $"{ ver.Major }.{ ver.Minor }.{ ver.Build }.{ ver.Revision }"; }
}