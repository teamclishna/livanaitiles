using Avalonia.Controls;
using ReactiveUI;
using System.Collections.Generic;
using System.Reactive;

using clUtils.gui;

using LivanaiTiles.Utils;
using System;

namespace LivanaiTiles.ViewModels;

/// <summary>
/// アプリケーションのバージョン情報ダイアログの ViewModel
/// </summary>
public class AboutWindowViewModel : NotifiableViewModel
{
    #region fields

    private string _appAuthor;

    private Version _appVersion;

    private List<string[]> _pluginList;

    #endregion

    #region Properties

    /// <summary>
    /// この ViewModel をバインディングする設定画面
    /// </summary>
    /// <value></value>
    public Window? ParentDiaog { get; set; }

    /// <summary>
    /// アプリの制作者名
    /// </summary>
    /// <value></value>
    public string AppAuthor
    {
        get { return this._appAuthor; }
        set
        {
            this._appAuthor = value;
            OnPropertyChanged(nameof(AppAuthor));
        }
    }

    /// <summary>
    /// アプリケーションのバージョン
    /// </summary>
    /// <value></value>
    public Version AppVersion
    {
        get { return this._appVersion; }
        set
        {
            this._appVersion = value;
            OnPropertyChanged(nameof(AppVersion));
            OnPropertyChanged(nameof(AppVersionString));
        }
    }

    /// <summary>
    /// アプリケーションバージョンの画面表記用の文字列表現を返す
    /// </summary>
    /// <value></value>
    public string AppVersionString
    {
        get { return this.AppVersion.FormatVersionString(); }
    }

    /// <summary>
    /// インストールされているプラグインの一覧.
    /// 各要素がプラグインの DLL の情報.
    /// 要素の配列は順に
    /// <list type="number">
    ///   <item>プラグイン名</item>
    ///   <item>プラグインの DLL のファイル名</item>
    ///   <item>プラグインのバージョン</item>
    /// </list>
    /// </summary>
    /// <value></value>
    public List<string[]> PluginList
    {
        get { return this._pluginList; }
        set
        {
            this._pluginList = value;
            OnPropertyChanged(nameof(PluginList));
        }
    }

    /// <summary>
    /// アプリの紹介ページへのハイパーリンクを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> OpenBrowserCommand{ get; private set; }

    /// <summary>
    /// OKボタンを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> OKCommand{ get; private set; }

    #endregion


    #region Commands

    public void OpenBrowserCommandImpl(string param)
    {
        // ハイパーリンクのURLをブラウザで開き、このダイアログを閉じる
        var resources = new ResourceDictionaryWrapper();
        var startInfo = new System.Diagnostics.ProcessStartInfo(resources.SUPPORT_PAGE_URL) {
            UseShellExecute = true
        };
        System.Diagnostics.Process.Start(startInfo);
        this.ParentDiaog.Close();
    }

    public void OKCommandImpl(string param)
    {
        // このダイアログを閉じる
        this.ParentDiaog.Close();
    }

    #endregion

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public AboutWindowViewModel()
    {
        this.OpenBrowserCommand = ReactiveCommand.Create<string>(this.OpenBrowserCommandImpl);
        this.OKCommand = ReactiveCommand.Create<string>(this.OKCommandImpl);
    }
}
