using System.Reactive;
using ReactiveUI;

using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.Plugins;
using LivanaiTiles.ViewModels;
using LivanaiTiles.Views;
using Avalonia.Controls;
using System.Collections.Generic;

namespace LivanaiTiles.ViewModels;

/// <summary>
/// アプリケーション本体と TrayIcon の操作を行うための ViewModel
/// </summary>
public class AppViewModel : ViewModelBase {

    #region Properties

    /// <summary>
    /// アプリケーションの設定ダイアログを開くコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> SettingsCommand{ get; private set; }

    /// <summary>
    /// 配置しているタイルの情報を保存するコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> SaveSessionsCommand{get ;private set; }

    /// <summary>
    /// アプリケーションの終了コマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> ExitCommand{ get; private set; }

    /// <summary>
    /// バージョン情報ダイアログ表示コマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> AboutCommand{ get; private set; }

    /// <summary>
    /// 新規タイル起動コマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> LaunchTileCommand{ get; private set; }

    #endregion

    #region Commands

    /// <summary>
    /// プラグインのタイルを起動するコマンド
    /// </summary>
    /// <param name="param"></param>
    public void LaunchTileCommandImpl(string param) {
        var logger = AppControllerProvider.Instance.Logger;
        logger.PrintPush(LogLevel.DEBUG, $"タイル起動! [{ param }]");

        var pluginId = new System.Guid(param);
        AppControllerProvider.Instance.CreateWindow(pluginId);

    }

    /// <summary>
    /// アプリケーションの設定ダイアログを開くコマンド
    /// </summary>
    /// <param name="param"></param>
    public void SettingsCommandImpl(string param) {
        var controller = AppControllerProvider.Instance;
        var logger = controller.Logger;
        logger.PrintPush(LogLevel.DEBUG, "設定画面を開く");

        // 設定画面はタスクトレイから開くのでタイルを親として開く ShowDialog(Window) ではなく Show() で開く
        var wnd = new SettingsWindow();
        var vm = new SettingsWindowViewModel() {
            ParentDiaog = wnd,
            Config = controller.Config.Clone(),
        };
        wnd.DataContext = vm;
        wnd.Show();
        
        logger.PrintPop();
    }
    
    /// <summary>
    /// タイルの配置を保存するためのコマンド
    /// </summary>
    /// <param name="param"></param>
    public void SaveSessionsCommandImpl(string param) {
        var controller = AppControllerProvider.Instance;
        var logger = controller.Logger;
        logger.PrintPush(LogLevel.DEBUG, "タイル状態の保存");

        controller.SaveSession();

        logger.PrintPop();
    }

    /// <summary>
    /// アプリケーションの終了コマンド
    /// </summary>
    /// <param name="param"></param>
    public void ExitCommandImpl(string param) {
        var logger = AppControllerProvider.Instance.Logger;
        logger.PrintPush(LogLevel.DEBUG, "アプリケーションのシャットダウン");

        logger.PrintPop();
        logger.Print(LogLevel.INFO, "アプリケーション終了");
        System.Environment.Exit(0);
    }

    /// <summary>
    /// アプリケーションのバージョン情報ダイアログを開くコマンド
    /// </summary>
    /// <param name="param"></param>
    public void AboutCommandImpl(string param) {
        var logger = AppControllerProvider.Instance.Logger;
        logger.PrintPush(LogLevel.DEBUG, "バージョン情報ダイアログを開く");

        var versionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(typeof(App).Assembly.Location);
        var wnd = new AboutWindow();
        var plugins = GetPluginVersions();
        var vm = new AboutWindowViewModel()
        {
            ParentDiaog = wnd,
            AppAuthor = versionInfo.CompanyName,
            // アプリケーションのバージョン情報を取得
            AppVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version,
            PluginList = plugins,
        };
        wnd.DataContext = vm;
        wnd.Show();
        logger.PrintPop();
    }

    /// <summary>
    /// インストールされているプラグインとそのバージョンの一覧を取得する
    /// 
    /// </summary>
    /// <returns></returns>
    private List<string[]> GetPluginVersions()
    {
        var plugins = new List<string[]>();
        foreach (var provider in  Controllers.AppControllerProvider.Instance.Providers)
        {
            var versionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(provider.PluginLocation.FullName);
            plugins.Add(new string[]
            {
                provider.PluginName,
                provider.PluginLocation.Name,
                versionInfo.ProductVersion
            });
        }

        return plugins;
    }

    #endregion

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public AppViewModel() {
        var logger = AppControllerProvider.Instance.Logger;
        logger.PrintPush(LogLevel.INFO, "AppViewModel 初期化開始");

        // TrayIcon にバインディングするコマンドの初期化
        SettingsCommand = ReactiveCommand.Create<string>(SettingsCommandImpl);
        SaveSessionsCommand = ReactiveCommand.Create<string>(SaveSessionsCommandImpl);
        ExitCommand = ReactiveCommand.Create<string>(ExitCommandImpl);
        AboutCommand = ReactiveCommand.Create<string>(AboutCommandImpl);
        LaunchTileCommand = ReactiveCommand.Create<string>(LaunchTileCommandImpl);

        logger.PrintPop();
    }
}