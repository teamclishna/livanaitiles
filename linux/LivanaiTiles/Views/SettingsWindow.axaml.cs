using Avalonia.Controls;

namespace LivanaiTiles.Views;

/// <summary>
/// アプリケーションの設定を行うダイアログ
/// </summary>
public partial class SettingsWindow : Window {
    
    public SettingsWindow() {
        InitializeComponent();
    }
}