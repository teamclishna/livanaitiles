using Avalonia.Controls;

using System.IO;
using System.Runtime.Serialization.Json;

using LivanaiTiles.Controllers;
using LivanaiTiles.DateTime.Config;
using LivanaiTiles.DateTime.ViewModels;
using LivanaiTiles.DateTime.Views;
using LivanaiTiles.Plugins;
using LivanaiTiles.Utils;

namespace LivanaiTiles.DateTime;

/// <summary>
/// カレンダーを表示するためのタイル
/// </summary>
public class CalendarProvider : ITileProvider
{
    /// <inheritdoc/>
    public Guid ProviderID => new Guid("{b97bccb5-b528-4a9a-9c92-7766c52aea23}");

    /// <inheritdoc/>
    public string Name => "Calendar";

    /// <summary>
    /// タイルに表示する月名表示のデフォルト値
    /// </summary>
    public static readonly string DEFAULT_MONTH_FORMAT = "MMMM";

    /// <summary>
    /// タイルにマウスオーバーした時のツールチップに表示する今日の日付表記のデフォルト値
    /// </summary>
    public static readonly string DEFAULT_TODAY_FORMAT = "dddd, yyyy-MM-dd";

    /// <summary>
    /// カレンダーに関する設定情報.
    /// この設定情報は複数タイルを開いた場合でも共有する.
    /// </summary>
    public CalendarConfig GlobalConfig { get; set; }

    /// <inheritdoc/>
    public bool InitProvider(IAppController controller)
    {
        try
        {
            // グローバルなカレンダー情報のロード
            this.GlobalConfig = LoadGlobalConfig(controller);
        }
        catch(Exception)
        {
            // 例外スロー時はデフォルトの設定を生成する
            this.GlobalConfig = new CalendarConfig();
        }
        return true; 
    }

    /// <inheritdoc/>
    public TileInfo CreateTile(IAppController controller, string parameter)
    {
        var ctl = new CalendarTile() { Owner = this, };
        ctl.Init();

        return new TileInfo()
        {
            UseAeroGlass = null,
            BackGroundColor = null,
            WindowTitle = this.Name,
            TileContent = ctl,
            TileHeight = 150,
            TileWidth = 150,
        };
    }

    /// <inheritdoc/>
    public TileInfo RestoreTile(IAppController controller, Guid windowID)
    {
        // 特に復元する情報は無いので新規作成で返す
        return CreateTile(controller, null); 
    }

    /// <inheritdoc/>
    public void SaveProviderSession(IAppController controller)
    {
        // 設定情報を保存する
        SaveGlobalConfig(controller);
    }

    /// <inheritdoc/>
    public void SaveTileSession(IAppController controller, Guid windowID, UserControl tile)
    { }

    /// <inheritdoc/>
    public void ShutdownProvider(IAppController controller)
    { }

    /// <inheritdoc/>
    public void ShutdownTile(IAppController controller, Guid windowID, UserControl tile)
    { }

    /// <summary>
    /// カレンダー全体で共有する設定情報のロード
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private CalendarConfig LoadGlobalConfig(IAppController controller)
    {
        // 指定したファイルがある場合、このファイルの設定を読み込む
        var confPath = Path.Combine(
            FileUtils.GetProviderConfigDirectory(this, false).FullName,
            "calendar.conf");

        if (new FileInfo(confPath).Exists)
        {
            // 保存したファイルから設定情報を読み込む
            return FileUtils.LoadFromJson<CalendarConfig>(confPath, controller.Logger);
        }
        else
        {
            // JSON ファイルからデフォルトの祝日情報を読み込む
            var asm = System.Reflection.Assembly.GetExecutingAssembly();
            using (var st = asm.GetManifestResourceStream("LivanaiTiles.DateTime.Resources.DefaultHolidays.json"))
            {
                var deserializer = new DataContractJsonSerializer(typeof(CalendarConfig));
                var conf = deserializer.ReadObject(st) as CalendarConfig;

                return conf;
            }
        }
    }

    /// <summary>
    /// カレンダー全体で共有する設定情報のセーブ
    /// </summary>
    /// <param name="controller"></param>
    private void SaveGlobalConfig(IAppController controller)
    {
        var confPath = Path.Combine(
            FileUtils.GetProviderConfigDirectory(this, true).FullName,
            "calendar.conf");

        FileUtils.SaveToJson(this.GlobalConfig, confPath, controller.Logger);
    }
}
