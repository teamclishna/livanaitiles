using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.DateTime.Config;

/// <summary>
/// カレンダーの設定情報
/// </summary>
[DataContract]
public class CalendarConfig
{
    /// <summary>
    /// カレンダーの月名表記に使用する書式文字列
    /// </summary>
    [DataMember]
    public string MonthNameFormat { get; set; }

    /// <summary>
    /// カレンダーの月名表記に使用するカルチャ
    /// </summary>
    [DataMember]
    public string MonthNameCulture { get; set; }

    /// <summary>
    /// タイルのツールチップに表示する今日の日付表記に使用する書式文字列
    /// </summary>
    [DataMember]
    public string DateFormat { get; set; }

    /// <summary>
    /// タイルのツールチップに表示する今日の日付表記に使用するカルチャ
    /// </summary>
    [DataMember]
    public string TodayCulture { get; set; }

    /// <summary>
    /// 祝日の情報
    /// </summary>
    [DataMember]
    public List<HolidayInfo> Holidays { get; set; }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public CalendarConfig()
    {
        this.Holidays = new List<HolidayInfo>();
    }
}
