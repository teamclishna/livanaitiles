using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.DateTime.Config;

/// <summary>
/// 日付、時刻表示タイルの設定情報
/// </summary>
[DataContract]
public class DateTimeConfig
{
    /// <summary>
    /// 日付の書式文字列
    /// </summary>
    [DataMember]
    public string DateFormat { get; set; }

    /// <summary>
    /// 曜日の書式文字列
    /// </summary>
    [DataMember]
    public string WeekdayFormat { get; set; }

    /// <summary>
    /// 日付、曜日の書式化に使用するカルチャ文字列
    /// </summary>
    [DataMember]
    public string Culture { get; set; }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public DateTimeConfig()
    {
        this.DateFormat = null;
        this.WeekdayFormat = null;
        this.Culture = null;
    }
}
