using System.Runtime.Serialization;

namespace LivanaiTiles.DateTime.Config;

/// <summary>
/// 祝日情報
/// </summary>
[DataContract]
public class HolidayInfo
{
    /// <summary>
    /// 月の指定. 「毎月」の場合は0を設定する
    /// </summary>
    [DataMember]
    public int Month { get; set; }

    /// <summary>
    /// 「第nX曜日」を指定する場合の週の指定.
    /// 指定しない場合は0、する場合は1始まりで指定する.
    ///  月の１～7日を「第1週」として、以後7日毎に第2週、第3週...とする
    ///  Week、Dayの両方を指定した場合はDayを優先し、どちらも指定していない場合は「毎週Weekday曜日」を対象とする
    /// </summary>
    [DataMember]
    public int Week { get; set; }

    /// <summary>
    /// 曜日の指定
    /// </summary>
    [DataMember]
    public DayOfWeek Weekday { get; set; }

    /// <summary>
    /// 日の指定.
    /// 指定しない場合は0、指定する場合は1始まりで指定する.
    /// </summary>
    [DataMember]
    public int Day { get; set; }

    /// <summary>
    /// この祝日が振替休日を考慮するかどうか.
    /// true を指定した場合、この祝日が日曜日であれば翌日に移動する.
    /// Day を指定している祝日のみこの設定を考慮する.
    /// </summary>
    [DataMember]
    public bool Transfer { get; set; }

    /// <summary>
    /// この祝日の名称
    /// </summary>
    [DataMember]
    public string Name { get; set; }
}
