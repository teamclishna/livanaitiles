using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LivanaiTiles.DateTime.Config;

namespace LivanaiTiles.DateTime.Utils;

/// <summary>
/// カレンダーの祝日判定、日付、時刻などの表記に使用するユーティリティ関数群のクラス
/// </summary>
public static class CalendarUtil
{
    /// <summary>
    /// 指定した日付の月の最終日を計算して返す
    /// </summary>
    /// <param name="today"></param>
    /// <returns></returns>
    public static int GetLastDayOfMonth(this System.DateTime today)
    {
        // 「今日の翌月1日」の1日前で計算するが、VBと違って13月を翌年1月に自動補正してくれないので
        // 12月の場合は「翌年1月1日」を求める
        return (
            today.Month < 12 ? new System.DateTime(today.Year, today.Month + 1, 1) :
            new System.DateTime(today.Year + 1, 1, 1)
            ).AddDays(-1).Day;
    }

    /// <summary>
    /// 指定した日付を元に指定した書式文字列とカルチャで書式化した文字列を返す
    /// </summary>
    /// <param name="dt">書式化対象の日付、時刻</param>
    /// <param name="customFormat">書式化文字列</param>
    /// <param name="cultureName">カルチャ名. null を指定した場合はデフォルトのカルチャを使用する</param>
    /// <param name="defaultFormat">書式化文字列にnull、もしくは空白を指定した場合のデフォルト書式化文字列</param>
    /// <returns></returns>
    public static string Format(
        this System.DateTime dt,
        string customFormat,
        string cultureName,
        string defaultFormat
        )
    {
        var formatString = (
            !string.IsNullOrWhiteSpace(customFormat) ? customFormat :
            defaultFormat);

        var culture = (
            cultureName != null ? new System.Globalization.CultureInfo(cultureName) :
            System.Globalization.CultureInfo.CurrentCulture);

        return dt.ToString(formatString, culture);
    }

    /// <summary>
    /// 指定した日付に該当する休日情報が存在するかどうかを検索してい返す
    /// </summary>
    /// <param name="today">今日の日付</param>
    /// <param name="holidays">休日情報</param>
    /// <returns>今日の日付に一致する休日情報</returns>
    public static HolidayInfo[] FindHoliday(
        this System.DateTime today,
        List<HolidayInfo> holidays)
    {
        // 今日が何週目かを求める
        var week = (int)(today.Day / 7) + 1;

        // 今月の休日から今日の日付に一致するものを求める
        var resultThisMonth = holidays
            .Where(h => (h.Month == today.Month) || (h.Month == 0))
            .Where((h) =>
            {
                // 振替休日を考慮しない、または今日が平日の場合日が一致
                // 今日が月曜日である場合、前日の振り替え休日
                // 週と曜日が一致
                return (
                    ((!h.Transfer || today.DayOfWeek != DayOfWeek.Sunday) && h.Day == today.Day) ||
                    (today.DayOfWeek == DayOfWeek.Monday && h.Transfer && h.Day == today.Day - 1) ||
                    (h.Week == week && today.DayOfWeek == h.Weekday)
                    );
            }).ToList();

        // 今日が1日、かつ月曜日である場合は先月末に振替休日があればこれを追加
        if(today.Day == 1 && today.DayOfWeek== DayOfWeek.Monday)
        {
            var lastMonth = (1 < today.Month ? today.Month - 1 : 12);
            var lastDay = new System.DateTime(today.Year, today.Month, 1).AddDays(-1).Day;

            var lastDayHolidays = holidays
                .Where(h => (h.Month == lastMonth) || (h.Month == 0))
                .Where((h) => h.Transfer && (h.Day == lastDay))
                .ToList();

            resultThisMonth.AddRange(lastDayHolidays);
        }

        return resultThisMonth.ToArray();
    }
}
