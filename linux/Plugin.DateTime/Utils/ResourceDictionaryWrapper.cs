using Avalonia.Controls;
using LivanaiTiles.Utils;

namespace LivanaiTiles.DateTime.Utils;

/// <summary>
/// リソースディクショナリのラッパクラス
/// </summary>
public class ResourceDictionaryWrapper {

    #region constant values.

    private static readonly string RESOURCE_URL = "avares://Plugin.DateTime/Assets/strings.axaml";

    #endregion

    #region properties

    private ResourceDictionary? Resources { get; set; }

    #endregion

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public ResourceDictionaryWrapper() {
        this.Resources = Avalonia.Markup.Xaml.AvaloniaXamlLoader.Load(new Uri(RESOURCE_URL)) as ResourceDictionary;
    }

    public string ARGS_COUNT_INVALID => this.Resources.Get<string>("ARGS_COUNT_INVALID");
    public string ARGS_SOUND_FILE_NOT_FOUND => this.Resources.Get<string>("ARGS_SOUND_FILE_NOT_FOUND");
    public string ARGS_SOUND_MISSING => this.Resources.Get<string>("ARGS_SOUND_MISSING");
    public string ARGS_TIME_INVALID => this.Resources.Get<string>("ARGS_TIME_INVALID");
}