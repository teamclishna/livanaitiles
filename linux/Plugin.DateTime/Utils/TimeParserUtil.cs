using System.Collections.Generic;
using System.Text;

namespace LivanaiTiles.DateTime.Utils;

/// <summary>
/// 時刻の入力値の変換に関するユーティリティ関数
/// </summary>
public static class TimeParserUtil
{
    /// <summary>
    /// 指定した文字列を時刻に解釈して返す.
    /// </summary>
    /// <param name="src"></param>
    /// <returns>
    /// 文字列を "H:m" (24時間型式、1桁表記可) の書式で解釈した時刻.
    /// この時、時刻が現在時刻よりも未来であれば今日の時刻、過去であれば翌日の時刻として返す.
    /// 書式が不正で解釈できなかった場合は MinValue を返す
    /// </returns>
    public static System.DateTime Parse(string src)
    {
        System.DateTime parsed;
        if (System.DateTime.TryParseExact(
            src,
            "H:m",
            null,
            System.Globalization.DateTimeStyles.None,
            out parsed))
        {
            var now = System.DateTime.Now;
            return (now < parsed ? parsed : parsed.AddDays(1.0));
        }
        else
        { return System.DateTime.MinValue; }
    }
}
