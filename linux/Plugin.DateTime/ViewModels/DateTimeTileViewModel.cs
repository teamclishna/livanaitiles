﻿using Avalonia.Controls;
using Avalonia.Threading;
using ReactiveUI;
using System.Reactive;

using System.Collections.Generic;
using System.Text;
using clUtils.gui;

using LivanaiTiles.DateTime.Utils;

namespace LivanaiTiles.DateTime.ViewModels;

public class DateTimeTileViewModel : NotifiableViewModel
{
    #region Fields

    public bool _timerCountdown;
    public bool _timerReminder;
    public bool _countDownMinutes;
    public bool _countDownSeconds;
    public int _countdownTime;
    public System.DateTime _reminderTime;
    public string _soundFilePath;

    public bool _useTimer;
    public int _remainTime;

    public string _countdownTimerText;
    public string _reminderTimeText;

    public string _weekdayFormat;
    public string _dateFormat;
    public string _formatCulture;

    #endregion

    #region Properties

    /// <summary>
    /// このViewModelをバインドするタイル
    /// </summary>
    /// <value></value>
    public UserControl? ParentTile { get; set; }

    /// <summary>
    /// 指定した分、または秒をカウントするタイマーを使用する場合はtrue.
    /// UserTimerCountdown, UseTimerReminder はいずれか一方のみを true にする
    /// </summary>
    public bool UseTimerCountdown
    {
        get { return this._timerCountdown; }
        set
        {
            this._timerCountdown = value;
            OnPropertyChanged(nameof(UseTimerCountdown));
        }
    }

    /// <summary>
    /// 指定した時刻を通知するタイマーを使用する場合はtrue
    /// UserTimerCountdown, UseTimerReminder はいずれか一方のみを true にする
    /// </summary>
    public bool UseTimerReminder
    {
        get { return this._timerReminder; }
        set
        {
            this._timerReminder = value;
            OnPropertyChanged(nameof(UseTimerReminder));
        }
    }

    /// <summary>
    /// カウントダウンタイマの設定を分単位で行う場合はtrue
    /// CountdownMinutes, CountdownSeconds はいずれか一方のみを true にする
    /// </summary>
    public bool CountdownMinutes
    {
        get { return this._countDownMinutes; }
        set
        {
            this._countDownMinutes = value;
            OnPropertyChanged(nameof(CountdownMinutes));
        }
    }

    /// <summary>
    /// カウントダウンタイマの設定を秒単位で行う場合はtrue
    /// CountdownMinutes, CountdownSeconds はいずれか一方のみを true にする
    /// </summary>
    public bool CountdownSeconds
    {
        get { return this._countDownSeconds; }
        set
        {
            this._countDownSeconds = value;
            OnPropertyChanged(nameof(CountdownSeconds));
        }
    }

    /// <summary>
    /// カウントダウンタイマを使用する場合、設定した分数、または秒数.
    /// </summary>
    public int CountdownTime
    {
        get { return this._countdownTime; }
        set
        {
            this._countdownTime = value;
            OnPropertyChanged(nameof(CountdownTime));
        }
    }

    /// <summary>
    /// カウントダウンタイマの入力値
    /// </summary>
    public string CountdownTimeText
    {
        get { return this._countdownTimerText; }
        set
        {
            this._countdownTimerText = value;
            int.TryParse(value, out this._countdownTime);

            OnPropertyChanged(nameof(CountdownTime));
            OnPropertyChanged(nameof(CountdownTimeText));
            OnPropertyChanged(nameof(InvalidCountdown));
        }
    }

    /// <summary>
    /// カウントダウン時間の注意書きの表示可否
    /// </summary>
    public bool InvalidCountdown
    {
        get { return !int.TryParse(_countdownTimerText, out int number) || number <= 0; }
    }

    /// <summary>
    /// 特定時刻を通知するタイマを使用する場合、その通知時刻
    /// </summary>
    public System.DateTime ReminderTime
    {
        get { return this._reminderTime; }
        set
        {
            this._reminderTime = value;
            OnPropertyChanged(nameof(ReminderTime));
        }
    }

    /// <summary>
    /// 時刻通知タイマーの入力値
    /// </summary>
    public string ReminderTimeText
    {
        get { return this._reminderTimeText; }
        set
        {
            this._reminderTimeText = value;

            System.DateTime dt = TimeParserUtil.Parse(value);
            if(System.DateTime.MinValue<dt)
            {
                // 未来の時刻が指定された場合のみ有効
                this._reminderTime = dt;
                this._reminderTimeText = dt.ToString("HH:mm");
                OnPropertyChanged(nameof(ReminderTime));
            }

            OnPropertyChanged(nameof(ReminderTimeText));
            OnPropertyChanged(nameof(InvalidReminder));
        }
    }

    /// <summary>
    /// 通知時刻に関する注意書きの表示可否
    /// </summary>
    public bool InvalidReminder
    {
        get { return TimeParserUtil.Parse(ReminderTimeText) <= System.DateTime.MinValue; }
    }

    /// <summary>
    /// タイマーを使用する場合true
    /// </summary>
    public bool UseTimer
    {
        get { return this._useTimer; }
        set
        {
            this._useTimer = value;
            OnPropertyChanged(nameof(UseTimer));
        }
    }

    /// <summary>
    /// カウントダウンタイマ使用時の残り時間
    /// </summary>
    public int RemainTime
    {
        get { return this._remainTime; }
        set
        {
            this._remainTime = value;
            OnPropertyChanged(nameof(RemainTime));
        }
    }

    /// <summary>
    /// 指定時間に達した時に再生するサウンドのフルパス
    /// </summary>
    public string SoundFilePath
    {
        get { return this._soundFilePath; }
        set
        {
            this._soundFilePath = value;
            OnPropertyChanged(nameof(SoundFilePath));
        }
    }

    /// <summary>
    /// 曜日表記の書式文字列
    /// </summary>
    public string WeekdayFormat
    {
        get { return this._weekdayFormat; }
        set
        {
            this._weekdayFormat = value;
            OnPropertyChanged(nameof(WeekdayFormat));
        }
    }

    /// <summary>
    /// 日付表記の書式文字列
    /// </summary>
    public string DateFormat
    {
        get { return this._dateFormat; }
        set
        {
            this._dateFormat = value;
            OnPropertyChanged(nameof(DateFormat));
        }
    }

    /// <summary>
    /// 日付、曜日表記に使用する文字列のカルチャ
    /// </summary>
    public string FormatCulture
    {
        get { return this._formatCulture; }
        set
        {
            this._formatCulture = value;
            OnPropertyChanged(nameof(FormatCulture));
        }
    }

    /// <summary>
    /// タイマー完了時に音を鳴らす場合、そのwaveファイルを選択するダイアログを開くコマンド
    /// </summary>
    public ReactiveCommand<string, Unit> BrowseAlermFileCommand { get; private set; }

    /// <summary>
    /// タイルのタイマーが１秒経過するごとに呼び出されるコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> TickTimerCommand { get; private set; }

    #endregion
    
    #region Commands

    /// <summary>
    /// タイマー完了ときにならすwaveファイルを選択するダイアログを開くコマンドの実装
    /// </summary>
    /// <param name="param"></param>
    public void BrowseAlermFileCommandImpl(string param)
    {
        // var dlg = new Microsoft.Win32.OpenFileDialog()
        // {
        //     Multiselect = false,
        //     FilterIndex = 0,
        //     Filter = "Wave Files (*.wav)|*.wav",
        // };
        // var dlgResult = dlg.ShowDialog();
        // if (dlgResult.HasValue && dlgResult.Value)
        // { this.SoundFilePath = dlg.FileName; }

    }

    /// <summary>
    /// タイルのタイマーが１秒経過するごとに呼び出されるコマンドの実装
    /// </summary>
    /// <param name="param"></param>
    public void TickTimerCommandImpl(string param)
    {

    }

    #endregion

    /// <summary>
    /// このオブジェクトの複製を生成して返す
    /// </summary>
    /// <returns></returns>
    public DateTimeTileViewModel Clone()
    {
        return new DateTimeTileViewModel()
        {
            CountdownMinutes = this.CountdownMinutes,
            CountdownSeconds = this.CountdownSeconds,
            CountdownTime = this.CountdownTime,
            CountdownTimeText = this.CountdownTimeText,
            RemainTime = this.RemainTime,
            ReminderTime = this.ReminderTime,
            ReminderTimeText = this.ReminderTimeText,
            UseTimer = this.UseTimer,
            UseTimerCountdown = this.UseTimerCountdown,
            UseTimerReminder = this.UseTimerReminder,
            SoundFilePath = this.SoundFilePath,
        };
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public DateTimeTileViewModel()
    {
        this.BrowseAlermFileCommand = ReactiveCommand.Create<string>(this.BrowseAlermFileCommandImpl);
        this.TickTimerCommand = ReactiveCommand.Create<string>(this.TickTimerCommandImpl);
    }
}
