using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.Threading;

using System.Collections.Generic;
using System.Text;

using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.DateTime.Utils;
using LivanaiTiles.DateTime.ViewModels;

namespace LivanaiTiles.DateTime.Views;

/// <summary>
/// Calendar.xaml の相互作用ロジック
/// </summary>
public partial class CalendarTile : UserControl
{
    /// <summary>
    /// このカレンダーを生成した TileProvidor
    /// </summary>
    public CalendarProvider Owner { get; set; }

    /// <summary>
    /// 日付、時刻を更新するためのタイマー
    /// </summary>
    private DispatcherTimer timer;

    private System.DateTime currentDay;

    public CalendarTile()
    {
        InitializeComponent();
    }

    /// <summary>
    /// タイルの初期処理
    /// </summary>
    public void Init()
    {
        this.timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 1, 0, 0), };
        this.timer.Tick += Timer_Tick;
        this.timer.Start();

        currentDay = System.DateTime.MinValue;
        UpdateCalendar(true);
    }

    /// <summary>
    /// タイマーのコールバック
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Timer_Tick(object sender, EventArgs e)
    { UpdateCalendar(false); }

    /// <summary>
    /// カレンダーの再描画を行う
    /// </summary>
    private void UpdateCalendar(bool forceUpdate)
    {
        var logger = LivanaiTiles.Controllers.AppControllerProvider.Instance.Logger;
        
        // 日付が変わっていなければ何もしない
        var today = System.DateTime.Today;
        if (!forceUpdate && this.currentDay == today)
        { return; }

        logger.PrintPush(clUtils.log.LogLevel.DEBUG, "Draw calender.");

        // グローバルのカレンダー設定情報を取得
        var globalConf = this.Owner.GlobalConfig;

        // ラベルのデフォルト色を取得する
        var labelColor = new SolidColorBrush(AppControllerProvider.Instance.Config.ForeGroundColor);
        var bgColor = new SolidColorBrush(Colors.Transparent);

        // 今月1日の日付を取得
        var firstDay = new System.DateTime(today.Year, today.Month, 1);

        // 月名を表示
        this.lblMonthName.Foreground = labelColor;
        this.lblMonthName.Content = $"{firstDay:MMMM}";

        // 第1週を今日の曜日までスキップ
        for (var index = 0; index < (int)firstDay.DayOfWeek; index++)
        {
            var lbl = this.FindControl<Label>($"day0{index}");
            lbl.Content = "";
        }

        // 今月の最終日を求める
        // 「今日の翌月1日」の1日前で計算するが、VBと違って13月を翌年1月に自動補正してくれないので
        // 12月の場合は「翌年1月1日」を求める
        var lastDay = today.GetLastDayOfMonth();

        int col = (int)firstDay.DayOfWeek;
        int row = 0;
        for (var d = 1; d <= lastDay; d++)
        {
            // この日の祝日情報を求める
            var thisDay = new System.DateTime(today.Year, today.Month, d);
            var holidays = thisDay.FindHoliday(globalConf.Holidays);
            if (0 < holidays.Length)
            {
                logger.Print(LogLevel.DEBUG, $"Holidays of {thisDay:yyyy-MM-dd} ");
                foreach (var holiday in holidays)
                {
                    logger.Print(LogLevel.DEBUG, holiday.Name);
                }
                logger.Print(LogLevel.DEBUG, "}");
            }

            var lbl = this.FindControl<Label>($"day{row}{col}");
            var todayColor = (
                // 祝日
                0 < holidays.Length ? new SolidColorBrush(Colors.Red) :
                // 平日
                (0 < col && col < 6) ? labelColor :
                // 日曜
                col == 0 ? new SolidColorBrush(Colors.Red) :
                // 土曜
                new SolidColorBrush(Colors.Blue));

            // 今日は色を反転
            if (d == today.Day)
            {
                // 反転時の前景色は背景のRGB要素の輝度の合計が50%以上なら黒、未満なら白にする
                var totalBlightness = (int)todayColor.Color.R + (int)todayColor.Color.G + (int)todayColor.Color.B;
                lbl.Foreground = new SolidColorBrush((127 * 3) <= totalBlightness ? Colors.Black : Colors.White);
                lbl.Background = todayColor;
            }
            else
            {
                lbl.Foreground = todayColor;
                lbl.Background = bgColor;
            }
            lbl.Content = $"{d}";

            // ツールチップを設定する
            var todayString = thisDay.Format(
                globalConf.DateFormat,
                globalConf.TodayCulture,
                CalendarProvider.DEFAULT_TODAY_FORMAT);

            var sb = new StringBuilder();
            sb.Append(todayString);
            if (0 < holidays.Length)
            {
                sb.Append("\n");
                foreach (var h in holidays)
                { sb.Append("\n").Append(h.Name); }
            }

            // TODO: 祝日をツールチップで表示する
            // lbl.ToolTip = sb.ToString();
            ToolTip.SetTip(lbl, sb.ToString());

            col++;
            if (col == 7)
            {
                col = 0;
                row++;
            }
        }

        // 最終日以降を空白にする
        while (row < 6)
        {
            var lbl = this.FindControl<Label>($"day{row}{col}");
            lbl.Content = "";

            col++;
            if (col == 7)
            {
                col = 0;
                row++;
            }
        }

        this.currentDay = today;
        logger.PrintPop();
    }

    // private void CalendarSettingsButton_Click(object sender, System.Windows.RoutedEventArgs e)
    // {
    //     var conf = this.Owner.GlobalConfig;
    //     var vm = new CalendarSettingsWindowViewModel(conf);

    //     // カレンダーの設定画面を開く
    //     var wnd = new CalendarSettingsWindow()
    //     { DataContext = vm, };

    //     var dlgResult = wnd.ShowDialog();
    //     if (dlgResult.HasValue && dlgResult.Value)
    //     {
    //         // ダイアログのViewModelから設定情報を取り出す
    //         vm = wnd.DataContext as CalendarSettingsWindowViewModel;
    //         if(vm != null)
    //         {
    //             // 新しい設定情報を更新する
    //             var newConf = vm.Config;
    //             this.Owner.GlobalConfig = newConf;

    //             // 新しい設定情報でカレンダーを再描画する
    //             UpdateCalendar(true);
    //         }
    //     }
    // }
}
