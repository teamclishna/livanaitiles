using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.Threading;

using System.CodeDom;

using LivanaiTiles.Controllers;
using LivanaiTiles.DateTime.Utils;
using LivanaiTiles.DateTime.ViewModels;
using LivanaiTiles.DateTime.Views;

namespace LivanaiTiles.DateTime.Views;

/// <summary>
/// DateTimeTile.xaml の相互作用ロジック
/// </summary>
public partial class DateTimeTile : UserControl
{
    /// <summary>
    /// 日付、時刻を更新するためのタイマー
    /// </summary>
    private DispatcherTimer timer;

    public DateTimeTile()
    {
        InitializeComponent();

        var dc = new DateTimeTileViewModel()
        {
            ParentTile = this,
            UseTimer = false,
            UseTimerCountdown = true,
            UseTimerReminder = false,
            CountdownMinutes = true,
            CountdownSeconds = false,
            CountdownTime = 3,
            CountdownTimeText = "3",
            ReminderTime = System.DateTime.MinValue,
            ReminderTimeText = "00:00",
            RemainTime = 0,
        };
        this.DataContext = dc;

        // 現在時刻を更新するためのタイマー起動
        this.timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 1000), };
        this.timer.Tick += Timer_Tick;
        this.timer.Start();
    }

    /// <summary>
    /// ロード完了時のイベント
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    // private void UserControl_Loaded(object sender, RoutedEventArgs e)
    // {
    //     // タイル起動時に DataContext が渡されていなければここで空の ViewModel を生成して設定しておく
    //     if (this.DataContext == null)
    //     {
    //         this.DataContext = new DateTimeTileViewModel()
    //         {
    //             UseTimer = false,
    //             UseTimerCountdown = true,
    //             UseTimerReminder = false,
    //             CountdownMinutes = true,
    //             CountdownSeconds = false,
    //             CountdownTime = 3,
    //             CountdownTimeText = "3",
    //             ReminderTime = System.DateTime.MinValue,
    //             ReminderTimeText = "00:00",
    //             RemainTime = 0,
    //         };
    //     }

    //     // 起動時にアラームが設定されていればここで開始
    //     CheckActivateTimer(this.DataContext as DateTimeTileViewModel);
    // }

    /// <summary>
    /// タイマーのコールバック
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Timer_Tick(object sender, EventArgs e)
    {
        var currentTime = System.DateTime.Now;
        var dotted = ((currentTime.Second % 2) == 0);

        this.lblTime.Content = (dotted ? $"{currentTime:HH:mm}" : $"{currentTime:HH mm}");
        this.lblSecond.Content = (dotted ? $"{currentTime::ss}" : $"{currentTime: ss}");

        var context = this.DataContext as DateTimeTileViewModel;

        // 日付と曜日の表記はDataContextで書式化する
        // 書式化に失敗して例外スローされた場合は強制的にデフォルト値で表示する
        try
        {
            this.lblDate.Content = currentTime.Format(
                context.DateFormat,
                context.FormatCulture,
                DateTimeProvider.DEFAULT_DATETIME_FORMAT
                );
        }
        catch (Exception)
        { this.lblDate.Content = currentTime.ToString(DateTimeProvider.DEFAULT_DATETIME_FORMAT); }

        try
        {
            this.lblWeekday.Content = currentTime.Format(
                context.WeekdayFormat,
                context.FormatCulture,
                DateTimeProvider.DEFAULT_WEEKDAY_FORMAT
                );
        }
        catch (Exception)
        { this.lblWeekday.Content = currentTime.ToString(DateTimeProvider.DEFAULT_WEEKDAY_FORMAT); }

        // タイマー起動中の場合、タイマーを更新する
        if (context != null && context.UseTimer)
        { UpdateTimer(context); }
    }

    // private void TimerButton_Click(object sender, System.Windows.RoutedEventArgs e)
    // {
    //     // タイマーの設定画面を開く
    //     var wnd = new TimerSettingWindow()
    //     {
    //         DataContext = ((DateTimeTileViewModel)this.DataContext).Clone(),
    //     };

    //     var dlgResult = wnd.ShowDialog();
    //     if (dlgResult.HasValue && dlgResult.Value)
    //     {
    //         // ダイアログで設定したViewModelの内容を戻す
    //         var context = wnd.DataContext as DateTimeTileViewModel;
    //         this.DataContext = context;

    //         // タイマー有効ならカウントダウン開始
    //         // 既にタイマー起動中だった場合は新しい設定で最初からカウントダウンを開始する
    //         CheckActivateTimer(context);
    //     }
    // }

    // private void DateTimeFormatButton_Click(object sender, RoutedEventArgs e)
    // {
    //     var context = this.DataContext as DateTimeTileViewModel;

    //     // 日付書式の設定画面を開く
    //     var wnd = new DateTimeSettingsWindow()
    //     {
    //         DataContext = new DateTimeSettingsWindowViewModel()
    //         {
    //             DateFormat = context.DateFormat,
    //             WeekdayFormat = context.WeekdayFormat,
    //             FormatCulture = context.FormatCulture,
    //         },
    //     };

    //     var dlgResult = wnd.ShowDialog();
    //     if (dlgResult.HasValue && dlgResult.Value)
    //     {
    //         // ダイアログで設定した書式設定を反映する
    //         var dlgContext = wnd.DataContext as DateTimeSettingsWindowViewModel;
    //         context.DateFormat = dlgContext.DateFormat;
    //         context.WeekdayFormat = dlgContext.WeekdayFormat;
    //         context.FormatCulture = dlgContext.FormatCulture;
    //     }
    // }

    /// <summary>
    /// アラームが設定されているかどうかを確認し、ラベルの状態を更新する
    /// </summary>
    /// <param name="vm"></param>
    private void CheckActivateTimer(DateTimeTileViewModel vm)
    {
        this.lblTimerInfo.IsVisible = vm.UseTimer;
        if (vm.UseTimer)
        {
            this.iconTimer.Content = new string(vm.UseTimerCountdown ? new char[] { (char)0xea8f } : new char[] { (char)0xed5a });
            vm.RemainTime = CalcRemainTime(vm);
            UpdateTimerLabel(vm);
        }
    }

    /// <summary>
    /// タイマーのラベルを更新する
    /// </summary>
    /// <param name="vm"></param>
    private void UpdateTimerLabel(DateTimeTileViewModel vm)
    {
        this.lblTimer.Content = (
            vm.UseTimerCountdown ? FormatRemainTime(vm.RemainTime) :
            vm.ReminderTime.ToString("HH:mm"));

        // 残り時間が5秒を切った場合、ラベルを赤色にする
        var labelColor = new SolidColorBrush(
            vm.RemainTime <= 5 ? Colors.Red :
            AppControllerProvider.Instance.Config.ForeGroundColor);

        this.lblTimer.Foreground = labelColor;
        this.iconTimer.Foreground = labelColor;
    }

    /// <summary>
    /// 通知時刻までの時刻を計算して返す
    /// </summary>
    /// <param name="vm"></param>
    /// <returns></returns>
    private int CalcRemainTime(DateTimeTileViewModel vm)
    {
        if (vm.UseTimerCountdown)
        { return (vm.CountdownMinutes ? vm.CountdownTime * 60 : vm.CountdownTime); }
        else
        { return (int)((vm.ReminderTime - System.DateTime.Now).TotalSeconds) + 1; }
    }

    /// <summary>
    /// タイマーを更新する
    /// </summary>
    /// <param name="vm"></param>
    private void UpdateTimer(DateTimeTileViewModel vm)
    {
        if (0 < vm.RemainTime)
        {
            // 残り時間の更新
            if (vm.UseTimerCountdown)
            { vm.RemainTime--; }
            else
            { vm.RemainTime = (int)((vm.ReminderTime - System.DateTime.Now).TotalSeconds) + 1; }

            UpdateTimerLabel(vm);

            if (vm.RemainTime == 0)
            {
                // 時間になったらアラームを鳴らす
                // ファイル型式が異常、ファイルが存在しないなどの場合は特にエラー通知せずに無視
                if (!string.IsNullOrEmpty(vm.SoundFilePath))
                {
                    // try
                    // { (new SoundPlayer(vm.SoundFilePath)).Play(); }
                    // catch (Exception)
                    // { }
                }
            }
        }
    }

    /// <summary>
    /// タイマーの残り時間を表示するラベルを生成する
    /// </summary>
    /// <param name="remainTime"></param>
    /// <returns></returns>
    private string FormatRemainTime(int remainTime)
    {
        var sec = remainTime % 60;
        var min = (remainTime - sec) / 60;

        return $"{min.ToString("00")}:{sec.ToString("00")}";
    }
}
