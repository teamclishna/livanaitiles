using System.Collections.Generic;
using System.Text;

using clUtils.gui;

namespace LivanaiTiles.Mail.Mail;

/// <summary>
/// メールサーバのタイプ
/// </summary>
public enum MailServerType
{
    POP3,
    IMAP4,
}

/// <summary>
/// メールアカウント情報
/// </summary>
public class MailAccount : ICloneable
{
    /// <summary>
    /// メールチェック時のデフォルトの取得件数
    /// </summary>
    public static readonly int DEFAULT_FETCH_COUNT = 10;

    /// <summary>
    /// デフォルトのメールチェック間隔 （分）
    /// </summary>
    public static readonly int DEFAULT_FETCH_INTERVAL = 60;

    /// <summary>
    /// メールサーバのタイプ
    /// </summary>
    public MailServerType ServerType { get; set; }

    /// <summary>
    /// メールサーバのホスト名、またはIPアドレス
    /// </summary>
    public string ServerName { get; set; }

    /// <summary>
    /// メールサーバに接続するときのポート番号
    /// </summary>
    public int Port { get; set; }

    /// <summary>
    /// メールサーバに接続するときのユーザ名
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// メールサーバに接続するときのパスワード
    /// </summary>
    public string Password { get; set; }

    /// <summary>
    /// メールチェック時の最大取得件数
    /// </summary>
    public int MaxFetchCount { get; set; }

    /// <summary>
    /// 分単位で指定するメールチェック間隔
    /// </summary>
    public int FetchIntervalMinutes { get; set; }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public MailAccount()
    {
        this.ServerType = MailServerType.POP3;
        this.MaxFetchCount = DEFAULT_FETCH_COUNT;
        this.FetchIntervalMinutes = DEFAULT_FETCH_INTERVAL;
    }

    /// <summary>
    /// このオブジェクトの複製を生成する
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
        return new MailAccount()
        {
            ServerName = this.ServerName,
            ServerType = this.ServerType,
            Port = this.Port,
            UserName = this.UserName,
            Password = this.Password,
            MaxFetchCount = this.MaxFetchCount,
        };
    }
}

/// <summary>
/// メールボックスから確認したメールの情報
/// </summary>
public class MailInfo
{
    /// <summary>
    /// メールを一意に識別する識別子.
    /// POP3の場合はUIDLを使用する
    /// </summary>
    public string MailID { get; set; }

    /// <summary>
    /// メールのタイトル
    /// </summary>
    public string Subject { get; set; }

    /// <summary>
    /// メールの差出人情報
    /// </summary>
    public string From { get; set; }
}

/// <summary>
/// メールチェックした内容を格納するクラス
/// </summary>
public class MailCheckInfo : NotifiableViewModel
{
    #region Fields

    private string _accountName;
    private MailAccount _account;
    private IEnumerable<MailInfo> _mailBox;

    #endregion

    /// <summary>
    /// アカウント名
    /// </summary>
    public string AccountName {
        get { return this._accountName; }
        set
        {
            this._accountName = value;
            OnPropertyChanged("AccountName");
        }
    }

    /// <summary>
    /// メールアカウント情報
    /// </summary>
    public MailAccount Account {
        get { return this._account; }
        set
        {
            this._account = value;
            OnPropertyChanged("Account");
        }
    }

    /// <summary>
    /// メールボックスから取得したメールの情報
    /// </summary>
    public IEnumerable<MailInfo> MailBox {
        get { return this._mailBox; }
        set
        {
            this._mailBox = value;
            OnPropertyChanged("MailBox");
        }
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public MailCheckInfo()
    {
        this.MailBox = new List<MailInfo>();
    }
}