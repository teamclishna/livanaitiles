using Avalonia.Controls;
using LivanaiTiles.Utils;

namespace LivanaiTiles.Mail.Utils;

/// <summary>
/// リソースディクショナリのラッパクラス
/// </summary>
public class ResourceDictionaryWrapper {

    #region constant values.

    private static readonly string RESOURCE_URL = "avares://Plugin.Mail/Assets/strings.axaml";

    #endregion

    #region properties

    private ResourceDictionary? Resources { get; set; }

    #endregion

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public ResourceDictionaryWrapper() {
        this.Resources = Avalonia.Markup.Xaml.AvaloniaXamlLoader.Load(new Uri(RESOURCE_URL)) as ResourceDictionary;
    }

    /// <summary>
    /// メール取得中にタイルに表示するメッセージ
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string INFO_FETCHING_MAIL => this.Resources.Get<string>("INFO_FETCHING_MAIL");
    /// <summary>
    /// エラーメッセージ　メールアカウントが未設定
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string ERR_NO_ACCOUNT => this.Resources.Get<string>("ERR_NO_ACCOUNT");
    /// <summary>
    /// エラーメッセージ　メールサーバがエラーを返した
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string ERR_FETCH_ERROR => this.Resources.Get<string>("ERR_FETCH_ERROR");
    /// <summary>
    /// エラーメッセージ　メールボックスにメールなし
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string INFO_NO_MAIL => this.Resources.Get<string>("INFO_NO_MAIL");
}