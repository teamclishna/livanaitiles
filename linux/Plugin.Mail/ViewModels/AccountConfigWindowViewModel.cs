﻿using Avalonia.Controls;
using ReactiveUI;
using System.Reactive;

using clUtils.gui;

using LivanaiTiles.Mail.Mail;

namespace LivanaiTiles.Mail.ViewModels;

/// <summary>
/// アカウント情報設定画面のViewModel
/// </summary>
public class AccountConfigWindowViewModel : NotifiableViewModel
{
    #region Fields

    private string _accountName;
    private MailAccount _account;
    private string _portNumberText;

    #endregion

    #region  Properties

    /// <summary>
    /// この ViewModel をバインディングする設定画面
    /// </summary>
    /// <value></value>
    public Window? ParentDiaog { get; set; }

    /// <summary>
    /// アカウント名
    /// </summary>
    public string AccountName
    {
        get { return this._accountName; }
        set
        {
            this._accountName = value;
            OnPropertyChanged("AccountName");
        }
    }

    /// <summary>
    /// 編集対象のメールアカウント
    /// </summary>
    public MailAccount Account
    {
        get { return this._account; }
        set
        {
            this._account = value;
            this.PortNumberText = this._account.Port.ToString();
            OnPropertyChanged("Account");
            OnPropertyChanged("IsPOP3");
            OnPropertyChanged("IsIMAP4");
        }
    }

    /// <summary>
    /// ポート番号としてテキストボックスに入力した値
    /// </summary>
    public string PortNumberText
    {
        get { return this._portNumberText; }
        set
        {
            this._portNumberText = value;
            int number;
            if (int.TryParse(value, out number) && 0 < number)
            {
                this._account.Port = number;
                OnPropertyChanged("Account");
            }
            OnPropertyChanged("PortNumberText");
            OnPropertyChanged("InValidPortNumber");
        }
    }

    /// <summary>
    /// ポート番号の入力欄に表示する注意書きの表示可否
    /// </summary>
    public bool InValidPortNumber
    {
        get
        {
            int number;
            return !int.TryParse(PortNumberText, out number) && number <= 0;
        }
    }

    /// <summary>
    /// プロトコルのラジオボタン「POP3」の状態
    /// </summary>
    public bool IsPOP3
    {
        get { return (this._account.ServerType == MailServerType.POP3); }
        set
        {
            if(value)
            {
                this._account.ServerType = MailServerType.POP3;
                OnPropertyChanged("IsPOP3");
                OnPropertyChanged("IsIMAP4");
            }
        }
    }

    /// <summary>
    /// プロトコルのラジオボタン「IMAP4」の状態
    /// </summary>
    public bool IsIMAP4
    {
        get { return (this._account.ServerType == MailServerType.IMAP4); }
        set
        {
            if (value)
            {
                this._account.ServerType = MailServerType.IMAP4;
                OnPropertyChanged("IsPOP3");
                OnPropertyChanged("IsIMAP4");
            }
        }
    }

    /// <summary>
    /// メールの表示数
    /// </summary>
    /// <value></value>
    public int MaxFetchCount
    {
        get { return (this.Account != null ? this.Account.MaxFetchCount : 0); }
        set
        {
            if (this.Account != null)
            {
                this.Account.MaxFetchCount = value;
                OnPropertyChanged("MaxFetchCount");
                OnPropertyChanged("MaxFetchCountLabel");
            }
        }
    }

    /// <summary>
    /// メール表示数の文字列表記
    /// </summary>
    /// <value></value>
    public string MaxFetchCountLabel
    {
        get { return $"{ this.Account.MaxFetchCount }"; }
    }

    /// <summary>
    /// 新着メールチェックの間隔
    /// </summary>
    /// <value></value>
    public int FetchIntervalMinutes
    {
        get { return ( this.Account != null ? this.Account.FetchIntervalMinutes : 0); }
        set
        {
            if (this.Account != null)
            {
                this.Account.FetchIntervalMinutes = value;
                OnPropertyChanged("FetchIntervalMinutes");
                OnPropertyChanged("FetchIntervalMinutesLabel");
            }
        }
    }

    /// <summary>
    /// 新着メールチェックの間隔の文字列表記
    /// </summary>
    /// <value></value>
    public string FetchIntervalMinutesLabel
    {
        get { return $"{ this.FetchIntervalMinutes } minutes."; }
    }

    /// <summary>
    /// メールサーバのプロトコルを選択するラジオボタンをクリックした時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> CheckedProtocolPop3Command { get; private set; }
    public ReactiveCommand<string, Unit> CheckedProtocolImap4Command { get; private set; }

    /// <summary>
    /// OKボタンを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> OKCommand{ get; private set; }
    /// <summary>
    /// キャンセルボタンを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> CancelCommand{ get; private set; }

    #endregion

    #region Commands

    public void CheckedProtocolPop3CommandImpl(string param) {
        this.IsPOP3 = true;
    }
    public void CheckedProtocolImap4CommandImpl(string param) {
        this.IsIMAP4 = true;
    }

    public void OKCommandImpl(string param) {
        if (this.ParentDiaog != null) {
            ParentDiaog.Close(true);
        }
    }

    public void CanceCommandImpl(string param) {
        if (this.ParentDiaog != null) {
            ParentDiaog.Close(false);
        }
    }

    #endregion

    public AccountConfigWindowViewModel() {
        this.CheckedProtocolPop3Command = ReactiveCommand.Create<string>(this.CheckedProtocolPop3CommandImpl);
        this.CheckedProtocolImap4Command = ReactiveCommand.Create<string>(this.CheckedProtocolImap4CommandImpl);
        this.OKCommand = ReactiveCommand.Create<string>(this.OKCommandImpl);
        this.CancelCommand = ReactiveCommand.Create<string>(this.CanceCommandImpl);
    }
}
