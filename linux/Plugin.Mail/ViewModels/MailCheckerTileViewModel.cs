using Avalonia.Controls;
using Avalonia.Threading;
using ReactiveUI;
using System.Reactive;

using clUtils.gui;

using LivanaiTiles.Controllers;
using LivanaiTiles.Mail.Mail;
using LivanaiTiles.Mail.Views;
using LivanaiTiles.Mail.Utils;

namespace LivanaiTiles.Mail.ViewModels;

/// <summary>
/// メールチェック結果を表示するためのViewModel
/// </summary>
public class MailCheckerTileViewModel : NotifiableViewModel, ICloneable
{
    #region Fields

    /// <summary>
    /// メールボックスの状態を示すアイコン (空)
    /// </summary>
    private static readonly string ICON_MAIL_EMPTY = new string(new char[] { (char)0xe158 });
    /// <summary>
    /// メールボックスの状態を示すアイコン (メールあり)
    /// </summary>
    private static readonly string ICON_MAIL_RECEIVED= new string(new char[] { (char)0xf18a });
    /// <summary>
    /// メールボックスの状態を示すアイコン (エラー)
    /// </summary>
    private static readonly string ICON_MAIL_ERROR = new string(new char[] { (char)0xe002 });

    /// <summary>
    /// このタイルで表示するメール情報
    /// </summary>
    private MailCheckInfo _mailInfo;

    /// <summary>
    /// メールボックス内で現在表示しているメール情報のインデックス
    /// </summary>
    private int _currentMailIndex;

    /// <summary>
    /// メールサーバにアクセス中の場合 true
    /// </summary>
    private bool _fetchingMail;

    #endregion

    #region Properties

    /// <summary>
    /// このViewModelをバインドするタイル
    /// </summary>
    /// <value></value>
    public UserControl? ParentTile { get; set; }

    private ResourceDictionaryWrapper Resources { get; set; }

    /// <summary>
    /// メールアカウント情報
    /// </summary>
    public MailCheckInfo MailInfo
    {
        get { return this._mailInfo; }
        set
        {
            this._mailInfo = value;
            OnPropertyChanged("MailInfo");
            OnPropertyChanged("AccountName");
            OnPropertyChanged("MailIcon");
            OnPropertyChanged("CurrentMailSubject");
            OnPropertyChanged("CurrentMailIndex");
            OnPropertyChanged("MaxMailIndex");
        }
    }

    /// <summary>
    /// メールアカウント名
    /// </summary>
    public string AccountName
    { get { return (this._mailInfo != null ? this._mailInfo.AccountName : ""); } }

    /// <summary>
    /// タイルのアイコンとして表示するメールのアイコン
    /// </summary>
    public string MailIcon
    {
        get {
            return (
                this._mailInfo == null ? ICON_MAIL_ERROR :
                this._mailInfo.MailBox == null ? ICON_MAIL_ERROR :
                0 < this._mailInfo.MailBox.Count() ? ICON_MAIL_RECEIVED :
                ICON_MAIL_EMPTY);
        }
    }

    /// <summary>
    /// 現在表示しているメールの表題
    /// </summary>
    public string CurrentMailSubject
    {
        get
        {
            return (
                // メール取得中
                this._fetchingMail ? Resources.INFO_FETCHING_MAIL :
                // メールアカウント未設定
                this._mailInfo == null ? Resources.ERR_NO_ACCOUNT :
                // メール取得失敗
                this._mailInfo.MailBox == null ? Resources.ERR_FETCH_ERROR :
                // 正常終了したがメールボックスにメールなし
                this._mailInfo.MailBox.Count() == 0 ? Resources.INFO_NO_MAIL :
                // メールボックス中のカレントインデックスのメールを表示
                this._mailInfo.MailBox.ElementAt(this.CurrentMailIndex - 1).Subject
            );
        }
    }

    /// <summary>
    /// 表示するメール情報の1から始まるインデックス.
    /// メール情報が存在しない、またはメールボックスが空の場合は0を返す.
    /// </summary>
    public int CurrentMailIndex
    {
        get { return this._currentMailIndex; }
        set
        {
            if (this._mailInfo == null)
            { this._currentMailIndex = 0; }
            else if(this.MailInfo.MailBox == null)
            { this._currentMailIndex = 0; }
            else if (this._mailInfo.MailBox.Count() < value)
            { this._currentMailIndex = this._mailInfo.MailBox.Count(); }
            else
            { this._currentMailIndex = value; }

            OnPropertyChanged("CurrentMailIndex");
            OnPropertyChanged("CurrentMailSubject");
        }
    }

    /// <summary>
    /// 現在のメールボックス内のメール数.
    /// メール情報が存在しない、またはメールボックスが空の場合は0を返す.
    /// </summary>
    public int MaxMailIndex
    {
        get
        {
            return (
                this._mailInfo == null ? 0 :
                this._mailInfo.MailBox == null ? 0 :
                0 < this._mailInfo.MailBox.Count() ? this._mailInfo.MailBox.Count() :
                0);
        }
    }

    /// <summary>
    /// メールアカウント設定ダイアログを開くコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> ConfigCommand { get; private set; }

    #endregion

    #region Commands

    /// <summary>
    /// メールアカウントの設定ダイアログを開く
    /// </summary>
    /// <param name="param"></param>
    public async void ConfigCommandImpl(string param) {
        var logger = AppControllerProvider.Instance.Logger;

        // 設定画面ダイアログを開く
        // 設定対象のアカウントとしてこのタイルに設定されているアカウント情報を渡す
        // アカウント情報が未設定の場合はここで新規にアカウントオブジェクトを生成して渡す
        var currentAccount = this.MailInfo.Account;
        var editAccount = (
            currentAccount != null ? currentAccount.Clone() as MailAccount :
            new MailAccount() {
                ServerName = "example.com",
                Port = 110,
                UserName = "username@example.com",
                Password = "xxx",
            });

        var wnd = new AccountConfigWindow();
        var vm = new AccountConfigWindowViewModel(){
            ParentDiaog = wnd,
            AccountName = this.AccountName,
            Account = editAccount,
        };
        wnd.DataContext = vm;

        // 親ウィンドウが設定されていない場合はダイアログ表示できない
        if (this.ParentTile != null) {
            var result = await wnd.ShowDialog<bool>(Avalonia.VisualTree.VisualExtensions.GetVisualRoot(this.ParentTile) as Window);
            if (result) {
                // 上で設定したDCがここでnullになっているはずはないのだが、警告対応のため
                var dlgDC = wnd.DataContext as AccountConfigWindowViewModel;
                if (dlgDC != null) {
                    logger.Print(clUtils.log.LogLevel.DEBUG, "設定を保存します");

                    // ダイアログで入力した内容をタイルの ViewModel に戻す
                    this.MailInfo.AccountName = dlgDC.AccountName;
                    this.MailInfo.Account = dlgDC.Account;

                    // メール情報を再取得する
                    this.CheckMail(this.ParentTile as MailCheckerTile);
                }
            } else {
                logger.Print(clUtils.log.LogLevel.DEBUG, "設定をキャンセルします");
            }
        }

    }

    #endregion

    /// <summary>
    /// DataContextの更新を強制的に通知する.
    /// MailInfoの内容を更新した時に上手いこと通知されないので暫定的な対応
    /// </summary>
    public void UpdateDataContext()
    {
        OnPropertyChanged("MailInfo");
        OnPropertyChanged("AccountName");
        OnPropertyChanged("MailIcon");
        OnPropertyChanged("CurrentMailSubject");
        OnPropertyChanged("CurrentMailIndex");
        OnPropertyChanged("MaxMailIndex");
    }

    /// <summary>
    /// 現在格納されているアカウント情報でメールの着信チェックを行う
    /// </summary>
    /// <param name="tile">UIスレッドが所有しているタイル</param>
    public void CheckMail(MailCheckerTile tile)
    {
        // メールサーバへのアクセスはワーカスレッドで非同期に行う
        new Thread(() =>
        {
            // サーバにアクセス中を示すメッセージを表示
            this._fetchingMail = true;

            // メール取得中のメッセージを表示する
            // 画面の再描画のため、UIスレッド内でプロパティ更新の通知を行う
            Dispatcher.UIThread.Post(()=>{
                UpdateDataContext();
            });

            IMailChecker checker = (
                this.MailInfo.Account.ServerType == MailServerType.POP3 ? (IMailChecker)new Pop3MailChecker() :
                (IMailChecker)new Imap4MailChecker());

            checker.Logger = AppControllerProvider.Instance.Logger;
            checker.Account = this.MailInfo.Account;
            checker.FetchResult = new List<MailInfo>();

            if (checker.Fetch() == MailCheckResult.SUCCEEDED)
            {
                this.MailInfo.MailBox = checker.FetchResult;
                this.CurrentMailIndex = 1;
            }
            else
            {
                this.MailInfo.MailBox = null;
            }

            // アクセス完了したので表示を更新
            this._fetchingMail = false;
            // 画面の更新はUIスレッドで行う
            Dispatcher.UIThread.Post(()=>{
                UpdateDataContext();
            });

        }).Start();
    }

    /// <summary>
    /// プレビュー表示対象のメールを1つ進める.
    /// 現在保持しているメールが一つもない場合は何もしない
    /// </summary>
    /// <returns>
    /// 新しく表示するメールの1から始まるインデックス. 
    /// 表示対象のメールが存在しない場合は0
    /// </returns>
    public int NextMail()
    {
        if (this.MaxMailIndex == 0)
        { return 0; }

        this.CurrentMailIndex = (
            this.CurrentMailIndex == this.MaxMailIndex ? 1 :
            this.CurrentMailIndex + 1);

        return this.CurrentMailIndex;
    }

    /// <summary>
    /// このインスタンスのコピーを生成して返す
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
        var cloneMailInfo = new MailCheckInfo(){
             Account = this.MailInfo.Account.Clone() as MailAccount,
             AccountName = this.MailInfo.AccountName,
             MailBox = new List<MailInfo>(this.MailInfo.MailBox)
        };

        return new MailCheckerTileViewModel()
        {
            MailInfo = cloneMailInfo
        };
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public MailCheckerTileViewModel()
    {
        this.ConfigCommand = ReactiveCommand.Create<string>(this.ConfigCommandImpl);

        // リソースディクショナリを読み込む
        this.Resources = new ResourceDictionaryWrapper();
    }
}

