using Avalonia.Controls;
using Avalonia.Threading;

using LivanaiTiles.Mail.Mail;
using LivanaiTiles.Mail.ViewModels;

namespace LivanaiTiles.Mail.Views;

/// <summary>
/// MailCheckerTile.xaml の相互作用ロジック
/// </summary>
public partial class MailCheckerTile : UserControl
{
    /// <summary>
    /// このタイルを生成したプロバイダ
    /// </summary>
    public MailCheckProvider provider;

    /// <summary>
    /// メール再取得までのデフォルト間隔 (秒)
    /// </summary>
    private static readonly int DEFAULT_MAIL_FETCH_INTERVAL = 60 * 10;

    /// <summary>
    /// 表示メールを次に贈るまでのデフォルト間隔 (秒)
    /// </summary>
    private static readonly int DEFAULT_MAIL_VIEW_INTERVAL = 10;

    /// <summary>
    /// 一定間隔でメールの切り替え、メールの再取得を行うためのタイマー
    /// </summary>
    private DispatcherTimer timer;

    /// <summary>
    /// メールを再取得するまでの残り時間
    /// </summary>
    private int _remainMailFetch;

    /// <summary>
    /// 表示中のメールを切り替えるまでの残り時間
    /// </summary>
    private int _remainCurrentMailChange;

    public MailCheckerTile() {
        InitializeComponent();

        var vm = new MailCheckerTileViewModel()
        {
            ParentTile = this,
            MailInfo = new MailCheckInfo() { AccountName = "no account." }
        };
        this.DataContext = vm;

        // メールの再取得するためのタイマーを設定
        this._remainMailFetch = DEFAULT_MAIL_FETCH_INTERVAL;
        this._remainCurrentMailChange = DEFAULT_MAIL_VIEW_INTERVAL;
        this.timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 1000), };
        this.timer.Tick += Timer_Tick;
        this.timer.Start();
    }


    /// <summary>
    /// タイマーのコールバック
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Timer_Tick(object sender, EventArgs e)
    {
        var dc = this.DataContext as MailCheckerTileViewModel;

        this._remainCurrentMailChange--;
        if (this._remainCurrentMailChange < 0)
        {
            // 表示中のメールを進める
            dc.NextMail();
            this._remainCurrentMailChange = DEFAULT_MAIL_VIEW_INTERVAL;
        }

        this._remainMailFetch--;
        if (this._remainMailFetch < 0)
        {
            // メールを再取得する
            dc.CheckMail(this);
            this._remainMailFetch = (
                dc.MailInfo.Account == null ? dc.MailInfo.Account.FetchIntervalMinutes * 60 :
                DEFAULT_MAIL_FETCH_INTERVAL);
        }
    }
}
