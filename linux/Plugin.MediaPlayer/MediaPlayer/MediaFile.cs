using Avalonia.Media.Imaging;

namespace LivanaiTiles.MediaPlayer.MediaPlayer;

/// <summary>
/// 再生対象のメディアファイル1個を示すクラス
/// </summary>
public class MediaFile
{
    /// <summary>
    /// タイトル
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// メディアの作成者
    /// </summary>
    public string Author { get; set; }

    /// <summary>
    /// メディアのアートワーク画像
    /// </summary>
    public Bitmap Artwork { get; set; }

    /// <summary>
    /// メディアファイルの実態を示すオブジェクト
    /// </summary>
    public FileInfo File { get; set; }

    /// <summary>
    /// ListView上にバインディングした時に選択項目に含まれるかどうか
    /// </summary>
    public bool IsSelected { get; set; }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public MediaFile()
    { }
}
