using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.Utils;

namespace LivanaiTiles.MediaPlayer.MediaPlayer;


/// <summary>
/// メディアプレイヤーの設定情報をJSONファイルから読み込み、または書き込むクラス
/// アクセス頻度の低い設定情報なので、起動時に読み込んで保持し続けるのではなく
/// アクセス時に都度JSONファイルにアクセスする。
/// </summary>
public class MediaPlayerConfig
{
    #region Constants

    /// <summary>
    /// メディアファイルを再帰的に検索する場合の最大階層数のデフォルト値
    /// </summary>
    private static readonly int DEFAULT_MAX_RECURSIVE_DEPTH = 3;

    #endregion

    /// <summary>
    /// プレイリストを保存するデフォルトのフォルダのフルパス
    /// </summary>
    /// <value></value>
    public string DefaultPlaylistPath { get; set; }

    /// <summary>
    /// メディアファイルを保存するデフォルトのフォルダのパス
    /// </summary>
    /// <value></value>
    public string DefaultMusicPath { get; set; }

    /// <summary>
    /// メディアファイルを再帰的に検索する場合のフォルダの最大階層数
    /// </summary>
    /// <value></value>
    public int MaxRecursiveDepth { get; set; }

    /// <summary>
    /// メディアプレイヤー起動時のデフォルトのボリューム
    /// </summary>
    /// <value></value>
    public int DefaultVolume { get; set; }

    /// <summary>
    /// メディアプレイヤーの設定情報をファイルに保存する
    /// </summary>
    /// <param name="controller"></param>
    /// <param name="confDir"></param>
    /// <param name="conf"></param>
    public static void Save(
        IAppController controller,
        DirectoryInfo confDir,
        MediaPlayerConfig conf)
    {
        var logger = controller.Logger;
        var confFilePath = Path.Combine(confDir.FullName, "mediaplayer.conf");
        logger.PrintPush(LogLevel.DEBUG, $"Save Mediaplaye Config to \"{ confFilePath }\"");

        FileUtils.SaveToJson(conf, confFilePath, logger);

        logger.PrintPop();
    }
    
    /// <summary>
    /// メディアプレイヤーの設定情報を読み込む
    /// </summary>
    /// <param name="controller"></param>
    /// <param name="confDir"></param>
    /// <returns>
    /// JSON ファイルから読み込んだメディアプレイヤーの設定情報.
    /// 設定ファイルが未作成の場合はデフォルト値を格納したオブジェクト.
    /// </returns>
    public static MediaPlayerConfig Load(
        IAppController controller,
        DirectoryInfo confDir)
    {
        var logger = controller.Logger;
        var confFilePath = Path.Combine(confDir.FullName, "mediaplayer.conf");
        logger.PrintPush(LogLevel.DEBUG, $"Load Mediaplaye Config from \"{ confFilePath }\"");

        var confFile = new FileInfo(confFilePath);
        if (confFile.Exists)
        {
            // JSON ファイルから設定を読み込む
            // 読み込みに失敗した場合はデフォルト値を返す
            try
            {
                var conf = FileUtils.LoadFromJson<MediaPlayerConfig>(confFilePath, logger);
                logger.Print(LogLevel.DEBUG, "Load succeeded.");
                return conf;
            }
            catch (Exception eUnknown)
            {
                logger.Print(LogLevel.ERROR, $"Load failed. \"{ eUnknown.Message }\"");
                return new MediaPlayerConfig();
            }
        }
        else
        {
            // 該当のファイルが存在しない場合はデフォルト値を返す
            logger.Print(LogLevel.DEBUG, "config file not exist. return default value.");
            return new MediaPlayerConfig();
        }
    }

    /// <summary>
    /// コンストラクタ
    /// 外部から直接このインスタンスを作成することは認めない
    /// </summary>
    public MediaPlayerConfig()
    {
        // プレイリスト、メディアのデフォルトのパスは Musics とする
        // 取得できなかった場合 (デスクトップ未使用環境の場合など) はホームディレクトリを返す
        try
        {
            this.DefaultPlaylistPath = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic); 
            this.DefaultMusicPath = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic); 
        }
        catch (Exception)
        {
            this.DefaultPlaylistPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            this.DefaultMusicPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); 
        }
        this.MaxRecursiveDepth = DEFAULT_MAX_RECURSIVE_DEPTH;
    }
}

