using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using clUtils.log;
using LivanaiTiles.MediaPlayer.MediaPlayer;

namespace LivanaiTiles.MediaPlayer.Utils;

/// <summary>
/// プレイリストを読み込むためのユーティリティクラス
/// </summary>
public static class PlayListReader
{
    private static readonly string TAG_PLAYLIST_TITLE = "#PLAYLIST:";

    /// <summary>
    /// 指定したパスのプレイリストを読み込む
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static PlayList Open(string path)
    {
        using (StreamReader reader = new StreamReader(path, new UTF8Encoding(false)))
        {
            // 全体を読み込み、改行単位で分割する
            var lines = reader.ReadToEnd().Split(new char[]{ '\n' });

            // プレイリストのタイトル
            var playlistTitle = FindPlaylistTitle(lines);

            // タグ以外の行で存在するメディアファイルのパスが指定されている場合、
            // それをプレイリストに格納する
            var mediaFiles = new List<MediaFile>();
            var playlistFile = new FileInfo(path);
            foreach (var line in lines)
            {
                var parsed = ParseMediaFile(
                        playlistFile.Directory.FullName,
                        line);
                if (parsed != null)
                { mediaFiles.Add(parsed); }
            }

            var pl = new PlayList()
            {
                Title = playlistTitle,
                PlayListFile = playlistFile
            };
            foreach (var media in mediaFiles)
            { pl.PlayMediaList.Add(media); }

            return pl;
        }
    }

    /// <summary>
    /// プレイリストのタイトルを格納したタグを検索して返す
    /// </summary>
    /// <param name="lines"></param>
    /// <returns></returns>
    private static string FindPlaylistTitle(string[] lines)
    {
        var playlistTitleLine = lines.FirstOrDefault(x => 0 == x.Trim().IndexOf(TAG_PLAYLIST_TITLE));
        if (playlistTitleLine != null)
        { return playlistTitleLine.Substring(playlistTitleLine.IndexOf(TAG_PLAYLIST_TITLE) + TAG_PLAYLIST_TITLE.Length); }
        else
        { return null; }
    }

    private static MediaFile? ParseMediaFile(
        string currentDir,
        string line)
    {
        var trimmed = line.Trim();
        // タグ、コメントの行は除外
        if (trimmed.StartsWith("#"))
        { return null; }

        // ファイルパスとして有効か？
        var f = new FileInfo(Path.Combine(currentDir, trimmed));
        if (!f.Exists)
        { return null; }

        // ディレクトリではないか？
        // TODO: ディレクトリの場合は再帰的にメディアファイルを読み込むようにしたい
        var d = new DirectoryInfo(f.FullName);
        if (d.Exists)
        { return null; }

        // メディアファイルか？
        if (f.Extension == ".mp3" || f.Extension == ".wma" || f.Extension == ".wav")
        { return new MediaFile(){ File = f }; }
        
        return null;
    }

}