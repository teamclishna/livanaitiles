using System.Collections.Generic;
using System.IO;
using System.Text;
using LivanaiTiles.MediaPlayer.MediaPlayer;

namespace LivanaiTiles.MediaPlayer.Utils;

/// <summary>
/// プレイリストを保存するためのユーティリティクラス
/// </summary>
public static class PlayListWriterd
{
    /// <summary>
    /// 指定したプレイリストを保存する
    /// </summary>
    /// <param name="src">保存する対象のプレイリスト</param>
    /// <param name="path">保存先のフルパス</param>
    public static void Save(
        this PlayList src,
        string path)
    {
        var playlistString = CreatePlaylistString(src);
        using (StreamWriter writer = new StreamWriter(path, false, Encoding.UTF8))
        { writer.Write(playlistString); }
    }

    /// <summary>
    /// プレイリストの m3u 形式の文字列を生成して返す
    /// </summary>
    /// <param name="src">生成対象のプレイリスト</param>
    /// <returns></returns>
    private static string CreatePlaylistString(PlayList src)
    {
        var lines = new List<string>();
        // ヘッダ
        lines.Add("#EXTM3U");
        if (!string.IsNullOrWhiteSpace(src.Title))
        {
            // プレイリストのタイトル
            lines.Add($"#PLAYLIST:{ src.Title }");
        }
        // 対象のファイルパスを順に格納する
        foreach (var media in src.PlayMediaList)
        { lines.Add(media.File.FullName); }

        // 1つの文字列に格納して返す
        var sb = new StringBuilder();
        foreach(var line in lines)
        { sb.Append(line).Append("\n"); }

        return sb.ToString();
    }

}