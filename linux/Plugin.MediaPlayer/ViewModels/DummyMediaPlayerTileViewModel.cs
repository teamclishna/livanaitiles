using LivanaiTiles.Controllers;
using LivanaiTiles.MediaPlayer.MediaPlayer;
using LivanaiTiles.MediaPlayer.Utils;

namespace LivanaiTiles.MediaPlayer.ViewModels;

/// <summary>
/// MediaPlayerTileViewModel のテスト用のダミープレイリスト
/// </summary>
public class DummyMediaPlayerTileViewModel : MediaPlayerTileViewModel {

    public DummyMediaPlayerTileViewModel() : base() {
        var logger = AppControllerProvider.Instance.Logger;

        var playlist = new PlayList();
        playlist.PlayMediaList.Add(MediaPlayerUtils.DetectMP3File(new FileInfo("/home/clishna/OneDrive/Music/i.o.sound/青春ラビュー/01 青春ラビュー.mp3"), logger));
        playlist.PlayMediaList.Add(MediaPlayerUtils.DetectMP3File(new FileInfo("/home/clishna/OneDrive/Music/i.o.sound/青春ラビュー/02 はにかむキミに恋するワタシ.mp3"), logger));
        playlist.PlayMediaList.Add(MediaPlayerUtils.DetectMP3File(new FileInfo("/home/clishna/OneDrive/Music/i.o.sound/足りないハートビート/01 足りないハートビート.mp3"), logger));
        this.CurrentPlayList = playlist;
    }
}