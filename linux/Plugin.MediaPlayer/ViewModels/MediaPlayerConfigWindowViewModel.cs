using Avalonia.Controls;
using ReactiveUI;

using System.Reactive;

using clUtils.gui;
using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.MediaPlayer.MediaPlayer;
using LivanaiTiles.MediaPlayer.Views;
using LivanaiTiles.Utils;

namespace LivanaiTiles.MediaPlayer.ViewModels;

/// <summary>
/// メディアプレイヤーの設定用ダイアログの ViewModel
/// </summary>
public class MediaPlayerConfigWindowViewModel : NotifiableViewModel {

    #region Fields

    private string _mediaDirectoryPath;

    private string _playlistDirectoryPath;

    private int _mediaDirectoryRecursiveDepth;

    private MediaPlayerTile _parentTile;

    #endregion

    #region Properties

    /// <summary>
    /// この ViewModel をバインディングする設定画面
    /// </summary>
    /// <value></value>
    public Window? ParentDiaog { get; set; }

    /// <summary>
    /// 再生するメディアファイルのデフォルトのディレクトリ
    /// </summary>
    /// <value></value>
    public string MediaDirectoryPath
    {
        get { return this._mediaDirectoryPath; }
        set
        {
            this._mediaDirectoryPath = value;
            OnPropertyChanged(nameof(MediaDirectoryPath));
        }
    }

    /// <summary>
    /// プレイリストの保存場所のデフォルトのディレクトリ
    /// </summary>
    /// <value></value>
    public string PlaylistDefaultPath
    {
        get { return this._playlistDirectoryPath; }
        set
        {
            this._playlistDirectoryPath = value;
            OnPropertyChanged(nameof(PlaylistDefaultPath));
        }
    }

    /// <summary>
    /// メディアファイルのディレクトリを再帰的に検索する際の最大階層数
    /// </summary>
    /// <value></value>
    public int MediaDirectoryRecursiveDepth
    {
        get { return this._mediaDirectoryRecursiveDepth; }
        set
        {
            this._mediaDirectoryRecursiveDepth = value;
            OnPropertyChanged(nameof(MediaDirectoryRecursiveDepth));
        }
    }

    /// <summary>
    /// OKボタンを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> OKCommand{ get; private set; }

    /// <summary>
    /// キャンセルボタンを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> CancelCommand{ get; private set; }

    /// <summary>
    /// メディアファイルのデフォルトディレクトリ選択ボタンを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> OpenMediaDirectoryCommand { get; private set; }

    /// <summary>
    /// プレイリストのデフォルトディレクトリ選択ボタンを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> OpenPlaylistDirectoryCommand { get; private set; }

    #endregion

    #region Commands

    /// <summary>
    /// ダイアログの OK ボタンを押した時のイベント
    /// </summary>
    /// <param name="param"></param>
    public void OKCommandImpl(string param) {
        if (this.ParentDiaog != null) {

            // 変更内容を保存する
            var confPath = FileUtils.GetProviderConfigDirectory(
                    this._parentTile.ParentProvider,
                    true);
            var conf = MediaPlayerConfig.Load(
                    AppControllerProvider.Instance,
                    confPath);

            conf.DefaultMusicPath = this.MediaDirectoryPath;
            conf.DefaultPlaylistPath = this.PlaylistDefaultPath;
            conf.MaxRecursiveDepth = this.MediaDirectoryRecursiveDepth;
            
            MediaPlayerConfig.Save(
                    AppControllerProvider.Instance,
                    confPath,
                    conf);

            ParentDiaog.Close(true);
        }
    }

    public void CanceCommandImpl(string param)
    {
        if (this.ParentDiaog != null) {
            ParentDiaog.Close(false);
        }
    }

    /// <summary>
    /// メディアファイルのデフォルトのディレクトリ選択ボタンを押した時のイベント
    /// </summary>
    /// <param name="param"></param>
    /// <returns></returns>
    public async void OpenMediaDirectoryCommandImpl(string param)
    {
        var dlg = new OpenFolderDialog()
        {
            Directory = this.MediaDirectoryPath,
            Title = "Select Media Directory",
        };
        var result = await dlg.ShowAsync(this.ParentDiaog);
        if (result != null)
        { this.MediaDirectoryPath = result; }
    }

    /// <summary>
    /// プレイリストのデフォルトの保存場所選択ボタンを押した時のイベント
    /// </summary>
    /// <param name="param"></param>
    /// <returns></returns>
    public async void OpenPlaylistDirectoryCommandImpl(string param)
    {
        var dlg = new OpenFolderDialog()
        {
            Directory = this.PlaylistDefaultPath,
            Title = "Select Playlist Directory",
        };
        var result = await dlg.ShowAsync(this.ParentDiaog);
        if (result != null)
        { this.PlaylistDefaultPath = result; }
    }

    #endregion

    #region Methods

    /// <summary>
    /// インスタンスの初期化処理を行う
    /// </summary>
    /// <param name="parentTile"></param>
    public void Init(MediaPlayerTile parentTile)
    {
        // 現在の設定値を取得し、プロパティに格納する
        this._parentTile = parentTile;
        var confPath = FileUtils.GetProviderConfigDirectory(
                parentTile.ParentProvider,
                true);
        var conf = MediaPlayerConfig.Load(
                AppControllerProvider.Instance,
                confPath);

        this.MediaDirectoryPath = conf.DefaultMusicPath;
        this.PlaylistDefaultPath = conf.DefaultPlaylistPath;
        this.MediaDirectoryRecursiveDepth = conf.MaxRecursiveDepth;
    }

    #endregion

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public MediaPlayerConfigWindowViewModel()
    {
        this.OKCommand = ReactiveCommand.Create<string>(this.OKCommandImpl);
        this.CancelCommand = ReactiveCommand.Create<string>(this.CanceCommandImpl);
        this.OpenMediaDirectoryCommand = ReactiveCommand.Create<string>(this.OpenMediaDirectoryCommandImpl);
        this.OpenPlaylistDirectoryCommand = ReactiveCommand.Create<string>(this.OpenPlaylistDirectoryCommandImpl);
    }
}