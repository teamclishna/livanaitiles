using ReactiveUI;
using System.Reactive;

namespace LivanaiTiles.MediaPlayer.ViewModels;

public partial class MediaPlayerTileViewModel
{
    /// <summary>
    /// メディアプレイヤーの設定画面を開くコマンド
    /// </summary>
    /// <value></value>    
    public ReactiveCommand<string, Unit> ConfigCommand { get; private set; }

    /// <summary>
    /// 再生／一時停止ボタンのコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> PlayPauseCommand { get; private set; }

    /// <summary>
    /// 停止ボタンのコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> StopCommand { get; private set; }

    /// <summary>
    /// 前の曲へ戻るボタンのコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> PlayPrevCommand { get; private set; }

    /// <summary>
    /// 次の曲へ送るボタンのコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> PlayNextCommand { get; private set; }

    /// <summary>
    /// プレイリストの操作を行うためのウィンドウを表示するコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> ShowControlPanelCommand { get; private set; }

    /// <summary>
    /// リピートモードを切り替えるコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> RepeatCommand { get; private set; }

    /// <summary>
    /// プレイリストのシャッフルを行うコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> ShuffleCommand { get; private set; }

}
