
using LivanaiTiles.Controllers;
using LivanaiTiles.MediaPlayer.MediaPlayer;
using LivanaiTiles.MediaPlayer.Views;

using LivanaiTiles.Utils;

namespace LivanaiTiles.MediaPlayer.ViewModels;

public partial class MediaPlayerTileViewModel
{
    #region Command Impl

    /// <summary>
    /// 設定画面を開く
    /// </summary>
    /// <param name="param"></param>
    private async void ConfigCommandImpl(string param)
    {
        // 設定ウィンドウを開く
        var wnd = new MediaPlayerConfigWindow();
        var vm = new MediaPlayerConfigWindowViewModel()
        {
            ParentDiaog = wnd,
        };
        vm.Init(this.ParentTile as MediaPlayerTile);

        wnd.DataContext = vm;
        var result = await wnd.ShowDialog<bool>(Avalonia.VisualTree.VisualExtensions.GetVisualRoot(this.ParentTile) as Avalonia.Controls.Window);
    }

    /// <summary>
    /// 再生／一時停止
    /// </summary>
    private void PlayAndPauseCommandImpl(string param)
    {
        // VLCプレイヤーが使用不能な状態であれば何もせずに終了
        if (this.VLC == null || this.VLCPlayer == null)
        { return; }

        var logger = AppControllerProvider.Instance.Logger;

        if (!this.IsPlaying)
        {
            logger.Print(clUtils.log.LogLevel.DEBUG, $"Play media ({ this.CurrentMediaIndex }: { this.CurrentMedia.Title }).");

            // 停止状態であれば曲の再生を開始
            // この時、曲の再生を行うと (何故か) ボリュームがデフォルトに戻ってしまうので (VLCPlayer のプロパティ自体はそのままなのだが)、
            // 一旦明示的にプロパティを設定し直す
            this.VLCPlayer.Media= new LibVLCSharp.Shared.Media(this.VLC, this.CurrentMedia.File.FullName);
            this.VLCPlayer.Play();
            this.VLCPlayer.Volume = this.VLCPlayer.Volume;

            // 再生中を示すフラグをオンにし、一時停止状態を明示的に解除する
            this.IsPlaying = true;
            this.IsPause = false;

        } else {

            logger.Print(clUtils.log.LogLevel.DEBUG, this.IsPause ? "restore media." : "pause");

            // 再生中であれば再生中、一時停止の切り替えをする
            this.IsPause = !this.IsPause;
            this.VLCPlayer.SetPause(this.IsPause);
        }
    }

    /// <summary>
    /// 停止
    /// </summary>
    /// <param name="param"></param>
    private void StopCommandImpl(string param)
    {
        // VLCプレイヤーが使用不能な状態であれば何もせずに終了
        if (this.VLC == null || this.VLCPlayer == null)
        { return; }

        var logger = AppControllerProvider.Instance.Logger;
        logger.Print(clUtils.log.LogLevel.DEBUG, "stop media.");

        // 現在再生中のメディアを停止する
        if (this.IsPlaying) {
            this.VLCPlayer.Stop();
            this.IsPlaying = false;
        }
    }

    /// <summary>
    /// 次の曲に送る
    /// </summary>
    private void PlayNextCommandImpl(string param)
    {
        this.CurrentMediaIndex++;

        if (this.IsPlaying)
        {
            this.StopCommandImpl("");
            this.PlayAndPauseCommandImpl("");
        }
    }

    /// <summary>
    /// 前の曲に送る
    /// </summary>
    private void PlayPreviousCommandImpl(string param)
    {
        this.CurrentMediaIndex--;
        if (this.IsPlaying)
        {
            this.StopCommandImpl("");
            this.PlayAndPauseCommandImpl("");
        }
    }

    private void ShowControlPanelCommandImpl(string param) {
        // この ViewModel を DataContext にしたコントロールパネルウィンドウを開く
        this.ControlPanelWindow = new PlayControlWindow() {
            DataContext = this,
        };
        this.ControlPanelWindow.Show();
    }

    /// <summary>
    /// プレイリストのシャッフルを行う
    /// </summary>
    /// <param name="param"></param>
    private void ShuffleCommandImpl(string param)
    {
        // 再生中であれば停止する
        if (this.IsPlaying)
        { this.StopCommandImpl(""); }

        // 現在のプレイリストの内容を一旦リストに入れ、ランダムな要素を取り出しながら新しいリストに入れる
        var mediaList = this.CurrentPlayList.PlayMediaList.ToList<MediaFile>();
        var newList = new List<MediaFile>();
        var randomizer = new Random();

        while (1 < mediaList.Count())
        {
            var srcIndex = randomizer.Next(mediaList.Count());
            newList.Add(mediaList[srcIndex]);
            mediaList.RemoveAt(srcIndex);
        }
        newList.Add(mediaList[0]);

        // シャッフルした新しいリストの内容でプレイイストを入れ替える
        this.CurrentPlayList.PlayMediaList.Clear();
        foreach (var media in newList)
        { this.CurrentPlayList.PlayMediaList.Add(media); }

        OnPropertyChanged(nameof(CurrentPlayList));
        OnPropertyChanged(nameof(MediaCount));
        OnPropertyChanged(nameof(CanShuffle));
    }

    #endregion
}