using Avalonia.Controls;
using Avalonia.Media;

using LivanaiTiles.MediaPlayer.MediaPlayer;

namespace LivanaiTiles.MediaPlayer.ViewModels;

public partial class MediaPlayerTileViewModel
{
    /// <summary>
    /// このViewModelをバインドするタイル
    /// </summary>
    /// <value></value>
    public UserControl? ParentTile { get; set; }

    /// <summary>
    /// 操作パネルを表示している場合、そのウィンドウのインスタンス
    /// </summary>
    /// <value></value>
    private Window? ControlPanelWindow { get; set; }

    /// <summary>
    /// LibVLC のインスタンス
    /// </summary>
    public LibVLCSharp.Shared.LibVLC VLC { get; set; }

    /// <summary>
    /// VLCプレイヤーのインスタンス
    /// </summary>
    public LibVLCSharp.Shared.MediaPlayer VLCPlayer { get; set; }

    /// <summary>
    /// 再生対象のプレイリスト
    /// </summary>
    public PlayList? CurrentPlayList
    {
        get { return this._currentPlayList; }
        set
        {
            this._currentPlayList = value;
            OnPropertyChanged(nameof(CurrentPlayList));
            OnPropertyChanged(nameof(MediaCount));
            OnPropertyChanged(nameof(CanShuffle));
            // 新しいプレイリストをセットした場合、曲を停止状態にしてカレントを1曲目に設定する
            this.StopCommandImpl("");
            this.CurrentMediaIndex = 0;
        }
    }

    /// <summary>
    /// プレイリストに含まれるメディアの件数.
    /// メディアがロードされていない場合は0を返す
    /// </summary>
    public int MediaCount
    {
        get
        {
            return
                this._currentPlayList == null ? 0 :
                this._currentPlayList.PlayMediaList.Count;
        }
    }

    /// <summary>
    /// プレイリスト中の現在生成中のメディアを示す0から始まるインデックス.
    /// 再生可能なメディアが無い場合は負の値が設定される.
    /// </summary>
    public int CurrentMediaIndex
    {
        get
        {
            return
                this._currentPlayList == null ? -1 :
                this._currentPlayList.PlayMediaList.Count == 0 ? -1 :
                this._currentMediaIndex;
        }
        set
        {
            // プレイリストが読み込まれていない場合、プレイリストの曲数の範囲外の場合は設定値を無視して負の値に置き換える
            this._currentMediaIndex = (
                value < 0 ? -1 :
                this._currentPlayList == null ? -1 :
                this._currentPlayList.PlayMediaList.Count <= value ? -1 :
                value);

            OnPropertyChanged(nameof(CurrentMediaIndex));
            OnPropertyChanged(nameof(CurrentMedia));
            OnPropertyChanged(nameof(PlayTitle));
            OnPropertyChanged(nameof(PlayAuthor));
            OnPropertyChanged(nameof(PlayArtwork));
            OnPropertyChanged(nameof(CanPlayPause));
            OnPropertyChanged(nameof(CanStop));
            OnPropertyChanged(nameof(CanPrevious));
            OnPropertyChanged(nameof(CanForward));
        }
    }

    /// <summary>
    /// 現在再生中のメディア情報
    /// </summary>
    private MediaFile? CurrentMedia
    {
        get
        {
            return
                this.CurrentMediaIndex < 0 ? null :
                this.CurrentPlayList == null ? null :
                this.CurrentPlayList.PlayMediaList.Count == 0 ? null :
                this.CurrentPlayList.PlayMediaList[this.CurrentMediaIndex];
        }
    }

    /// <summary>
    /// 現在再生中のメディアのタイトル
    /// </summary>
    public string PlayTitle
    { get { return this.CurrentMedia == null ? "" : this.CurrentMedia.Title; } }

    /// <summary>
    /// 現在再生中のメディアの作成者
    /// </summary>
    public string PlayAuthor
    { get { return this.CurrentMedia == null ? "" : this.CurrentMedia.Author; } }

    ///
    /// <summary>
    /// 現在再生中のメディアのアートワーク画像
    /// </summary>
    public IImage PlayArtwork
    {
        get
        {
            // ・プレイリストが設定されていない、再生対象の曲がない場合
            // ・再生中ではない場合
            // ・再生中の曲にアートワークが設定されていない場合
            // はデフォルトのアイコンを表示する
            return
                this.CurrentMedia == null ? this.DefaultMediaPlayerIcon :
                !this.IsPlaying ? this.DefaultMediaPlayerIcon :
                this.CurrentMedia.Artwork == null ? this.DefaultMediaPlayerIcon :
                this.CurrentMedia.Artwork;
        }
    }

    /// <summary>
    /// 現在再生中であればtrue
    /// </summary>
    public bool IsPlaying
    {
        get { return this._isPlaying; }
        set
        {
            this._isPlaying = value;
            OnPropertyChanged(nameof(IsPlaying));
            OnPropertyChanged(nameof(CanStop));
            OnPropertyChanged(nameof(PlayArtwork));
        }
    }

    /// <summary>
    /// 現在再生中、かつ一時停止中であれば true
    /// </summary>
    /// <value></value>
    public bool IsPause
    {
        get { return this._isPause; }
        set
        {
            this._isPause = value;
            OnPropertyChanged(nameof(IsPause));
        }
    }

    /// <summary>
    /// リピート再生を行う場合はtrue
    /// </summary>
    public bool IsRepeat
    {
        get { return this._isRepeat; }
        set
        {
            this._isRepeat = value;
            OnPropertyChanged("IsRepeat");
        }
    }

    /// <summary>
    /// 再生／一時停止ボタンが押せるかどうか
    /// </summary>
    /// <value></value>
    public bool CanPlayPause
    {
        get
        {
            return
                this.CurrentPlayList != null &&
                0 < this.CurrentPlayList.PlayMediaList.Count &&
                this.CurrentMediaIndex < this.CurrentPlayList.PlayMediaList.Count;
        }
    }

    /// <summary>
    /// 停止ボタンが押せるかどうか
    /// </summary>
    /// <value></value>
    public bool CanStop
    {
        get { return this.IsPlaying; }
    }

    /// <summary>
    /// 「前の曲へ戻る」ボタンが押せるかどうか
    /// </summary>
    /// <value></value>
    public bool CanPrevious
    {
        get { return this.CanPlayPause && 0 < this.CurrentMediaIndex; }
    }

    /// <summary>
    /// 「次の曲へ送る」ボタンが押せるかどうか
    /// </summary>
    /// <value></value>
    public bool CanForward
    {
        get { return this.CanPlayPause && this.CurrentMediaIndex < this.CurrentPlayList.PlayMediaList.Count - 1; }
    }

    /// <summary>
    /// シャッフルボタンが押せるかどうか
    /// </summary>
    /// <value></value>
    public bool CanShuffle
    {
        get { return this.CurrentPlayList != null && 0 < this.CurrentPlayList.PlayMediaList.Count; }
    }

    /// <summary>
    /// VLCPlayer の再生ボリューム
    /// </summary>
    /// <value></value>
    public int Volume {
        get { return this.VLCPlayer.Volume; }
        set
        {
            // 設定可能なボリュームは 0 〜 100 の範囲内
            if (0 <= value && value <= 100) {
                this.VLCPlayer.Volume = value;
                OnPropertyChanged(nameof(Volume));
            }
        }
    }

}
