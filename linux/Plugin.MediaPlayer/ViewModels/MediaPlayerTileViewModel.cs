using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Media;
using Avalonia.Platform;
using Avalonia.Threading;
using Avalonia.Shared.PlatformSupport;
using ReactiveUI;
using System.Reactive;

using LibVLCSharp.Avalonia;
using LibVLCSharp.Shared;

using clUtils.gui;
using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.MediaPlayer.MediaPlayer;
using LivanaiTiles.MediaPlayer.Views;
using LivanaiTiles.Utils;

namespace LivanaiTiles.MediaPlayer.ViewModels;

/// <summary>
/// MediaPlayer タイルの ViewModel.
/// </summary>
public partial class MediaPlayerTileViewModel : NotifiableViewModel
{
    #region Constant values

    /// <summary>
    /// 停止状態で表示するメディアプレイヤーのアイコン画像のURI
    /// </summary>
    private readonly static string DEFAULT_MEDIA_PLAYER_ICON = "avares://Plugin.MediaPlayer/Assets/music_icon_large.png";
    
    #endregion
    
    #region Fields

    /// <summary>
    /// 曲の停止時に表示するメディアプレイヤーのアイコン
    /// </summary>
    private IImage DefaultMediaPlayerIcon;

    /// <summary>
    /// 現在タイルに読み込んでいるプレイリスト
    /// </summary>
    private PlayList? _currentPlayList = null;

    /// <summary>
    /// プレイリスト中で現在再生中の曲のインデックス
    /// </summary>
    private int _currentMediaIndex = 0;

    /// <summary>
    /// 現在曲を再生中か？
    /// </summary>
    private bool _isPlaying;
    /// <summary>
    /// 現在再生を一時停止中か？
    /// </summary>
    private bool _isPause;

    /// <summary>
    /// リピート再生を行うか？
    /// </summary>
    private bool _isRepeat;

    #endregion

    /// <summary>
    /// リピートモードの切り替えを行う
    /// </summary>
    // private void SetRepeatMode()
    // { this.IsRepeat = !this.IsRepeat; }

    /// <summary>
    /// メディア再生終了時のイベント
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnMediaEnd(object sender, EventArgs e)
    {
        var logger = AppControllerProvider.Instance.Logger;
        logger.PrintPush(clUtils.log.LogLevel.DEBUG, "End Reached.");

        if (this.CurrentMediaIndex < this.MediaCount - 1)
        {
            logger.Print(clUtils.log.LogLevel.DEBUG, "Play next song.");
            // 次の曲へ進む
            this.CurrentMediaIndex++;
            ThreadPool.QueueUserWorkItem(_ => {
                StopCommandImpl("");
                PlayAndPauseCommandImpl("");
            });
            
        }
        else if (this.IsRepeat)
        {
            logger.Print(clUtils.log.LogLevel.DEBUG, "Repeat from first song.");

            // リピートが設定されている場合は最初から
            this.CurrentMediaIndex = 0;
            ThreadPool.QueueUserWorkItem(_ => {
                StopCommandImpl("");
                PlayAndPauseCommandImpl("");
            });
        }
        else
        {
            logger.Print(clUtils.log.LogLevel.DEBUG, "End all playlist. play ended.");
            // 再生終了
            this.IsPlaying = false;
        }
        
        logger.PrintPop();
    }

    public void Init()
    {
        var confPath = FileUtils.GetProviderConfigDirectory((this.ParentTile as MediaPlayerTile).ParentProvider, true);
        var conf = MediaPlayerConfig.Load(AppControllerProvider.Instance, confPath);

        // ボリュームのデフォルトは曲の再生を開始した時点で初めて設定されるようなので、
        // ここで明示的に仮のデフォルト値を設定しておく
        this.Volume = conf.DefaultVolume;
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public MediaPlayerTileViewModel()
    {
        var logger = AppControllerProvider.Instance.Logger;

        // メディアプレイヤーのデフォルトアイコン画像をロードする
        var assetLoader = AvaloniaLocator.Current.GetService<IAssetLoader>();
        this.DefaultMediaPlayerIcon = new Avalonia.Media.Imaging.Bitmap(assetLoader.Open(new Uri(DEFAULT_MEDIA_PLAYER_ICON)));

        // VLCプレイヤーを初期化する
        logger.PrintPush(clUtils.log.LogLevel.DEBUG, "Init VLCPlayer...");
        this.VLC = new LibVLCSharp.Shared.LibVLC();
        this.VLCPlayer = new LibVLCSharp.Shared.MediaPlayer(this.VLC);

        // ボリュームのデフォルトは曲の再生を開始した時点で初めて設定されるようなので、
        // ここで明示的に仮のデフォルト値を設定しておく
        this.Volume = 75;
        
        logger.PrintPop();

        // メディアの演奏状態を示すフラグを初期化する
        logger.PrintPush(clUtils.log.LogLevel.DEBUG, "Init player properties...");
        this.IsPlaying = false;
        this.IsPause = false;
        logger.PrintPop();

        // 再生終了時のイベントを設定する
        this.VLCPlayer.EndReached += OnMediaEnd;

        // コマンド
        this.ConfigCommand = ReactiveCommand.Create<string>(this.ConfigCommandImpl);
        this.PlayPauseCommand = ReactiveCommand.Create<string>(this.PlayAndPauseCommandImpl);
        this.StopCommand = ReactiveCommand.Create<string>(this.StopCommandImpl);
        this.PlayPrevCommand = ReactiveCommand.Create<string>(this.PlayPreviousCommandImpl);
        this.PlayNextCommand = ReactiveCommand.Create<string>(this.PlayNextCommandImpl);
        this.ShowControlPanelCommand = ReactiveCommand.Create<string>(this.ShowControlPanelCommandImpl);
        this.ShuffleCommand = ReactiveCommand.Create<string>(this.ShuffleCommandImpl);
    }
}
