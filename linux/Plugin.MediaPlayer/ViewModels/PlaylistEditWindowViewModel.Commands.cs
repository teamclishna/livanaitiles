using ReactiveUI;

using System.Reactive;

namespace LivanaiTiles.MediaPlayer.ViewModels;

public partial class PlaylistEditWindowViewModel {

    /// <summary>
    /// このダイアログを閉じた時に呼び出すコールバック
    /// </summary>
    /// <value>コールバック関数. 引数には「OK」で閉じた場合 true, キャンセルで閉じた場合 false が渡される</value>
    public Action<bool>? DialogResultCallback
    { get; set; }

    public ReactiveCommand<string, Unit> DirectoryDoubleClickCommand { get; private set; }

    public ReactiveCommand<string, Unit> DirectoryAddCommand { get; private set; }
    public ReactiveCommand<string, Unit> FileAddCommand { get; private set; }
    public ReactiveCommand<string, Unit> FileRemoveCommand { get; private set; }
    public ReactiveCommand<string, Unit> PlaylistMoveUpCommand { get; private set; }
    public ReactiveCommand<string, Unit> PlaylistMoveDownCommand { get; private set; }
    
    public ReactiveCommand<string, Unit> PlaylistOpenCommand { get; private set; }
    public ReactiveCommand<string, Unit> PlaylistSaveCommand { get; private set; }
    public ReactiveCommand<string, Unit> PlaylistSaveAsCommand { get; private set; }
    public ReactiveCommand<string, Unit> OKCommand { get; private set; }
    public ReactiveCommand<string, Unit> CancelCommand { get; private set; }
    
}
