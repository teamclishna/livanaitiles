using Avalonia.Controls;

using System.Collections.ObjectModel;

using LivanaiTiles.MediaPlayer.MediaPlayer;

namespace LivanaiTiles.MediaPlayer.ViewModels;

public partial class PlaylistEditWindowViewModel {

    /// <summary>
    /// メディアプレイヤーのプロバイダーオブジェクト
    /// </summary>
    /// <value></value>
    public MediaPlayerProvider Providor { get; set; }

    /// <summary>
    /// この ViewModel をバインディングする設定画面
    /// </summary>
    /// <value></value>
    public Window? ParentDiaog { get; set; }

    /// <summary>
    /// 作成したプレイリスト.
    /// このプロパティは OkCommand の呼び出しによってダイアログを閉じた場合のみ有効である.
    /// </summary>
    /// <value></value>
    public PlayList Playlist
    { get { return this.CreatePlayList(); } }

    /// <summary>
    /// 一覧表示しているディレクトリ
    /// </summary>
    /// <value></value>
    public string CurrentDirectory
    {
        get { return this._currentDir; }
        set
        {
            // このディレクトリにあるサブディレクトリとメディアファイルの一覧を更新する
            this._currentDir = value;
            RefreshSubDirectoriesAndFiles(value);
            OnPropertyChanged(nameof(CurrentDirectory));
        }
    }

    /// <summary>
    /// カレントディレクトリ内のサブディレクトリの一覧.
    /// 実体のディレクトリに加えて .. (上のディレクトリへ) も対象とする
    /// </summary>
    /// <value></value>
    public string[] Directories
    {
        get { return this._directories; }
        set
        {
            this._directories = value;
            OnPropertyChanged(nameof(Directories));
        }
    }

    /// <summary>
    /// カレントディレクトリ内のメディアファイルの一覧
    /// </summary>
    /// <value></value>
    public string[] Files
    {
        get { return this._files; }
        set
        {
            this._files = value;
            OnPropertyChanged(nameof(Files));
        }
    }

    /// <summary>
    /// ディレクトリ一覧の中で現在選択されている項目のインデックス
    /// </summary>
    /// <value></value>
    public int DirectorySelectedIndex
    {
        get { return this._directorySelectedIndex; }
        set
        {
            this._directorySelectedIndex = value;
            OnPropertyChanged(nameof(DirectorySelectedIndex));
        }
    }

    /// <summary>
    /// ファイル一覧の中で現在選択されている項目のインデックス.
    /// </summary>
    /// <value></value>
    public int FileSelectedIndex
    {
        get { return this._fileSelectedIndex; }
        set
        {
            this._fileSelectedIndex = value;
            OnPropertyChanged(nameof(FileSelectedIndex));
        }
    }

    /// <summary>
    /// 既存のプレイリストを編集している場合、そのファイルパス
    /// </summary>
    /// <value></value>
    public FileInfo? PlaylistLocation
    {
        get { return this._playlistLocation; }
        set
        {
            this._playlistLocation = value;
            OnPropertyChanged(nameof(PlaylistLocation));
            OnPropertyChanged(nameof(IsFileOpened));
        }
    }

    public bool IsFileOpened
    { get { return this.PlaylistLocation != null; } }

    /// <summary>
    /// 編集中のプレイリスト名
    /// </summary>
    /// <value></value>
    public string PlaylistName
    {
        get { return this._playlistName; }
        set
        {
            this._playlistName = value;
            OnPropertyChanged(nameof(PlaylistName));
        }
    }

    /// <summary>
    /// 編集中のプレイリストの内容
    /// </summary>
    /// <value></value>
    public ObservableCollection<FileInfo> PlaylistFiles
    {
        get { return this._playlistFiles; }
        set
        {
            this._playlistFiles = value;
            OnPropertyChanged(nameof(PlaylistFiles));
        }
    }

    public int PlaylistSelectedIndex
    {
        get { return this._playlistSelectedIndex; }
        set
        {
            this._playlistSelectedIndex = value;
            OnPropertyChanged(nameof(PlaylistSelectedIndex));
        }
    }

}
