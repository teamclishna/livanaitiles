using Avalonia.Controls;
using ReactiveUI;

using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;
using DynamicData;

using clUtils.gui;
using clUtils.log;

using LivanaiTiles.MediaPlayer.MediaPlayer;
using LivanaiTiles.MediaPlayer.Utils;
using LivanaiTiles.Controllers;
using clUtils.io;
using LivanaiTiles.Utils;

namespace LivanaiTiles.MediaPlayer.ViewModels;

/// <summary>
/// プレイリスト編集ダイアログの ViewModel
/// </summary>
public partial class PlaylistEditWindowViewModel : NotifiableViewModel {

    private ConsoleLogger logger = new ConsoleLogger(){
        OutputLogLevel = new LogLevel[]{
            LogLevel.ERROR,
            LogLevel.WARN,
            LogLevel.INFO,
            LogLevel.DEBUG,
            LogLevel.VERBOSE
        },
    };

    #region Fields

    private string _currentDir;

    private string[] _directories;

    private string[] _files;

    private FileInfo _playlistLocation;

    private string _playlistName;

    private ObservableCollection<FileInfo> _playlistFiles;

    private int _directorySelectedIndex;
    private int _fileSelectedIndex;
    private int _playlistSelectedIndex;

    #endregion


    #region Commands

    /// <summary>
    /// ディレクトリ一覧の項目をダブルクリックして、そのディレクトリに移動するときのコマンド
    /// </summary>
    /// <param name="param"></param>
    public void DirectoryDoubleClickCommandImpl(string param)
    {
        var targetDirName = this.Directories[this.DirectorySelectedIndex];
        System.Diagnostics.Debug.Print($"SelectedIndex = { this.DirectorySelectedIndex }, \"{ targetDirName }\"");

        // 選択したディレクトリと現在のディレクトリを合成して新しいディレクトリパスを生成し
        // サブディレクトリとメディアファイルの一覧を再生成する
        var targetDir = new DirectoryInfo(Path.Combine(new string[]{ this.CurrentDirectory, targetDirName }));
        logger.Print(LogLevel.DEBUG, $"カレントディレクトリ変更: \"{ targetDir.FullName }\"");
        this.CurrentDirectory = targetDir.FullName;
    }

    /// <summary>
    /// 選択したディレクトリとそのサブディレクトリ内のメディアファイルをプレイリストに追加するコマンド
    /// </summary>
    /// <param name="param"></param>
    public void DirectoryAddCommandImpl(string param)
    {
        // このディレクトリ配下にあるメディアファイルを再帰的に取得する
        var targetDirName = this.Directories[this.DirectorySelectedIndex];
        var targetDir = new DirectoryInfo(Path.Combine(new string[]{ this.CurrentDirectory, targetDirName }));

        var mediaFiles = FindMediaFilesRecursive(1, targetDir);
        this.PlaylistFiles.AddRange(mediaFiles);

        // プレイリストの選択項目を末尾の項目へ移動
        if (0 < this.PlaylistFiles.Count)
        { this.PlaylistSelectedIndex = this.PlaylistFiles.Count - 1; }
    }

    /// <summary>
    /// 選択したメディアファイルをプレイリストに追加するコマンド
    /// </summary>
    /// <param name="param"></param>
    public void FileAddCommandImpl(string param)
    {
        var selectedFile = new FileInfo(Path.Combine(this.CurrentDirectory, this.Files[this.FileSelectedIndex]));
        logger.Print(LogLevel.DEBUG, $"Add Playlist: \"{ selectedFile.FullName }\"");
        this.PlaylistFiles.Add(selectedFile);

        // プレイリストの選択項目を末尾の項目へ移動
        this.PlaylistSelectedIndex = this.PlaylistFiles.Count - 1;
    }

    /// <summary>
    /// 選択したプレイリスト内のアイテムをプレイリストから削除するコマンド
    /// </summary>
    /// <param name="param"></param>
    public void FileRemoveCommandImpl(string param)
    {
        this.PlaylistFiles.RemoveAt(this.PlaylistSelectedIndex);
        this.PlaylistSelectedIndex = 
            this.PlaylistFiles.Count == 0 ? -1 :
            this.PlaylistFiles.Count <= this.PlaylistSelectedIndex ? this.PlaylistFiles.Count - 1 :
            this.PlaylistSelectedIndex;
    }

    /// <summary>
    /// プレイリスト中の選択したアイテムをひとつ上に移動するコマンド
    /// </summary>
    /// <param name="param"></param>
    public void PlaylistMoveUpCommandImpl(string param)
    {
        this.PlaylistFiles.Move(this.PlaylistSelectedIndex, this.PlaylistSelectedIndex - 1);
        this.PlaylistSelectedIndex--;
    }

    /// <summary>
    /// プレイリスト中の選択したアイテムをひとつ下に移動するコマンド
    /// </summary>
    /// <param name="param"></param>
    public void PlaylistMoveDownCommandImpl(string param)
    {
        this.PlaylistFiles.Move(this.PlaylistSelectedIndex, this.PlaylistSelectedIndex + 1);
        this.PlaylistSelectedIndex++;
    }

    /// <summary>
    /// プレイリストを開くコマンド
    /// </summary>
    /// <param name="param"></param>
    public async void PlaylistOpenCommandImpl(string param)
    {
        // プレイリストのデフォルトの保存先を取得する
        var confDir = FileUtils.GetProviderConfigDirectory(this.Providor, true);
        var controller = AppControllerProvider.Instance;
        var conf = MediaPlayerConfig.Load(controller, confDir);

        // ファイルを開くダイアログを開く
        var openDlg = new OpenFileDialog()
        {
            Directory = conf.DefaultPlaylistPath,
            Filters = new List<FileDialogFilter>(new FileDialogFilter[]{
                new FileDialogFilter()
                {
                    Name = "MP3 URL List (utf-8) (.m3u8)",
                    Extensions = new List<string>(new string[]{ "m3u8" }),
                }
            }),
            AllowMultiple = false,
        };
        var openFilePath = await openDlg.ShowAsync(this.ParentDiaog);
        if (openFilePath != null && 0 < openFilePath.Length)
        {
            var pl = PlayListReader.Open(openFilePath[0]);
            this.PlaylistFiles.Clear();
            foreach(var media in pl.PlayMediaList)
            { this.PlaylistFiles.Add(media.File); }

            this.PlaylistName = pl.Title;
            this.PlaylistLocation = pl.PlayListFile;
        }
   }

    /// <summary>
    /// プレイリストをファイルに保存するコマンド
    /// </summary>
    /// <param name="param"></param>
    public void PlaylistSaveCommandImpl(string param)
    {
        // プレイリストを上書き保存する
        var savePlayList = CreatePlayList();
        savePlayList.Save(this.PlaylistLocation.FullName);
    }

    /// <summary>
    /// プレイリストをファイルに名前をつけて保存するコマンド
    /// </summary>
    /// <param name="param"></param>
    public async void PlaylistSaveAsCommandImpl(string param)
    {
        // プレイリストのデフォルトの保存先を取得する
        var confDir = FileUtils.GetProviderConfigDirectory(this.Providor, true);
        var controller = AppControllerProvider.Instance;
        var conf = MediaPlayerConfig.Load(controller, confDir);

        // 保存ダイアログを開く
        var saveDlg = new SaveFileDialog()
        {
            Directory = conf.DefaultPlaylistPath,
            Filters = new List<FileDialogFilter>(new FileDialogFilter[]{
                new FileDialogFilter()
                {
                    Name = "MP3 URL List (utf-8) (.m3u8)",
                    Extensions = new List<string>(new string[]{ "m3u8" }),
                }
            }),
            DefaultExtension = "m3u8",
            // デフォルトのファイル名はタイムスタンプを元に生成する
            InitialFileName = $"playlist_{ DateTime.Now.ToString("yyyyMMdd_HHmmss") }.m3u8",
        };
        var saveFilePath = await saveDlg.ShowAsync(this.ParentDiaog);
        if (saveFilePath != null)
        {
            var savePlayList = CreatePlayList();
            savePlayList.Save(saveFilePath);

            // 保存したファイル情報をプロパティにセットする
            this.PlaylistLocation = new FileInfo(saveFilePath);
        }
    }

    public void OKCommandImpl(string param)
    {
        this.ParentDiaog.Close();

        // 選択したファイルを元にプレイリストを生成して返す
        if (this.DialogResultCallback != null)
        { this.DialogResultCallback(true); }
    }

    public void CanceCommandImpl(string param)
    {
        this.ParentDiaog.Close();

        // コールバックを呼び出す
        if (this.DialogResultCallback != null)
        { this.DialogResultCallback(false); }
    }


    #endregion

    /// <summary>
    /// 指定したパスのサブディレクトリ一覧とメディアファイル一覧を取得してプロパティに格納する
    /// </summary>
    /// <param name="path"></param>
    private void RefreshSubDirectoriesAndFiles(string path) {
        logger.PrintPush(LogLevel.DEBUG, $"ディレクトリ移動: { path }");

        try {
            var currentDir =new DirectoryInfo(path);
            if (!currentDir.Exists)
            {
                logger.Print(LogLevel.DEBUG, "このディレクトリは存在しない");

                // このディレクトリが存在しない場合は空にする
                this.Directories = path != "/" ? new string[0] :  new string[]{ ".." };
                this.Files = new string[0];
            }
            else
            {
                logger.PrintPush(LogLevel.DEBUG, "サブディレクトリ列挙");

                // サブディレクトリを取得する
                var subDirsList = new List<string>();
                if (path != "/")
                {
                    logger.Print(LogLevel.DEBUG, "..");
                    subDirsList.Add("..");
                }
                subDirsList.AddRange(currentDir
                    .GetDirectories()
                    .OrderBy(d => d.Name)
                    .Select((d) =>
                    {
                        logger.Print(LogLevel.DEBUG, d.Name);
                        return d.Name;
                    }));

                logger.PrintPop();
                logger.PrintPush(LogLevel.DEBUG, "メディアファイル列挙");

                // メディアファイル一覧を取得する
                var filesList = currentDir
                    .GetFiles()
                    .Where((f) =>
                    {
                        return
                            f.Extension == ".mp3" ||
                            f.Extension == ".wma" ||
                            f.Extension == ".wav";
                    })
                    .OrderBy(f => f.Name)
                    .Select((f) =>
                    {
                        logger.Print(LogLevel.DEBUG, f.Name);
                        return f.Name;
                    });

                logger.PrintPop();

                // プロパティに格納する
                this.Directories = subDirsList.ToArray();
                this.Files = filesList.ToArray();
            }
        }
        catch
        {
            // 何らかの例外がスローされた (例えば権限のないディレクトリを表示しようとした場合など) には
            // 存在しないディレクトリを指定した場合と同様の扱いとする
            this.Directories = path == "/" ? new string[0] :  new string[]{ ".." };
            this.Files = new string[0];
        }

        // 両リストボックスを選択解除状態にする
        this.DirectorySelectedIndex = -1;
        this.FileSelectedIndex = -1;
        logger.PrintPop();
    }

    /// <summary>
    /// 指定したディレクトリ配下にあるメディアファイルを再帰的に検索してリストに格納して返す
    /// </summary>
    /// <param name="recursiveLevel">dir で指定したディレクトリの階層. 最初に呼び出す場合は1を指定する</param>
    /// <param name="dir">探索対象のディレクトリ</param>
    /// <returns></returns>
    private List<FileInfo> FindMediaFilesRecursive(
        int recursiveLevel,
        DirectoryInfo dir)
    {
        logger.PrintPush(LogLevel.DEBUG, $"Find Media files recursive: { dir.FullName } (level = { recursiveLevel })");
        var result = new List<FileInfo>();

        // ディレクトリを再帰的に検索する階層数を取得する
        var confDir = FileUtils.GetProviderConfigDirectory(this.Providor, true);
        var controller = AppControllerProvider.Instance;
        var conf = MediaPlayerConfig.Load(controller, confDir);

        if (recursiveLevel < conf.MaxRecursiveDepth)
        {
            // まだ探索可能な階層が残っている場合はサブディレクトリを再帰的に検索する
            foreach(var child in dir.GetDirectories().OrderBy(d => d.Name))
            { result.AddRange(FindMediaFilesRecursive(recursiveLevel + 1, child)); }
        }

        // このディレクトリ内にあるメディアファイルを検索する
        var filesList = dir
            .GetFiles()
            .Where((f) =>
            {
                return
                    f.Extension == ".mp3" ||
                    f.Extension == ".wma" ||
                    f.Extension == ".wav";
            })
            .OrderBy(f => f.Name);
        result.AddRange(filesList);

        logger.Print(LogLevel.DEBUG, $"find { filesList.Count() } file(s).");
        logger.PrintPop();
        return result;
    }

    /// <summary>
    /// 画面で設定したプレイリストを元にオブジェクトを生成する
    /// </summary>
    /// <returns></returns>
    private PlayList CreatePlayList()
    {
        var logger = AppControllerProvider.Instance.Logger;
        var pl = new PlayList() { Title = this.PlaylistName };
        logger.PrintPush(LogLevel.DEBUG, "Create playlist");
        foreach(var fi in this.PlaylistFiles)
        { pl.PlayMediaList.Add(MediaPlayerUtils.DetectMP3File(fi, logger)); }
        logger.PrintPop();

        return pl;
    }

    /// <summary>l
    /// ViewModel の初期化処理.
    /// コンストラクタ呼び出し後、必要なプロパティを設定した上でこのメソッドを呼び出すこと.
    /// </summary>
    public void Init()
    {
        // プレイリストの編集対象ディレクトリのデフォルトを取得する
        var confDir = FileUtils.GetProviderConfigDirectory(this.Providor, true);
        var controller = AppControllerProvider.Instance;
        var conf = MediaPlayerConfig.Load(controller, confDir);

        this.CurrentDirectory = conf.DefaultMusicPath;
        this.PlaylistLocation = null;
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public PlaylistEditWindowViewModel()
    {
        this.DialogResultCallback = null;
        this.PlaylistFiles = new ObservableCollection<FileInfo>();

        // ディレクトリ一覧は先頭の項目 (..) が選択されている場合も追加ボタンを押せないようにする
        IObservable<bool> IsEnableDirectoryAdd = this.WhenAnyValue(
            x => x.DirectorySelectedIndex,
            x => 0 < x);
        IObservable<bool> IsEnableFileAdd = this.WhenAnyValue(
            x => x.FileSelectedIndex,
            x => 0 <= x);
        IObservable<bool> IsEnablePlaylistRemove = this.WhenAnyValue(
            x => x.PlaylistSelectedIndex,
            x => 0 <= x);
        IObservable<bool> IsEnablePlaylistMoveUp = this.WhenAnyValue(
            x => x.PlaylistSelectedIndex,
            x => 0 < x);
        IObservable<bool> IsEnablePlaylistMoveDown = this.WhenAnyValue(
            x => x.PlaylistSelectedIndex,
            x => x < this.PlaylistFiles.Count - 1);
        IObservable<bool> IsEnablePlaylistOverwrite = this.WhenAnyValue(
            x => x.IsFileOpened);

        this.DirectoryDoubleClickCommand = ReactiveCommand.Create<string>(this.DirectoryDoubleClickCommandImpl);
        this.DirectoryAddCommand = ReactiveCommand.Create<string>(this.DirectoryAddCommandImpl, IsEnableDirectoryAdd);
        this.FileAddCommand = ReactiveCommand.Create<string>(this.FileAddCommandImpl, IsEnableFileAdd);
        this.FileRemoveCommand = ReactiveCommand.Create<string>(this.FileRemoveCommandImpl, IsEnablePlaylistRemove);
        this.PlaylistMoveUpCommand = ReactiveCommand.Create<string>(this.PlaylistMoveUpCommandImpl, IsEnablePlaylistMoveUp);
        this.PlaylistMoveDownCommand = ReactiveCommand.Create<string>(this.PlaylistMoveDownCommandImpl, IsEnablePlaylistMoveDown);

        this.PlaylistOpenCommand = ReactiveCommand.Create<string>(this.PlaylistOpenCommandImpl);
        this.PlaylistSaveCommand = ReactiveCommand.Create<string>(this.PlaylistSaveCommandImpl, IsEnablePlaylistOverwrite);
        this.PlaylistSaveAsCommand = ReactiveCommand.Create<string>(this.PlaylistSaveAsCommandImpl);
        this.OKCommand = ReactiveCommand.Create<string>(this.OKCommandImpl);
        this.CancelCommand = ReactiveCommand.Create<string>(this.CanceCommandImpl);
    }
}
