using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Media;
using Avalonia.Threading;
using System.CodeDom;

using LivanaiTiles.MediaPlayer.ViewModels;
using LivanaiTiles.Utils;

namespace LivanaiTiles.MediaPlayer.Views;

/// <summary>
/// MediaPlayerTile.xaml の相互作用ロジック
/// </summary>
public partial class MediaPlayerTile : UserControl
{
    /// <summary>
    /// 演奏中の曲を操作する簡易コントロールパネルのコントロール名
    /// </summary>
    private static readonly string NAME_CONTROL_PANEL = "MusicControlPanel";

    /// <summary>e
    /// このタイルを生成したプロバイダ
    /// </summary>
    public MediaPlayerProvider ParentProvider { get; set; }

    public MediaPlayerTile()
    {
        InitializeComponent();

        // this.DataContext = new MediaPlayerTileViewModel()
        // {
        //     Logger = AppControllerProvider.Instance.Logger,
        //     CurrentPlayList = new PlayList()
        //     {
        //         Title = "",
        //         PlayListFile = null
        //     }
        // };
    }

    protected override void OnPointerEnter(PointerEventArgs e)
    {
        base.OnPointerEnter(e);
        
        // 簡易操作パネルを表示状態にする
        WindowControlUtils.EnumControl(0, this, NAME_CONTROL_PANEL).IsVisible = true;
    }

    protected override void OnPointerLeave(PointerEventArgs e)
    {
        base.OnPointerLeave(e);

        // 簡易操作パネルを隠す
        WindowControlUtils.EnumControl(0, this, NAME_CONTROL_PANEL).IsVisible = false;
    }

    // private void btnEditPlaylist_Click(object sender, RoutedEventArgs e)
    // {
    // // プレイリスト編集ウィンドウを開く
    // var vm = this.DataContext as MediaPlayerTileViewModel;
    // var dlgVM = new PlayListWindowViewModel();
    // dlgVM.Logger = AppControllerProvider.Instance.Logger;

    // dlgVM.EditPlayList = (
    //     vm == null ? new MediaPlayer.PlayList() :
    //     vm.CurrentPlayList == null ? new MediaPlayer.PlayList() :
    //     vm.CurrentPlayList.Clone() as PlayList
    //     );

    // var wnd = new PlayListWindow() { DataContext = dlgVM, };

    // var dlgResult = wnd.ShowDialog();
    // if(dlgResult.HasValue && dlgResult.Value)
    // {
    //     // 設定ダイアログで取得した内容を反映する
    //     vm.CurrentPlayList = dlgVM.EditPlayList;


    // private void btnEditPlaylist_Click(object sender, RoutedEventArgs e)
    // {
    // // プレイリスト編集ウィンドウを開く
    // var vm = this.DataContext as MediaPlayerTileViewModel;
    // var dlgVM = new PlayListWindowViewModel();
    // dlgVM.Logger = AppControllerProvider.Instance.Logger;

    // dlgVM.EditPlayList = (
    //     vm == null ? new MediaPlayer.PlayList() :
    //     vm.CurrentPlayList == null ? new MediaPlayer.PlayList() :
    //     vm.CurrentPlayList.Clone() as PlayList
    //     );

    // var wnd = new PlayListWindow() { DataContext = dlgVM, };

    // var dlgResult = wnd.ShowDialog();
    // if(dlgResult.HasValue && dlgResult.Value)
    // {
    //     // 設定ダイアログで取得した内容を反映する
    //     vm.CurrentPlayList = dlgVM.EditPlayList;


    // private void btnEditPlaylist_Click(object sender, RoutedEventArgs e)
    // {
    // // プレイリスト編集ウィンドウを開く
    // var vm = this.DataContext as MediaPlayerTileViewModel;
    // var dlgVM = new PlayListWindowViewModel();
    // dlgVM.Logger = AppControllerProvider.Instance.Logger;

    // dlgVM.EditPlayList = (
    //     vm == null ? new MediaPlayer.PlayList() :
    //     vm.CurrentPlayList == null ? new MediaPlayer.PlayList() :
    //     vm.CurrentPlayList.Clone() as PlayList
    //     );

    // var wnd = new PlayListWindow() { DataContext = dlgVM, };

    // var dlgResult = wnd.ShowDialog();
    // if(dlgResult.HasValue && dlgResult.Value)
    // {
    //     // 設定ダイアログで取得した内容を反映する
    //     vm.CurrentPlayList = dlgVM.EditPlayList;


    // private void btnEditPlaylist_Click(object sender, RoutedEventArgs e)
    // {
        // // プレイリスト編集ウィンドウを開く
        // var vm = this.DataContext as MediaPlayerTileViewModel;
        // var dlgVM = new PlayListWindowViewModel();
        // dlgVM.Logger = AppControllerProvider.Instance.Logger;

        // dlgVM.EditPlayList = (
        //     vm == null ? new MediaPlayer.PlayList() :
        //     vm.CurrentPlayList == null ? new MediaPlayer.PlayList() :
        //     vm.CurrentPlayList.Clone() as PlayList
        //     );

        // var wnd = new PlayListWindow() { DataContext = dlgVM, };

        // var dlgResult = wnd.ShowDialog();
        // if(dlgResult.HasValue && dlgResult.Value)
        // {
        //     // 設定ダイアログで取得した内容を反映する
        //     vm.CurrentPlayList = dlgVM.EditPlayList;
        // }
    //}

    // private void UserControl_Unloaded(object sender, RoutedEventArgs e)
    // {
    //     this.ParentProvider.NotifyCloseTile(); 
    // }


}
