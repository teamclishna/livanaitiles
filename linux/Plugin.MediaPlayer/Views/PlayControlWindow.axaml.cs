using Avalonia.Controls;
using Avalonia.Interactivity;

using LivanaiTiles.MediaPlayer.ViewModels;

namespace LivanaiTiles.MediaPlayer.Views;

/// <summary>
/// メディアプレイヤーの操作パネルウィンドウ
/// </summary>
public partial class PlayControlWindow : Window
{
    public PlayControlWindow() {
        InitializeComponent();
    }

    public void OnPlaylistEditClick(object sender, RoutedEventArgs e)
    {
        var dc = this.DataContext as MediaPlayerTileViewModel;
        var parentTile = dc.ParentTile as MediaPlayerTile;
        var vm = new PlaylistEditWindowViewModel(){ Providor = parentTile.ParentProvider };
        vm.Init();
        
        var wnd = new PlaylistEditWindow(){ DataContext = vm, };
        vm.ParentDiaog = wnd;
        vm.DialogResultCallback = (dialogResult) =>
        {
            if (dc != null)
            {
                // 現在曲を演奏中であればこれを停止してから新しいプレイリストをDataContextに渡す
                dc.StopCommand.Execute();
                dc.CurrentPlayList = vm.Playlist;
            }
        };
        wnd.Show(this);
    }
}