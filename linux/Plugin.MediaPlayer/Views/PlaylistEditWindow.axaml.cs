using Avalonia.Controls;
using Avalonia.Interactivity;

using LivanaiTiles.MediaPlayer.ViewModels;

namespace LivanaiTiles.MediaPlayer.Views;

public partial class PlaylistEditWindow : Window
{
    private void OnDirectoryDoubleClick(object sender, RoutedEventArgs e)
    {
        var dc = this.DataContext as PlaylistEditWindowViewModel;
        dc.DirectoryDoubleClickCommandImpl("");
    }

    public PlaylistEditWindow()
    {
        InitializeComponent();
    }
}