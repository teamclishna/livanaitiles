﻿using System.Runtime.Serialization;

namespace LivanaiTiles.Rss;

/// <summary>
/// RSSフィードの設定情報
/// </summary>
[DataContract]
public class FeedConfig
{
    /// <summary>
    /// フィードのタイトル. null を設定した場合はフィードからタイトルを設定する
    /// </summary>
    [DataMember]
    public string Title { get; set; }

    /// <summary>
    /// フィードのURL 
    /// </summary>
    [DataMember]
    public string URL { get; set; }

    /// <summary>
    /// フィードの再取得間隔を分単位で設定する
    /// </summary>
    [DataMember]
    public int FeedInterval { get; set; }

    /// <summary>
    /// フィードの保存数
    /// </summary>
    [DataMember]
    public int FeedCount { get; set; }

    /// <summary>
    /// フィード表示の更新間隔を秒単位で設定する
    /// </summary>
    [DataMember]
    public int FeedRefreshCount { get; set; }
    
    public override string ToString()
    { return System.Text.Json.JsonSerializer.Serialize(this); }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public FeedConfig()
    {
        this.Title = "";
        this.URL = "";
        this.FeedInterval = 60;
        this.FeedCount = 20;
        this.FeedRefreshCount = 10;
    }
}
