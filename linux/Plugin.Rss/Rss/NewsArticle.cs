namespace LivanaiTiles.Rss.Rss;

/// <summary>
/// フィードから取得した記事1件を示すクラス
/// </summary>
public class NewsArticle : ICloneable
{
    /// <summary>
    /// 記事のタイトル
    /// </summary>
    public string Title
    { get; set; }

    /// <summary>
    /// 記事を一意に識別するID
    /// </summary>
    public string ParmaLink
    { get; set; }

    /// <summary>
    /// 記事本文.
    /// ここには取得した全文を格納し、規定文字数へのシュリンクは表示時におこなう.
    /// </summary>
    public string Description
    { get; set; }

    /// <summary>
    /// 記事のリンク先URL
    /// </summary>
    public string LinkUrl
    { get; set; }

    /// <summary>
    /// 記事の公開日
    /// </summary>
    public DateTime PublishDate
    { get; set; }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public NewsArticle()
    { }

    /// <summary>
    /// このオブジェクトの複製を作って返す
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
        return new NewsArticle()
        {
            Title = this.Title,
            Description = this.Description,
            LinkUrl = this.LinkUrl,
            ParmaLink = this.ParmaLink,
            PublishDate = this.PublishDate,
        };
    }
}
