namespace LivanaiTiles.Rss.Rss;

/// <summary>
/// RSSフィードから解析したニュース情報
/// </summary>
public class NewsInfo
{
    /// <summary>
    /// 取得するフィードのタイトル
    /// </summary>
    public string NewsTitle
    { get; set; }

    /// <summary>
    /// 取得するRSSのURL
    /// </summary>
    public string RssURL
    { get; set; }

    /// <summary>
    /// 新着記事を取得する.
    /// 「時」のみを指定する。この配列に含まれる時の毎時5分0秒に取得を行う
    /// </summary>
    public int[] ReloadHours
    { get; set; }

    /// <summary>
    /// 新着記事をバックグラウンドで取得中にtrue
    /// </summary>
    public bool Checking
    { get; set; }

    /// <summary>
    /// このニュースが取得済みの記事を格納したリスト
    /// </summary>
    public List<NewsArticle> Articles
    { get; private set; }

    /// <summary>
    /// 現在コントロールに表示中の記事のインデックス
    /// </summary>
    public int CurrentIndex
    { get; set; }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public NewsInfo()
    {
        this.Articles = new List<NewsArticle>();
        this.CurrentIndex = 0;
    }
}
