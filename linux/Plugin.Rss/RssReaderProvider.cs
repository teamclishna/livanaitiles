﻿using Avalonia.Controls;

using clUtils.util;
using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.Plugins;
using LivanaiTiles.Rss.ViewModels;
using LivanaiTiles.Rss.Views;
using LivanaiTiles.Rss.Utils;
using LivanaiTiles.Utils;

namespace LivanaiTiles.Rss;

/// <summary>
/// RSS フィードを表示するタイル
/// </summary>
public class RssReaderProvider : ITileProvider
{
    #region constants values.

    /// <summary>
    /// このタイルに関する設定を保存するファイル名
    /// </summary>
    private static readonly string CONFIG_FILE_NAME = "feedconfig.conf";

    #endregion

    /// <inheritdoc/>
    public Guid ProviderID => new Guid("{3f5a4746-bdee-484c-8fe9-8a75ccfbb4be}");

    /// <inheritdoc/>
    public string Name => "RSS Feed";

    private ResourceDictionaryWrapper Resources { get; set; }

    /// <summary>
    /// セッション保存時に対象となったタイルのID.
    /// 保存ディレクトリ内で管理対象外のディレクトリを検出するのに使う.
    /// </summary>
    private HashSet<Guid> SavedTileIDList { get; set; }

    /// <inheritdoc/>
    public bool InitProvider(IAppController controller)
    {
        return true; 
    }

    /// <inheritdoc/>
    public TileInfo CreateTile(IAppController controller, string parameter)
    {
        // パラメータ未設定の場合は初期状態
        return CreateRssReaderTile(
            controller,
            (parameter != null ? ParseFeedConfig(parameter) : null));
    }

    /// <inheritdoc/>
    public TileInfo RestoreTile(IAppController controller, Guid windowID)
    {
        var saveDir = FileUtils.GetWindowConfigDirectory(this, windowID, false);
        var feedConf = LoadFeedConfig(controller, saveDir);

        return CreateRssReaderTile(controller, feedConf);
    }

    /// <summary>
    /// 起動パラメータを元にフィード情報を修正
    /// </summary>
    /// <param name="param"></param>
    /// <returns>コマンドラインオプションを元に生成したパラメータ情報. 指定した内容が不正な場合は例外スロー</returns>
    private FeedConfig ParseFeedConfig(string param)
    {
        var argsDef = new CommandLineOptionDefinition[]
        {
            new CommandLineOptionDefinition(){ RequireParam=true, AllowMultiple=false, LongSwitch="name" },
            new CommandLineOptionDefinition(){ RequireParam=true, AllowMultiple=false, LongSwitch="url" },
            new CommandLineOptionDefinition(){ RequireParam=true, AllowMultiple=false, LongSwitch="interval" },
            new CommandLineOptionDefinition(){ RequireParam=true, AllowMultiple=false, LongSwitch="count" },
            new CommandLineOptionDefinition(){ RequireParam=true, AllowMultiple=false, LongSwitch="refreshinterval" },
        };

        var result = CommandLineUtils.Parse(CommandLineUtils.ParseCommandLineArgs(param), argsDef, false);
        if (!result.Options.ContainsKey("name") || result.Options["name"].First().Trim().Length == 0)
        { throw new ArgumentException(this.Resources.ARGS_NAME_IS_EMPTY); }
        if (!result.Options.ContainsKey("url") || result.Options["url"].First().Trim().Length == 0)
        { throw new ArgumentException(this.Resources.ARGS_URL_IS_EMPTY); }

        if (result.Options.ContainsKey("interval"))
        {
            int intVal;
            if (!int.TryParse(result.Options["interval"].First(), out intVal))
            { throw new ArgumentException(this.Resources.ARGS_INTERVAL_INVALID); }

            if (!(new int[] { 30, 60, 180, 360 }.Contains(intVal)))
            { throw new ArgumentException(this.Resources.ARGS_INTERVAL_INVALID); }
        }

        if (result.Options.ContainsKey("count"))
        {
            int intVal;
            if (!int.TryParse(result.Options["count"].First(), out intVal))
            { throw new ArgumentException(this.Resources.ARGS_COUNT_INVALID); }

            if (intVal < 10 || 50 < intVal)
            { }
            else
            { throw new ArgumentException(this.Resources.ARGS_COUNT_INVALID); }
        }

        if (result.Options.ContainsKey("refreshinterval"))
        {
            int intVal;
            if (!int.TryParse(result.Options["refreshinterval"].First(), out intVal))
            { throw new ArgumentException(this.Resources.ARGS_REFRESH_INVALID); }

            if (intVal < 10 || 120 < intVal)
            { }
            else
            { throw new ArgumentException(this.Resources.ARGS_REFRESH_INVALID); }
        }

        return new FeedConfig()
        {
            Title = result.Options["name"].First().Trim(),
            URL = result.Options["url"].First().Trim(),
            FeedInterval = (result.Options.ContainsKey("interval") ? int.Parse(result.Options["interval"].First()) : 60),
            FeedCount = (result.Options.ContainsKey("count") ? int.Parse(result.Options["count"].First()) : 20),
            FeedRefreshCount = (result.Options.ContainsKey("refreshinterval") ? int.Parse(result.Options["refreshinterval"].First()) : 10),
        };
    }

    /// <summary>
    /// タイル情報を生成する
    /// </summary>
    /// <param name="controller">アプリケーションのコントローラ</param>
    /// <param name="feedConf">このタイルで表示するフィード情報. フィード情報未設定で生成する場合はnull</param>
    /// <returns>生成したタイル情報</returns>
    private TileInfo CreateRssReaderTile(
        IAppController controller,
        FeedConfig feedConf)
    {
        var ctl = new RssReaderTile() { provider = this, };

        // フィード情報を取得している場合はこれを設定する
        if (feedConf != null)
        { ctl.SetConfig(feedConf); }

        return new TileInfo()
        {
            UseAeroGlass = null,
            WindowTitle = (feedConf != null ? feedConf.Title : this.Name),
            BackGroundColor = null,
            TileContent = ctl,
            TileHeight = 150,
            TileWidth = 310,
        };
    }

    /// <inheritdoc/>
    public void SaveProviderSession(IAppController controller)
    {
        var logger = AppControllerProvider.Instance.Logger;
        logger.PrintPush(LogLevel.DEBUG, $"[RssReaderProvider] save provider session");

        if (this.SavedTileIDList == null)
        { this.SavedTileIDList = new HashSet<Guid>(); }

        // 保存したタイルID以外の名前を持つディレクトリは削除
        foreach(var child in  FileUtils.GetProviderConfigDirectory(this, true).GetDirectories())
        {
            try
            {
                var dirID = new Guid(child.Name);
                if(!this.SavedTileIDList.Contains(dirID))
                {
                    // 現在存在しているタイルのID以外のディレクトリは削除する
                    logger.PrintPush(LogLevel.DEBUG, $"[RssReaderProvider] remove directory removed tile id [{child.FullName}]");
                    child.Delete(true);
                    logger.PrintPop();
                }
            }
            catch(Exception)
            {
                // GUIDとして認識できないディレクトリ名や削除に失敗したディレクトリは無視
            }
        }
        logger.PrintPop();
    }

    /// <inheritdoc/>
    public void SaveTileSession(
        IAppController controller,
        Guid windowID,
        UserControl tile)
    {
        var logger = AppControllerProvider.Instance.Logger;
        logger.PrintPush(LogLevel.DEBUG, $"[RssReaderProvider] save tile session id {windowID.ToString()}");
        var vm = tile.DataContext as RssReaderTileViewModel;

        // タイルのフィード情報を保存する
        var saveDir = FileUtils.GetWindowConfigDirectory(this, windowID, true);
        SaveFeedConrig(controller, saveDir, vm.Config);

        if(this.SavedTileIDList == null)
        { this.SavedTileIDList = new HashSet<Guid>(); }
        this.SavedTileIDList.Add(windowID);

        logger.PrintPop();
    }

    /// <inheritdoc/>
    public void ShutdownProvider(IAppController controller)
    { }

    /// <inheritdoc/>
    public void ShutdownTile(IAppController controller, Guid windowID, UserControl tile)
    { }

    /// <summary>
    /// フィード情報を読み込む
    /// </summary>
    /// <param name="controller">コントローラオブジェクト</param>
    /// <param name="confDir">保存先のディレクトリ</param>
    /// <returns>ファイルから復元した設定情報. 該当のファイルが存在しない場合はnull</returns>
    private FeedConfig LoadFeedConfig(
        IAppController controller,
        DirectoryInfo confDir)
    {
        var confFilePath = Path.Combine(confDir.FullName, CONFIG_FILE_NAME);
        if ((new FileInfo(confFilePath)).Exists)
        { return FileUtils.LoadFromJson<FeedConfig>(confFilePath, controller.Logger); }
        else
        { return null; }
    }

    /// <summary>
    /// フィード情報を保存する
    /// </summary>
    /// <param name="controller">コントローラオブジェクト</param>
    /// <param name="confDir">保存先のディレクトリ</param>
    /// <param name="conf">保存する設定情報</param>
    private void SaveFeedConrig(
        IAppController controller,
        DirectoryInfo confDir,
        FeedConfig conf)
    {
        var confFilePath = Path.Combine(confDir.FullName, CONFIG_FILE_NAME);
        FileUtils.SaveToJson(conf, confFilePath, controller.Logger);
    }

    public RssReaderProvider() {
        this.SavedTileIDList = new HashSet<Guid>();
        // リソースディクショナリを読み込む
        this.Resources = new ResourceDictionaryWrapper();
    }
}
