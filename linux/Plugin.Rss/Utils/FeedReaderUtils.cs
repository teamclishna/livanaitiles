﻿using System.Net;
using System.Text;

using clUtils.log;
using clUtils.xml;

using LivanaiTiles.Rss.Rss;

namespace LivanaiTiles.Rss.Utils;

/// <summary>
/// RSSフィードの取得に関するユーティリティクラス
/// </summary>
public static class FeedReaderUtils
{
    /// <summary>
    /// 指定したURLからRSSフィードを取得する
    /// </summary>
    /// <param name="url">取得対象のURL</param>
    /// <param name="logger">ロガーオブジェクト</param>
    /// <returns>取得したフィードオブジェクト</returns>
    public static NewsInfo GetFeed(
        string url,
        ILogger logger)
    {
        logger.PrintPush(LogLevel.DEBUG, $"Get RSS feed from {url}");

        try
        {
            var req = WebRequest.Create(url);
            using (var res = req.GetResponse())
            using (var stream = res.GetResponseStream())
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                // RSSフィードを取得する
                string feeds = reader.ReadToEnd();
                reader.Close();
                stream.Close();
                logger.Print(LogLevel.DEBUG, $"Get feed {feeds.Length} characters.");

                // 取得したXML文字列からXMLノードを生成し、記事情報を生成する
                var xmlReader = new XmlReader();
                var articleNodes = xmlReader.LoadFromString(feeds);

                var articles = ParseArticles(articleNodes);

                var result = new NewsInfo()
                {
                    NewsTitle = articleNodes.FindNode(new string[] { "channel" }).GetNodeString("title", ""),
                    RssURL = articleNodes.FindNode(new string[] { "channel" }).GetNodeString("link", ""),
                };
                result.Articles.AddRange(articles);

                return result;
            }
        }
        finally
        { logger.PrintPop(); }
    }

    /// <summary>
    /// XMLノードから記事情報を生成して返す
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    private static List<NewsArticle> ParseArticles(XmlNode node)
    {
        var result = new List<NewsArticle>();
        foreach (var itemNode in node.FindNodes(new string[] { "channel", "item" }))
        {
            var article = ParseArticle(itemNode);
            if (article != null)
            { result.Add(article); }
        }
        return result;
    }

    /// <summary>
    /// XMLノードから1件の記事を生成して返す
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    private static NewsArticle ParseArticle(XmlNode node)
    {
        //記事タイトル、本文、パーマリンク、公開時刻のすべてがそろっている場合のみ記事を生成して返す
        var titleNode = node.FindNode("title");
        var descNode = node.FindNode("description");
        var guidNode = node.FindNode("guid");
        var pubdateNode = node.FindNode("pubDate");
        var linkNode = node.FindNode("link");

        if (titleNode != null &&
            descNode != null &&
            guidNode != null &&
            pubdateNode != null &&
            linkNode != null)
        {
            DateTime pubDate;
            if (!DateTime.TryParse(pubdateNode.Text, out pubDate))
            { pubDate = DateTime.Now; }

            return new NewsArticle()
            {
                Title = titleNode.Text,
                Description = descNode.Text,
                ParmaLink = guidNode.Text,
                LinkUrl = linkNode.Text,
                PublishDate = pubDate,
            };
        }
        else
        { return null; }
    }

    /// <summary>
    /// 指定した現在の記事と新しい記事を合わせ、指定した件数までの記事を新しい順に返す.
    /// </summary>
    /// <param name="currentArticles">取得済みの既存記事</param>
    /// <param name="newArticles">新しく取得した記事</param>
    /// <param name="maxCount">最大件数</param>
    /// <returns>2つの記事のリストを合わせ、新しい順に最大 maxCount の件数まで格納した配列.</returns>
    public static NewsArticle[] MargeArticle(
        IEnumerable< NewsArticle> currentArticles,
        IEnumerable<NewsArticle> newArticles,
        int maxCount)
    {
        var tmpList = new List<NewsArticle>();
        tmpList.AddRange(currentArticles);
        tmpList.AddRange(newArticles);

        return tmpList
            .Distinct(new NewsArticleComparer())
            .OrderBy(x => x.PublishDate)
            .Take(maxCount)
            .ToArray();
    }
}