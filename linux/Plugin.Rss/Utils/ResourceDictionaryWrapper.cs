using Avalonia.Controls;
using LivanaiTiles.Utils;

namespace LivanaiTiles.Rss.Utils;

/// <summary>
/// リソースディクショナリのラッパクラス
/// </summary>
public class ResourceDictionaryWrapper {

    #region constant values.

    private static readonly string RESOURCE_URL = "avares://Plugin.Rss/Assets/strings.axaml";

    #endregion

    #region properties

    private ResourceDictionary? Resources { get; set; }

    #endregion

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public ResourceDictionaryWrapper() {
        this.Resources = Avalonia.Markup.Xaml.AvaloniaXamlLoader.Load(new Uri(RESOURCE_URL)) as ResourceDictionary;
    }

    /// <summary>
    /// エラーメッセージ　フィード名が未指定
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string ARGS_NAME_IS_EMPTY => this.Resources.Get<string>("ARGS_NAME_IS_EMPTY");
    /// <summary>
    /// エラーメッセージ　RSSフィードのURLが未指定
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string ARGS_URL_IS_EMPTY => this.Resources.Get<string>("ARGS_URL_IS_EMPTY");
    /// <summary>
    /// エラーメッセージ　フィード取得数の指定が不正
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string ARGS_COUNT_INVALID => this.Resources.Get<string>("ARGS_COUNT_INVALID");
    /// <summary>
    /// エラーメッセージ　フィードの取得間隔が不正
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string ARGS_INTERVAL_INVALID => this.Resources.Get<string>("ARGS_INTERVAL_INVALID");
    /// <summary>
    /// エラーメッセージ　フィードの表示切り替え間隔が不正
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public string ARGS_REFRESH_INVALID => this.Resources.Get<string>("ARGS_REFRESH_INVALID");
}