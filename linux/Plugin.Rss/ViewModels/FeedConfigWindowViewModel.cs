using Avalonia.Controls;
using ReactiveUI;
using System.Reactive;

using clUtils.gui;

namespace LivanaiTiles.Rss.ViewModels;

/// <summary>
/// 設定ダイアログのViewModel
/// </summary>
public class FeedConfigWindowViewModel : NotifiableViewModel
{
    #region Fields

    private FeedConfig _conf;
    
    #endregion

    #region Properties

    /// <summary>
    /// この ViewModel をバインディングする設定画面
    /// </summary>
    /// <value></value>
    public Window? ParentDiaog { get; set; }

    /// <summary>
    /// この ViewModel で編集対象とする設定オブジェクト
    /// </summary>
    /// <value></value>
    public FeedConfig Config
    {
        get { return this._conf; }
        set
        {
            this._conf = value;
            OnPropertyChanged(nameof(Config));
            OnPropertyChanged(nameof(Title));
            OnPropertyChanged(nameof(URL));
            OnPropertyChanged(nameof(FetchInterval));
            OnPropertyChanged(nameof(IsInterval30Checked));
            OnPropertyChanged(nameof(IsInterval60Checked));
            OnPropertyChanged(nameof(IsInterval180Checked));
            OnPropertyChanged(nameof(IsInterval360Checked));
            OnPropertyChanged(nameof(ChangeDisplayInterval));
            OnPropertyChanged(nameof(FeedCount));
        }
    }

    /// <summary>
    /// フィードのタイトル
    /// </summary>
    public string Title
    {
        get { return this._conf.Title; }
        set
        {
            this._conf.Title = value;
            OnPropertyChanged(nameof(Title));
        }
    }

    /// <summary>
    /// フィード取得先のURL
    /// </summary>
    public string URL
    {
        get { return this._conf.URL; }
        set
        {
            this._conf.URL = value;
            OnPropertyChanged(nameof(URL));
        }
    }

    /// <summary>
    /// フィードの再取得間隔
    /// </summary>
    public int FetchInterval
    {
        get { return this._conf.FeedInterval; }
        set
        {
            this._conf.FeedInterval = value;
            OnPropertyChanged(nameof(FetchInterval));
            OnPropertyChanged(nameof(IsInterval30Checked));
            OnPropertyChanged(nameof(IsInterval60Checked));
            OnPropertyChanged(nameof(IsInterval180Checked));
            OnPropertyChanged(nameof(IsInterval360Checked));
        }
    }

    /// <summary>
    /// フィードの表示を自動更新するまでの間隔
    /// </summary>
    public int ChangeDisplayInterval
    {
        get { return this._conf.FeedRefreshCount; }
        set
        {
            this._conf.FeedRefreshCount = value;
            OnPropertyChanged(nameof(ChangeDisplayInterval));
        }
    }

    /// <summary>
    /// フィードの保存件数
    /// </summary>
    public int FeedCount
    {
        get { return this._conf.FeedCount; }
        set
        {
            this._conf.FeedCount = value;
            OnPropertyChanged(nameof(FeedCount));
        }
    }

    /// <summary>
    /// 更新間隔のラジオボタンがチェックされているかどうか（30分）
    /// </summary>
    public bool IsInterval30Checked
    {
        get { return (this._conf.FeedInterval == 30); }
        set
        {
            if(value)
            {
                this._conf.FeedInterval = 30;
                UpdateIntervalChecked(30);
            }
        }
    }

    /// <summary>
    /// 更新間隔のラジオボタンがチェックされているかどうか（1時間）
    /// </summary>
    public bool IsInterval60Checked
    {
        get { return (this._conf.FeedInterval == 60); }
        set
        {
            if (value)
            {
                this._conf.FeedInterval = 60;
                UpdateIntervalChecked(60);
            }
        }
    }

    /// <summary>
    /// 更新間隔のラジオボタンがチェックされているかどうか（3時間）
    /// </summary>
    public bool IsInterval180Checked
    {
        get { return (this._conf.FeedInterval == 180); }
        set
        {
            if (value)
            {
                this._conf.FeedInterval = 180;
                UpdateIntervalChecked(180);
            }
        }
    }

    /// <summary>
    /// 更新間隔のラジオボタンがチェックされているかどうか（6時間）
    /// </summary>
    public bool IsInterval360Checked
    {
        get { return (this._conf.FeedInterval == 360); }
        set
        {
            if (value)
            {
                this._conf.FeedInterval = 360;
                UpdateIntervalChecked(360);
            }
        }
    }

    /// <summary>
    /// OKボタンを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> OKCommand{ get; private set; }
    /// <summary>
    /// キャンセルボタンを押した時のコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> CancelCommand{ get; private set; }

    #endregion

    #region Commands

    public void OKCommandImpl(string param) {
        if (this.ParentDiaog != null) {
            ParentDiaog.Close(true);
        }
    }

    public void CanceCommandImpl(string param) {
        if (this.ParentDiaog != null) {
            ParentDiaog.Close(false);
        }
    }

    #endregion


    /// <summary>
    /// 更新間隔のラジオボタンがクリックされたときの同期処理
    /// </summary>
    /// <param name="newInterval"></param>
    private void UpdateIntervalChecked(int newInterval)
    {
        this._conf.FeedInterval = newInterval;
        OnPropertyChanged(nameof(FetchInterval));
        OnPropertyChanged(nameof(IsInterval30Checked));
        OnPropertyChanged(nameof(IsInterval60Checked));
        OnPropertyChanged(nameof(IsInterval180Checked));
        OnPropertyChanged(nameof(IsInterval360Checked));
    }

    public FeedConfigWindowViewModel() {
        this.OKCommand = ReactiveCommand.Create<string>(this.OKCommandImpl);
        this.CancelCommand = ReactiveCommand.Create<string>(this.CanceCommandImpl);
    }
}
