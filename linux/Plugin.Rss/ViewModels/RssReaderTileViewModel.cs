using Avalonia.Controls;
using Avalonia.Threading;
using ReactiveUI;
using System.Reactive;

using clUtils.gui;

using LivanaiTiles.Controllers;
using LivanaiTiles.Rss.Rss;
using LivanaiTiles.Rss.Utils;
using LivanaiTiles.Rss.Views;

namespace LivanaiTiles.Rss.ViewModels;

/// <summary>
/// RSSフィード表示のタイルを表示するためのViewModel
/// </summary>
public class RssReaderTileViewModel : NotifiableViewModel, ICloneable
{
    #region Fields

    private FeedConfig _config;
    private string _feedTitle;
    private NewsArticle[] _articles;
    private int _currentArticleIndex;
    
    #endregion

    #region Properties

    /// <summary>
    /// このViewModelをバインドするタイル
    /// </summary>
    /// <value></value>
    public UserControl? ParentTile { get; set; }

    /// <summary>
    /// フィード設定ダイアログを開くコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> ConfigCommand { get; private set; }

    /// <summary>
    /// フィード一覧ウィンドウを開くコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> ShowDescriptionCommand { get; private set; }

    /// <summary>
    /// 記事のURLを規定のアプリケーションで開くコマンド
    /// </summary>
    /// <value></value>
    public ReactiveCommand<string, Unit> OpenBrowserCommand { get; private set; }

    /// <summary>
    /// フィード取得の設定情報
    /// </summary>
    public FeedConfig Config
    {
        get { return this._config; }
        set {
            this._config = value;
            OnPropertyChanged(nameof(TileTitle));
        }
    }

    /// <summary>
    /// タイルに表示するこのフィードのタイトル.
    /// 設定画面で明示的に設定した場合はその内容を表示する.
    /// 未設定の場合は現在取得した記事のタイトルを表示する
    /// </summary>
    public string TileTitle
    {
        get
        {
            return (
                this._config == null ? "" :
                !string.IsNullOrWhiteSpace(this._config.Title) ? this._config.Title :
                this._feedTitle != null ? this._feedTitle :
                "");
        }
    }

    /// <summary>
    /// RSSフィードから取得したフィードのタイトル
    /// </summary>
    public string FeedTitle
    {
        get { return this._feedTitle; }
        set
        {
            this._feedTitle = value;
            OnPropertyChanged(nameof(FeedTitle));
            OnPropertyChanged(nameof(TileTitle));
        }
    }

    /// <summary>
    /// RSSフィードから取得した記事
    /// </summary>
    public NewsArticle[] Articles
    {
        get { return this._articles; }
        set
        {
            this._articles = value;
            this.CurrentArticleIndex = (
                (value != null) && (0 < value.Length) ? (this.CurrentArticleIndex % this._articles.Length) :
                0);
            OnPropertyChanged(nameof(Articles));
            OnPropertyChanged(nameof(CurrentArticleIndex));
            OnPropertyChanged(nameof(CurrentArticle));
        }
    }

    /// <summary>
    /// 表示対象の記事のインデックス.
    /// 取得対象の記事が存在しない場合、記事の件数の範囲外のインデックスを設定した場合は0に設定される.
    /// </summary>
    public int CurrentArticleIndex
    {
        get { return this._currentArticleIndex; }
        set
        {
            if (this._articles == null || this._articles.Length == 0 || value < 0)
            { this._currentArticleIndex = 0; }
            else
            { this._currentArticleIndex = (value % this._articles.Length); }
            OnPropertyChanged(nameof(CurrentArticleIndex));
            OnPropertyChanged(nameof(CurrentArticle));
        }
    }

    /// <summary>
    /// 現在の表示対象の記事.
    /// 記事が未取得の場合、インデックスが不正な値の場合は空の記事を返す
    /// </summary>
    public NewsArticle CurrentArticle
    {
        get
        {
            if (this._articles == null || this._articles.Length <= this._currentArticleIndex)
            {
                return new NewsArticle()
                {
                    Title = "",
                    LinkUrl = "",
                    Description = "",
                    ParmaLink = "",
                    PublishDate = DateTime.MinValue,
                };
            }
            else
            {
                return this._articles[this._currentArticleIndex];
            }
        }
    }

    #endregion

    #region Commands

    /// <summary>
    /// フィード設定ダイアログを開く
    /// </summary>
    /// <param name="param"></param>
    public async void ConfigCommandImpl(string param) {
        var logger = AppControllerProvider.Instance.Logger;

        // 設定画面ダイアログを開く
        var wnd = new FeedConfigWindow();
        var vm = new FeedConfigWindowViewModel(){
            ParentDiaog = wnd,
            Config = (this.Config != null ? this.Config : new FeedConfig()),
        };
        wnd.DataContext = vm;

        // 親ウィンドウが設定されていない場合はダイアログ表示できない
        if (this.ParentTile != null) {
            var result = await wnd.ShowDialog<bool>(Avalonia.VisualTree.VisualExtensions.GetVisualRoot(this.ParentTile) as Window);
            if (result) {
                // 上で設定したDCがここでnullになっているはずはないのだが、警告対応のため
                var dlgDC = wnd.DataContext as FeedConfigWindowViewModel;
                if (dlgDC != null) {
                    logger.Print(clUtils.log.LogLevel.DEBUG, dlgDC.Config.ToString());
                    logger.Print(clUtils.log.LogLevel.DEBUG, "設定を保存します");

                    this.Config = dlgDC.Config;
                    ReloadFeed();
                }
            } else {
                logger.Print(clUtils.log.LogLevel.DEBUG, "設定をキャンセルします");
            }
        }
    }

    /// <summary>
    /// フィード一覧を開く
    /// </summary>
    /// <param name="param"></param>
    /// <returns></returns>
    public void ShowDescriptionCommandImpl(string param) {

        // フィード一覧ウィンドウを開く
        // この ViewModel の複製を生成し、これをウィンドウの DataContext に設定する。
        var wnd = new FeedDescriptionWindow() {
            DataContext = this.Clone()
        };
        wnd.Show();
    }

    /// <summary>
    /// 記事の URL を規定のアプリケーションで開く
    /// </summary>
    /// <param name="param"></param>
    /// <returns></returns>
    public void OpenBrowserCommandImpl(string param) {

        var startInfo = new System.Diagnostics.ProcessStartInfo(this.CurrentArticle.LinkUrl) {
            UseShellExecute = true
        };
        System.Diagnostics.Process.Start(startInfo);
    }

    #endregion

    /// <summary>
    /// 現在の設定を元にRSSフィードの取得を行う
    /// </summary>
    public void ReloadFeed() {
        
        var parentTile = this.ParentTile as RssReaderTile;
        if (parentTile == null) {
            return;
        }
        var dc = parentTile.DataContext as RssReaderTileViewModel;
        if (dc == null) {
            return;
        }

        // フィードの取得は非同期に行う
        new Thread(() => {
            try
            {
                var newArticles = FeedReaderUtils.GetFeed(dc.Config.URL, AppControllerProvider.Instance.Logger);
                var marged = FeedReaderUtils.MargeArticle(dc.Articles, newArticles.Articles, dc.Config.FeedCount);
                dc.FeedTitle = newArticles.NewsTitle;
                dc.Articles = marged;

                // 取得したフィードの内容は UI スレッドで反映させなければならない
                Dispatcher.UIThread.Post(
                    ()=>{
                        // リロード値の内容で ViewModel を更新する
                        parentTile.DataContext = dc;
                        // タイマーの残り時間をリセットする
                        parentTile.SetRemainTimeForReload(dc.Config.FeedInterval);
                        parentTile.SetRemainTimeForNextArticle(dc.Config.FeedRefreshCount);
                    },
                    DispatcherPriority.Background);
            }
            catch (Exception)
            {
                // TODO: エラー処理を入れる
            }
        }).Start();
    }

    /// <summary>
    /// このインスタンスのコピーを生成して返す
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
        return new RssReaderTileViewModel()
        {
            Config = new FeedConfig()
            {
                FeedCount = this.Config.FeedCount,
                FeedInterval = this.Config.FeedInterval,
                FeedRefreshCount = this.Config.FeedRefreshCount,
                Title = this.Config.Title,
                URL = this.Config.URL,
            },
            Articles = this.Articles.Select(a => a.Clone() as NewsArticle).ToArray<NewsArticle>(),
            CurrentArticleIndex = this.CurrentArticleIndex,
        };
    }

    public RssReaderTileViewModel()
    {
        this.ConfigCommand = ReactiveCommand.Create<string>(this.ConfigCommandImpl);
        this.ShowDescriptionCommand = ReactiveCommand.Create<string>(this.ShowDescriptionCommandImpl);
        this.OpenBrowserCommand = ReactiveCommand.Create<string>(this.OpenBrowserCommandImpl);

        this.Articles = new NewsArticle[0];
    }
}
