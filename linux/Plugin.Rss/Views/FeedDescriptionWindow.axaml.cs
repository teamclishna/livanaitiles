using Avalonia.Controls;
using Avalonia.Interactivity;

namespace LivanaiTiles.Rss.Views;

/// <summary>
/// 取得したRSSフィードの記事を表示するウィンドウ
/// </summary>
public partial class FeedDescriptionWindow : Window {
    
    public FeedDescriptionWindow() {
        InitializeComponent();
    }

    /// <summary>
    /// ウィンドウを閉じるボタン
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonOK_Click(object sender, RoutedEventArgs e)
    {
        this.Close();
    }

}