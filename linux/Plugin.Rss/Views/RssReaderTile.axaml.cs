using Avalonia.Controls;
using Avalonia.Threading;

using LivanaiTiles.Rss.Rss;
using LivanaiTiles.Rss.ViewModels;

namespace LivanaiTiles.Rss.Views;

/// <summary>
/// RssReaderTile.xaml の相互作用ロジック
/// </summary>
public partial class RssReaderTile : UserControl
{
    /// <summary>
    /// このタイルを生成したプロバイダ
    /// </summary>
    public RssReaderProvider provider;

    /// <summary>
    /// フィード再取得までの残り秒数
    /// </summary>
    private int _remainTimeReload;

    /// <summary>
    /// 次のフィードを表示するまでの残り秒数
    /// </summary>
    private int _remainTimeViewChange;

    /// <summary>
    /// 一定間隔で表示するフィードを切り替えるためのタイマー
    /// </summary>
    private DispatcherTimer timer;

    /// <summary>
    /// キャスト済みのViewModel.
    /// set はキャスト無しでも出来るのでgetのみ
    /// </summary>
    private RssReaderTileViewModel TypedDataContext
    { get { return this.DataContext as RssReaderTileViewModel; } }

    public RssReaderTile()
    {
        InitializeComponent();

        var dc = new RssReaderTileViewModel()
        {
            ParentTile = this,
            Config = null,
            FeedTitle = "",
            Articles = new NewsArticle[0],
            CurrentArticleIndex = 0,
        };
        this.DataContext = dc;

        this.timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 1000), };
        this.timer.Tick += Timer_Tick;
        this.timer.Start();
    }

    /// <summary>
    /// 設定情報を更新する
    /// </summary>
    /// <param name="newConfig">新しい設定情報</param>
    public void SetConfig(FeedConfig newConfig)
    {
        var dc = this.TypedDataContext;

        var oldURL = (dc.Config != null ? dc.Config.URL : "");
        dc.Config = newConfig;

        // URLが変更された場合は記事を再取得する
        if (oldURL != newConfig.URL)
        { dc.ReloadFeed(); }
    }

    /// <summary>
    /// RSSフィードを再取得するまでの残り時間を再設定する
    /// </summary>
    /// <param name="remainTime">新しく設定する残り時間</param> <summary>
    /// 
    /// </summary>
    public void SetRemainTimeForReload(int remainTime) {
        this._remainTimeReload = remainTime;
    }

    /// <summary>
    /// 次の記事を表示するまでの残り時間を設定する
    /// </summary>
    /// <param name="remainTime"></param>
    public void SetRemainTimeForNextArticle(int remainTime) {
        this._remainTimeViewChange = remainTime;
    }

    /// <summary>
    /// タイマーのコールバック
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Timer_Tick(object sender, EventArgs e)
    {
        var dc = this.TypedDataContext;

        // フィード再取得
        if (dc.Config != null && 0 < dc.Config.FeedInterval)
        {
            this._remainTimeReload--;
            if (this._remainTimeReload == 0)
            { dc.ReloadFeed(); }
        }

        // 次のフィードを表示
        if (dc.Config != null && 0 < dc.Config.FeedRefreshCount)
        {
            this._remainTimeViewChange--;
            if (this._remainTimeViewChange == 0)
            {
                dc.CurrentArticleIndex++;
                this._remainTimeViewChange = dc.Config.FeedRefreshCount;
            }
        }
    }

}
