# Livanai Tiles for Linux

## これは何? (What's this?)

![Tiles Sample](./readme_images/livanaitiles_overview.png)

Windows Phone 7 で導入されたタイル風のデスクトップアプリケーションです。
元は Windows 10 用に制作したものですが、私の普段使いの環境を Linux に移行したのに伴い
Linux デスクトップで使えるように移植しました。
現在、最低限の機能が使える程度に移植したタイルは

  * カレンダー
  * 時計
  * メールチェック (POP3 / IMAP4 対応, SSL非対応)
  * RSS チェッカ
  * メディアプレイヤー (MP3 / WMA 対応)

で、

  * 時刻表表示 (NextTrain 形式)

の移植は未着手です。
また、Windows そのままの移植ではなく、私の環境に合わせて仕様が変わっている箇所も多々あります。

タイルはプラグインとして実装しており、新しい機能を持つタイルを実装することができます。

A tile-like desktop application introduced in Windows Phone 7.
It was originally created for Windows 10, but as I moved my daily environment to Linux,
Ported for use on Linux desktop.
Currently, the following tiles have been ported to the extent that the minimum functionality can be used.

  * Calendar
  * clock
  * Email check (POP3 / IMAP4. SSL not supported)
  * RSS checker
  * Media player (MP3 / WMA)

The following features have not been ported and will not work on Linux.

  * Timetable (NextTrain file format)

The tiles are implemented as plugins and you can implement tiles with new functionality.

## 必要な動作環境 (System Requirements)

* Ubuntu 22.04 またはそれ以降 ※1
* .net 6 またはそれ以降
* LibVLC (apt からインストールする場合は libvlc-dev)
* Ubuntu Desktop 環境 ※2

※1 .net と Avalonia UI が動作する環境であれば他のディストリでも動作するはずですが
両方が公式にサポートしている Ubuntu が無難だと思います。

※2 LXDE 等のデフォルト以外のデスクトップ環境でも動作するかもしれませんが、
AvaloniaUI のシステムトレイの実装は feedesktop.org の仕様に準拠していないようで
Window Maker や fluxbox などのクラシックなウィンドウマネージャでは動作しませんでした。

* Ubuntu 22.04 or later *1
* .net 6 or later
* LibVLC (if you install from apt, install libvlc-dev)
* Ubuntu Desktop Environment *2

*1 I think it will work with other distributions as long as it works with .net and Avalonia UI.
However, it is safe to use Ubuntu, which is officially supported by these two execution environments.

*2 It may also work in desktop environments other than the default, such as LXDE.
However, AvaloniaUI's "system tray" implementation does not comply with the feedesktop.org specifications and seems to depend on the desktop environment library.
It did not work with classic window managers such as Window Maker or fluxbox.

## インストール (How to install)

Linux 版はソースコードでのみ提供するので、ダウンロードしたユーザ側でビルドする必要があります。

The Linux version is only available as source code, so users need to download the source code from the repository and build it.

linux ディレクトリの build.sh で本体と各プラグインのビルドを行い、

When you run build.sh in the linux directory, it compiles the application body and plugins and copies the plugin files to the main body directory.

``LivanaiTiles/bin/debug/net6.0``

ディレクトリに生成されたすべてのファイル、ディレクトリを適当なディレクトリに移動して

Copy all the files and directories in the above directory to another directory and run the following command:

``LC_ALL=C LivanaiTiles &``

で起動すると思います。

The application will then start.

## アンインストール (How to uninstall)

このアプリケーションのアンインストーラはありません。
インストール手順で説明したディレクトリを削除します。

This application does not use an uninstaller.
When deleting an application, delete the files described in the installation procedure.

以下のディレクトリにアプリケーションの設定ファイルが保存されます。
設定を残す必要がないのならこのフォルダとサブフォルダを削除します。

If you do not need to leave the application settings, delete this folder and its subfolders.

``~/.config/TeamClishnA/LivanaiTiles/``

## 使い方 (How to use)

アプリケーションを起動するとタスクトレイにアイコンが追加されます。
アイコンをクリックし、"New Tile" メニューから配置するタイルを選択するとタイルが表示されます。

When you start the application, an icon will be displayed in the task tray.
Click the icon and select the tile you want to place from the "New Tile" menu to display the tile.

![Icon on TaskTray](./readme_images/tasktray.png)
![Context Menu](./readme_images/contextmenu.png)

> *"Launch As..." メニューによる任意パラメータでの起動は移植されていません。*
> 
> *Launching with arbitrary parameters via the "Launch As..." menu has not been ported.*
>
> 「Launch As...」をクリックするとパラメータを指定してタイルを開くことができます。
> 一部のタイルはパラメータを渡して起動することで、初期状態を設定することができます。
>
> ![Launch As Dialog](./readme_images/launch_as.png)

> Windows 版では表示中のタイルを選択して閉じる機能がありましたが移植されていません。
> タイルをクリックしてフォーカスした状態で (外見上はフォーカスしているかどうかを判別できませんが) Alt + F4 を押すと閉じることが出来ます。
>
> The Windows version had the ability to select and close the displayed tile, but this functionality has not yet been ported to the Linux version.
> You can close it by clicking on the tile and pressing Alt + F4 while the tile is in focus (although it is not visually obvious whether it is in focus or not).
>
>  ![Close Tile](./readme_images/contextmenu_close.png)


「Settings...」をクリックするとアプリケーションの設定が表示されます。

*設定画面の項目のうち、タイル位置の自動補正 (TIle Position Adjustment) と
タイルの背景色の透明度 (Alpha) は Linux 版では機能しません。*

Click "Settings..." to display the application settings.

*Among the settings screen items, TIle Position Adjustment and
Tile background color transparency (Alpha) does not work on Linux version. *

![Settings](./readme_images/settings.png)

「Save Tiles Settings」をクリックすると現在開いているタイルの位置と設定を保存します。

「Exit」をクリックするとすべてのタイルを閉じ、アプリケーションを終了します。


Click "Save Tiles Settings" to save the position and settings of the tile that is currently open.

Click “Exit” to close all tiles and exit the application.


## バージョンアップの履歴 (Recent updated)

### ver 1.3.0

  * Linux 環境に移植した
  * Ported to Linux environment
  
    ![Show plugin versions](./readme_images/about.png)
  

## 未実装の機能、既知のバグ (Unimplemented features and known bugs)

時計タイルのタイマー機能、書式設定は移植できていません。

The timer function and formatting of the clock tile cannot be ported.

メディアプレイヤーのタイルは Windows 版の実装が Windows Media Player に依存するものであったものを
VLC Player のライブラリを使うようにしたので完全に別物になりました。

特にプレイリスト関係は対応するプレイリストのファイル形式が XML 形式から
m3u8 もどきの独自形式 (ローカルファイルパスの指定のみ対応) になった他、
画面も Windows 版とは別物なので操作方法などは適当に触ってみて確認してください。

Especially regarding playlists, the corresponding playlist file format has changed from XML format to a unique file format that imitates m3u8.

(Only the playlist title and local file path can be specified for playback.)

The UI for controlling the player is completely different from the Windows version, so please try it out to understand.

メディアプレイヤーで複数の曲を再生する時に2曲目以降の音量が正しく設定されません。
曲の再生が始まってからコントロールウィンドウの音量を設定することで正しい音量に修正することが出来ます。

When playing more than one song in the media player, the volume when starting the second song is incorrect.
You can play at the correct volume by manually adjusting the volume after starting playback.

RSSフィードの配信時刻は取得したXMLから取得していますが、タイムゾーンを考慮していません。

The delivery time of the RSS feed is obtained from the obtained XML, 
but the time zone is not taken into consideration.


## ライセンス (License)

このアプリケーションはシェアウェアです。
アプリケーションを使用するための料金は株式会社ベクター様の「シェアレジ」で支払う事が出来ます。

料金は 500円 です。実際に支払う料金はシェアレジ手数料 (100円) と消費税が加算され、660円です。
料金を支払わないことによる機能の制限はなく、また料金を支払うことによる機能の追加はありません。
また、料金を支払った場合に特に必要な手続きはありません。継続して使用することができます。

Team ClishnA のシェアウェアに関する FAQ は以下の URL を参照してください。

This application is shareware.
The fee for using the application can be paid at "Share Registration" of Vector Co., Ltd.

The fee is 500 JPY. The actual fee to be paid is 660 JPY,
including the share registration fee (100 JPY) and consumption tax.

There are no functional restrictions due to not paying, and no additional features due to paying.
In addition, there is no special procedure required when paying the fee. You can continue to use it.

For FAQs on Team ClishnA shareware, please refer to the following URL.

* [FAQ](https://clishna.info/apps/shareware/)

### 無償バージョンについて (for Use fee free version)

Windows 版の Ver. 1.2.0 未満のバージョンは無償で使用することが出来ましたが、
当然ながら Linux 版はシェアウェア版しかありません。
前述のようにベクター様のシェアレジで料金を支払うか、無償での試用を継続する必要があります。

Windows version versions lower than Ver. 1.2.0 could be used free of charge.
However, all versions of the Linux version are shareware, so you have to pay to use them.

As mentioned in the previous chapter, you need to pay the fee at Vector's share register or continue the free trial.

### ソースコードについて (Source codes)

このアプリケーションはソースコードを公開しています。
このソースコードをダウンロードしてあなたはバグを修正したり自分に必要な機能を追加することができます。

このアプリケーションはオープンソースソフトウェア**ではありません**。

あなたはこのアプリケーションを配布、転載することはできません。
あなたが修正したソースコードを配布する場合、修正箇所のみを配布することができます。

You can use this application for free.

This application has released the source code.
You can download the source code and fix bugs or add functions.

This application is **not** open source software.

You cannot distribute or reprint this application.
When you distribute the source code that you modified,
you can distribute only the modified part.

## 作者 (Author)

Team ClishnA

* [InfoClishnA](https://clishna.info/apps/livanaitiles/)
* [Source code](https://bitbucket.org/teamclishna/livanaitiles/)
