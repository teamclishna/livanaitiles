#!/bin/bash
# 各プロジェクトをビルドしてプラグインのDLLを本体のディレクトリにコピーするためのシェルスクリプト

# スイッチによって通常ビルド、リリースビルドを判定する
if [ $# -lt 1 ]; then
  echo "通常ビルドを行います"
  BUILD_MODE=build
  BUILD_OPTION=""
  DLL_DIR=bin/Debug/net6.0
elif [ ${1} = "build" ]; then
  echo "通常ビルドを行います"
  BUILD_MODE=build
  BUILD_OPTION=""
  DLL_DIR=bin/Debug/net6.0
elif [ ${1} = "publish" ]; then
  echo "リリースビルドを行います"
  BUILD_MODE=publish
  BUILD_OPTION="-c Release"
  DLL_DIR=bin/Release/net6.0/publish
else
  echo "不正な引数が指定されました"
  exit 1
fi

# ビルドを実行する
echo "dotnet ${BUILD_MODE} ${BUILD_OPTION}"
dotnet ${BUILD_MODE} ${BUILD_OPTION}
exitcode=$?

if [ $exitcode -ne 0 ]; then
  echo "ビルド失敗. 処理を中断します."
  exit 1
fi

echo "ライブラリをコピーします..."

libs=(
  Plugin.Rss
  Plugin.Mail
  Plugin.DateTime
  Plugin.MediaPlayer
  )
for i in "${libs[@]}"
do
  echo "  ${i}"
  cp ./${i}/${DLL_DIR}/${i}.dll ./LivanaiTiles/${DLL_DIR}
done

echo "コピー完了"
exit 0
