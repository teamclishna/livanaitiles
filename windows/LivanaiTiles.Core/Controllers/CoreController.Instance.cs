﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Media;

using clUtils.log;

using LivanaiTiles.Plugins;
using LivanaiTiles.Utils;
using LivanaiTiles.Windows;

namespace LivanaiTiles.Controllers
{
    public partial class CoreController : IAppController
    {
        /// <summary>
        /// ウィンドウのリストア情報を保存するファイル名
        /// </summary>
        private static readonly string RESTORE_WINDOWS_FILENAME = "windows.conf";

        /// <summary>
        /// コントローラのロガー.
        /// </summary>
        public ILogger Logger { get; internal set; }
        
        /// <summary>
        /// 使用可能なscreenの座標の範囲情報
        /// </summary>
        public System.Windows.Forms.Screen[] ScreenInfoList { get; private set; }

        /// <summary>
        /// アプリケーションの設定情報
        /// </summary>
        public AppConfig Config { get; private set; }

        /// <summary>
        /// プラグインのリスト
        /// </summary>
        public IEnumerable<PluginInfo> Providers { get; private set; }

        /// <summary>
        /// このコントローラで管理しているウィンドウ情報
        /// </summary>
        private List<ShownWindowInfo> WindowStatus { get; set; }

        /// <summary>
        /// コントローラの初期化処理を行う.
        /// この処理はアプリケーションの起動時に1回だけ呼び出す
        /// </summary>
        private CoreController InitController()
        {
            this.Logger.PrintPush(LogLevel.INFO, "Controller initialize.");

            // ディスプレイ情報を取得して格納しておく
            this.ScreenInfoList = System.Windows.Forms.Screen.AllScreens;

            // アプリケーションの設定ファイルを読み込む
            this.Config = LoadConfig(this.Config);

            // プラグインの走査を行う
            this.Providers = LoadPlugins(GetEntryPointDirectory());

            this.Logger.PrintPop();
            return this;
        }

        /// <summary>
        /// コントローラのシャットダウン処理を行う.
        /// この処理はアプリケーションの終了時に1回だけ呼び出す
        /// </summary>
        private void ShutdownController()
        {
            this.Logger.PrintPush(LogLevel.INFO, "Controller shutdown.");

            ShutdownAllWindows();
            ShutdownAllPlugins();

            this.Logger.PrintPop();
        }

        /// <inheritdoc/>
        public void SaveSession()
        {
            this.Logger.PrintPush(LogLevel.INFO, "Save session.");

            SaveWindowPositions();
            SaveWindowSessions();
            SavePluginSessions();

            this.Logger.PrintPop();
        }

        /// <inheritdoc/>
        public void RestoreSession()
        {
            this.Logger.PrintPush(LogLevel.INFO, "Restore session.");

            RestoreWindows();

            this.Logger.PrintPop();
        }

        /// <inheritdoc/>
        public void RestartWindows()
        {
            ShutdownAllWindows();
            RestoreWindows();
        }

        /// <summary>
        /// 新規タイルウィンドウを作成する.
        /// </summary>
        /// <param name="pluginID">プラグインの識別子</param>
        /// <returns>
        /// 新しく生成したウィンドウ.
        /// </returns>
        public TiledWindow CreateWindow(Guid pluginID)
        { return CreateWindow(pluginID, Guid.NewGuid(), null); }

        /// <summary>
        /// 新規タイルウィンドウを作成する
        /// </summary>
        /// <param name="pluginID">プラグインの識別子</param>
        /// <param name="guid">ウィンドウに割り当てるGUID</param>
        /// <returns>
        /// 新しく生成したウィンドウ.
        /// </returns>
        public TiledWindow CreateWindow(Guid pluginID, Guid guid)
        { return CreateWindow(pluginID, guid, null); }

        /// <summary>
        /// 新規タイルウィンドウを作成する
        /// </summary>
        /// <param name="pluginID">プラグインの識別子</param>
        /// <param name="arg">プラグインに渡すパラメータ文字列</param>
        /// <returns>
        /// 新しく生成したウィンドウ.
        /// </returns>
        public TiledWindow CreateWindow(Guid pluginID, string arg)
        { return CreateWindow(pluginID, Guid.NewGuid(), arg); }

        /// <summary>
        /// 新規タイルウィンドウを作成する
        /// </summary>
        /// <param name="pluginID">プラグインの識別子</param>
        /// <param name="guid">ウィンドウに割り当てるGUID</param>
        /// <param name="arg">プラグインに渡すパラメータ文字列</param>
        /// <returns>
        /// 新しく生成したウィンドウ.
        /// </returns>
        public TiledWindow CreateWindow(
            Guid pluginID,
            Guid guid,
            string arg)
        {
            this.Logger.PrintPush(LogLevel.INFO, $"create new window ID [{guid.ToString()}] by plugin [{pluginID.ToString()}]");

            var prov = Providers.FirstOrDefault(x => x.PluginID.Equals(pluginID));
            if (prov == null)
            { return null; }

            var tileInfo = prov.Provider.CreateTile(this, arg);
            var wnd = CreateBaseWindow(guid, tileInfo);
            RegisterWindow(pluginID, wnd);

            this.Logger.PrintPop();
            return wnd;
        }

        /// <summary>
        /// 指定したIDでタイルウィンドウの復元を要求する.
        /// </summary>
        /// <returns></returns>
        public TiledWindow RestoreWindow(
            Guid pluginID,
            Guid guid)
        {
            this.Logger.PrintPush(LogLevel.INFO, $"restore window ID [{guid.ToString()}] by plugin [{pluginID.ToString()}]");

            var prov = Providers.FirstOrDefault(x => x.PluginID.Equals(pluginID));
            if (prov == null)
            { return null; }

            var tileInfo = prov.Provider.RestoreTile(this, guid);
            var wnd = CreateBaseWindow(guid, tileInfo);
            RegisterWindow(pluginID, wnd);

            this.Logger.PrintPop();
            return wnd;
        }

        /// <summary>
        /// 現在開いているタイルウィンドウの一覧を取得する
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TiledWindow> GetAllWindowList()
        { return this.WindowStatus.Select(x => x.Wnd); }

        /// <summary>
        /// 保存したセッション情報を元にウィンドウをリストアする
        /// </summary>
        private void RestoreWindows()
        {
            this.Logger.PrintPush(LogLevel.INFO, "Restore windows");

            try
            {
                var restoreInfo = LoadWindowPositions();
                foreach (var wndInfo in restoreInfo.WindowList)
                {
                    this.Logger.PrintPush(LogLevel.DEBUG, $"Restore [{wndInfo.WindowID}] by [{wndInfo.PluginID}] in position ({wndInfo.X}, {wndInfo.Y})");
                    try
                    {
                        var restored = RestoreWindow(wndInfo.PluginID, wndInfo.WindowID);
                        restored.Left = wndInfo.X;
                        restored.Top = wndInfo.Y;
                    }
                    catch (Exception eUnknown)
                    {
                        this.Logger.Print(LogLevel.WARN, $"Window [{wndInfo.WindowID}] restore failed: {eUnknown}");
                    }
                    this.Logger.PrintPop();
                }
            }
            catch(FileNotFoundException)
            {
                // ウィンドウ情報のファイルが存在しない場合は何もしないで終了
                this.Logger.Print(LogLevel.INFO, "Windows restore info not found.");
            }

            this.Logger.PrintPop();
        }

        /// <summary>
        /// プラグインをシャットダウンする
        /// </summary>
        private void ShutdownAllPlugins()
        {
            this.Logger.PrintPush(LogLevel.INFO, $"Shutdown plugins.");

            foreach(var plugin in this.Providers)
            {
                this.Logger.PrintPush(LogLevel.DEBUG, $"Shutdown plugin [{plugin.PluginName}] [{plugin.PluginID}].");
                plugin.Provider.ShutdownProvider(this);
                this.Logger.PrintPop();
            }

            this.Logger.PrintPop();
        }

        /// <summary>
        /// 現在開いているタイルウィンドウをすべて閉じる
        /// </summary>
        private void ShutdownAllWindows()
        {
            this.Logger.PrintPush(LogLevel.INFO, $"Shutdown Tiles.");

            // 現在管理しているウィンドウをすべて取得し、順にCloseさせる
            // ウィンドウが閉じるときに WindowStatus の削除要求が来るので
            // 一旦別のリストに退避させる
            var wndList = this.WindowStatus.ToArray();
            foreach (var wndInfo in wndList)
            {
                this.Logger.PrintPush(LogLevel.DEBUG, $"Shutdown Window [{wndInfo.WindowID}] owned by plugin [{wndInfo.PluginID}].");

                var plugin = this.Providers
                    .Where(x => (x.PluginID == wndInfo.PluginID))
                    .First()
                    .Provider;

                plugin.ShutdownTile(
                    this,
                    wndInfo.WindowID,
                    wndInfo.Wnd.GetTileContent());

                wndInfo.Wnd.Close();

                this.Logger.PrintPop();
            }
            this.Logger.PrintPop();
        }

        private WindowRestoreInfoList LoadWindowPositions()
        {
            this.Logger.PrintPush(LogLevel.INFO, $"Restore tile positions.");

            var configFilePath = Path.Combine(
                FileUtils.GetConfigDirectory(true).FullName,
                RESTORE_WINDOWS_FILENAME);
            var result = FileUtils.LoadFromJson<WindowRestoreInfoList>(configFilePath, this.Logger);

            this.Logger.PrintPop();
            return result;
        }

        /// <summary>
        /// 現在開いているタイルの情報を保存する.
        /// この情報を元に次回起動時にウィンドウの復元を要求する
        /// </summary>
        private void SaveWindowPositions()
        {
            this.Logger.PrintPush(LogLevel.INFO, $"Save tile positions.");

            var restoreList = CreateWindowPositionList(this.WindowStatus);
            var configFilePath = Path.Combine(
                FileUtils.GetConfigDirectory(true).FullName,
                RESTORE_WINDOWS_FILENAME);
            FileUtils.SaveToJson(restoreList, configFilePath, this.Logger);

            this.Logger.PrintPop();
        }

        /// <summary>
        /// ウィンドウの復元情報を生成して返す
        /// </summary>
        /// <param name="windowList">ウィンドウの情報</param>
        /// <returns></returns>
        private WindowRestoreInfoList CreateWindowPositionList(IEnumerable<ShownWindowInfo> windowList)
        {
            var restoreInfoList = windowList.Select((x) => {
                return new WindowRestoreInfo()
                {
                    PluginID = x.PluginID,
                    WindowID = x.WindowID,
                    X = x.Wnd.Left,
                    Y = x.Wnd.Top,
                };
            });

            return new WindowRestoreInfoList() { WindowList = restoreInfoList.ToArray(), };
        }

        /// <summary>
        /// プラグインのセッション情報を保存する.
        /// </summary>
        private void SavePluginSessions()
        {
            this.Logger.PrintPush(LogLevel.INFO, $"Save plugin sessions.");

            foreach (var plugin in this.Providers)
            {
                this.Logger.PrintPush(LogLevel.DEBUG, $"Save session plugin [{plugin.PluginName}] [{plugin.PluginID}].");
                plugin.Provider.SaveProviderSession(this);
                this.Logger.PrintPop();
            }

            this.Logger.PrintPop();
        }

        /// <summary>
        /// タイルのセッション情報を保存する.
        /// </summary>
        private void SaveWindowSessions()
        {
            this.Logger.PrintPush(LogLevel.INFO, $"Save tile sessions.");

            // 現在管理しているウィンドウ情報の保存を各プラグインに要求する
            foreach (var wndInfo in this.WindowStatus)
            {
                this.Logger.PrintPush(LogLevel.DEBUG, $"Save session [{wndInfo.WindowID}] owned by plugin [{wndInfo.PluginID}].");

                var plugin = this.Providers
                    .Where(x => (x.PluginID == wndInfo.PluginID))
                    .First()
                    .Provider;

                plugin.SaveTileSession(this, wndInfo.WindowID, wndInfo.Wnd.GetTileContent());

                this.Logger.PrintPop();
            }

            // ウィンドウ
            this.Logger.PrintPop();
        }

        /// <summary>
        /// ウィンドウ情報を登録する
        /// </summary>
        /// <param name="pluginID">このウィンドウを使用するプラグインのID</param>
        /// <param name="wnd">生成したタイルウィンドウ</param>
        private void RegisterWindow(
            Guid pluginID,
            TiledWindow wnd)
        {
            this.Logger.PrintPush(LogLevel.INFO, $"register window ID [{wnd.WindowID}] owned by [{pluginID.ToString()}]");
            this.WindowStatus.Add(new ShownWindowInfo() { PluginID = pluginID, WindowID = wnd.WindowID, Wnd = wnd, });
            this.Logger.PrintPop();
        }

        /// <summary>
        /// ウィンドウ情報を削除する
        /// </summary>
        /// <param name="guid">削除対象のウィンドウID</param>
        /// <returns>削除に成功したらtrue</returns>
        public bool DeleteWindow(Guid guid)
        {
            this.Logger.PrintPush(LogLevel.INFO, $"remove window ID [{guid.ToString()}]");

            var removed = this.WindowStatus.RemoveAll(x => x.WindowID == guid);

            this.Logger.PrintPop();
            return (0 < removed);
        }

        /// <summary>
        ///  タイルウィンドウを生成する
        /// </summary>
        /// <param name="windowID">生成したウィンドウの識別子</param>
        /// <param name="info">プラグインから返されたタイルのコントロール情報</param>
        /// <returns>生成したウィンドウ情報</returns>
        private TiledWindow CreateBaseWindow(
            Guid windowID,
            TileInfo info)
        {
            var useDefaultBackGround = false;
            if (info.BackGroundColor == null)
            { useDefaultBackGround = true; }

            var wnd = new TiledWindow()
            {
                WindowID = windowID,
                Background = (useDefaultBackGround ? new SolidColorBrush(Config.BackgroundColor) : info.BackGroundColor),
                Height = info.TileHeight,
                Width = info.TileWidth,
            };

            var sp = wnd.FindName("contentPanel") as System.Windows.Controls.Grid;
            sp.Children.Add(info.TileContent);

            // ウィンドウのタイトルを設定
            if (info.WindowTitle != null)
            { wnd.Title = info.WindowTitle; }

            // タイル効果を適用する?
            // タイル効果はウィンドウを表示してからでないと反映されないようなのでこの時点でウィンドウを表示する
            wnd.Show();
            var effectType = (
                this.Config.AeroglassEffectType != AppConfig.AeroGlassType.UNDEFINED ? this.Config.AeroglassEffectType :
                this.Config.UseAeroGlass ? AppConfig.AeroGlassType.BLUR_BEHIND :
                AppConfig.AeroGlassType.NONE
                );
            if (info.UseAeroGlass.HasValue ? info.UseAeroGlass.Value : (effectType != AppConfig.AeroGlassType.NONE))
            { WindowControlUtils.EnableBlur(wnd, effectType); }

            return wnd;
        }
        /// <summary>
        /// アプリケーションのデフォルト設定値を生成して返す
        /// </summary>
        /// <returns>デフォルトの設定値</returns>
        private AppConfig CreateDefaultConfig()
        {
            return new AppConfig()
            {
                UseAeroGlass = true,
                ForeGroundColor = Colors.White,
                BackgroundColor = Color.FromArgb(0x80, 0x00, 0x00, 0x00),
                UseAutoPosition = true,
                TileMargin = 10,
            };
        }

        /// <summary>
        /// アプリケーションの設定を読み込み、引数で渡したデフォルト設定に上書きして返す
        /// </summary>
        /// <param name="previousConfig">デフォルト設定. ファイルから読み込めなかった項目はここで渡した内容を格納して返す.</param>
        /// <returns>ロードした結果を格納したオブジェクト</returns>
        private AppConfig LoadConfig(AppConfig previousConfig)
        {
            try
            {
                var prevConfig = previousConfig.Clone();
                var loaded = FileUtils.LoadFromJson<AppConfig>(
                    AppConfig.FilePath,
                    this.Logger);

                return loaded;
            }
            catch (Exception)
            {
                // ロードに失敗した場合は最初の値をそのまま返す.
                return previousConfig.Clone(); 
            }
        }

        /// <summary>
        /// 設定ファイルを読み直す
        /// </summary>
        public void ReloadConfig()
        {
            this.Config = LoadConfig(CreateDefaultConfig());
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CoreController()
        {
            this.Logger = LoggerFactory.CreateLogger(LoggerName.NullLogger);
            this.WindowStatus = new List<ShownWindowInfo>();

            // デフォルトの設定値をここで生成する
            this.Config = CreateDefaultConfig();
        }
    }
}
