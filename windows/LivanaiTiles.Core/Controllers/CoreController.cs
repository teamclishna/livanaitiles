﻿using System;
using System.Collections.Generic;
using System.Text;

using clUtils.log;

namespace LivanaiTiles.Controllers
{
    /// <summary>
    /// アプリケーションのコントローラクラス
    /// </summary>
    public partial class CoreController : IAppController
    {
        /// <inheritdoc/>
        public IAppController Init(ILogger logger)
        {
            this.InitController();
            return this;
        }

        /// <inheritdoc/>
        public void Shutdown()
        {
            this.ShutdownController();
        }
    }
}
