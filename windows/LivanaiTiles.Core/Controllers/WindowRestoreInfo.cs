﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace LivanaiTiles.Controllers
{
    /// <summary>
    /// 復元するウィンドウの情報
    /// </summary>
    [DataContract]
    internal class WindowRestoreInfo
    {
        /// <summary>
        /// ウィンドウを復元するプラグインのID
        /// </summary>
        [DataMember]
        public Guid PluginID { get; set; }

        /// <summary>
        /// ウィンドウのID
        /// </summary>
        [DataMember]
        public Guid WindowID { get; set; }

        /// <summary>
        /// ウィンドウ位置 （X座標）
        /// </summary>
        [DataMember]
        public double X { get; set; }

        /// <summary>
        /// ウィンドウ位置 （Y座標）
        /// </summary>
        [DataMember]
        public double Y { get; set; }
    }

    /// <summary>
    /// アプリケーション起動時にウィンドウを復元するための情報
    /// </summary>
    [DataContract]
    internal class WindowRestoreInfoList
    {
        [DataMember]
        public WindowRestoreInfo[] WindowList { get; set; }
    }
}
