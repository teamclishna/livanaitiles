﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LivanaiTiles.Plugins
{
    /// <summary>
    /// 同一種類のタイルの複数個生成を認めない場合にスローする例外
    /// </summary>
    public class NotAllowMultipleTileException : Exception
    {
    }
}
