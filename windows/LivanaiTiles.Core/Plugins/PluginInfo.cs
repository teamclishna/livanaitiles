﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LivanaiTiles.Plugins
{
    /// <summary>
    /// プラグイン情報
    /// </summary>
    public class PluginInfo
    {
        /// <summary>
        /// プラグインの識別子
        /// </summary>
        public Guid PluginID { get; set; }

        /// <summary>
        /// プラグイン名
        /// </summary>
        public string PluginName { get; set; }

        /// <summary>
        /// プラグインの実装されているDLLファイル名
        /// </summary>
        public FileInfo PluginLocation { get; set; }

        /// <summary>
        /// プラグインの実装クラス
        /// </summary>
        public ITileProvider Provider { get; set; }
    }
}
