﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.Pkcs;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;

namespace LivanaiTiles.Plugins
{
    /// <summary>
    /// コントローラからタイルの生成を要求された時にプラグインからコントローラに返すタイル情報
    /// </summary>
    public class TileInfo
    {
        /// <summary>
        /// タイルに格納するUserControl
        /// </summary>
        public UserControl TileContent { get; set; }

        /// <summary>
        /// タイルの幅
        /// </summary>
        public double TileWidth { get; set; }

        /// <summary>
        /// タイルの高さ
        /// </summary>
        public double TileHeight { get; set; }

        /// <summary>
        /// タイルウィンドウのタイトル
        /// 設定が不要である場合はnullを設定する.
        /// </summary>
        public string WindowTitle { get; set; }

        /// <summary>
        /// タイルの背景色をプラグイン側で指定する場合、背景色.
        /// アプリケーションのデフォルトに任せる場合は null を指定する.
        /// </summary>
        public Brush BackGroundColor { get; set; }

        /// <summary>
        /// タイルのグラス効果を有効にする場合true.
        /// アプリケーションのデフォルトに任せる場合は Transparent を指定する.
        /// </summary>
        public bool? UseAeroGlass { get; set; }
        
    }
}
