﻿using System;
using System.IO;
using System.Text;
using System.Text.Json;

using clUtils.log;
using LivanaiTiles.Plugins;

namespace LivanaiTiles.Utils
{
    /// <summary>
    /// アプリケーションが使用するファイル I/O で使用するユーティリティクラス
    /// </summary>
    public class FileUtils
    {
        /// <summary>
        /// ユーザ固有のアプリケーションディレクトリを起点とした
        /// このアプリケーションの設定情報を保存するディレクトリの相対パス
        /// </summary>
        private static readonly string CONF_PATH = "./TeamClishnA/LivanaiTiles/Config";

        /// <summary>
        /// アプリケーションが設定ファイルを保存するためのディレクトリを返す.
        /// </summary>
        /// <param name="createIfNotExists">このディレクトリが存在しない場合、このメソッド呼び出し時点でディレクトリを作成する場合は true</param>
        /// <returns>ディレクトリの場所を示す DirectoryInfo</returns>
        public static DirectoryInfo GetConfigDirectory(bool createIfNotExists)
        {
            var configDir = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                CONF_PATH);
            var confDirInfo = new DirectoryInfo(configDir);

            if (!confDirInfo.Exists && createIfNotExists)
            { confDirInfo.Create(); }

            return confDirInfo;
        }

        /// <summary>
        /// 指定したプロバイダが固有の設定情報を保存するためのディレクトリを返す.
        /// </summary>
        /// <param name="provider">プロバイダ</param>
        /// <param name="createIfNotExists">このディレクトリが存在しない場合、このメソッド呼び出し時点でディレクトリを作成する場合は true</param>
        /// <returns>ディレクトリの場所を示す DirectoryInfo</returns>
        public static DirectoryInfo GetProviderConfigDirectory(
            ITileProvider provider,
            bool createIfNotExists)
        {
            var providerDir = Path.Combine(
                GetConfigDirectory(createIfNotExists).FullName,
                provider.ProviderID.ToString("D"));
            var providerDirInfo = new DirectoryInfo(providerDir);

            if (!providerDirInfo.Exists && createIfNotExists)
            { providerDirInfo.Create(); }

            return providerDirInfo;
        }

        /// <summary>
        /// 指定したウィンドウが固有のデータを保存するためのディレクトリを返す
        /// </summary>
        /// <param name="provider">プロバイダ</param>
        /// <param name="wndId">ウィンドウの固有ID</param>
        /// <param name="createIfNotExists">このディレクトリが存在しない場合、このメソッド呼び出し時点でディレクトリを作成する場合は true</param>
        /// <returns>ディレクトリの場所を示す DirectoryInfo</returns>
        public static DirectoryInfo GetWindowConfigDirectory(
            ITileProvider provider,
            Guid wndId,
            bool createIfNotExists)
        {
            var wndDir = Path.Combine(
                GetProviderConfigDirectory(provider, createIfNotExists).FullName,
                wndId.ToString("D"));
            var wndDirInfo = new DirectoryInfo(wndDir);

            if (!wndDirInfo.Exists && createIfNotExists)
            { wndDirInfo.Create(); }

            return wndDirInfo;
        }

        /// <summary>
        /// 指定したオブジェクトをJSON形式で指定したパスに書き込む
        /// </summary>
        /// <param name="o">書き込み対象のオブジェクト</param>
        /// <param name="path">書き込み先のフルパス</param>
        /// <param name="logger">ロガーオブジェクト</param>
        public static void SaveToJson(
            object o,
            string path,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.DEBUG, $"write json file {path}");

            using (var writer = new BinaryWriter(new FileStream(path, FileMode.Create)))
            {
                var jsonBytes = JsonSerializer.SerializeToUtf8Bytes(o, new JsonSerializerOptions() { WriteIndented = true, });
                var jsonString = (new UTF8Encoding(false)).GetString(jsonBytes);
                logger.Print(LogLevel.DEBUG, jsonString);
                writer.Write(jsonBytes);
            }

            logger.PrintPop();
        }

        /// <summary>
        /// 指定したjsonファイルを読み込み、オブジェクトに変換して返す
        /// </summary>
        /// <typeparam name="T">返すオブジェクトの型</typeparam>
        /// <param name="path"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static T LoadFromJson<T>(string path, ILogger logger)
        {
            logger.PrintPush(LogLevel.DEBUG, $"read json file {path}");
            try
            {
                using (var reader = new BinaryReader(new FileStream(path, FileMode.Open)))
                {
                    var fi = new FileInfo(path);
                    var bytes = reader.ReadBytes((int)fi.Length);
                    var jsonReader = new Utf8JsonReader(bytes);
                    object result = JsonSerializer.Deserialize<T>(ref jsonReader);

                    logger.PrintPop();
                    return (T)result;
                }
            }
            catch(Exception eUnknown)
            {
                logger.PrintPop();
                throw eUnknown;
            }
        }
    }
}
