﻿using LivanaiTiles.Controllers;
using LivanaiTiles.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;

namespace LivanaiTiles.Utils
{
    /// <summary>
    /// ウィンドウ操作に関するユーティリティクラス
    /// </summary>

    public static class WindowControlUtils
    {
        /// <summary>マウスオーバー時の表示／非表示操作の対象にするコントロール名</summary>
        public static readonly string NANE_SETTINGS_PANEL = "SettingPanel";

        internal enum AccentState
        {
            ACCENT_DISABLED = 1,
            ACCENT_ENABLE_GRADIENT = 0,
            ACCENT_ENABLE_TRANSPARENTGRADIENT = 2,
            ACCENT_ENABLE_BLURBEHIND = 3,
            ACCENT_ENABLE_ACRYLICBLURBEHIND = 4,
            ACCENT_ENABLE_HOSTBACKDROP = 5,
            ACCENT_INVALID_STATE = 6,
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct AccentPolicy
        {
            public AccentState AccentState;
            public int AccentFlags;
            public uint GradientColor;
            public int AnimationId;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct WindowCompositionAttributeData
        {
            public WindowCompositionAttribute Attribute;
            public IntPtr Data;
            public int SizeOfData;
        }

        internal enum WindowCompositionAttribute
        {
            WCA_ACCENT_POLICY = 19
        }

        [DllImport("user32.dll")]
        internal static extern int SetWindowCompositionAttribute(IntPtr hwnd, ref WindowCompositionAttributeData data);

        /// <summary>
        /// ウィンドウのガラス効果を有効にする
        /// </summary>
        /// <param name="wnd">エフェクト対象のウィンドウ</param>
        /// <param name="aeroglassType">エフェクトの種類</param>
        public static void EnableBlur(
            Window wnd,
            AppConfig.AeroGlassType aeroglassType)
        {
            var windowHelper = new WindowInteropHelper(wnd);
            var accent = new AccentPolicy();

            switch (aeroglassType)
            {
                case AppConfig.AeroGlassType.BLUR_BEHIND:
                    // WDM のブラー効果
                    accent.AccentState = AccentState.ACCENT_ENABLE_BLURBEHIND;
                    accent.AccentFlags = 0;
                    accent.AnimationId = 2;
                    break;
                case AppConfig.AeroGlassType.ACRYLIC:
                    // アクリル効果
                    accent.AccentState = AccentState.ACCENT_ENABLE_ACRYLICBLURBEHIND;
                    accent.GradientColor = 0xffffff;
                    break;
                default:
                    // 効果なし
                    return;
            }

            var accentStructSize = Marshal.SizeOf(accent);
            var accentPtr = Marshal.AllocHGlobal(accentStructSize);
            Marshal.StructureToPtr(accent, accentPtr, false);

            var data = new WindowCompositionAttributeData();
            data.Attribute = WindowCompositionAttribute.WCA_ACCENT_POLICY;
            data.SizeOfData = accentStructSize;
            data.Data = accentPtr;

            SetWindowCompositionAttribute(
                windowHelper.Handle,
                ref data);
            Marshal.FreeHGlobal(accentPtr);
        }

        /// <summary>
        /// タイル状に配置する指定したコントロールに対して前景色の設定を適用する
        /// </summary>
        /// <param name="controls"></param>
        public static void ApplyTileControlStylle(
            IEnumerable<FrameworkElement> controls,
            System.Windows.Media.Brush fgColor,
            System.Windows.Media.Brush disableColor)
        {
            foreach(var control in controls)
            {
                // ボタンの場合はDisable時の色設定を合わせてスタイル設定する
                // それ以外の場合は Foreground に色設定可能な場合のみ設定する
                var t = control.GetType();
                if (typeof(Button).IsAssignableFrom(t))
                { ApplyButtonStyle((Button)control, fgColor, disableColor); }
                else if (t.GetProperty("Foreground", typeof(System.Windows.Media.Brush)) != null)
                { ApplyStaticControlStyle(control, fgColor); }
            }
        }

        /// <summary>
        /// タイル上に配置した静的コントロール (ラベルなど)に前景色の設定を適用する
        /// </summary>
        public static void ApplyStaticControlStyle(
            FrameworkElement ctl,
            System.Windows.Media.Brush fgColor)
        {
            // リフレクションでForegroundプロパティを取得し、そこに前景色を設定する
            var prop = ctl.GetType().GetProperty("Foreground", typeof(System.Windows.Media.Brush));
            prop.SetValue(ctl, fgColor);
        }

        /// <summary>
        /// タイル上に配置したラベルに前景色の設定を適用する
        /// </summary>
        /// <param name="btn"></param>
        public static void ApplyButtonStyle(
            Button btn,
            System.Windows.Media.Brush fgColor,
            System.Windows.Media.Brush disableColor)
        {
            // リソースディクショナリからボタンのスタイルをロードする
            var rd = new ResourceDictionary() { Source = new Uri("pack://application:,,,/LivanaiTIles.Core;component/Windows/Styles.xaml"), };
            var styles = rd["btnTileButton"] as Style;

            // 前景色
            styles
                .Setters
                .Select(x => (Setter)x)
                .First(x => x.Property.Name.Equals("Foreground"))
                .Value = fgColor;

            // 背景色
            // ボタンは背景色もスタイル上で設定しないと、disable時にグレーで塗りつぶされてしまう
            styles
                .Setters
                .Select(x => (Setter)x)
                .First(x => x.Property.Name.Equals("Background"))
                .Value = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0x01, 0x00, 0x00, 0x00));

            var templateSetter = styles
                .Setters
                .Select(x => (Setter)x)
                .First(x => x.Property.Name.Equals("Template"))
                .Value as ControlTemplate;

            var templateTrigger = styles.Triggers[0] as Trigger;
            // 操作不能時の前景色
            templateTrigger
                .Setters
                .Select(x => (Setter)x)
                .First(x => x.Property.Name.Equals("Foreground"))
                .Value = disableColor;

            // 操作不能時の背景色
            templateTrigger
                .Setters
                .Select(x => (Setter)x)
                .First(x => x.Property.Name.Equals("Background"))
                .Value = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0x01, 0x00, 0x00, 0x00));

            btn.Style = styles;
        }

        /// <summary>
        /// 指定したウィンドウが画面の境界や他のウィンドウとくっつくように位置を調整する
        /// </summary>
        /// <param name="wnd"></param>
        public static void AdjustWindowPosition(Window wnd)
        {
            var screenBorders = AppControllerProvider.Instance.ScreenInfoList;
            var windows = AppControllerProvider.Instance.GetAllWindowList();

            AdjustWindowPositionScreenBorder(wnd, screenBorders);
            AdjustWindowPositionOtherWindow(wnd, windows, 30.0, AppControllerProvider.Instance.Config.TileMargin);
        }

        /// <summary>
        /// 指定したウィンドウが画面の境界とくっつくように位置を調整する
        /// </summary>
        /// <param name="wnd">位置調整の対象とするウィンドウ</param>
        /// <param name="screenBorders">ウィンドウの座標範囲を格納した配列</param>
        private static void AdjustWindowPositionScreenBorder(
            Window wnd,
            System.Windows.Forms.Screen[] screenBorders)
        {
            // 画面からはみ出たりスクリーンの境界に跨らないようにウィンドウ位置を調整する
            foreach (var dsp in screenBorders)
            {
                if (Math.Abs(wnd.Left - dsp.Bounds.X) < 16.0)
                {
                    // 左端
                    wnd.Left = dsp.Bounds.X;
                }
                else if (Math.Abs((wnd.Left + wnd.Width) - (dsp.Bounds.X + dsp.Bounds.Width)) < 16.0)
                {
                    // 右端
                    wnd.Left = dsp.Bounds.X + dsp.Bounds.Width - wnd.Width;
                }
                if (Math.Abs(wnd.Top - dsp.Bounds.Y) < 16.0)
                {
                    // 上端
                    wnd.Top = 0.0;
                }
                else if (Math.Abs((wnd.Top + wnd.Height) - (dsp.Bounds.Y + dsp.Bounds.Height)) < 16.0)
                {
                    // 下端
                    wnd.Top = dsp.Bounds.Y + dsp.Bounds.Height - wnd.Height;
                }
            }
        }

        /// <summary>
        /// 指定したウィンドウが他のウィンドウとくっつくように位置を調整する
        /// </summary>
        /// <param name="wnd"></param>
        /// <param name="windows">アプリが開いているウィンドウの一覧</param>
        /// <param name="magnetalityDistance">タイル位置を自動調整するための距離</param>
        /// <param name="margin">タイル間のマージン</param>
        private static void AdjustWindowPositionOtherWindow(
            Window wnd,
            IEnumerable<TiledWindow> windows,
            double magnetalityDistance,
            double margin)
        {
            foreach (var other in windows)
            {
                if (other.Equals(wnd))
                { continue; }

                bool magnet = false;

                if ((wnd.Top <= other.Top + other.Height) && (other.Top <= wnd.Top + wnd.Height))
                {
                    // このウインドウと他のウィンドウが左右で接しているかどうか

                    if (Math.Abs((wnd.Left + wnd.Width) - other.Left) < magnetalityDistance)
                    {
                        // このウィンドウの右端と他のウィンドウの左端
                        wnd.Left = other.Left - wnd.Width - margin;
                        magnet = true;

                    }
                    else if (Math.Abs(wnd.Left - (other.Left + other.Width)) < magnetalityDistance)
                    {
                        // このウィンドウの左端と他のウィンドウの右端
                        wnd.Left = other.Left + other.Width + margin;
                        magnet = true;
                    }

                    if (magnet)
                    {
                        if (Math.Abs(wnd.Top - other.Top) < magnetalityDistance)
                        {
                            // 上辺が並んでいる
                            wnd.Top = other.Top;
                        }
                        else if (Math.Abs((wnd.Top + wnd.Height) - (other.Top + other.Height)) < magnetalityDistance)
                        {
                            // 下辺が並んでいる
                            wnd.Top = other.Top + other.Height - wnd.Height;
                        }
                    }
                }

                magnet = false;

                if ((wnd.Left <= other.Left + other.Width) && (other.Left <= wnd.Left + wnd.Width))
                {
                    // このウィンドウと他のウィンドウが上下で接しているかどうか

                    if (Math.Abs((wnd.Top + wnd.Height) - other.Top) < magnetalityDistance)
                    {
                        // このウィンドウの下端と他のウィンドウの上端
                        wnd.Top = other.Top - wnd.Height - margin;
                        magnet = true;
                    }
                    else if (Math.Abs(wnd.Top - (other.Top + other.Height)) < magnetalityDistance)
                    {
                        // このウィンドウの上端と他のウィンドウの下端
                        wnd.Top = other.Top + other.Height + margin;
                        magnet = true;
                    }

                    if (magnet)
                    {
                        if (Math.Abs(wnd.Left - other.Left) < magnetalityDistance)
                        {
                            // 左辺が並んでいる
                            wnd.Left = other.Left;
                        }
                        else if (Math.Abs((wnd.Left + wnd.Width) - (other.Left + other.Width)) < magnetalityDistance)
                        {
                            // 右辺が並んでいる
                            wnd.Left = other.Left + other.Width - wnd.Width;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 指定したタイルに乗せているコンテンツのUserControlを返す
        /// </summary>
        /// <param name="wnd">ウィンドウ</param>
        /// <returns>ウィンドウ上のUserControl. コントロールが載っていない場合はnull</returns>
        public static UserControl GetTileContent(this TiledWindow wnd)
        {
            var g = wnd.FindName("contentPanel") as Grid;
            return (0 < g.Children.Count ? g.Children[0] : null) as UserControl;
        }

        /// <summary>
        /// 指定したタイルの設定パネルの表示可否を設定する.
        /// タイル内の Name プロパティが "SettingPanel" であるコントロールを設定対象にする.
        /// </summary>
        /// <param name="ctl">タイルのコンテンツ</param>
        /// <param name="visibility">表示属性.</param>
        public static void SetSettingPanelVisilirity(this UserControl ctl, Visibility visibility)
        {
            if (ctl == null)
            { return; }

            // 対象のコントロールを検索する
            var settingsPanel = EnumControl(1, ctl);
            if(settingsPanel != null)
            { settingsPanel.Visibility = visibility; }
        }

        /// <summary>
        /// 指定したコントロールの子コントロールを列挙し、設定パネルのコントロールが見つかれば返す
        /// </summary>
        /// <param name="level"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        private static FrameworkElement EnumControl(int level, FrameworkElement current)
        {
            foreach (var child in LogicalTreeHelper.GetChildren(current))
            {
                var ctl = child as FrameworkElement;
                if (ctl != null)
                {
                    if (ctl.Name == NANE_SETTINGS_PANEL)
                    {
                        // コントロール名を確認し、設定パネルのコントロールであれば返す
                        return ctl; 
                    }
                    else
                    {
                        // このコントロールの子に目的のコントロールがあればそれを返す
                        var fromChild = EnumControl(level + 1, ctl);
                        if (fromChild != null)
                        { return fromChild; }
                    }
                }
            }
            return null;
        }
    }
}
