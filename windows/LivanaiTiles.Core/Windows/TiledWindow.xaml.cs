﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using LivanaiTiles.Controllers;
using LivanaiTiles.Utils;

namespace LivanaiTiles.Windows
{
    /// <summary>
    /// TiledWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class TiledWindow : Window
    {
        /// <summary>
        /// このウィンドウに対して割り当てる GUID
        /// </summary>
        public Guid WindowID { get; set; }

        public TiledWindow()
        {
            InitializeComponent();
        }

        private void TiledWindowBase_Loaded(object sender, RoutedEventArgs e)
        {
            // マウスオーバー時に設定ボタンを隠す設定の場合、ロード時に設定ボタンのパネルを非表示にする
            if (AppControllerProvider.Instance.Config.ShowButtonMouseoverOnly)
            { this.GetTileContent().SetSettingPanelVisilirity(Visibility.Collapsed); }
        }

        private void TiledWindow_LocationChanged(object sender, EventArgs e)
        {
            // タイルの位置調整を行う？
            if (AppControllerProvider.Instance.Config.UseAutoPosition)
            { WindowControlUtils.AdjustWindowPosition(this); }
        }

        private void TiledWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void TiledWindowBase_Closed(object sender, EventArgs e)
        {
            // このウィンドウの情報をコントローラから削除する
            AppControllerProvider.Instance.DeleteWindow(this.WindowID);
        }

        private void TiledWindowBase_MouseEnter(object sender, MouseEventArgs e)
        {
            // マウスオーバーで設定パネルを表示する
            if (AppControllerProvider.Instance.Config.ShowButtonMouseoverOnly)
            { this.GetTileContent().SetSettingPanelVisilirity(Visibility.Visible); }
        }

        private void TiledWindowBase_MouseLeave(object sender, MouseEventArgs e)
        {
            // マウスカーソルが離れたら設定ボタンのパネルを非表示にする
            if (AppControllerProvider.Instance.Config.ShowButtonMouseoverOnly)
            { this.GetTileContent().SetSettingPanelVisilirity(Visibility.Collapsed); }
        }
    }
}
