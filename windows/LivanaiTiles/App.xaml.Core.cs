﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;

using Hardcodet.Wpf.TaskbarNotification;

using LivanaiTiles.Controllers;
using LivanaiTiles.Plugins;
using LivanaiTiles.Resources;

namespace LivanaiTiles
{
    public partial class App
    {
        /// <summary>
        /// タスクトレイに表示するアイコンをリソースからロードする.
        /// </summary>
        /// <param name="iconPath">アイコンのリソースのパス</param>
        /// <returns>ロードしたアイコン</returns>
        private Icon LoadIcon(string iconPath)
        {
            var asm = System.Reflection.Assembly.GetExecutingAssembly();
            using (var iconStream = asm.GetManifestResourceStream(iconPath))
            { return new Icon(iconStream); }
        }

        /// <summary>
        /// タスクトレイのアイコンを右クリックした時に表示するメニューを生成して返す
        /// </summary>
        /// <returns>生成したコンテキストメニュー</returns>
        private ContextMenu CreateContextMenu()
        {
            var ctxMenu = new ContextMenu();

            // プラグインが提供しているタイルを表示するためのメニュー
            var ctl = AppControllerProvider.Instance;
            foreach (var plugin in ctl.Providers)
            { ctxMenu.Items.Add(CreateContextMenuItem(plugin.PluginName, plugin.PluginID, TileMenu_Click)); }

            // 固定メニュー
            ctxMenu.Items.Add(new Separator());
            ctxMenu.Items.Add(new MenuItem() { Header = LivanaiTiles.Resources.Strings.CTXMENU_CLOSE_CAPTION });
            ctxMenu.Items.Add(CreateContextMenuItem(LivanaiTiles.Resources.Strings.CTXMENU_RUNAS_CAPTION, RunAsMenu_Click));
            ctxMenu.Items.Add(CreateContextMenuItem(LivanaiTiles.Resources.Strings.CTXMENU_SETTINGS_CAPTION, SettingMenu_Click));
            ctxMenu.Items.Add(CreateContextMenuItem(LivanaiTiles.Resources.Strings.CTXMENU_SAVE_SESSION_CAPTION, SaveSessionMenu_Click));
            ctxMenu.Items.Add(CreateContextMenuItem(LivanaiTiles.Resources.Strings.CTXMENU_ABOUT_CAPTION, AboutMenu_Click));
            ctxMenu.Items.Add(CreateContextMenuItem(LivanaiTiles.Resources.Strings.CTXMENU_EXIT_CAPTION, ExitMenu_Click));

            ctxMenu.Opened += CtxMenu_Opened;
            return ctxMenu;
        }

        /// <summary>
        /// コンテキストメニューに追加するメニューアイテムを生成する
        /// </summary>
        /// <param name="header">メニューのラベル</param>
        /// <param name="clickEvent">メニュークリック時のイベント</param>
        /// <returns>生成したメニューアイテム</returns>
        private MenuItem CreateContextMenuItem(
            string header,
            RoutedEventHandler clickEvent)
        {
            var newItem = new MenuItem() { Header = header };
            newItem.Click += clickEvent;

            return newItem;
        }

        /// <summary>
        /// コンテキストメニューに追加するメニューアイテムを生成する
        /// </summary>
        /// <param name="header">メニューのラベル</param>
        /// <param name="tag">メニューアイテムに設定するタグ</param>
        /// <param name="clickEvent">メニュークリック時のイベント</param>
        /// <returns>生成したメニューアイテム</returns>
        private MenuItem CreateContextMenuItem(
            string header,
            object tag,
            RoutedEventHandler clickEvent)
        {
            var newItem = new MenuItem() { Header = header, Tag = tag };
            newItem.Click += clickEvent;

            return newItem;
        }

        /// <summary>
        /// タイルを閉じるメニューに現在表示されているタイルの一覧を追加する
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private void CreateCloseTileMenu(MenuItem parent)
        {
            // 現在の子メニューをすべて消去
            parent.Items.Clear();

            // このメニューのサブアイテムを列挙する
            foreach (var wnd in AppControllerProvider.Instance.GetAllWindowList())
            {
                var closeItem = new MenuItem() { Header = wnd.Title, Tag = wnd, };
                closeItem.Click += CloseMenu_Click;

                parent.Items.Add(closeItem);
            }

            // ウィンドウが一つも無かった場合はメニューを操作不可にする
            parent.IsEnabled = (0 < parent.Items.Count);
        }

        /// <summary>
        /// 指定したGUIDのプラグインでタイルを起動する
        /// </summary>
        /// <param name="pluginName">起動するプラグイン名.</param>
        /// <param name="tileID">起動するプラグインのGUID. パラメータを指定しない場合はnullを渡す</param>
        /// <param name="parameter">起動パラメータ</param>
        private void LaunchTile(
            string pluginName,
            Guid tileID,
            string parameter)
        {
            try
            {
                if (parameter != null)
                { AppControllerProvider.Instance.CreateWindow(tileID, parameter); }
                else
                { AppControllerProvider.Instance.CreateWindow(tileID); }
            }
            catch (NotAllowMultipleTileException)
            {
                // 同一種類で複数個タイルの生成が認められないタイルの場合はバルーンでエラー通知
                this.taskbarIcon.ShowBalloonTip(
                    Strings.ALERT_TITLE_CREATE_TILE,
                    string.Format(Strings.ALERT_NOTALLOWMULTIPLETILE, pluginName),
                    BalloonIcon.Error);
            }
            catch(Exception eUnknown)
            {
                // その他の例外
                this.taskbarIcon.ShowBalloonTip(
                    Strings.ALERT_TITLE_CREATE_TILE,
                    string.Format(Strings.ALERT_EXCEPTION, eUnknown.ToString(), eUnknown.Message),
                    BalloonIcon.Error);
            }
        }
    }
}
