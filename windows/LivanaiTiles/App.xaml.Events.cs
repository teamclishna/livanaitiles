﻿using System;
using System.Windows;
using System.Windows.Controls;

using LivanaiTiles.Controllers;
using LivanaiTiles.VM;
using LivanaiTiles.Windows;

namespace LivanaiTiles
{
    public partial class App
    {
        /// <summary>
        /// コンテキストメニューを表示する時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CtxMenu_Opened(object sender, RoutedEventArgs e)
        {
            // 閉じるメニューを取得する
            for (var index = 0; index < this.taskbarIcon.ContextMenu.Items.Count; index++)
            {
                var menuItem = this.taskbarIcon.ContextMenu.Items[index] as MenuItem;
                if (menuItem != null && menuItem.Header.Equals(LivanaiTiles.Resources.Strings.CTXMENU_CLOSE_CAPTION))
                {
                    CreateCloseTileMenu(menuItem);
                    return;
                }
            }
        }

        /// <summary>
        /// 設定画面表示メニューをクリックした時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingMenu_Click(object sender, RoutedEventArgs e)
        {
            var wnd = new SettingWindow()
            {
                DataContext = new SettingsWindowViewModel()
                { Config = AppControllerProvider.Instance.Config.Clone(), }
            };
            wnd.Show();
        }

        private void SaveSessionMenu_Click(object sender, RoutedEventArgs e)
        {
            // 現在表示しているタイルの情報を保存する
            AppControllerProvider.Instance.SaveSession();
        }

        private void AboutMenu_Click(object sender, RoutedEventArgs e)
        {
            var wnd = new AboutDialog();
            wnd.Show();
        }

        private void ExitMenu_Click(object sender, RoutedEventArgs e)
        {
            // コントローラのシャットダウンを行う
            AppControllerProvider.Shutdown();

            System.Windows.Application.Current.Shutdown();
        }

        /// <summary>
        /// タイル表示メニューをクリックした時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TileMenu_Click(object sender, RoutedEventArgs e)
        {
            var menu = sender as MenuItem;
            LaunchTile(menu.Header.ToString(), (Guid)menu.Tag, null);
        }

        /// <summary>
        /// パラメータ指定起動メニューをクリックした時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RunAsMenu_Click(object sender, RoutedEventArgs e)
        {
            var vm = new RunAsWindowViewModel();
            foreach (var plugin in AppControllerProvider.Instance.Providers)
            {
                vm.PluginList.Add(new VM.RunAsPluginInfo()
                {
                    PluginID = plugin.PluginID,
                    PluginName = plugin.PluginName,
                });
            }
            var wnd = new RunAsWindow() { DataContext = vm, };
            var dlgResult = wnd.ShowDialog();
            if (dlgResult.HasValue && dlgResult.Value)
            {
                var selectedItem = wnd.cbLaunchTile.SelectedItem as RunAsPluginInfo;
                if (selectedItem != null)
                {
                    LaunchTile(
                        selectedItem.PluginName,
                        selectedItem.PluginID,
                        vm.LaunchParameter);
                }
            }
        }

        /// <summary>
        /// タイルを閉じるメニューをクリックした時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseMenu_Click(object sender, RoutedEventArgs e)
        {
            var mi = sender as MenuItem;
            if (mi != null)
            {
                var wnd = mi.Tag as TiledWindow;
                if (wnd != null)
                { wnd.Close(); }
            }
        }
    }
}
