﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using clUtils.log;
using Hardcodet.Wpf.TaskbarNotification;

using LivanaiTiles.Controllers;

namespace LivanaiTiles
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        /// <summary>
        /// タスクトレイに常駐するアイコン
        /// </summary>
        private TaskbarIcon taskbarIcon;

        /// <summary>
        /// アプリケーションの起動処理
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // アプリケーションの Shutdown メソッドが明示的に呼ばれたときにのみ終了する
            // (ウィンドウを全部閉じても自動的にアプリ終了しない)
            this.ShutdownMode = ShutdownMode.OnExplicitShutdown;

            try
            {
                InitializeApp();

                // タスクトレイに常駐するアイコンを生成
                this.taskbarIcon = new TaskbarIcon()
                {
                    Visibility = Visibility.Visible,
                    Icon = LoadIcon(LivanaiTiles.Resources.Strings.RESOURCE_TASKTRAY_ICON),
                    ToolTipText = LivanaiTiles.Resources.Strings.TASKTRAY_CAPTION,
                    ContextMenu = CreateContextMenu(),
                    MenuActivation = PopupActivationMode.LeftOrRightClick,
                };

                // タイルのリストアを行う
                AppControllerProvider.Instance.RestoreSession();
            }
            catch(Exception eUnknown)
            {
                System.Windows.MessageBox.Show(
                    $"{eUnknown.ToString()}\n\n{eUnknown.Message}",
                    "Application initialization failed",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                System.Windows.Application.Current.Shutdown();
            }
            finally
            { }
        }
        /// <summary>
        /// アプリケーションの終了処理
        /// </summary>
        /// <param name="e"></param>
        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            // 常駐していたアイコンを破棄して終了
            this.taskbarIcon.Dispose();
        }

        /// <summary>
        /// アプリケーションの初期化処理
        /// </summary>
        private void InitializeApp()
        {
            // アプリケーションのコントローラを初期化する
            AppControllerProvider.Init(InitLogger());
        }

        /// <summary>
        /// コントローラが使用するロガーオブジェクトを初期化する
        /// </summary>
        /// <returns></returns>
        private ILogger InitLogger()
        {
#if DEBUG
            return new clUtils.log.ConsoleLogger()
            {
                OutputLogLevel = new clUtils.log.LogLevel[] {
                    clUtils.log.LogLevel.ERROR,
                    clUtils.log.LogLevel.WARN,
                    clUtils.log.LogLevel.INFO,
                    clUtils.log.LogLevel.DEBUG
                },
                ShowCurrentLine = false,
                ShowThreadID = true,
                ShowCallStackIndentation = true,
                IndentString = " ",
                ShowTimestamp = true,
                ShowLogLevel = true,
            };
#else
            return new clUtils.log.NullLogger();
#endif
        }
    }
}
