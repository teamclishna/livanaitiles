﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace LivanaiTiles.Resources {
    using System;
    
    
    /// <summary>
    ///   ローカライズされた文字列などを検索するための、厳密に型指定されたリソース クラスです。
    /// </summary>
    // このクラスは StronglyTypedResourceBuilder クラスが ResGen
    // または Visual Studio のようなツールを使用して自動生成されました。
    // メンバーを追加または削除するには、.ResX ファイルを編集して、/str オプションと共に
    // ResGen を実行し直すか、または VS プロジェクトをビルドし直します。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   このクラスで使用されているキャッシュされた ResourceManager インスタンスを返します。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("LivanaiTiles.Resources.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   すべてについて、現在のスレッドの CurrentUICulture プロパティをオーバーライドします
        ///   現在のスレッドの CurrentUICulture プロパティをオーバーライドします。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Plugins に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string ABOUT_LABEL_PLUGINS {
            get {
                return ResourceManager.GetString("ABOUT_LABEL_PLUGINS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   https://clishna.iplus.to/download/livanaitiles/ に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string ABOUT_URL {
            get {
                return ResourceManager.GetString("ABOUT_URL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   About に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string ABOUT_WINDOW_CAPTION {
            get {
                return ResourceManager.GetString("ABOUT_WINDOW_CAPTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Exception occured: {0} {1} に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string ALERT_EXCEPTION {
            get {
                return ResourceManager.GetString("ALERT_EXCEPTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Not allow multiple tiles for {0} に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string ALERT_NOTALLOWMULTIPLETILE {
            get {
                return ResourceManager.GetString("ALERT_NOTALLOWMULTIPLETILE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Create Tile に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string ALERT_TITLE_CREATE_TILE {
            get {
                return ResourceManager.GetString("ALERT_TITLE_CREATE_TILE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Livanai Tiles に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string APP_NAME {
            get {
                return ResourceManager.GetString("APP_NAME", resourceCulture);
            }
        }
        
        /// <summary>
        ///   About... に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string CTXMENU_ABOUT_CAPTION {
            get {
                return ResourceManager.GetString("CTXMENU_ABOUT_CAPTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Close に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string CTXMENU_CLOSE_CAPTION {
            get {
                return ResourceManager.GetString("CTXMENU_CLOSE_CAPTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Exit に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string CTXMENU_EXIT_CAPTION {
            get {
                return ResourceManager.GetString("CTXMENU_EXIT_CAPTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Launch As... に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string CTXMENU_RUNAS_CAPTION {
            get {
                return ResourceManager.GetString("CTXMENU_RUNAS_CAPTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Save Session に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string CTXMENU_SAVE_SESSION_CAPTION {
            get {
                return ResourceManager.GetString("CTXMENU_SAVE_SESSION_CAPTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Settings... に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string CTXMENU_SETTINGS_CAPTION {
            get {
                return ResourceManager.GetString("CTXMENU_SETTINGS_CAPTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   LivanaiTiles.Resources.Images.LivanaiTiles.ico に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string RESOURCE_TASKTRAY_ICON {
            get {
                return ResourceManager.GetString("RESOURCE_TASKTRAY_ICON", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Launch Parameter に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string RUNAS_LAUNCH_PARAMETER {
            get {
                return ResourceManager.GetString("RUNAS_LAUNCH_PARAMETER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Launch Tile に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string RUNAS_TILE_NAME {
            get {
                return ResourceManager.GetString("RUNAS_TILE_NAME", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Launch As に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string RUNAS_TITLE {
            get {
                return ResourceManager.GetString("RUNAS_TITLE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Background color に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_BG_COLOR {
            get {
                return ResourceManager.GetString("SETTINGS_BG_COLOR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Default Tile Appearance に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_DEFAULT_APPEARANCE {
            get {
                return ResourceManager.GetString("SETTINGS_DEFAULT_APPEARANCE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   General Settings に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_GENERAL {
            get {
                return ResourceManager.GetString("SETTINGS_GENERAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Acrylic Blur (ver. 1803 or avove) に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_GLASS_EFFECT_ACRYLIC {
            get {
                return ResourceManager.GetString("SETTINGS_GLASS_EFFECT_ACRYLIC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Blur Effect (not working after ver. 1809) に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_GLASS_EFFECT_BLUR {
            get {
                return ResourceManager.GetString("SETTINGS_GLASS_EFFECT_BLUR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   None に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_GLASS_EFFECT_NONE {
            get {
                return ResourceManager.GetString("SETTINGS_GLASS_EFFECT_NONE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Glass Effect に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_GLASS_EFFECT_TYPE {
            get {
                return ResourceManager.GetString("SETTINGS_GLASS_EFFECT_TYPE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Label color に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_LABEL_COLOR {
            get {
                return ResourceManager.GetString("SETTINGS_LABEL_COLOR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Show setting buttons MouseOver only に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_SHOW_BUTTON_MOUSEOVER_ONLY {
            get {
                return ResourceManager.GetString("SETTINGS_SHOW_BUTTON_MOUSEOVER_ONLY", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Tile margin に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_TILE_MARGINE {
            get {
                return ResourceManager.GetString("SETTINGS_TILE_MARGINE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Tile Position Adjustment に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_TILE_POSITION_ADJUST {
            get {
                return ResourceManager.GetString("SETTINGS_TILE_POSITION_ADJUST", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Auto position adjust に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_TILE_POSITION_AUTO_ADJUST {
            get {
                return ResourceManager.GetString("SETTINGS_TILE_POSITION_AUTO_ADJUST", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Settings に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_TITLE {
            get {
                return ResourceManager.GetString("SETTINGS_TITLE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Application Settings に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string SETTINGS_WINDOW_CAPTION {
            get {
                return ResourceManager.GetString("SETTINGS_WINDOW_CAPTION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Livanai Tiles に類似しているローカライズされた文字列を検索します。
        /// </summary>
        public static string TASKTRAY_CAPTION {
            get {
                return ResourceManager.GetString("TASKTRAY_CAPTION", resourceCulture);
            }
        }
    }
}
