﻿using System;
using System.Collections.ObjectModel;
using clUtils.gui;

namespace LivanaiTiles.VM
{
    /// <summary>
    /// 起動するタイルのプラグイン情報
    /// </summary>
    public class RunAsPluginInfo
    {
        /// <summary>プラグインID</summary>
        public Guid PluginID { get; set; }

        /// <summary>プラグイン名</summary>
        public string PluginName { get; set; }
    }

    /// <summary>
    /// パラメータ指定での起動ウィンドウに使用するViewModel
    /// </summary>
    public class RunAsWindowViewModel : NotifiableViewModel
    {
        #region Fields

        private ObservableCollection<RunAsPluginInfo> _pluginInfoList;
        private string _launchParameter;
        private int _selectedIndex;

        #endregion

        /// <summary>
        /// タイル起動時に選択するプラグインの情報
        /// </summary>
        public ObservableCollection<RunAsPluginInfo> PluginList
        {
            get { return this._pluginInfoList; }
            set
            {
                this._pluginInfoList = value;
                OnPropertyChanged("PluginList");
            }
        }

        /// <summary>
        /// 選択中のプラグインのインデックス
        /// </summary>
        public int SelectedIndex
        {
            get { return this._selectedIndex; }
            set
            {
                this._selectedIndex = value;
                OnPropertyChanged("SelectedIndex");
                OnPropertyChanged("EnableLaunch");
            }
        }

        /// <summary>
        /// タイルの起動が可能かどうか
        /// </summary>
        public bool EnableLaunch
        { get { return (0 <= SelectedIndex); } }

        /// <summary>
        /// タイル起動時に指定するパラメータ
        /// </summary>
        public string LaunchParameter
        {
            get { return this._launchParameter; }
            set
            {
                this._launchParameter = value;
                OnPropertyChanged("LaunchParameter");
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public RunAsWindowViewModel()
        {
            this.PluginList = new ObservableCollection<RunAsPluginInfo>();
        }
    }
}
