﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using clUtils.gui;
using LivanaiTiles.Controllers;

namespace LivanaiTiles.VM
{
    /// <summary>
    /// アプリケーションの設定と設定画面の各項目を関連付けるためのViewModel
    /// </summary>
    public class SettingsWindowViewModel : NotifiableViewModel
    {
        #region fields

        private AppConfig _config;

        #endregion

        public AppConfig Config
        {
            get { return this._config; }

            set
            {
                this._config = value;
                OnPropertyChanged("Config");
                OnPropertyChanged("ShowButtonMouseoverOnly");
                OnPropertyChanged("UseAutoPosition");
                OnPropertyChanged("TileMargin");
                OnPropertyChanged("FGColor");
                OnPropertyChanged("FGRed");
                OnPropertyChanged("FGGreen");
                OnPropertyChanged("FGBlue");
                OnPropertyChanged("BGColor");
                OnPropertyChanged("BGRed");
                OnPropertyChanged("BGGreen");
                OnPropertyChanged("BGBlue");
                OnPropertyChanged("BGAlpha");
                OnPropertyChanged("UseAeroGlass");
                OnPropertyChanged("IsGlassNone");
                OnPropertyChanged("IsGlassBlur");
                OnPropertyChanged("IsGlassAcrylic");
            }
        }

        public bool ShowButtonMouseoverOnly
        {
            get { return this._config.ShowButtonMouseoverOnly; }
            set
            {
                this._config.ShowButtonMouseoverOnly = value;
                OnPropertyChanged("ShowButtonMouseoverOnly");
            }
        }

        public bool UseAutoPosition
        {
            get { return this._config.UseAutoPosition; }
            set
            {
                this._config.UseAutoPosition = value;
                OnPropertyChanged("UseAutoPosition");
            }
        }

        public int TileMargin
        {
            get { return (int)this._config.TileMargin; }
            set
            {
                this._config.TileMargin = (double)value;
                OnPropertyChanged("TileMargin");
            }
        }

        public SolidColorBrush FGColor { get { return new SolidColorBrush(this._config.ForeGroundColor); } }

        public int FGRed
        {
            get { return this._config.ForeGroundColor.R; }
            set
            {
                var prevColor = this._config.ForeGroundColor;
                this._config.ForeGroundColor = Color.FromArgb(prevColor.A, (byte)value, prevColor.G, prevColor.B);
                OnPropertyChanged("FGRed");
                OnPropertyChanged("FGColor");
            }
        }

        public int FGGreen
        {
            get { return this._config.ForeGroundColor.G; }
            set
            {
                var prevColor = this._config.ForeGroundColor;
                this._config.ForeGroundColor = Color.FromArgb(prevColor.A, prevColor.R, (byte)value, prevColor.B);
                OnPropertyChanged("FGGreen");
                OnPropertyChanged("FGColor");
            }
        }

        public int FGBlue
        {
            get { return this._config.ForeGroundColor.B; }
            set
            {
                var prevColor = this._config.ForeGroundColor;
                this._config.ForeGroundColor = Color.FromArgb(prevColor.A, prevColor.R, prevColor.G, (byte)value);
                OnPropertyChanged("FGBlue");
                OnPropertyChanged("FGColor");
            }
        }

        public SolidColorBrush BGColor { get { return new SolidColorBrush(this._config.BackgroundColor); } }

        public int BGRed
        {
            get { return this._config.BackgroundColor.R; }
            set
            {
                var prevColor = this._config.BackgroundColor;
                this._config.BackgroundColor = Color.FromArgb(prevColor.A, (byte)value, prevColor.G, prevColor.B);
                OnPropertyChanged("BGRed");
                OnPropertyChanged("BGColor");
            }
        }

        public int BGGreen
        {
            get { return this._config.BackgroundColor.G; }
            set
            {
                var prevColor = this._config.BackgroundColor;
                this._config.BackgroundColor = Color.FromArgb(prevColor.A, prevColor.R, (byte)value, prevColor.B);
                OnPropertyChanged("BGGreen");
                OnPropertyChanged("BGColor");
            }
        }

        public int BGBlue
        {
            get { return this._config.BackgroundColor.B; }
            set
            {
                var prevColor = this._config.BackgroundColor;
                this._config.BackgroundColor = Color.FromArgb(prevColor.A, prevColor.R, prevColor.G, (byte)value);
                OnPropertyChanged("BGBlue");
                OnPropertyChanged("BGColor");
            }
        }

        public int BGAlpha
        {
            get { return this._config.BackgroundColor.A; }
            set
            {
                var prevColor = this._config.BackgroundColor;
                this._config.BackgroundColor = Color.FromArgb((byte)value, prevColor.R, prevColor.G, prevColor.B);
                OnPropertyChanged("BGAlpha");
                OnPropertyChanged("BGColor");
            }
        }

        public bool UseAeroGlass
        {
            get { return this._config.UseAeroGlass; }
            set
            {
                this._config.UseAeroGlass = value;
                OnPropertyChanged("UseAeroGlass");
            }
        }

        public bool IsGlassNone
        {
            get
            {
                return
                    this._config.AeroglassEffectType == AppConfig.AeroGlassType.NONE ||
                    (this._config.AeroglassEffectType == AppConfig.AeroGlassType.UNDEFINED && !this._config.UseAeroGlass);
            }
            set
            {
                if (value)
                {
                    this._config.UseAeroGlass = false;
                    this._config.AeroglassEffectType = AppConfig.AeroGlassType.NONE;
                    OnPropertyChanged("IsGlassNone");
                }
            }
        }

        public bool IsGlassBlur
        {
            get
            {
                return
                    this._config.AeroglassEffectType == AppConfig.AeroGlassType.BLUR_BEHIND ||
                    (this._config.AeroglassEffectType == AppConfig.AeroGlassType.UNDEFINED && this._config.UseAeroGlass);
            }
            set
            {
                if (value)
                {
                    this._config.UseAeroGlass = false;
                    this._config.AeroglassEffectType = AppConfig.AeroGlassType.BLUR_BEHIND;
                    OnPropertyChanged("IsGlassBlur");
                }
            }
        }

        public bool IsGlassAcrylic
        {
            get { return this._config.AeroglassEffectType == AppConfig.AeroGlassType.ACRYLIC; }
            set
            {
                if (value)
                {
                    this._config.UseAeroGlass = false;
                    this._config.AeroglassEffectType = AppConfig.AeroGlassType.ACRYLIC;
                    OnPropertyChanged("IsGlassAcrylic");
                }
            }
        }
    }
}
