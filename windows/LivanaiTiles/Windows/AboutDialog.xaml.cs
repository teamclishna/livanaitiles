﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LivanaiTiles.Windows
{
    /// <summary>
    /// AboutDialog.xaml の相互作用ロジック
    /// </summary>
    public partial class AboutDialog : Window
    {
        public AboutDialog()
        {
            InitializeComponent();

            // バージョン情報を取得してラベルに表示する
            var fi = System.Diagnostics.FileVersionInfo.GetVersionInfo(typeof(App).Assembly.Location);
            this.txtAuthor.Content = fi.CompanyName;
            this.txtVersion.Text = fi.ProductVersion;

            // プラグインのバージョンをListViewに表示する
            var pluginListItem = new ObservableCollection<string[]>();
            foreach (var pi in Controllers.AppControllerProvider.Instance.Providers)
            {
                var versionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(pi.PluginLocation.FullName);
                pluginListItem.Add(new string[]
                {
                    pi.PluginName,
                    pi.PluginLocation.Name,
                    versionInfo.ProductVersion,
                });
            }
            this.pluginVersionView.ItemsSource = pluginListItem;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ManualPage_RequestNavigate(
            object sender,
            System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            // システム既定のブラウザでURLを開く
            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(e.Uri.AbsoluteUri) { UseShellExecute = true, });
            e.Handled = true;
        }
    }
}
