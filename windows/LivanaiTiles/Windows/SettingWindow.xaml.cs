﻿using LivanaiTiles.VM;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using clUtils.log;

using LivanaiTiles.Utils;
using LivanaiTiles.Controllers;

namespace LivanaiTiles.Windows
{
    /// <summary>
    /// SettingWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class SettingWindow : Window
    {
        public SettingWindow()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            // 現在編集中の設定を取得し、保存してから綴じる
            var dc = this.DataContext as SettingsWindowViewModel;
            var conf = dc.Config;

            FileUtils.SaveToJson(
                conf,
                AppConfig.FilePath,
                AppControllerProvider.Instance.Logger);

            // 保存したらリロードする
            var controller = AppControllerProvider.Instance;
            controller.ReloadConfig();
            controller.RestartWindows();
            
            //AppControllerProvider.Instance.ReloadConfig();

            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            return;
        }
    }
}
