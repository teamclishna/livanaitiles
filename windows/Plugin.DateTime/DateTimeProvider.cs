﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using clUtils.util;
using LivanaiTiles.Controllers;
using LivanaiTiles.DateTime.Resources;
using LivanaiTiles.DateTime.Utils;
using LivanaiTiles.DateTime.VM;
using LivanaiTiles.Plugins;
using LivanaiTiles.Utils;

namespace LivanaiTiles.DateTime
{
    /// <summary>
    /// 現在の日付と時刻を表示するためのタイル
    /// </summary>
    public class DateTimeProvider : ITileProvider
    {
        /// <inheritdoc/>
        public Guid ProviderID => new Guid("{d240708a-b294-4b26-bc91-6aae2366e7fb}");

        /// <inheritdoc/>
        public string Name => "Date and Time";

        /// <summary>
        /// タイルに表示する日付のデフォルト書式
        /// </summary>
        public static readonly string DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// タイルに表示する曜日のデフォルト書式
        /// </summary>
        public static readonly string DEFAULT_WEEKDAY_FORMAT = "dddd";

        /// <summary>
        /// セッション保存時に対象となったタイルのID.
        /// 保存ディレクトリ内で管理対象外のディレクトリを検出するのに使う.
        /// </summary>
        private HashSet<Guid> SavedTileIDList { get; set; }

        /// <inheritdoc/>
        public bool InitProvider(IAppController controller)
        { return true; }

        /// <inheritdoc/>
        public TileInfo CreateTile(IAppController controller, string parameter)
        {
            return CreateDateTimeTile(
                controller,
                (parameter != null ? ParseConfig(parameter) : null));
        }

        /// <inheritdoc/>
        public TileInfo RestoreTile(IAppController controller, Guid windowID)
        {
            // このウィンドウの設定情報を取得する
            var saveDir = FileUtils.GetWindowConfigDirectory(this, windowID, false);
            var conf = LoadConfig(controller, saveDir);

            // 設定情報が存在した場合、これを元にViewModelを生成する
            DateTimeTileViewModel vm = null;
            if(conf != null)
            {
                vm = new DateTimeTileViewModel()
                {
                    DateFormat = conf.DateFormat,
                    WeekdayFormat = conf.WeekdayFormat,
                    FormatCulture = conf.Culture,
                };
            }

            var ti = CreateDateTimeTile(controller, null);
            ti.TileContent.DataContext = vm;

            return ti;
        }

        /// <summary>
        /// タイル情報を生成する
        /// </summary>
        /// <param name="controller">アプリケーションのコントローラ</param>
        /// <param name="vm">このタイルで表示するタイマーの設定情報. タイマーの設定なしで生成する場合はnull</param>
        /// <returns>生成したタイル情報</returns>
        public TileInfo CreateDateTimeTile(
            IAppController controller,
            DateTimeTileViewModel vm)
        {
            var ctl = new DateTimeTile();

            WindowControlUtils.ApplyTileControlStylle(
                new FrameworkElement[] {
                    ctl.lblTime,
                    ctl.lblSecond,
                    ctl.lblDate,
                    ctl.lblWeekday,
                    ctl.iconTimer,
                    ctl.lblTimer,
                },
                new SolidColorBrush(controller.Config.ForeGroundColor),
                new SolidColorBrush(Colors.Gray));

            // タイマーの情報が設定されている場合はここで渡す
            if (vm != null)
            { ctl.DataContext = vm; }

            return new TileInfo()
            {
                UseAeroGlass = null,
                BackGroundColor = null,
                WindowTitle = this.Name,
                TileContent = ctl,
                TileHeight = 150,
                TileWidth = 150,
            };
        }

        /// <summary>
        /// タイルの起動パラメータを解釈し、ViewModelを生成して返す
        /// </summary>
        /// <param name="param"></param>
        /// <returns>コマンドラインオプションを元に生成したパラメータ情報. 指定した内容が不正な場合は例外スロー</returns>
        private DateTimeTileViewModel ParseConfig(string param)
        {
            var argsDef = new CommandLineOptionDefinition[]
            {
                new CommandLineOptionDefinition(){ RequireParam = true, AllowMultiple = false, LongSwitch = "count" },
                new CommandLineOptionDefinition(){ RequireParam = true, AllowMultiple = false, LongSwitch = "time" },
                new CommandLineOptionDefinition(){ RequireParam = true, AllowMultiple = false, LongSwitch = "sound" },
            };

            var result = CommandLineUtils.Parse(CommandLineUtils.ParseCommandLineArgs(param), argsDef, false);
            if (result.Options.ContainsKey("count") && result.Options["count"].First().Trim().Length == 0)
            { throw new ArgumentException(Strings.ARGS_COUNT_INVALID); }
            if (result.Options.ContainsKey("time") && result.Options["time"].First().Trim().Length == 0)
            { throw new ArgumentException(Strings.ARGS_TIME_INVALID); }
            if (result.Options.ContainsKey("sound") && result.Options["sound"].First().Trim().Length == 0)
            { throw new ArgumentException(Strings.ARGS_SOUND_MISSING); }

            int countTime = 0;
            bool countdownMinute = false;
            bool countdownSecond = false;
            System.DateTime alarmTime = System.DateTime.MinValue;
            string alarmFileName = null;

            if (result.Options.ContainsKey("count"))
            {
                // パラメータの引数は
                // ・数値のみ
                // ・数値 + "s|S" (秒単位)
                // ・数値 + "m|M" (分単位)
                // のいずれか
                var arg = result.Options["count"].First();
                if (Regex.IsMatch(arg, "^[0-9]+$"))
                {
                    countdownSecond = true;
                    countTime = int.Parse(arg); 
                }
                else if (Regex.IsMatch(arg, "^[0-9]+(s|S)$"))
                {
                    countdownSecond = true;
                    countTime = int.Parse(arg.Substring(0, arg.Length - 1));
                }
                else if (Regex.IsMatch(arg, "^[0-9]+(m|M)$"))
                {
                    countdownMinute = true;
                    countTime = int.Parse(arg.Substring(0, arg.Length - 1));
                }
                else
                { throw new ArgumentException(Strings.ARGS_COUNT_INVALID); }

                // 指定された数値が0の場合はエラー
                if (countTime == 0)
                { throw new ArgumentException(Strings.ARGS_COUNT_INVALID); }
            }

            if(result.Options.ContainsKey("time"))
            {
                System.DateTime parsed = TimeParserUtil.Parse(result.Options["time"].First());
                if (System.DateTime.MinValue < parsed)
                { alarmTime = parsed; }
                else
                { throw new ArgumentException(Strings.ARGS_TIME_INVALID); }
            }

            if (result.Options.ContainsKey("sound"))
            {
                // 指定したファイルが存在しない、またはファイルではない場合はエラー
                var filePath = result.Options["sound"].First();
                var f = new FileInfo(filePath);
                if (!f.Exists)
                { throw new ArgumentException(Strings.ARGS_SOUND_FILE_NOT_FOUND.Replace("%FILE%", filePath)); }

                alarmFileName = filePath;
            }

            return new DateTimeTileViewModel()
            {
                UseTimer = (0 < countTime || System.DateTime.MinValue < alarmTime),
                UseTimerCountdown = (0 < countTime),
                UseTimerReminder = (0 == countTime && System.DateTime.MinValue < alarmTime),
                CountdownTime = countTime,
                CountdownTimeText = (0 < countTime ? countTime.ToString() : ""),
                CountdownMinutes = countdownMinute,
                CountdownSeconds = countdownSecond,
                ReminderTime = alarmTime,
                ReminderTimeText = (System.DateTime.MinValue < alarmTime ? alarmTime.ToString("HH:mm") : ""),
                RemainTime = 0,
                SoundFilePath = alarmFileName,
            };
        }

        /// <inheritdoc/>
        public void SaveProviderSession(IAppController controller)
        { }

        /// <inheritdoc/>
        public void SaveTileSession(
            IAppController controller,
            Guid windowID,
            UserControl tile)
        {
            var vm = tile.DataContext as DateTimeTileViewModel;
            var conf = new DateTimeConfig()
            {
                DateFormat = vm.DateFormat,
                WeekdayFormat = vm.WeekdayFormat,
                Culture = vm.FormatCulture,
            };

            // このタイルの設定情報を保存する
            var saveDir = FileUtils.GetWindowConfigDirectory(this, windowID, true);
            SaveConfg(controller, saveDir, conf);

            if (this.SavedTileIDList == null)
            { this.SavedTileIDList = new HashSet<Guid>(); }
            this.SavedTileIDList.Add(windowID);
        }

        /// <inheritdoc/>
        public void ShutdownProvider(IAppController controller)
        { }

        /// <inheritdoc/>
        public void ShutdownTile(IAppController controller, Guid windowID, UserControl tile)
        { }


        /// <summary>
        /// このタイルで表示する書式情報をロードする
        /// </summary>
        /// <param name="controller">コントローラオブジェクト</param>
        /// <param name="confDir">保存先のディレクトリ</param>
        /// <returns>書式情報</returns>
        private DateTimeConfig LoadConfig(
            IAppController controller,
            DirectoryInfo confDir)
        {
            var confFilePath = Path.Combine(confDir.FullName, "datetime.conf");
            if ((new FileInfo(confFilePath)).Exists)
            { return FileUtils.LoadFromJson<DateTimeConfig>(confFilePath, controller.Logger); }
            else
            { return null; }
        }

        /// <summary>
        /// このタイルの書式情報を保存する
        /// </summary>
        /// <param name="controller">コントローラオブジェクト</param>
        /// <param name="confDir">保存先のディレクトリ</param>
        /// <returns>書式情報</returns>
        private void SaveConfg(
            IAppController controller,
            DirectoryInfo saveDir,
            DateTimeConfig conf)
        {
            var confFilePath = Path.Combine(saveDir.FullName, "datetime.conf");
            FileUtils.SaveToJson(conf, confFilePath, controller.Logger);
        }
    }
}
