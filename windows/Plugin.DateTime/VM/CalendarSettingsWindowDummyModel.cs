﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.DateTime.VM
{
    /// <summary>
    /// カレンダータイルの設定ウィンドウのViewModel
    /// </summary>
    public class CalendarSettingsWindowDummyModel : CalendarSettingsWindowViewModel
    {
        public CalendarSettingsWindowDummyModel() : base()
        {
            this.TodayFormat = "dddd, yyyy-MM-dd";
            this.TodayCulture = null;
            this.MonthNameFormat = "MMMM";
            this.MonthNameCulture = null;
            this.Holidays.Add(new HolidayInfo() { Name = "Holiday Sample 1", });
            this.Holidays.Add(new HolidayInfo() { Name = "Holiday Sample 2", });
            this.Holidays.Add(new HolidayInfo() { Name = "Holiday Sample 3", });

            this.CurrentHoliday = new HolidayInfo()
            {
                Month = 3,
                Day = 3,
                Name = "Holiday Sample",
                Transfer = true,
                Week = 0,
                Weekday = DayOfWeek.Sunday,
            };
        }
    }
}
