﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.gui;
using LivanaiTiles.DateTime.Utils;

namespace LivanaiTiles.DateTime.VM
{
    /// <summary>
    /// カレンダータイルの設定ウィンドウのViewModel
    /// </summary>
    public class CalendarSettingsWindowViewModel : NotifiableViewModel
    {
        #region Fields

        private string _monthNameFormat;
        private string _monthNameCulture;
        private string _todayFormat;
        private string _todayCulture;
        private ObservableCollection<HolidayInfo> _holidays;
        private int _currentHolidayIndex;

        private HolidayInfo _currentHoliday;

        private bool _everyMonth;
        private bool _dayHoliday;
        private bool _weekHoliday;

        #endregion

        #region Properties

        /// <summary>
        /// タイル上に表示する今月の月名の書式文字列
        /// </summary>
        public string MonthNameFormat
        {
            get { return this._monthNameFormat; }
            set
            {
                this._monthNameFormat = value;
                OnPropertyChanged(nameof(MonthNameFormat));
                OnPropertyChanged(nameof(MonthSample));
            }
        }

        /// <summary>
        /// タイル上に表示する今月の月名のカルチャ名
        /// </summary>
        public string MonthNameCulture
        {
            get { return this._monthNameCulture; }
            set
            {
                this._monthNameCulture = value;
                OnPropertyChanged(nameof(MonthNameCulture));
                OnPropertyChanged(nameof(MonthSample));
            }
        }

        /// <summary>
        /// ツールチップに表示する今日の日付の書式文字列
        /// </summary>
        public string TodayFormat
        {
            get { return this._todayFormat; }
            set
            {
                this._todayFormat = value;
                OnPropertyChanged(nameof(TodayFormat));
                OnPropertyChanged(nameof(TodaySample));
            }
        }

        /// <summary>
        /// ツールチップに表示する今日の日付のカルチャ名
        /// </summary>
        public string TodayCulture
        {
            get { return this._todayCulture; }
            set
            {
                this._todayCulture = value;
                OnPropertyChanged(nameof(TodayCulture));
                OnPropertyChanged(nameof(TodaySample));
            }
        }

        /// <summary>
        /// 祝日情報
        /// </summary>
        public ObservableCollection<HolidayInfo> Holidays
        {
            get { return this._holidays; }
            set
            {
                this._holidays = value;
                OnPropertyChanged(nameof(Holidays));
            }
        }

        /// <summary>
        /// 現在編集中の祝日情報
        /// </summary>
        public HolidayInfo CurrentHoliday
        {
            get { return this._currentHoliday; }
            set
            {
                this._currentHoliday = value;

                // この時点で祝日の日付／週・曜日指定のラジオボタンの値を設定する
                if (value != null)
                {
                    this.IsDayHoliday = (value.Day != 0);
                    this.IsWeekdayHoliday = (value.Week != 0);

                    // 毎月指定の祝日であればチェックボックスをオンにする
                    this.IsEveryMonth = (value.Month == 0);
                }
                OnPropertyChanged(nameof(CurrentHoliday));
                OnPropertyChanged(nameof(EditingCurrentHoliday));
                OnPropertyChanged(nameof(WeekdaySelected));
            }
        }

        /// <summary>
        /// 現在編集中の祝日情報に設定されている曜日情報
        /// </summary>
        public bool[] WeekdaySelected
        {
            get
            {
                var result = new bool[7];
                for (int cnt = 0; cnt < 7; cnt++)
                { result[cnt] = (this._currentHoliday != null && (int)this._currentHoliday.Weekday == cnt); }

                return result;
            }
            set
            {
                for (int cnt = 0; cnt < 7; cnt++)
                {
                    if (this._currentHoliday != null && value[cnt])
                    { this._currentHoliday.Weekday = (DayOfWeek)cnt; }
                }
            }
        }

        /// <summary>
        /// Holidays に格納されている祝日情報中の現在選択中要素のインデックス
        /// </summary>
        public int CurrentHolidayIndex
        {
            get { return this._currentHolidayIndex; }
            set
            {
                this._currentHolidayIndex = value;
                this.CurrentHoliday = (
                    0 <= value && value < this.Holidays.Count ? this.Holidays[value] :
                    null);
                OnPropertyChanged(nameof(CurrentHolidayIndex));
            }
        }

        /// <summary>
        /// 祝日情報を現在編集中かどうか
        /// </summary>
        public bool EditingCurrentHoliday
        {
            get { return this.CurrentHoliday != null; }
        }
        
        /// <summary>
        /// 毎月の休日かどうか?
        /// </summary>
        public bool IsEveryMonth
        {
            get { return this._everyMonth; }
            set
            {
                this._everyMonth = value;
                if (value)
                { this.CurrentHoliday.Month = 0; }
                OnPropertyChanged(nameof(IsEveryMonth));
                OnPropertyChanged(nameof(EditableMonth));
            }
        }

        /// <summary>
        /// 毎月の休日である場合に月を入力不能にするためのプロパティ
        /// </summary>
        public bool EditableMonth
        { get { return !this.IsEveryMonth; } }

        /// <summary>
        /// 現在編集中の祝日情報が日付指定かどうか
        /// </summary>
        public bool IsDayHoliday
        {
            get { return this._dayHoliday; }
            set 
            {
                this._dayHoliday = value;
                OnPropertyChanged(nameof(IsDayHoliday));

                // 週指定の日付をリセットする
                if(value)
                {
                    this.CurrentHoliday.Week = 0;
                    this.CurrentHoliday.Weekday = DayOfWeek.Sunday;
                }
            }
        }

        /// <summary>
        /// 現在編集中の祝日情報が週、曜日による指定かどうか
        /// </summary>
        public bool IsWeekdayHoliday
        {
            get { return this._weekHoliday; }
            set
            {
                this._weekHoliday = value;
                OnPropertyChanged(nameof(IsWeekdayHoliday));

                // 日付指定をリセットする
                if(value)
                {
                    this.CurrentHoliday.Day = 0;
                }
            }
        }

        /// <summary>
        /// タイルに表示する今月の月名表記のサンプル
        /// </summary>
        public string MonthSample
        {
            get
            {
                return System.DateTime.Now
                    .Format(
                        this.MonthNameFormat,
                        this.MonthNameCulture,
                        CalendarProvider.DEFAULT_MONTH_FORMAT
                        );
            }
        }

        /// <summary>
        /// 今日の日付表記のサンプル
        /// </summary>
        public string TodaySample
        {
            get
            {
                return System.DateTime.Now
                    .Format(
                        this.TodayFormat,
                        this.TodayCulture,
                        CalendarProvider.DEFAULT_TODAY_FORMAT
                        );
            }
        }

        /// <summary>
        /// ViewModel で設定した設定情報
        /// </summary>
        public CalendarConfig Config
        {
            get
            {
                return new CalendarConfig()
                {
                    MonthNameFormat = this._monthNameFormat,
                    MonthNameCulture = this._monthNameCulture,
                    DateFormat = this._todayFormat,
                    TodayCulture = this._todayCulture,
                    Holidays = this._holidays.ToList(),
                };
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// 新規の祝日を追加するコマンド
        /// </summary>
        public RelayCommand AddHolidayCommand { get; private set; }

        /// <summary>
        /// 選択した祝日を削除するコマンド
        /// </summary>
        public RelayCommand RemoveHolidayCommand { get; private set; }

        #endregion

        /// <summary>
        /// コマンドの初期化
        /// </summary>
        private void InitCommands()
        {
            this.AddHolidayCommand = new RelayCommand(
                x => true,
                (x) =>
                {
                    // 新規の祝日情報を生成して追加
                    var newHoliday = new HolidayInfo()
                    {
                        Name = "",
                        Month = System.DateTime.Now.Month,
                        Day = System.DateTime.Now.Day,
                        Week = 0,
                        Transfer = false,
                        Weekday = 0,
                    };

                    this.Holidays.Add(newHoliday);
                    this.CurrentHolidayIndex = this.Holidays.Count - 1;
                });

            this.RemoveHolidayCommand = new RelayCommand(
                (x) => (0 <= this.CurrentHolidayIndex && this.CurrentHolidayIndex < this.Holidays.Count),
                (x) =>
                {
                    this.Holidays.RemoveAt(this.CurrentHolidayIndex);
                    this.CurrentHolidayIndex = -1;
                });
        }

        #region Constructors

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="conf">初期状態として渡す設定情報</param>
        public CalendarSettingsWindowViewModel(CalendarConfig conf) : this()
        {
            this._monthNameFormat = conf.MonthNameFormat;
            this._monthNameCulture = conf.MonthNameCulture;
            this._todayFormat = conf.DateFormat;
            this._todayCulture = conf.TodayCulture;
            foreach (var holiday in conf.Holidays)
            { this._holidays.Add(holiday); }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CalendarSettingsWindowViewModel()
        {
            this._holidays = new ObservableCollection<HolidayInfo>();
            InitCommands();
        }

        #endregion
    }
}
