﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.DateTime.VM
{
    /// <summary>
    /// 日付書式設定ウィンドウのViewModel
    /// (レイアウト確認用のダミー)
    /// </summary>
    public class DateTimeSettingsWindowDummyModel : DateTimeSettingsWindowViewModel
    {
        public DateTimeSettingsWindowDummyModel():base()
        {
            this.FormatCulture = "ja-jp";
            this.DateFormat = "yyyy/mm/dd";
            this.WeekdayFormat = "dddd";
        }
    }
}
