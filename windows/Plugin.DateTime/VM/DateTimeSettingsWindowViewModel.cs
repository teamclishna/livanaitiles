﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.gui;

namespace LivanaiTiles.DateTime.VM
{
    /// <summary>
    /// 日付書式設定ウィンドウのViewModel
    /// </summary>
    public class DateTimeSettingsWindowViewModel : NotifiableViewModel
    {
        #region Fields

        private string _dateFormat;
        private string _weekdayFormat;
        private string _formatculture;

        private bool _isDateValid;
        private bool _isWeekdayValid;

        #endregion

        /// <summary>
        /// 日付の書式文字列
        /// </summary>
        public string DateFormat
        {
            get { return this._dateFormat; }
            set
            {
                this._dateFormat = value;
                OnPropertyChanged(nameof(DateFormat));
                OnPropertyChanged(nameof(FormattedDate));
            }
        }

        /// <summary>
        /// 曜日の書式文字列
        /// </summary>
        public string WeekdayFormat
        {
            get { return this._weekdayFormat; }
            set
            {
                this._weekdayFormat = value;
                OnPropertyChanged(nameof(WeekdayFormat));
                OnPropertyChanged(nameof(FormattedWeekday));
            }
        }

        /// <summary>
        /// 書式化の際のカルチャ指定
        /// </summary>
        public string FormatCulture
        {
            get { return this._formatculture; }
            set
            {
                this._formatculture = value;
                OnPropertyChanged(nameof(FormatCulture));
                OnPropertyChanged(nameof(FormattedDate));
                OnPropertyChanged(nameof(FormattedWeekday));
            }
        }

        /// <summary>
        /// 書式化のサンプルにする時刻
        /// </summary>
        public System.DateTime SourceDateTime { get; set; }

        /// <summary>
        /// 指定したカルチャ、書式化文字列で書式化された日付
        /// </summary>
        public string FormattedDate
        {
            get
            {
                try
                {
                    var formatString = (
                        !string.IsNullOrWhiteSpace(this.DateFormat) ? this.DateFormat :
                        DateTimeProvider.DEFAULT_DATETIME_FORMAT);

                    var culture = (
                        this.FormatCulture != null ? new System.Globalization.CultureInfo(this.FormatCulture) :
                        System.Globalization.CultureInfo.CurrentCulture);

                    var result = this.SourceDateTime.ToString(formatString, culture);

                    this._isDateValid = true;
                    OnPropertyChanged(nameof(IsValid));
                    return result;
                }
                catch (Exception)
                {
                    this._isDateValid = false;
                    OnPropertyChanged(nameof(IsValid));
                    return "ERROR"; 
                }
            }
        }

        /// <summary>
        /// 指定したカルチャ、書式化文字列で書式化された曜日
        /// </summary>
        public string FormattedWeekday
        {
            get
            {
                try
                {
                    var formatString = (
                        !string.IsNullOrWhiteSpace(this.WeekdayFormat) ? this.WeekdayFormat :
                        DateTimeProvider.DEFAULT_WEEKDAY_FORMAT);

                    var culture = (
                        this.FormatCulture != null ? new System.Globalization.CultureInfo(this.FormatCulture) :
                        System.Globalization.CultureInfo.CurrentCulture);

                    var result = this.SourceDateTime.ToString(formatString, culture);

                    this._isWeekdayValid = true;
                    OnPropertyChanged(nameof(IsValid));
                    return result;
                }
                catch (Exception)
                {
                    this._isWeekdayValid = false;
                    OnPropertyChanged(nameof(IsValid));
                    return "ERROR";
                }
            }
        }

        /// <summary>
        /// 現在の書式、カルチャ設定が正常に設定されているかどうか
        /// </summary>
        public bool IsValid
        { get { return this._isDateValid && this._isWeekdayValid; } }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DateTimeSettingsWindowViewModel()
        {
            this.SourceDateTime = System.DateTime.Now;
            this._isDateValid = true;
            this._isWeekdayValid = true;
        }
    }
}
