﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LivanaiTiles.DateTime.Windows
{
    /// <summary>
    /// CalendarSettingsWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class CalendarSettingsWindow : Window
    {
        public CalendarSettingsWindow()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void optionWeekday_Checked(object sender, RoutedEventArgs e)
        {
            var radioButtons =new RadioButton[] {
                this.optionWeekdaySun ,
                this.optionWeekdayMon,
                this.optionWeekdayTue,
                this.optionWeekdayWed,
                this.optionWeekdayThu,
                this.optionWeekdayFri,
                this.optionWeekdaySat,
            };

            var context = this.DataContext as DateTime.VM.CalendarSettingsWindowViewModel;
            for (var n = 0; n < radioButtons.Length; n++)
            {
                if (sender.Equals(radioButtons[n]))
                { context.CurrentHoliday.Weekday = (DayOfWeek)n; }
            }
        }
    }
}
