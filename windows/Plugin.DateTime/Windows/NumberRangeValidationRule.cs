﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace LivanaiTiles.DateTime.Windows
{
    /// <summary>
    /// 数値の範囲入力を行うための入力ルール
    /// </summary>
    public class NumberRangeValidationRule:ValidationRule
    {
        /// <summary>
        /// 入力ルールの対象とする項目の名前
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 入力可能な数値の最小値
        /// </summary>
        public int MinValue { get; set; }

        /// <summary>
        /// 入力可能な数値の最大値
        /// </summary>
        public int MaxValue { get; set; }

        /// <inheritdoc/>
        public override ValidationResult Validate(
            object value,
            CultureInfo cultureInfo)
        {
            if (int.TryParse(value as string, out int m) && this.MinValue <= m && m <= this.MaxValue)
            { return ValidationResult.ValidResult; }
            else
            { return new ValidationResult(false, $"{this.Name} can set between {this.MinValue} and {this.MaxValue}"); }
        }
    }
}
