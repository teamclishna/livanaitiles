﻿using LivanaiTiles.DateTime.VM;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LivanaiTiles.DateTime.Windows
{
    /// <summary>
    /// TimerSettingWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class TimerSettingWindow : Window
    {
        public TimerSettingWindow()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            // タイマーを有効化して閉じる
            var context = (DateTimeTileViewModel)this.DataContext;
            context.UseTimer = true;

            this.DialogResult = true;
            this.Close();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            // 現在実行中のタイマーを無効化してから閉じる
            var context = (DateTimeTileViewModel)this.DataContext;
            context.UseTimer = false;

            this.DialogResult = true;
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
