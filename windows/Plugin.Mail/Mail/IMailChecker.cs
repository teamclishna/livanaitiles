﻿using System;
using System.Collections.Generic;
using System.Text;

using clUtils.log;

namespace LivanaiTiles.Mail.Mail
{
    /// <summary>
    /// メールチェック処理の結果を示すエラーコード
    /// </summary>
    public enum MailCheckResult
    {
        /// <summary>正常終了</summary>
        SUCCEEDED,
        /// <summary>接続時に ”＋OK” のプロンプトが返されなかった</summary>
        CONNECTION_FAILED,
        /// <summary>ユーザ名、またはパスワードのレスポンスとしてエラーが返された</summary>
        AUTH_FAILED,
        /// <summary>メール数の取得でエラーが返された</summary>
        GET_MAIL_COUNT_FAILED,
        /// <summary>UIDLコマンドに対してエラーが返された</summary>
        GET_UIDL_FAILED,
        /// <summary>TOPコマンドでメールヘッダを取得しようとしたときにエラーが返された</summary>
        GET_MAIL_HEADER_FAILED,
    }

    /// <summary>
    /// メールチェック機能のインターフェイス
    /// </summary>
    public interface IMailChecker
    {
        /// <summary>
        /// ロガーオブジェクト
        /// </summary>
        ILogger Logger { get; set; }

        /// <summary>
        /// メールアカウント情報
        /// </summary>
        MailAccount Account { get; set; }

        /// <summary>
        /// 直前に呼び出したFetchコマンドの結果
        /// </summary>
        MailCheckResult Result { get; set; }

        /// <summary>
        /// メールボックスの内容.
        /// 前回取得したメールと比較し、今回の新規メールだけをフェッチする場合
        /// Fetchメソッド呼び出し前にここに前回取得した内容を格納しておく.
        /// </summary>
        IEnumerable<MailInfo> FetchResult { get; set; }

        /// <summary>
        /// メールを取得する
        /// </summary>
        /// <returns>
        /// メールサーバにアクセスした処理の結果.
        /// この結果はResultプロパティにも格納される.
        /// 取得したメールはFetchResultプロパティに格納する.
        /// </returns>
        MailCheckResult Fetch();
    }
}
