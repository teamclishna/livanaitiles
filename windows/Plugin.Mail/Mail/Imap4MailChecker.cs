﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using clUtils.io;
using clUtils.log;
using clUtils.util;

namespace LivanaiTiles.Mail.Mail
{
    /// <summary>
    /// IMAP4でメールを取得するためのオブジェクト
    /// </summary>
    public class Imap4MailChecker : IMailChecker
    {
        /// <summary>ソケット通信のデフォルトタイムアウト時間(ミリ秒単位)</summary>
        public static readonly int DEFAULT_TIMEOUT_TIME = 30 * 1000;
        /// <summary>ソケットから受け取ったデータを格納するためのバッファ長</summary>
        public static readonly int RECEIVE_BUFFER_LENGTH = 4096;
        /// <summary>レスポンス文字列中の改行文字</summary>
        public static readonly string[] NEWLINE = new string[] { "\r\n" };

        /// <inheritdoc/>
        public ILogger Logger { get; set; }
        /// <inheritdoc/>
        public MailAccount Account { get; set; }
        /// <inheritdoc/>
        public MailCheckResult Result { get; set; }
        /// <inheritdoc/>
        public IEnumerable<MailInfo> FetchResult { get; set; }

        /// <summary>
        /// コマンド発行時のタグにつける連番
        /// </summary>
        private int Seq { get; set; }

        /// <summary>
        /// コマンド発行時につけるタグを生成して返す
        /// </summary>
        /// <returns></returns>
        private string GetCommandTag()
        { return $"A{this.Seq++:000}"; }

        /// <inheritdoc/>
        public MailCheckResult Fetch()
        {
            var tcpClient = new TcpClient()
            {
                ReceiveTimeout = DEFAULT_TIMEOUT_TIME,
                SendTimeout = DEFAULT_TIMEOUT_TIME,
            };

            // 前回取得時の内容を保持しておく
            var prevMailbox = (
                this.FetchResult != null ? this.FetchResult :
                new List<MailInfo>());

            try
            {
                tcpClient.Connect(
                    this.Account.ServerName,
                    this.Account.Port);

                var streamBuffer = new NetworkStreamBuffer(RECEIVE_BUFFER_LENGTH) { Logger = this.Logger, };
                using (var stream = tcpClient.GetStream())
                {
                    if (!ReceivePrompt(stream, ref streamBuffer, this.Logger))
                    { return MailCheckResult.CONNECTION_FAILED; }

                    try
                    {
                        // LOGIN
                        if (!AuthUser(this.Account.UserName, this.Account.Password, stream, ref streamBuffer, this.Logger))
                        { return MailCheckResult.AUTH_FAILED; }

                        // メールボックスを移動
                        if (!SelectMailbox("INBOX", stream, ref streamBuffer, this.Logger))
                        {
                            Logout(stream, ref streamBuffer, this.Logger);
                            return MailCheckResult.GET_MAIL_HEADER_FAILED;
                        }

                        // メッセージを取得する
                        var fetchedMailList = new List<MailInfo>();
                        var messageIDList = SearchMail(stream, ref streamBuffer, this.Logger);

                        foreach (var messageID in messageIDList)
                        {
                            var fetchedMail = FetchHeader(int.Parse(messageID), stream, ref streamBuffer, this.Logger);
                            fetchedMailList.Add(fetchedMail);
                        }

                        // LOGOUT
                        Logout(stream, ref streamBuffer, this.Logger);

                        this.FetchResult = fetchedMailList;
                        return MailCheckResult.SUCCEEDED;
                    }
                    catch (Exception)
                    {
                        return MailCheckResult.CONNECTION_FAILED;
                    }
                }
            }
            finally
            { }
        }

        /// <summary>
        /// IMAPサーバに接続したときに最初に返される"* OK"を待つ
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        /// <returns>接続に成功して "* OK" が返された場合はtrue</returns>
        private static bool ReceivePrompt(
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var responce = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
            logger.Print(LogLevel.DEBUG, string.Format("RECV>{0}", responce));
            return responce.StartsWith("* OK");
        }

        /// <summary>
        /// ユーザ名とパスワードを送信する
        /// </summary>
        /// <param name="userName">ユーザ名</param>
        /// <param name="password">パスワード</param>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        /// <returns>認証に成功して "+OK" が返された場合はtrue</returns>
        private bool AuthUser(
            string userName,
            string password,
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var tag = GetCommandTag();
            var command = $"{tag} LOGIN {userName} {password}\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);

            var responces = ReceiveImap4Responce(tag, stream, ref streamBuffer, logger);
            var responce = GetResponceCode(responces);

            return (responce == "OK");
        }

        /// <summary>
        /// メールボックスを移動する
        /// </summary>
        /// <returns></returns>
        private bool SelectMailbox(
            string mailboxName,
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var tag = GetCommandTag();
            var command = $"{tag} SELECT \"{mailboxName}\"\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);

            var responces = ReceiveImap4Responce(tag, stream, ref streamBuffer, logger);
            var responce = GetResponceCode(responces);

            return (responce == "OK");
        }

        /// <summary>
        /// メールボックス内のメール情報を取得し、そのメッセージ番号を格納した配列を返す
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        private IEnumerable<string> SearchMail(
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var tag = GetCommandTag();
            var command = $"{tag} SEARCH ALL\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);

            var responces = ReceiveImap4Responce(tag, stream, ref streamBuffer, logger);
            var responce = GetResponceCode(responces);

            if (responce == "OK")
            { return GetMessageID(responces); }
            else
            { return new string[] { }; }
        }

        /// <summary>
        /// 指定したメッセージ番号のヘッダ情報を取得して送信者とタイトルを取得する
        /// </summary>
        /// <param name="messageNumber"></param>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        private MailInfo FetchHeader(
            int messageNumber,
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            // メールヘッダを RFC822 形式で取得し、送信者とタイトル部分を取り出す
            var tag = GetCommandTag();
            var command = $"{tag} FETCH {messageNumber} RFC822.HEADER\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);

            var responces = ReceiveImap4Responce(tag, stream, ref streamBuffer, logger);
            var responce = GetResponceCode(responces);

            if (responce == "OK")
            {
                // メールヘッダの各項目を辞書に格納、その中から必要な項目を取得して返す
                // 日本語を含む可能性のあるヘッダ項目は
                // 普通BASE64、QPのどちらか一方が使われているので両方のデコードを試みてみる
                // 使われていない方は対応する開始文字列が無いのでそのまま返す
                var headers = CreateHeaderDictionary(responces);
                return new MailInfo()
                {
                    From = (headers.ContainsKey("from") ? headers["from"].Base64ToString().QPToString() : ""),
                    Subject = (headers.ContainsKey("subject") ? headers["subject"].Base64ToString().QPToString() : ""),
                    MailID = (headers.ContainsKey("message-id") ? headers["message-id"].Base64ToString().QPToString() : ""),
                };
            }
            else
            { return null; }

        }

        /// <summary>
        /// ログアウトする
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        private void Logout(
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var tag = GetCommandTag();
            var command = $"{tag} LOGOUT\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);

            // ログアウト時はタグ無し行が1行来て終わり
            var responceLine = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
            logger.Print(LogLevel.DEBUG, $"RECV<{responceLine}");
        }

        /// <summary>
        /// 指定したタグの終端が現れるまでレスポンスを読み込む
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        private string[] ReceiveImap4Responce(
            string tag,
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            logger.Print(LogLevel.DEBUG, $"Responce for tag {tag}");

            var result = new List<string>();
            var responceLine = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
            logger.Print(LogLevel.DEBUG, $"RECV<{responceLine}");
            while (!responceLine.StartsWith(tag))
            {
                result.Add(responceLine);
                responceLine = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
                logger.Print(LogLevel.DEBUG, $"RECV<{responceLine}");
            }
            result.Add(responceLine);

            logger.PrintPop();
            return result.ToArray();
        }

        /// <summary>
        /// 指定したレスポンスの最終行からレスポンス文字列 (OK, ERR, NG, etc...) を取得する
        /// </summary>
        /// <param name="responces"></param>
        /// <returns></returns>
        private string GetResponceCode(string[] responces)
        {
            var lastLine = responces[responces.Length - 1];
            var sentenecs = lastLine.Split(new char[] { ' ' });
            if (2 < sentenecs.Length)
            { return sentenecs[1]; }
            else
            { return ""; }
        }

        /// <summary>
        /// Search コマンドのレスポンスからメッセージ番号を返す
        /// </summary>
        /// <param name="responces"></param>
        /// <returns></returns>
        private IEnumerable<string> GetMessageID(string[] responces)
        {
            var messageIDs = new List<string>();
            foreach (var line in responces)
            {
                // "* SEARCH" で始まるタグ無し行が対象
                if (line.ToLower().StartsWith("* search"))
                {
                    var values = line.Split(new char[] { ' ' });
                    for (var index = 2; index < values.Length; index++)
                    { messageIDs.Add(values[index]); }
                }
            }

            return messageIDs;
        }
        /// <summary>
        /// 行ごとの配列として取得したメールヘッダを、ヘッダ名をキー、本文を値とした辞書に変換して返す.
        /// ヘッダ名は小文字に変換して格納する
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        private static Dictionary<string, string> CreateHeaderDictionary(string[] headers)
        {
            var result = new Dictionary<string, string>();
            foreach (string line in headers)
            {
                if (0 < line.IndexOf(':'))
                {
                    var pos = line.IndexOf(':');
                    var key = line.Substring(0, pos).Trim().ToLower();
                    var value = line.Substring(pos + 1).Trim();
                    //キーが重複する場合は最初の1つを格納する
                    //※そうするとReceivedヘッダが格納できないがとりあえず置いておく
                    if (!result.ContainsKey(key))
                    { result.Add(key, value); }
                }
            }
            return result;
        }
    }
}
