﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

using clUtils.io;
using clUtils.log;
using clUtils.util;

namespace LivanaiTiles.Mail.Mail
{
    /// <summary>
    /// POP3でメールを取得するためのオブジェクト
    /// </summary>
    public class Pop3MailChecker : IMailChecker
    {
        /// <summary>ソケット通信のデフォルトタイムアウト時間(ミリ秒単位)</summary>
        public static readonly int DEFAULT_TIMEOUT_TIME = 30 * 1000;
        /// <summary>ソケットから受け取ったデータを格納するためのバッファ長</summary>
        public static readonly int RECEIVE_BUFFER_LENGTH = 4096;
        /// <summary>レスポンス文字列中の改行文字</summary>
        public static readonly string[] NEWLINE = new string[] { "\r\n" };

        /// <inheritdoc />
        public ILogger Logger { get; set; }
        /// <inheritdoc />
        public MailAccount Account { get; set; }
        /// <inheritdoc />
        public MailCheckResult Result { get; set; }
        /// <inheritdoc />
        public IEnumerable<MailInfo> FetchResult { get; set; }

        /// <inheritdoc />
        public MailCheckResult Fetch()
        {
            
            var tcpClient = new TcpClient()
            {
                ReceiveTimeout = DEFAULT_TIMEOUT_TIME,
                SendTimeout = DEFAULT_TIMEOUT_TIME,
            };

            // 前回取得時の内容を保持しておく
            var prevMailbox = (
                this.FetchResult != null ? this.FetchResult :
                new List<MailInfo>());

            try
            {
                tcpClient.Connect(
                    this.Account.ServerName,
                    this.Account.Port);

                var streamBuffer = new NetworkStreamBuffer(RECEIVE_BUFFER_LENGTH) { Logger = this.Logger, };
                using (var stream = tcpClient.GetStream())
                {
                    if (!ReceivePrompt(stream, ref streamBuffer, this.Logger))
                    { return MailCheckResult.CONNECTION_FAILED; }

                    try
                    {
                        //USER, PASS
                        if (!AuthUser(this.Account.UserName, this.Account.Password, stream, ref streamBuffer, this.Logger))
                        { return MailCheckResult.AUTH_FAILED; }

                        //STAT
                        var mailCount = GetMailCount(stream, ref streamBuffer, this.Logger);
                        if (mailCount < 0)
                        { return MailCheckResult.GET_MAIL_COUNT_FAILED; }

                        //メールボックス内のメールの内容を確認し、新着メールを判定する
                        var maxFetchCount = (mailCount <= this.Account.MaxFetchCount ? mailCount : this.Account.MaxFetchCount);
                        var fetchedMailList = new List<MailInfo>();
                        for (var index = 1; index <= maxFetchCount; index++)
                        {
                            //UIDL
                            var mailItem = new MailInfo();
                            var uidl = GetMailUidl(index, stream, ref streamBuffer, this.Logger);
                            if (uidl == null)
                            { return MailCheckResult.GET_UIDL_FAILED; }
                            else
                            { mailItem.MailID = uidl; }

                            //TOP
                            var headers = GetMailHeader(index, stream, ref streamBuffer, this.Logger);
                            if (headers.Length == 0)
                            { return MailCheckResult.GET_MAIL_HEADER_FAILED; }
                            else
                            {
                                // 日本語を含むヘッダをデコードする
                                // 普通BASE64、QPのどちらか一方が使われているので両方のデコードを試みてみる
                                // 使われていない方は対応する開始文字列が無いのでそのまま返す
                                var headerDictionary = CreateHeaderDictionary(headers);
                                if (headerDictionary.ContainsKey("from"))
                                {
                                    mailItem.From = headerDictionary["from"]
                                        .Base64ToString()
                                        .QPToString(); 
                                }
                                if (headerDictionary.ContainsKey("subject"))
                                {
                                    mailItem.Subject = headerDictionary["subject"]
                                        .Base64ToString()
                                        .QPToString();
                                }
                            }

                            fetchedMailList.Add(mailItem);
                        }

                        //CheckNewMail(prevMailBox, account.mails);
                        this.FetchResult = fetchedMailList;
                        return MailCheckResult.SUCCEEDED;
                    }
                    catch (Exception eUnknown)
                    {
                        this.Logger.Print(LogLevel.DEBUG, "例外発生： " + eUnknown.ToString() + "\n" + eUnknown.StackTrace.ToString());
                        return MailCheckResult.CONNECTION_FAILED;
                    }
                    finally
                    {
                        //最初のプロンプト表示まで成功していた場合は処理終了時に必ずQUITコマンドを送るようにする
                        Quit(stream, ref streamBuffer, this.Logger);
                    }
                }
            }
            catch (Exception eUnknown)
            {
                this.Logger.Print(LogLevel.ERROR, eUnknown.Message);
                this.Logger.Print(LogLevel.ERROR, eUnknown.StackTrace);
                this.Result = MailCheckResult.CONNECTION_FAILED;
                return this.Result;
            }
            finally
            { }
        }

        /// <summary>
        /// STATコマンドのレスポンスとして返された文字列からメール数を取得する。
        /// 具体的には "+OK n m" のn部分を取得する
        /// </summary>
        /// <param name="src">STATコマンドのレスポンス</param>
        /// <returns>メール数</returns>
        private static int ParseMailCount(string src)
        {
            char[] delim = { ' ' };

            var saTmp = src.Split(delim);
            //文字列が想定外の内容の場合、0を返す
            if (saTmp.Length != 3)
            { return 0; }

            try
            { return int.Parse(saTmp[1]); }
            catch (Exception)
            { return 0; }
        }

        /// <summary>
        /// UIDLコマンドのレスポンスとして返された文字列からUIDLを取得する
        /// </summary>
        /// <param name="src">UIDLコマンドのレスポンス</param>
        /// <returns>UIDL</returns>
        private static string ParseUIDL(string src)
        {
            char[] delim = { ' ' };

            var saTmp = src.Split(delim);
            //文字列が想定外の内容の場合、空文字列を返す
            if (saTmp.Length != 3)
            { return ""; }

            try
            { return saTmp[2].Trim(); }
            catch (Exception)
            { return ""; }
        }

        /// <summary>
        /// メールのヘッダ部分を受信する
        /// </summary>
        /// <param name="maxCount">受信するメールの最大数</param>
        /// <param name="stream">受信に使用するストリーム</param>
        /// <param name="taskScheduler">UIスレッドのタスクスケジューラ</param>
        /// <returns></returns>
        private static List<MailInfo> GetMailItems(
            int maxCount,
            NetworkStream stream,
            TaskScheduler taskScheduler)
        {
            var streamBuffer = new NetworkStreamBuffer(RECEIVE_BUFFER_LENGTH);
            var result = new List<MailInfo>();
            var logger = LoggerFactory.CreateLogger(LoggerName.ConsoleLogger);


            for (int cnt = 1; cnt <= maxCount; cnt++)
            {
                var newItem = new MailInfo();

                //UIDLを取得する
                Task.Factory.StartNew(() =>
                {
                    var command = $"UIDL {cnt.ToString()}\r\n";
                    logger.Print(LogLevel.DEBUG, $"SEND>{command}");
                    NetworkStreamUtil.SendAsciiString(stream, command);

                    var responce = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
                    logger.Print(LogLevel.DEBUG, $"RECV>{responce}");
                    if (responce[0] == '-')
                    { throw new Exception(); }

                    newItem.MailID = ParseUIDL(responce);
                },
                    CancellationToken.None,
                    TaskCreationOptions.None,
                    taskScheduler);

                //メールアイテムをリストに加える
                result.Add(newItem);
            }

            //取得したメール一覧を返す
            return result;
        }


        /// <summary>
        /// POP3サーバに接続したときに最初に返される"+OK"を待つ
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        /// <returns>接続に成功して "+OK" が返された場合はtrue</returns>
        private static bool ReceivePrompt(
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var responce = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
            logger.Print(LogLevel.DEBUG, string.Format("RECV>{0}", responce));
            return (responce[0] == '+');
        }

        /// <summary>
        /// ユーザ名とパスワードを送信する
        /// </summary>
        /// <param name="userName">ユーザ名</param>
        /// <param name="password">パスワード</param>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        /// <returns>認証に成功して "+OK" が返された場合はtrue</returns>
        private static bool AuthUser(
            string userName,
            string password,
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            //ユーザ名を送る
            var command = $"USER {userName}\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);

            var responce = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
            logger.Print(LogLevel.DEBUG, $"RECV<{responce}");
            if (responce[0] == '-')
            { return false; }

            //パスワードを送る
            command = $"PASS {password}\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);
            responce = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
            if (responce[0] == '-')
            { return false; }

            return true;
        }

        /// <summary>
        /// メールボックス内のメールの件数を取得する
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        /// <returns>STATコマンドの結果として返されたメールの件数. エラーが返された場合は負の値を返す</returns>
        private static int GetMailCount(
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var command = "STAT\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);

            var responce = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
            logger.Print(LogLevel.DEBUG, $"RECV<{responce}");

            return (responce[0] == '-' ? -1 : ParseMailCount(responce));
        }

        /// <summary>
        /// メールボックス中の指定したインデックスのメールのUIDLを取得する
        /// </summary>
        /// <param name="index">メールボックス内の1から始まるインデックス</param>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        /// <returns>UIDLとして返された文字列. エラーが返された場合はnull</returns>
        private static string GetMailUidl(
            int index,
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var command = $"UIDL {index.ToString()}\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);

            var responce = NetworkStreamUtil.ReceiveSingleLineString(stream, ref streamBuffer);
            logger.Print(LogLevel.DEBUG, $"RECV<{responce}");

            return (responce[0] == '-' ? null : ParseUIDL(responce));
        }

        /// <summary>
        /// メールボックス中の指定したインデックスのメールのヘッダ部分を取得する
        /// </summary>
        /// <param name="index">メールボックス内の1から始まるインデックス</param>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        /// <returns>TOPコマンドの結果として返されたメールのヘッダ部分. エラーが返された場合は空の配列</returns>
        private static string[] GetMailHeader(
            int index,
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var command = $"TOP {index.ToString()} 0\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);

            var responce = NetworkStreamUtil.ReceiveMultiLineString(stream, ref streamBuffer, ".\r\n");
            logger.Print(LogLevel.DEBUG, $"RECV<{responce}");

            //複数行の文字列で取得するので、これを行単位に分割
            return (responce[0] == '-' ? new string[0] : responce.Split(NEWLINE, StringSplitOptions.RemoveEmptyEntries));
        }

        /// <summary>
        /// 行ごとの配列として取得したメールヘッダを、ヘッダ名をキー、本文を値とした辞書に変換して返す.
        /// ヘッダ名は小文字に変換して格納する
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        private static Dictionary<string, string> CreateHeaderDictionary(string[] headers)
        {
            var result = new Dictionary<string, string>();
            foreach (string line in headers)
            {
                if (0 < line.IndexOf(':'))
                {
                    var pos = line.IndexOf(':');
                    var key = line.Substring(0, pos).Trim().ToLower();
                    var value = line.Substring(pos + 1).Trim();
                    //キーが重複する場合は最初の1つを格納する
                    //※そうするとReceivedヘッダが格納できないがとりあえず置いておく
                    if (!result.ContainsKey(key))
                    { result.Add(key, value); }
                }
            }
            return result;
        }

        /// <summary>
        /// QUITコマンドを送信してメールサーバとの通信を終了する
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="streamBuffer"></param>
        /// <param name="logger"></param>
        private static void Quit(
            NetworkStream stream,
            ref NetworkStreamBuffer streamBuffer,
            ILogger logger)
        {
            var command = "QUIT\r\n";
            logger.Print(LogLevel.DEBUG, $"SEND>{command}");
            NetworkStreamUtil.SendAsciiString(stream, command);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Pop3MailChecker()
        {
            this.Logger = LoggerFactory.CreateLogger(LoggerName.NullLogger);
        }
    }
}
