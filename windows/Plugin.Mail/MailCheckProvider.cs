﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.ActiveDirectory;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Xml.Schema;
using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.Mail.Mail;
using LivanaiTiles.Mail.VM;
using LivanaiTiles.Plugins;
using LivanaiTiles.Utils;

namespace LivanaiTiles.Mail
{
    /// <summary>
    /// メールチェックと新着メールの通知を行うタイル
    /// </summary>
    public class MailCheckProvider : ITileProvider
    {
        /// <inheritdoc/>
        public Guid ProviderID => new Guid("{1E077960-54C6-40A1-9C03-D785A90E1283}");

        /// <inheritdoc/>
        public string Name => "MailCheck";

        private ILogger Logger { get; set; }

        /// <summary>
        /// セッション保存時に対象となったタイルのID.
        /// 保存ディレクトリ内で管理対象外のディレクトリを検出するのに使う.
        /// </summary>
        private HashSet<Guid> SavedTileIDList { get; set; }

        /// <inheritdoc/>
        public TileInfo CreateTile(IAppController controller, string parameter)
        {
            var ctl = new MailCheckerTile() { };

            WindowControlUtils.ApplyTileControlStylle(
                new FrameworkElement[] {
                    ctl.lblTitle,
                    ctl.lblIcon,
                    ctl.btnSettings,
                    ctl.txtCurrentSubject,
                    ctl.txtCurrentMailCount,
                    ctl.txtOf,
                    ctl.txtMaxMailCount,
                },
                new SolidColorBrush(controller.Config.ForeGroundColor),
                new SolidColorBrush(Colors.Gray));

            return new TileInfo()
            {
                UseAeroGlass = null,
                BackGroundColor = null,
                WindowTitle = this.Name,
                TileContent = ctl,
                TileHeight = 150,
                TileWidth = 310,
            };
        }

        /// <inheritdoc/>
        public bool InitProvider(IAppController controller)
        {
            this.Logger = controller.Logger;
            return true;
        }

        /// <inheritdoc/>
        public TileInfo RestoreTile(IAppController controller, Guid windowID)
        {
            var saveDir = FileUtils.GetWindowConfigDirectory(this, windowID, false);
            var accountInfo = LoadAccountInfo(controller, saveDir);
            if (accountInfo != null)
            {
                var ctl = new MailCheckerTile() { };

                WindowControlUtils.ApplyTileControlStylle(
                    new FrameworkElement[] {
                        ctl.lblTitle,
                        ctl.lblIcon,
                        ctl.btnSettings,
                        ctl.txtCurrentSubject,
                        ctl.txtCurrentMailCount,
                        ctl.txtOf,
                        ctl.txtMaxMailCount,
                    },
                    new SolidColorBrush(controller.Config.ForeGroundColor),
                    new SolidColorBrush(Colors.Gray));

                var vm = ctl.DataContext as MailCheckerTileViewModel;
                vm.MailInfo.AccountName = accountInfo.AccountName;
                vm.MailInfo.Account = accountInfo.Account;

                // この時点で初回のメールチェックを行っておく
                vm.CheckMail(ctl);

                return new TileInfo()
                {
                    UseAeroGlass = null,
                    BackGroundColor = null,
                    WindowTitle = vm.MailInfo.AccountName,
                    TileContent = ctl,
                    TileHeight = 150,
                    TileWidth = 310,
                };
            }
            else
            {
                // 復元するためのファイルが存在しない場合は初期情報で生成する
                return CreateTile(controller, null);
            }
        }

        /// <inheritdoc/>
        public void SaveProviderSession(IAppController controller)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"[RssReaderProvider] save provider session");

            if (this.SavedTileIDList == null)
            { this.SavedTileIDList = new HashSet<Guid>(); }

            // 保存したタイルID以外の名前を持つディレクトリは削除
            foreach (var child in FileUtils.GetProviderConfigDirectory(this, true).GetDirectories())
            {
                try
                {
                    var dirID = new Guid(child.Name);
                    if (!this.SavedTileIDList.Contains(dirID))
                    {
                        // 現在存在しているタイルのID以外のディレクトリは削除する
                        this.Logger.PrintPush(LogLevel.DEBUG, $"[MailChecker] remove directory removed tile id [{child.FullName}]");
                        child.Delete(true);
                        this.Logger.PrintPop();
                    }
                }
                catch (Exception)
                {
                    // GUIDとして認識できないディレクトリ名や削除に失敗したディレクトリは無視
                }
            }
            this.Logger.PrintPop();
        }

        /// <inheritdoc/>
        public void SaveTileSession(
            IAppController controller,
            Guid windowID,
            UserControl tile)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"[MailChecker] save tile session id {windowID.ToString()}");
            var vm = tile.DataContext as MailCheckerTileViewModel;

            // タイルに設定したアカウント情報を保存する
            if (vm.MailInfo != null && vm.MailInfo.Account != null)
            {
                var saveDir = FileUtils.GetWindowConfigDirectory(this, windowID, true);
                SaveAccountInfo(controller, saveDir, vm.AccountName, vm.MailInfo.Account);
            }

            if (this.SavedTileIDList == null)
            { this.SavedTileIDList = new HashSet<Guid>(); }
            this.SavedTileIDList.Add(windowID);

            this.Logger.PrintPop();
        }

        /// <summary>
        /// メールアカウント情報を保存する
        /// </summary>
        /// <param name="controller">コントローラオブジェクト</param>
        /// <param name="confDir">保存先のディレクトリ</param>
        /// <param name="accountName">メールアカウント名</param>
        /// <param name="account">保存するメールアカウント情報</param>
        private void SaveAccountInfo(
            IAppController controller,
            DirectoryInfo confDir,
            string accountName,
            MailAccount account)
        {
            var confFilePath = Path.Combine(confDir.FullName, "accountname.conf");
            FileUtils.SaveToJson(accountName, confFilePath, controller.Logger);

            confFilePath = Path.Combine(confDir.FullName, "account.conf");
            FileUtils.SaveToJson(account, confFilePath, controller.Logger);
        }

        /// <summary>
        /// アカウント情報をロードする
        /// </summary>
        /// <param name="controller">コントローラオブジェクト</param>
        /// <param name="confDir">保存先のディレクトリ</param>
        /// <returns>ロードしたメールアカウント情報</returns>
        private MailCheckInfo LoadAccountInfo(
            IAppController controller,
            DirectoryInfo confDir)
        {
            try
            {
                // メールアカウント名
                var confFilePath = Path.Combine(confDir.FullName, "accountname.conf");
                var accountName = (
                    new FileInfo(confFilePath).Exists ? FileUtils.LoadFromJson<string>(confFilePath, controller.Logger) :
                    "");

                confFilePath = Path.Combine(confDir.FullName, "account.conf");
                var accountInfo = (
                    new FileInfo(confFilePath).Exists ? FileUtils.LoadFromJson<MailAccount>(confFilePath, controller.Logger):
                    null);

                return (
                    accountInfo != null ? new MailCheckInfo() { AccountName = accountName, Account = accountInfo, } :
                    null);
            }
            catch (Exception)
            {
                // ロードに失敗した場合はnullを返す
                return null;
            }
        }

        /// <inheritdoc/>
        public void ShutdownProvider(IAppController controller)
        { }

        /// <inheritdoc/>
        public void ShutdownTile(IAppController controller, Guid windowID, UserControl tile)
        { }
    }
}
