﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using LivanaiTiles.Controllers;
using LivanaiTiles.Mail.Mail;
using LivanaiTiles.Mail.VM;
using LivanaiTiles.Mail.Windows;

namespace LivanaiTiles.Mail
{
    /// <summary>
    /// MailCheckerTile.xaml の相互作用ロジック
    /// </summary>
    public partial class MailCheckerTile : UserControl
    {
        /// <summary>
        /// メール再取得までのデフォルト間隔 (秒)
        /// </summary>
        private static readonly int DEFAULT_MAIL_FETCH_INTERVAL = 60 * 10;

        /// <summary>
        /// 表示メールを次に贈るまでのデフォルト間隔 (秒)
        /// </summary>
        private static readonly int DEFAULT_MAIL_VIEW_INTERVAL = 10;

        /// <summary>
        /// 一定間隔でメールの切り替え、メールの再取得を行うためのタイマー
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// メールを再取得するまでの残り時間
        /// </summary>
        private int _remainMailFetch;

        /// <summary>
        /// 表示中のメールを切り替えるまでの残り時間
        /// </summary>
        private int _remainCurrentMailChange;

        public MailCheckerTile()
        {
            InitializeComponent();

            var vm = new MailCheckerTileViewModel()
            {
                MailInfo = new MailCheckInfo() { AccountName = "no account." }
            };
            this.DataContext = vm;

            // メールの再取得するためのタイマーを設定
            this._remainMailFetch = DEFAULT_MAIL_FETCH_INTERVAL;
            this._remainCurrentMailChange = DEFAULT_MAIL_VIEW_INTERVAL;
            this.timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 1000), };
            this.timer.Tick += Timer_Tick;
            this.timer.Start();
        }

        /// <summary>
        /// アカウント設定画面を表示する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            var dc = this.DataContext as MailCheckerTileViewModel;

            // アカウント設定画面に今持っているアカウント情報の複製を渡す
            // アカウント情報が未設定の場合はここで新規オブジェクトとして作成する
            var editAccount = (
                dc.MailInfo.Account != null ? dc.MailInfo.Account.Clone() as MailAccount :
                new MailAccount()
                {
                    ServerName = "example.com",
                    Port = 110,
                    UserName = "username@example.com",
                    Password = "xxx",
                });
            var wnd = new AccountConfigWindow()
            {
                DataContext = new AccountConfigWindowViewModel()
                {
                    AccountName = dc.AccountName,
                    Account = editAccount,
                }
            };
            
            var dlgResult = wnd.ShowDialog();
            if (dlgResult.HasValue && dlgResult.Value)
            {
                // 編集後の情報をコンテキストに戻す
                var modifiedInfo = wnd.DataContext as AccountConfigWindowViewModel;
                dc.MailInfo.AccountName = modifiedInfo.AccountName;
                dc.MailInfo.Account = modifiedInfo.Account;

                // 編集後のメール情報で再取得する
                dc.CheckMail(this);

                // メール取得間隔のタイマーをリセットする
                this._remainMailFetch = dc.MailInfo.Account.FetchIntervalMinutes * 60;
            }
        }

        private void btnMailTitle_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// タイマーのコールバック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            var dc = this.DataContext as MailCheckerTileViewModel;

            this._remainCurrentMailChange--;
            if (this._remainCurrentMailChange < 0)
            {
                // 表示中のメールを進める
                dc.NextMail();
                this._remainCurrentMailChange = DEFAULT_MAIL_VIEW_INTERVAL;
            }

            this._remainMailFetch--;
            if (this._remainMailFetch < 0)
            {
                // メールを再取得する
                dc.CheckMail(this);
                this._remainMailFetch = (
                    dc.MailInfo.Account == null ? dc.MailInfo.Account.FetchIntervalMinutes * 60 :
                    DEFAULT_MAIL_FETCH_INTERVAL);
            }
        }
    }
}
