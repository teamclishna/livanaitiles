﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using clUtils.gui;

using LivanaiTiles.Mail.Mail;

namespace LivanaiTiles.Mail.VM
{
    /// <summary>
    /// アカウント情報設定画面のViewModel
    /// </summary>
    public class AccountConfigWindowViewModel : NotifiableViewModel
    {
        #region Fields

        private string _accountName;
        private MailAccount _account;
        private string _portNumberText;

        #endregion

        /// <summary>
        /// アカウント名
        /// </summary>
        public string AccountName
        {
            get { return this._accountName; }
            set
            {
                this._accountName = value;
                OnPropertyChanged("AccountName");
            }
        }

        /// <summary>
        /// 編集対象のメールアカウント
        /// </summary>
        public MailAccount Account
        {
            get { return this._account; }
            set
            {
                this._account = value;
                this.PortNumberText = this._account.Port.ToString();
                OnPropertyChanged("Account");
                OnPropertyChanged("IsPOP3");
                OnPropertyChanged("IsIMAP4");
            }
        }

        /// <summary>
        /// ポート番号としてテキストボックスに入力した値
        /// </summary>
        public string PortNumberText
        {
            get { return this._portNumberText; }
            set
            {
                this._portNumberText = value;
                int number;
                if (int.TryParse(value, out number) && 0 < number)
                {
                    this._account.Port = number;
                    OnPropertyChanged("Account");
                }
                OnPropertyChanged("PortNumberText");
                OnPropertyChanged("ValidPortNumber");
            }
        }

        /// <summary>
        /// ポート番号の入力欄に表示する注意書きの表示可否
        /// </summary>
        public Visibility ValidPortNumber
        {
            get
            {
                int number;
                return ((int.TryParse(PortNumberText, out number) && 0 < number) ? Visibility.Collapsed : Visibility.Visible);
            }
        }

        /// <summary>
        /// プロトコルのラジオボタン「POP3」の状態
        /// </summary>
        public bool IsPOP3
        {
            get { return (this._account.ServerType == MailServerType.POP3); }
            set
            {
                if(value)
                {
                    this._account.ServerType = MailServerType.POP3;
                    OnPropertyChanged("IsPOP3");
                    OnPropertyChanged("IsIMAP4");
                }
            }
        }

        /// <summary>
        /// プロトコルのラジオボタン「IMAP4」の状態
        /// </summary>
        public bool IsIMAP4
        {
            get { return (this._account.ServerType == MailServerType.IMAP4); }
            set
            {
                if (value)
                {
                    this._account.ServerType = MailServerType.IMAP4;
                    OnPropertyChanged("IsPOP3");
                    OnPropertyChanged("IsIMAP4");
                }
            }
        }
    }
}
