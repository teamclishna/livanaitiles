﻿using System;
using System.Collections.Generic;
using System.Text;

using LivanaiTiles.Mail.Mail;

namespace LivanaiTiles.Mail.VM
{
    public class DummyMailCheckerTileViewModel : MailCheckerTileViewModel
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DummyMailCheckerTileViewModel() : base()
        {
            var mailinfo = new MailCheckInfo()
            {
                AccountName = "Dummy Account",
                Account = new MailAccount()
                {
                    ServerName = "mail.example.com",
                    Port = 110,
                    ServerType = MailServerType.POP3,
                    MaxFetchCount = 10,
                    FetchIntervalMinutes = 30,
                    UserName = "mailuser",
                    Password = "p@ssw0rd",
                },
                MailBox = new List<MailInfo>()
                {
                    new MailInfo()
                    {
                        From="sender@example.com",
                        MailID="12345",
                        Subject="Dummy Mail",
                    },
                    new MailInfo()
                    {
                        From="sender@example.com",
                        MailID="98765",
                        Subject="Dummy Mail 2",
                    },
                },
            };
            this.MailInfo = mailinfo;
            this.CurrentMailIndex = 1;
        }
    }
}
