﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using clUtils.gui;
using LivanaiTiles.Controllers;
using LivanaiTiles.Mail.Mail;
using LivanaiTiles.Mail.Resources;

namespace LivanaiTiles.Mail.VM
{
    /// <summary>
    /// メールチェック結果を表示するためのViewModel
    /// </summary>
    public class MailCheckerTileViewModel : NotifiableViewModel
    {
        #region Fields

        /// <summary>
        /// メールボックスの状態を示すアイコン (空)
        /// </summary>
        private static readonly string ICON_MAIL_EMPTY = new string(new char[] { (char)0x2c });
        /// <summary>
        /// メールボックスの状態を示すアイコン (メールあり)
        /// </summary>
        private static readonly string ICON_MAIL_RECEIVED= new string(new char[] { (char)0x2e });
        /// <summary>
        /// メールボックスの状態を示すアイコン (エラー)
        /// </summary>
        private static readonly string ICON_MAIL_ERROR = new string(new char[] { (char)0x4d });

        /// <summary>
        /// このタイルで表示するメール情報
        /// </summary>
        private MailCheckInfo _mailInfo;

        /// <summary>
        /// メールボックス内で現在表示しているメール情報のインデックス
        /// </summary>
        private int _currentMailIndex;

        /// <summary>
        /// メールサーバにアクセス中の場合 true
        /// </summary>
        private bool _fetchingMail;

        #endregion

        /// <summary>
        /// メールアカウント情報
        /// </summary>
        public MailCheckInfo MailInfo
        {
            get { return this._mailInfo; }
            set
            {
                this._mailInfo = value;
                OnPropertyChanged("MailInfo");
                OnPropertyChanged("AccountName");
                OnPropertyChanged("MailIcon");
                OnPropertyChanged("CurrentMailSubject");
                OnPropertyChanged("CurrentMailIndex");
                OnPropertyChanged("MaxMailIndex");
            }
        }

        /// <summary>
        /// メールアカウント名
        /// </summary>
        public string AccountName
        { get { return (this._mailInfo != null ? this._mailInfo.AccountName : ""); } }

        /// <summary>
        /// タイルのアイコンとして表示するメールのアイコン
        /// </summary>
        public string MailIcon
        {
            get {
                return (
                    this._mailInfo == null ? ICON_MAIL_ERROR :
                    this._mailInfo.MailBox == null ? ICON_MAIL_ERROR :
                    0 < this._mailInfo.MailBox.Count() ? ICON_MAIL_RECEIVED :
                    ICON_MAIL_EMPTY);
            }
        }

        /// <summary>
        /// 現在表示しているメールの表題
        /// </summary>
        public string CurrentMailSubject
        {
            get
            {
                return (
                    this._fetchingMail ? Strings.INFO_FETCHING_MAIL :
                    this._mailInfo == null ? Strings.ERR_NO_ACCOUNT :
                    this._mailInfo.MailBox == null ? Strings.ERR_FETCH_ERROR :
                    this._mailInfo.MailBox.Count() == 0 ? Strings.ERR_NO_MAIL :
                    this._mailInfo.MailBox.ElementAt(this.CurrentMailIndex - 1).Subject);
            }
        }

        /// <summary>
        /// 表示するメール情報の1から始まるインデックス.
        /// メール情報が存在しない、またはメールボックスが空の場合は0を返す.
        /// </summary>
        public int CurrentMailIndex
        {
            get { return this._currentMailIndex; }
            set
            {
                if (this._mailInfo == null)
                { this._currentMailIndex = 0; }
                else if(this.MailInfo.MailBox == null)
                { this._currentMailIndex = 0; }
                else if (this._mailInfo.MailBox.Count() < value)
                { this._currentMailIndex = this._mailInfo.MailBox.Count(); }
                else
                { this._currentMailIndex = value; }

                OnPropertyChanged("CurrentMailIndex");
                OnPropertyChanged("CurrentMailSubject");
            }
        }

        /// <summary>
        /// 現在のメールボックス内のメール数.
        /// メール情報が存在しない、またはメールボックスが空の場合は0を返す.
        /// </summary>
        public int MaxMailIndex
        {
            get
            {
                return (
                    this._mailInfo == null ? 0 :
                    this._mailInfo.MailBox == null ? 0 :
                    0 < this._mailInfo.MailBox.Count() ? this._mailInfo.MailBox.Count() :
                    0);
            }
        }

        /// <summary>
        /// DataContextの更新を強制的に通知する.
        /// MailInfoの内容を更新した時に上手いこと通知されないので暫定的な対応
        /// </summary>
        public void UpdateDataContext()
        {
            OnPropertyChanged("MailInfo");
            OnPropertyChanged("AccountName");
            OnPropertyChanged("MailIcon");
            OnPropertyChanged("CurrentMailSubject");
            OnPropertyChanged("CurrentMailIndex");
            OnPropertyChanged("MaxMailIndex");
        }

        /// <summary>
        /// 現在格納されているアカウント情報でメールの着信チェックを行う
        /// </summary>
        /// <param name="tile">UIスレッドが所有しているタイル</param>
        public void CheckMail(MailCheckerTile tile)
        {

            // メールサーバへのアクセスはワーカスレッドで非同期に行う
            new Thread(() =>
            {
                // サーバにアクセス中を示すメッセージを表示
                this._fetchingMail = true;
                tile.Dispatcher.Invoke((Action)(() =>
                {
                    UpdateDataContext();
                }));

                IMailChecker checker = (
                    this.MailInfo.Account.ServerType == MailServerType.POP3 ? (IMailChecker)new Pop3MailChecker() :
                    (IMailChecker)new Imap4MailChecker());

                checker.Logger = AppControllerProvider.Instance.Logger;
                checker.Account = this.MailInfo.Account;
                checker.FetchResult = new List<MailInfo>();

                if (checker.Fetch() == MailCheckResult.SUCCEEDED)
                {
                    this.MailInfo.MailBox = checker.FetchResult;
                    this.CurrentMailIndex = 1;
                }
                else
                {
                    this.MailInfo.MailBox = null;
                }

                // アクセス完了したので表示を更新
                this._fetchingMail = false;
                tile.Dispatcher.Invoke((Action)(() => {
                    UpdateDataContext();
                }));

            }).Start();
        }

        /// <summary>
        /// プレビュー表示対象のメールを1つ進める.
        /// 現在保持しているメールが一つもない場合は何もしない
        /// </summary>
        /// <returns>
        /// 新しく表示するメールの1から始まるインデックス. 
        /// 表示対象のメールが存在しない場合は0
        /// </returns>
        public int NextMail()
        {
            if (this.MaxMailIndex == 0)
            { return 0; }

            this.CurrentMailIndex = (
                this.CurrentMailIndex == this.MaxMailIndex ? 1 :
                this.CurrentMailIndex + 1);

            return this.CurrentMailIndex;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MailCheckerTileViewModel()
        { }
    }
}
