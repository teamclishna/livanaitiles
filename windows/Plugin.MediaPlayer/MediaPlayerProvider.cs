﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.MediaPlayer.MediaPlayer;
using LivanaiTiles.MediaPlayer.VM;
using LivanaiTiles.Plugins;
using LivanaiTiles.Utils;

namespace LivanaiTiles.MediaPlayer
{
    /// <summary>
    /// MediaPlayer を操作するためのタイル
    /// </summary>
    public class MediaPlayerProvider : ITileProvider
    {
        /// <inheritdoc/>
        public string Name => "Media Player";

        /// <inheritdoc/>
        public Guid ProviderID => new Guid("{870DD5DF-46D8-41CD-8685-8DC946E03FD5}");

        private ILogger Logger { get; set; }

        /// <summary>
        /// MediaPlayerのタイルが既に生成済みであればtrue.
        /// タイルの複数個生成を認めないのでそれを判定するためのフラグに使う.
        /// </summary>
        private bool ExistsTile { get; set; }

        /// <inheritdoc/>
        public TileInfo CreateTile(IAppController controller, string parameter)
        {
            // 既にタイルが存在するのに生成を要求された場合はエラー
            if(this.ExistsTile)
            { throw new NotAllowMultipleTileException(); }

            var ctl = new MediaPleyerTile();
            ctl.ParentProvider = this;

            WindowControlUtils.ApplyTileControlStylle(
                new FrameworkElement[] {
                    ctl.lblTitle,
                    ctl.lblArtist,
                    ctl.btnSettings,
                    ctl.btnEditPlaylist,
                    ctl.btnPrevious,
                    ctl.btnForward,
                    ctl.btnRepeat,
                    ctl.btnPlay
                },
                new SolidColorBrush(controller.Config.ForeGroundColor),
                new SolidColorBrush(Colors.Gray));

            this.ExistsTile = true;

            return new TileInfo()
            {
                UseAeroGlass = null,
                BackGroundColor = null,
                WindowTitle = this.Name,
                TileContent = ctl,
                TileHeight = 150,
                TileWidth = 310,
            };
        }

        /// <inheritdoc/>
        public bool InitProvider(IAppController controller)
        {
            this.Logger = controller.Logger;
            return true; 
        }

        /// <inheritdoc/>
        public TileInfo RestoreTile(IAppController controller, Guid windowID)
        {
            var createdTile = CreateTile(controller, "");

            // 復元するプレイリストはプレイリスト設定ウィンドウの ViewModel を使って復元する
            var playlistFile = new FileInfo(Path.Combine(FileUtils.GetProviderConfigDirectory(this, true).FullName, "playlist.conf"));
            if (playlistFile.Exists)
            {
                var playlistVM = new PlayListWindowViewModel()
                {
                    Logger = this.Logger,
                    EditPlayList = new PlayList(),
                };
                var vm = createdTile.TileContent.DataContext as MediaPlayerTileViewModel;

                playlistVM.AddPlayList(LoadPlaylistFile(playlistFile));
                foreach (var media in playlistVM.EditPlayList.PlayMediaList)
                { vm.CurrentPlayList.PlayMediaList.Add(media); }

                vm.CurrentMediaIndex = 0;
            }

            return createdTile;
        }

        /// <inheritdoc/>
        public void SaveProviderSession(IAppController controller)
        { }

        /// <inheritdoc/>
        public void SaveTileSession(
            IAppController controller,
            Guid windowID,
            UserControl tile)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"[MediaPlayer] save tile session id {windowID.ToString()}");
            var vm = tile.DataContext as MediaPlayerTileViewModel;

            // プレイリストを保存する
            // このタイルは複数個の表示は認めないのでプロバイダのディレクトリに直接保存する
            if (vm.CurrentPlayList != null && vm.CurrentPlayList.PlayMediaList != null)
            {
                // プレイリストの対象ファイルのフルパスを行単位のテキストで出力する
                var saveDir = FileUtils.GetProviderConfigDirectory(this, true);
                var playlist = vm.CurrentPlayList.PlayMediaList;

                var encoder = new UTF8Encoding(false);
                using (var playlistFile = new FileStream(Path.Combine(saveDir.FullName, "playlist.conf"), FileMode.Create))
                {
                    foreach(var f in playlist)
                    {
                        playlistFile.Write(encoder.GetBytes(f.File.FullName));
                        playlistFile.Write(new byte[] { (byte)'\n' });
                    }
                }
            }
            this.Logger.PrintPop();
        }

        /// <inheritdoc/>
        public void ShutdownProvider(IAppController controller)
        { }

        /// <inheritdoc/>
        public void ShutdownTile(IAppController controller, Guid windowID, UserControl tile)
        { }

        /// <summary>
        /// プレイリストとして保存したファイル名一覧を読み込んで返す
        /// </summary>
        /// <returns>
        /// プレイリストのファイル名一覧を行単位の要素に分割した配列.
        /// 空行、空白のみの行が含まれた場合は要素から取り除く.
        /// </returns>
        private string[] LoadPlaylistFile(FileInfo playlistFile)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"[MediaPlayer] restore playlist file {playlistFile.FullName}");

            var encoder = new UTF8Encoding(false);
            using (var fs = new FileStream(playlistFile.FullName, FileMode.Open))
            {
                var b = new byte[playlistFile.Length];
                var readSize = 0;
                while (readSize < playlistFile.Length)
                {
                    var currentRead = fs.Read(b, readSize, (int)playlistFile.Length - readSize);
                    readSize += currentRead;
                }

                var playlistString = (new UTF8Encoding(false)).GetString(b);

                this.Logger.PrintPop();
                return playlistString
                    .Split(new char[] { '\n' })
                    .Where(x => (0 < x.Trim().Length))
                    .ToArray();
            }
        }

        /// <summary>
        /// プレイリストを保存する
        /// </summary>
        /// <param name="controller">コントローラオブジェクト</param>
        /// <param name="confDir">保存先のディレクトリ</param>
        /// <param name="playlist">プレイリスト</param>
        private void SavePlaylist(
            CoreController controller,
            DirectoryInfo confDir,
            IEnumerable<MediaFile> playlist)
        {
            var confFilePath = Path.Combine(confDir.FullName, "playlist.conf");
            FileUtils.SaveToJson(playlist, confFilePath, controller.Logger);
        }

        /// <summary>
        /// タイルから自身が閉じられた通知を受け取る.
        /// </summary>
        public void NotifyCloseTile()
        { this.ExistsTile = false; }
    }
}
