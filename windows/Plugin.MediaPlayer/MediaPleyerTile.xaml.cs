﻿using LivanaiTiles.Controllers;
using LivanaiTiles.MediaPlayer.MediaPlayer;
using LivanaiTiles.MediaPlayer.VM;
using LivanaiTiles.MediaPlayer.Windows;
using System.Windows;
using System.Windows.Controls;

namespace LivanaiTiles.MediaPlayer
{
    /// <summary>
    /// MediaPleyerTile.xaml の相互作用ロジック
    /// </summary>
    public partial class MediaPleyerTile : UserControl
    {
        /// <summary>
        /// このタイルを生成したプロバイダ
        /// </summary>
        public MediaPlayerProvider ParentProvider { get; set; }

        public MediaPleyerTile()
        {
            InitializeComponent();

            this.DataContext = new MediaPlayerTileViewModel()
            {
                Logger = AppControllerProvider.Instance.Logger,
                CurrentPlayList = new PlayList()
                {
                    Title = "",
                    PlayListFile = null
                }
            };
        }

        private void btnEditPlaylist_Click(object sender, RoutedEventArgs e)
        {
            // プレイリスト編集ウィンドウを開く
            var vm = this.DataContext as MediaPlayerTileViewModel;
            var dlgVM = new PlayListWindowViewModel();
            dlgVM.Logger = AppControllerProvider.Instance.Logger;

            dlgVM.EditPlayList = (
                vm == null ? new MediaPlayer.PlayList() :
                vm.CurrentPlayList == null ? new MediaPlayer.PlayList() :
                vm.CurrentPlayList.Clone() as PlayList
                );

            var wnd = new PlayListWindow() { DataContext = dlgVM, };

            var dlgResult = wnd.ShowDialog();
            if(dlgResult.HasValue && dlgResult.Value)
            {
                // 設定ダイアログで取得した内容を反映する
                vm.CurrentPlayList = dlgVM.EditPlayList;
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ParentProvider.NotifyCloseTile(); 
        }
    }
}
