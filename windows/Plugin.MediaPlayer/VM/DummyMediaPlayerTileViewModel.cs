﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LivanaiTiles.MediaPlayer.VM
{
    /// <summary>
    /// デザイナ上で画面表示を確認するためのダミーViewModel
    /// </summary>
    public class DummyMediaPlayerTileViewModel : MediaPlayerTileViewModel
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DummyMediaPlayerTileViewModel() : base()
        {
            this.CurrentPlayList = new MediaPlayer.PlayList();
            this.CurrentPlayList.PlayMediaList.Add(new MediaPlayer.MediaFile()
            {
                Title = "Sample Media",
                Author = "Sample Author"
            });
            this.CurrentMediaIndex = 0;
        }
    }
}
