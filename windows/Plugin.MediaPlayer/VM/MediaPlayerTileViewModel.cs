﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using clUtils.gui;
using clUtils.log;
using LivanaiTiles.MediaPlayer.MediaPlayer;

namespace LivanaiTiles.MediaPlayer.VM
{
    /// <summary>
    /// MediaPlayer タイルの ViewModel
    /// </summary>
    public class MediaPlayerTileViewModel : NotifiableViewModel
    {
        #region Fields

        private PlayList _currentPlayList;
        private int _currentMediaIndex;
        private bool _isRepeat;
        private bool _isPlaying;

        /// <summary>
        /// 再生ボタン用のアイコン（停止→再生）
        /// </summary>
        private static readonly string ICON_WMP_PLAY = new string(new char[] { (char)0xe768 });
        /// <summary>
        /// 再生ボタン用のアイコン（再生→停止）
        /// </summary>
        private static readonly string ICON_WMP_STOP = new string(new char[] { (char)0xe769 });
        /// <summary>
        /// 再生ボタン用のアイコン（1回再生）
        /// </summary>
        private static readonly string ICON_WMP_ONETIME = new string(new char[] { (char)0xe8ed });
        /// <summary>
        /// 再生ボタン用のアイコン（繰り返し再生）
        /// </summary>
        private static readonly string ICON_WMP_REPEAT = new string(new char[] { (char)0xe8ee });

        /// <summary>
        /// 再生中のメディアにアートワークが無い場合、または非再生時に表示するダミーのアートワーク画像
        /// </summary>
        private ImageSource _emptyArtwork;

        #endregion

        #region Properties

        public ILogger Logger { get; set; }

        /// <summary>
        /// MediaPlayerコントロールのオブジェクト
        /// </summary>
        private System.Windows.Media.MediaPlayer MP { get; set; }

        /// <summary>
        /// 再生対象のプレイリスト
        /// </summary>
        public PlayList CurrentPlayList
        {
            get { return this._currentPlayList; }
            set
            {
                this._currentPlayList = value;
                this.CurrentMediaIndex = 0;
                OnPropertyChanged("CurrentPlayList");
                OnPropertyChanged("MediaCount");
                OnPropertyChanged("CurrentMediaIndex");
                OnPropertyChanged("CurrentMedia");
                OnPropertyChanged("PlayTitle");
                OnPropertyChanged("PlayAuthor");
                OnPropertyChanged("PlayArtwork");

                if (this.NextMediaCommand != null)
                { this.NextMediaCommand.RaiseCanExecuteChanged(); }
                if (this.PrevMediaCommand != null)
                { this.PrevMediaCommand.RaiseCanExecuteChanged(); }
                if (this.PlayCommand != null)
                { this.PlayCommand.RaiseCanExecuteChanged(); }
                if (this.RepeatCommand != null)
                { this.RepeatCommand.RaiseCanExecuteChanged(); }
            }
        }

        /// <summary>
        /// プレイリストに含まれるメディアの件数.
        /// メディアがロードされていない場合は0を返す
        /// </summary>
        public int MediaCount
        {
            get
            {
                return (
                    this._currentPlayList == null ? 0 :
                    this._currentPlayList.PlayMediaList.Count);
            }
        }

        /// <summary>
        /// プレイリスト中の現在生成中のメディアを示す0から始まるインデックス.
        /// 再生可能なメディアが無い場合は負の値が設定される.
        /// </summary>
        public int CurrentMediaIndex
        {
            get { return this._currentMediaIndex; }
            set
            {
                this._currentMediaIndex = (
                    value < 0 ? -1 :
                    this._currentPlayList == null ? -1 :
                    this._currentPlayList.PlayMediaList.Count <= value ? -1 :
                    value);
                OnPropertyChanged("CurrentMediaIndex");
                OnPropertyChanged("CurrentMedia");
                OnPropertyChanged("PlayTitle");
                OnPropertyChanged("PlayAuthor");
                OnPropertyChanged("PlayArtwork");

                if (this.NextMediaCommand != null)
                { this.NextMediaCommand.RaiseCanExecuteChanged(); }
                if (this.PrevMediaCommand != null)
                { this.PrevMediaCommand.RaiseCanExecuteChanged(); }
            }
        }

        /// <summary>
        /// 現在再生中のメディア情報
        /// </summary>
        private MediaFile CurrentMedia
        { get { return (this.CurrentMediaIndex < 0 ? null : this.CurrentPlayList.PlayMediaList[this.CurrentMediaIndex]); } }

        /// <summary>
        /// 現在再生中のメディアのタイトル
        /// </summary>
        public string PlayTitle
        { get { return (this.CurrentMedia == null ? "" : this.CurrentMedia.Title); } }

        /// <summary>
        /// 現在再生中のメディアの作成者
        /// </summary>
        public string PlayAuthor
        { get { return (this.CurrentMedia == null ? "" : this.CurrentMedia.Author); } }

        /// <summary>
        /// 現在再生中のメディアのアートワーク画像
        /// </summary>
        public ImageSource PlayArtwork
        {
            get
            {
                return (
                    this.CurrentMedia == null ? this._emptyArtwork :
                    this.CurrentMedia.Artwork == null ? this._emptyArtwork :
                    this.CurrentMedia.Artwork);
            }
        }

        /// <summary>
        /// 現在再生中であればtrue
        /// </summary>
        public bool IsPlaying
        {
            get { return this._isPlaying; }
            set
            {
                this._isPlaying = value;
                OnPropertyChanged("IsPlaying");
                OnPropertyChanged("PlayButtonIcon");
            }
        }

        /// <summary>
        /// リピート再生を行う場合はtrue
        /// </summary>
        public bool IsRepeat
        {
            get { return this._isRepeat; }
            set
            {
                this._isRepeat = value;
                OnPropertyChanged("IsRepeat");
                OnPropertyChanged("RepeatButtonIcon");
            }
        }

        /// <summary>
        /// 再生ボタンのアイコン
        /// </summary>
        public string PlayButtonIcon
        { get { return (this.IsPlaying ? ICON_WMP_STOP : ICON_WMP_PLAY); } }

        /// <summary>
        /// 繰り返し再生ボタンのアイコン
        /// </summary>
        public string RepeatButtonIcon
        { get { return (this.IsRepeat ? ICON_WMP_REPEAT : ICON_WMP_ONETIME); } }

        #endregion

        #region Commands

        /// <summary>
        /// 前の曲に送るコマンド
        /// </summary>
        public RelayCommand PrevMediaCommand { get; set; }

        /// <summary>
        /// 次の曲に送るコマンド
        /// </summary>
        public RelayCommand NextMediaCommand { get; set; }

        /// <summary>
        /// 再生／停止コマンド
        /// </summary>
        public RelayCommand PlayCommand { get; set; }

        /// <summary>
        /// リピートモードを切り替えるコマンド
        /// </summary>
        public RelayCommand RepeatCommand { get; set; }


        #endregion

        /// <summary>
        /// 再生／停止
        /// </summary>
        private void PlayAndStop()
        {
            if(!this.IsPlaying)
            {
                // 再生開始
                this.MP.Open(new Uri(this.CurrentMedia.File.FullName));
                this.MP.Play();
                this.IsPlaying = true;
            }
            else
            {
                // 停止
                this.MP.Stop();
                this.IsPlaying = false;
            }
        }

        /// <summary>
        /// 次の曲に送る
        /// </summary>
        private void DoNext()
        {
            this.CurrentMediaIndex++;
            if (this.IsPlaying)
            {
                this.MP.Open(new Uri(this.CurrentMedia.File.FullName));
                this.MP.Play();
            }
        }

        /// <summary>
        /// 前の曲に送る
        /// </summary>
        private void DoPrev()
        {
            this.CurrentMediaIndex--;
            if (this.IsPlaying)
            {
                this.MP.Open(new Uri(this.CurrentMedia.File.FullName));
                this.MP.Play();
            }
        }

        /// <summary>
        /// リピートモードの切り替えを行う
        /// </summary>
        private void SetRepeatMode()
        { this.IsRepeat = !this.IsRepeat; }

        /// <summary>
        /// メディア再生終了時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMediaEnd(object sender, EventArgs e)
        {
            if (this.CurrentMediaIndex < this.MediaCount - 1)
            {
                // 次の曲へ進む
                DoNext();
            }
            else if (this.IsRepeat)
            {
                // リピートが設定されている場合は最初から
                this.CurrentMediaIndex = 0;
                this.MP.Open(new Uri(this.CurrentMedia.File.FullName));
                this.MP.Play();
            }
            else
            {
                // 再生終了
                this.IsPlaying = false;
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MediaPlayerTileViewModel()
        {
            this.Logger = LoggerFactory.CreateLogger(LoggerName.NullLogger);
            this.CurrentMediaIndex = -1;

            // ダミーのアートワーク画像をロードする
            var asm = System.Reflection.Assembly.GetExecutingAssembly();
            this._emptyArtwork = new BitmapImage(new Uri(
                "pack://application:,,,/Plugin.MediaPlayer;component/Resources/music_icon_large.png",
                UriKind.Absolute));

            this.MP = new System.Windows.Media.MediaPlayer();
            this.MP.MediaEnded += OnMediaEnd;

            // コマンド
            this.PrevMediaCommand = new RelayCommand(
                (x) => { return (0 < this.CurrentMediaIndex); },
                (x) => { DoPrev(); });
            this.NextMediaCommand = new RelayCommand(
                (x) => { return (this.CurrentMediaIndex < this.MediaCount - 1); },
                (x) => { DoNext(); });
            this.PlayCommand = new RelayCommand(
                (x) => { return (0 < this.MediaCount); },
                (x) => { PlayAndStop(); });
            this.RepeatCommand = new RelayCommand(
                (x) => { return (1 < this.MediaCount); },
                (x) => { SetRepeatMode(); });
        }
    }
}
