﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Windows;
using System.Windows.Navigation;

using clUtils.gui;
using clUtils.log;

using LivanaiTiles.MediaPlayer.MediaPlayer;
using LivanaiTiles.MediaPlayer.Utils;
using Microsoft.Win32;

namespace LivanaiTiles.MediaPlayer.VM
{
    /// <summary>
    /// プレイリスト編集ウィンドウのViewModel
    /// </summary>
    public class PlayListWindowViewModel : NotifiableViewModel
    {
        #region Fields

        private PlayList _editPlayList;

        #endregion

        #region Properties
        public ILogger Logger { get; set; }

        /// <summary>
        /// 編集対象のプレイリスト
        /// </summary>
        public PlayList EditPlayList
        {
            get { return this._editPlayList; }
            set
            {
                this._editPlayList = value;
                OnPropertyChanged("EditPlayList");
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// ファイル選択ダイアログでメディアを追加するコマンド
        /// </summary>
        public RelayCommand AddCommand { get; set; }

        /// <summary>
        /// 選択したメディアをリストから削除するコマンド
        /// </summary>
        public RelayCommand RemoveSelectedCommand { get; set; }

        /// <summary>
        /// 全てのメディアをリストから削除するコマンド
        /// </summary>
        public RelayCommand RemoveAllCommand { get; set; }

        #endregion

        /// <summary>
        /// 指定したファイル、またはディレクトリの一覧をプレイリストに追加する
        /// </summary>
        /// <param name="filePathList">追加対象のファイル、またはディレクトリ</param>
        /// <returns>
        /// リストに実際に追加したファイルの総数. ディレクトリ、追加対象外としたファイルは含めない、
        /// また、ディレクトリ内のファイルは再帰的に検索するため filePathList の要素数とは異なる。
        /// </returns>
        public int AddPlayList(string[] filePathList)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"Add media to playlist.");

            // ドロップしたファイル、ディレクトリからプレイリストに追加可能なファイルを再帰的に検索する
            var loadFiles = GetMediaFilesRecursive(filePathList);

            // 検索したファイルから現在のプレイリストに既に含まれるものを除いて追加する
            var filteredList = FindNotExistedFiles(loadFiles, this.EditPlayList.PlayMediaList);
            foreach (var media in filteredList)
            { this.EditPlayList.PlayMediaList.Add(media); }

            UpdateUI();

            this.Logger.PrintPop();
            return filteredList.Count();
        }

        /// <summary>
        /// 指定したファイル、またはディレクトリの一覧をプレイリストの指定位置に挿入する
        /// </summary>
        /// <param name="filePathList">追加対象のファイル、またはディレクトリ</param>
        /// <param name="position">
        /// 挿入位置を示す0から始まるインデックス.
        /// リストの先頭に挿入する場合は0、末尾に挿入する場合は負の値、または現在のリストの要素数以上の値を指定する.
        /// </param>
        /// <returns>
        /// リストに実際に追加したファイルの総数. ディレクトリ、追加対象外としたファイルは含めない、
        /// また、ディレクトリ内のファイルは再帰的に検索するため filePathList の要素数とは異なる。
        /// </returns>
        public int InsertPlayList(string[] filePathList, int position)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"Insert media to playlist position {position}.");

            if (position < 0 || this.EditPlayList.PlayMediaList.Count <= position)
            {
                this.Logger.PrintPop();
                return AddPlayList(filePathList);
            }

            // ドロップしたファイル、ディレクトリからプレイリストに追加可能なファイルを再帰的に検索する
            var loadFiles = GetMediaFilesRecursive(filePathList);

            // 検索したファイルから現在のプレイリストに既に含まれるものを除いて追加する
            var filteredList = FindNotExistedFiles(loadFiles, this.EditPlayList.PlayMediaList);
            var insertPosition = position;
            foreach (var media in filteredList)
            { this.EditPlayList.PlayMediaList.Insert(insertPosition++, media); }

            UpdateUI();

            this.Logger.PrintPop();
            return filteredList.Count();
        }

        /// <summary>
        /// 指定してアイテムをプレイリストの指定したインデックスの位置に移動する.
        /// </summary>
        /// <param name="moveItemList">移動対象のプレイリストのアイテム</param>
        /// <param name="position">
        /// 移動先を示すインデックス.
        /// このインデックスが示すアイテムの前に移動する.
        /// 末尾に移動する場合は負の値、または現在のアイテムの個数以上の値を指定する
        /// </param>
        public void MovePlayList(IEnumerable<MediaFile> moveItemList, int position)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"Move {moveItemList.Count()} items move to position {position}.");

            // 移動対象のアイテムを除いたインデックスを取得
            var insertPosition = (
                position < 0 ? -1 :
                this.EditPlayList.PlayMediaList.Count <= position ? -1 :
                GetInsertPositionItem(this.EditPlayList.PlayMediaList, moveItemList, position));

            // プレイリストから移動対象として指定しているアイテムを取り除く
            for (var index = this.EditPlayList.PlayMediaList.Count - 1; 0 <= index; index--)
            {
                if (moveItemList.Any(x => x.File.FullName == this.EditPlayList.PlayMediaList[index].File.FullName))
                { this.EditPlayList.PlayMediaList.RemoveAt(index); }
            }

            if(insertPosition<0)
            {
                // 末尾に追加
                foreach (var media in moveItemList)
                { this.EditPlayList.PlayMediaList.Add(media); }
            }
            else
            {
                // 指定位置に追加
                var currentInsertPosition = insertPosition;
                foreach (var media in moveItemList)
                { this.EditPlayList.PlayMediaList.Insert(currentInsertPosition++, media); }
            }

            UpdateUI();

            this.Logger.PrintPop();
        }

        /// <summary>
        /// プレイリスト中の移動先に指定したインデックスの位置を、移動対象として指定したアイテムを除いたインデックスに修正して返す.
        /// </summary>
        /// <param name="playlist">プレイリスト</param>
        /// <param name="moveItemList">移動対象として選択しているアイテム</param>
        /// <param name="position">移動先として指定しているインデックス</param>
        /// <returns></returns>
        private int GetInsertPositionItem(ObservableCollection<MediaFile> playlist, IEnumerable<MediaFile> moveItemList, int position)
        {
            var fixedPosition = -1;
            var currentIndex = 0;
            while (currentIndex < playlist.Count)
            {
                var item = playlist[currentIndex];
                if (!moveItemList.Any(x => x.File.FullName == item.File.FullName))
                { fixedPosition++; }

                if (position <= currentIndex && 0 <= fixedPosition)
                { return fixedPosition; }
                currentIndex++;
            }
            return fixedPosition;
        }

        /// <summary>
        /// 指定したファイルパスの配列を再帰的に検索し、プレイリストに追加可能なファイル情報のリストを生成して返す
        /// </summary>
        /// <param name="filePathList">挿入対象のファイルパスを格納したリスト</param>
        /// <returns>ファイルパスを再帰的に検索した結果のメディアファイルのリスト.</returns>
        private IEnumerable<MediaFile> GetMediaFilesRecursive(IEnumerable<string> filePathList)
        {
            var loadFiles = new List<MediaFile>();
            foreach (var path in filePathList)
            {
                this.Logger.PrintPush(LogLevel.DEBUG, path);

                var di = new DirectoryInfo(path);
                if (di.Exists)
                { loadFiles.AddRange(FindMediaFiles(di)); }
                else
                {
                    var fi = new FileInfo(path);
                    if (fi.Exists)
                    { loadFiles.AddRange(FindMediaFiles(fi)); }
                }
                this.Logger.PrintPop();
            }

            return loadFiles;
        }

        /// <summary>
        /// 指定したファイル、またはディレクトリを元にプレイリストに追加する対象のメディア情報を返す
        /// </summary>
        /// <param name="fsi">追加対象のファイル、またはディレクトリ</param>
        /// <returns>追加対象とするファイルのリスト</returns>
        public IEnumerable<MediaFile> FindMediaFiles(FileSystemInfo fsi)
        {
            var result = new List<MediaFile>();
            if(fsi is DirectoryInfo)
            {
                var di = fsi as DirectoryInfo;
                this.Logger.PrintPush(LogLevel.DEBUG, $"Find in directory {di.FullName}");

                // ディレクトリであればこのディレクトリ内のファイル、サブディレクトリを再帰的に検索する
                foreach (var f in di.GetFiles())
                { result.AddRange(FindMediaFiles(f)); }

                foreach (var d in di.GetDirectories())
                { result.AddRange(FindMediaFiles(d)); }

                this.Logger.PrintPop();
            }
            else
            {
                // ファイルであればファイルの型を確認する
                var fi = fsi as FileInfo;
                this.Logger.PrintPush(LogLevel.DEBUG, $"load file {fi.FullName}");

                switch (MediaPlayerUtils.ChecKFileType(fi.FullName))
                {
                    case MediaFileTypeEnum.MP3:
                        result.Add(MediaPlayerUtils.DetectMP3File(fi, this.Logger));
                        break;

                    case MediaFileTypeEnum.WMA:
                        result.Add(MediaPlayerUtils.DetectWMAFile(fi, this.Logger));
                        break;

                    case MediaFileTypeEnum.PLAYLIST:
                        // プレイリストはその内容を解析し、存在するファイルだけ追加.
                        var playlistFromWPL = MediaPlayerUtils.LoadPlayList(fi, this.Logger);
                        result.AddRange(playlistFromWPL.PlayMediaList);
                        break;
                }

                this.Logger.PrintPop();
            }
            return result;
        }

        /// <summary>
        /// プレイリストに追加しようとしているファイルから、
        /// 現在のプレイリストに含まれないファイルだけを選択してそのリストを返す。
        /// </summary>
        /// <param name="newFiles">プレイリストに追加しようとしているファイルのリスト</param>
        /// <param name="currentFiles">現在のプレイリストに含まれるファイルのリスト</param>
        /// <returns>newFiles で指定したリストの中から currentFiles に含まれないファイルだけを選択したリスト</returns>
        private IEnumerable<MediaFile> FindNotExistedFiles(IEnumerable<MediaFile> newFiles, IEnumerable<MediaFile> currentFiles)
        {
            return newFiles.Where((x) =>
            {
                return !currentFiles.Any((y) =>
                {
                    return x.File.FullName == y.File.FullName;
                });
            }).ToList();
        }

        /// <summary>
        /// ファイル選択ダイアログで追加するメディアファイルを選択する
        /// </summary>
        private void AddMedia()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog()
            {
                Multiselect = true,
                FilterIndex = 1,
                Filter = "MP3 Files (*.mp3)|*.mp3|Windows Media Audio (*.wma)|*.wma",
            };
            var dlgResult = dlg.ShowDialog();
            if (dlgResult.HasValue && dlgResult.Value)
            { AddPlayList(dlg.FileNames); }
        }

        /// <summary>
        /// プレイリスト中の選択項目を削除する
        /// </summary>
        private void RemoveSelected()
        {
            var selectedItems = this.EditPlayList
                .PlayMediaList
                .Where(x => x.IsSelected)
                .ToList();
            foreach (var item in selectedItems)
            { this.EditPlayList.PlayMediaList.Remove(item); }
            UpdateUI();
        }

        /// <summary>
        /// プレイリスト内のすべての項目を削除する
        /// </summary>
        private void RemoveAll()
        {
            var result = MessageBox.Show(
                "Remove all items in list?",
                "Remove All",
                MessageBoxButton.OKCancel,
                MessageBoxImage.Warning);
            if(result == MessageBoxResult.OK)
            {
                this.EditPlayList.PlayMediaList.Clear();
                UpdateUI();
            }
        }

        /// <summary>
        /// プレイリストの状態によるUIの状態を更新する
        /// </summary>
        private void UpdateUI()
        {
            if (this.RemoveSelectedCommand != null)
            { this.RemoveSelectedCommand.RaiseCanExecuteChanged(); }
            if (this.RemoveAllCommand != null)
            { this.RemoveAllCommand.RaiseCanExecuteChanged(); }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PlayListWindowViewModel()
        {
            this.Logger = LoggerFactory.CreateLogger(LoggerName.NullLogger);

            this.AddCommand = new RelayCommand(
                (x) => { return true; },
                (x) => { AddMedia(); });
            this.RemoveSelectedCommand = new RelayCommand(
                (x) => { return (0 < this.EditPlayList.PlayMediaList.Count); },
                (x) => { RemoveSelected(); });
            this.RemoveAllCommand = new RelayCommand(
                (x) => { return (0 < this.EditPlayList.PlayMediaList.Count); },
                (x) => { RemoveAll(); });
        }
    }
}
