﻿using System;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace LivanaiTiles.MediaPlayer.Windows
{
    /// <summary>
    /// ListView にドロップする時の挿入位置を表示するためのオブジェクト
    /// </summary>
    public class InsertPositionAdorner : Adorner
    {
        /// <summary>
        /// 装飾要素を配置するためのレイヤー
        /// </summary>
        private AdornerLayer _layer;

        /// <summary>
        /// 装飾要素を描画するユーザーコントロール
        /// </summary>
        private InsertPositionAdornerView _adornerView;

        protected override int VisualChildrenCount
        { get { return 1; } }

        protected override Size ArrangeOverride(Size finalSize)
        {
            _adornerView.Arrange(new Rect(finalSize));
            return finalSize;
        }

        protected override Visual GetVisualChild(int index)
        {
            if (VisualChildrenCount <= index)
            { throw new IndexOutOfRangeException(); }
            // 常に Canvas を返す
            return _adornerView;
        }

        /// <summary>
        /// Adornerの表示を解除する
        /// </summary>
        public void Detach()
        { _layer.Remove(this); }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="parent"></param>
        public InsertPositionAdorner(UIElement parent)
            : base(parent)
        {
            this._layer = AdornerLayer.GetAdornerLayer(parent);
            this._adornerView = new InsertPositionAdornerView();
            if (this._layer != null)
            { _layer.Add(this); }
        }
    }
}
