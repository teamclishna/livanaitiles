﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using LivanaiTiles.MediaPlayer.MediaPlayer;
using LivanaiTiles.MediaPlayer.Utils;
using LivanaiTiles.MediaPlayer.VM;

namespace LivanaiTiles.MediaPlayer.Windows
{
    /// <summary>
    /// PlayListWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class PlayListWindow : Window
    {
        /// <summary>
        /// リストのドラッグ中に挿入位置を示すためのAdorner
        /// </summary>
        private InsertPositionAdorner _insertAdorner;

        /// <summary>
        /// プレイリスト中のアイテムをドラッグするときに選択されていたアイテム
        /// </summary>
        private IEnumerable<MediaFile> _selectedItems = null;

        /// <summary>
        /// プレイリストのアイテムをドラッグする場合のドラッグ開始位置
        /// </summary>
        Point? _dragStartPosition = null;

        public PlayListWindow()
        {
            InitializeComponent();
        }

        private void playMediaList_DragEnter(object sender, DragEventArgs e)
        {
            if (sender == e.Source)
            {
                var droppedItem = GetListViewItem(this.playMediaList, e.GetPosition(this.playMediaList));
                if (droppedItem != null)
                { ShowAdorner(droppedItem); }
            }
            else
            {
                // ドロップしようとする対象が対応データであれば挿入位置の判定を行い、
                // 挿入位置を示すAdornerを表示する
                if (IsDropTargetItem(e))
                {
                    var droppedItem = GetListViewItem(this.playMediaList, e.GetPosition(this.playMediaList));
                    if (droppedItem != null)
                    {
                        if ((droppedItem.DataContext as MediaFile) != null)
                        { ShowAdorner(droppedItem); }
                    }
                }
            }
        }

        private void playMediaList_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // ドロップ対象に対応外のファイルが含まれる場合はドロップを許可しない
                e.Effects = (IsDropTargetItem(e) ? DragDropEffects.Copy : DragDropEffects.None);
                e.Handled = true;
            }
            else if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                // プレイリスト内のアイテムをドラッグ中の場合はドロップを許可
                e.Effects = DragDropEffects.Copy;
                e.Handled = true;
            }
        }

        private void playMediaList_Drop(object sender, DragEventArgs e)
        {
            // HitTestでAdornerの装飾要素がヒットするため、先にAdornerを解除する
            HideAdorner();
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var droppedData = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (droppedData != null)
                {
                    // ドロップしたファイルに対応外のファイルが含まれる場合は何もしない
                    if (MediaPlayerUtils.CheckFileTypes(droppedData).Contains(MediaFileTypeEnum.OTHER))
                    { return; }

                    var vm = this.DataContext as PlayListWindowViewModel;
                    vm.InsertPlayList(
                        droppedData,
                        GetListViewItemIndex(
                            this.playMediaList,
                            e.GetPosition(this.playMediaList)));
                }
            }
            else if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                // ドロップ対象のアイテムを移動させる
                var droppedItemIndex = GetListViewItemIndex(this.playMediaList, e.GetPosition(this.playMediaList));
                if (this._selectedItems != null && 0 < this._selectedItems.Count())
                {
                    var vm = this.DataContext as PlayListWindowViewModel;
                    vm.MovePlayList(this._selectedItems, droppedItemIndex);
                }

                _selectedItems = null;
                _dragStartPosition = null;
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void playMediaList_DragLeave(object sender, DragEventArgs e)
        {
            HideAdorner();
        }

        /// <summary>
        /// 指定したListViewの指定した座標にあるアイテムを取得して返す
        /// </summary>
        /// <param name="parent">検査対象のListBox</param>
        /// <param name="position">parentを基準とした相対座標</param>
        /// <returns>指定した座標にあるアイテム. 指定した座標にアイテムが存在しない場合はnull</returns>
        public static ListViewItem GetListViewItem(ListView parent, Point position)
        {
            var result = VisualTreeHelper.HitTest(parent, position);
            if (result == null)
            { return null; }
            else
            {
                // HitTestの結果から親コントロールを順位たどり、
                // ListBoxItemが見つかればそのインデックスを返す
                // ListViewまで遡った場合はアイテム上に無いものとしてnullを返す
                var item = result.VisualHit;
                while (item != null)
                {
                    if (item is ListViewItem)
                    { return (ListViewItem)item; }
                    else if (item is ListViewItem)
                    { return null; }
                    else
                    { item = VisualTreeHelper.GetParent(item); }
                }
                return null;
            }
        }

        /// <summary>
        /// 指定したListViewの指定した座標にあるアイテムのインデックスを取得して返す
        /// </summary>
        /// <param name="parent">検査対象のListBox</param>
        /// <param name="position">parentを基準とした相対座標</param>
        /// <returns>指定した座標にあるアイテムの0から始まるインデックス。指定した座標にアイテムが無い場合は-1</returns>
        public static int GetListViewItemIndex(ListView parent, Point position)
        {
            var itemOnPosition = GetListViewItem(parent, position);
            if (itemOnPosition == null)
            { return -1; }
            else
            { return parent.Items.IndexOf(itemOnPosition.Content); }
        }

        /// <summary>
        /// 挿入位置を表示するためのAdornerを表示する
        /// </summary>
        /// <param name="dropPositionItem">Adornerの表示位置にあるListBoxItem</param>
        private void ShowAdorner(ListViewItem dropPositionItem)
        {
            _insertAdorner = new InsertPositionAdorner(dropPositionItem);
        }

        /// <summary>
        /// 挿入位置を表示するためのAdornerの表示を解除する
        /// </summary>
        private void HideAdorner()
        {
            if (_insertAdorner != null)
            {
                _insertAdorner.Detach();
                _insertAdorner = null;
            }
        }

        /// <summary>
        /// ドロップしようとしているアイテムがプレイリストに対応したアイテムかどうかを判定してその結果を返す
        /// </summary>
        /// <param name="e">ドラッグドロップイベントで渡されたオブジェクト</param>
        /// <returns>ドロップ対象であればtrueを返す</returns>
        private bool IsDropTargetItem(DragEventArgs e)
        {
            var draggingData = e.Data.GetData(DataFormats.FileDrop) as string[];
            return (
                draggingData == null ? false :
                MediaPlayerUtils.CheckFileTypes(draggingData).Contains(MediaFileTypeEnum.OTHER) ? false :
                true);
        }

        private void playMediaList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void playMediaList_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // アイテムのある場所でマウス押下した場合はボタン押下位置を記録しておく
            var mouseDownItemIndex = GetListViewItemIndex(this.playMediaList, e.GetPosition(this.playMediaList));
            if (0 <= mouseDownItemIndex)
            { _dragStartPosition = e.GetPosition(this.playMediaList); }
        }

        private void playMediaList_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // マウスボタンを離したらドラッグ開始位置を開放
            _dragStartPosition = null;
        }

        private void playMediaList_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if(_dragStartPosition.HasValue)
            {
                // マウスボタン押下時の場所からの移動距離を取得し、一定以上移動していればドラッグ開始
                var currentPos = e.GetPosition(this.playMediaList);
                if(8<GetMoveDirection(_dragStartPosition.Value,currentPos))
                {
                    var vm = this.DataContext as PlayListWindowViewModel;
                    var selectedItemList = new List<MediaFile>();
                    _selectedItems = vm.EditPlayList.PlayMediaList.Where(x => x.IsSelected).ToArray();

                    var mouseDownItem = GetListViewItem(this.playMediaList, e.GetPosition(this.playMediaList));
                    DragDrop.DoDragDrop(this.playMediaList, mouseDownItem, DragDropEffects.Copy);
                }
            }
        }

        private double GetMoveDirection(Point p1, Point p2)
        { return Math.Abs(p1.X - p2.X) + Math.Abs(p1.Y - p2.Y); }
    }
}
