﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.util;
using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.Plugins;

using LivanaiTiles.NextTrain.VM;
using LivanaiTiles.Utils;
using System.Windows.Controls;

namespace LivanaiTiles.NextTrain
{
    /// <summary>
    /// NextTrain型式の時刻表表示タイルの抽象クラス.
    /// 大小のタイルの共通部分をこのクラスに実装する
    /// </summary>
    public abstract class AbstractNextTrainProvider : ITileProvider
    {
        /// <inheritdoc/>
        public abstract Guid ProviderID { get; }

        /// <inheritdoc/>
        public abstract string Name { get; }

        public ILogger Logger { get; set; }

        /// <summary>
        /// セッション保存時に対象となったタイルのID.
        /// 保存ディレクトリ内で管理対象外のディレクトリを検出するのに使う.
        /// </summary>
        protected HashSet<Guid> SavedTileIDList { get; set; }

        /// <inheritdoc/>
        public bool InitProvider(IAppController controller)
        {
            this.Logger = controller.Logger;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            return true;
        }

        /// <inheritdoc/>
        public TileInfo CreateTile(
            IAppController controller,
            string parameter)
        {
            return CreateNextTrainTile(
                controller,
                (string.IsNullOrWhiteSpace(parameter) ? null : ParseConfig(parameter)));
        }

        /// <inheritdoc/>
        public TileInfo RestoreTile(
            IAppController controller,
            Guid windowID)
        {
            // 時刻表ファイルがあれば読み込み対象にする
            var saveDir = FileUtils.GetWindowConfigDirectory(this, windowID, false);
            var saveFilePath = Path.Combine(saveDir.FullName, "nexttrain.tbl");

            return CreateNextTrainTile(
                controller,
                ((saveDir.Exists && new FileInfo(saveFilePath).Exists) ? saveFilePath : null));
        }

        /// <inheritdoc/>
        public void SaveProviderSession(IAppController controller)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"[AbstractNextTrainProvider] save provider session");

            if (this.SavedTileIDList == null)
            { this.SavedTileIDList = new HashSet<Guid>(); }

            // 保存したタイルID以外の名前を持つディレクトリは削除
            foreach (var child in FileUtils.GetProviderConfigDirectory(this, true).GetDirectories())
            {
                try
                {
                    var dirID = new Guid(child.Name);
                    if (!this.SavedTileIDList.Contains(dirID))
                    {
                        // 現在存在しているタイルのID以外のディレクトリは削除する
                        this.Logger.PrintPush(LogLevel.DEBUG, $"[AbstractNextTrainProvider] remove directory removed tile id [{child.FullName}]");
                        child.Delete(true);
                        this.Logger.PrintPop();
                    }
                }
                catch (Exception)
                {
                    // GUIDとして認識できないディレクトリ名や削除に失敗したディレクトリは無視
                }
            }
            this.Logger.PrintPop();
        }

        /// <inheritdoc/>
        public void SaveTileSession(
            IAppController controller,
            Guid windowID,
            UserControl tile)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"[AbstractNextTrainProvider] save tile session id {windowID.ToString()}");

            // タイルが表示している時刻表情報を保存する
            var vm = tile.DataContext as NextTrainTileViewModel;
            if (vm != null)
            {
                try
                {
                    var saveDir = FileUtils.GetWindowConfigDirectory(this, windowID, true);
                    var saveFilePath = Path.Combine(saveDir.FullName, "nexttrain.tbl");

                    using (var writer = new StreamWriter(
                        new FileStream(
                            saveFilePath,
                            FileMode.Create,
                            FileAccess.Write),
                        new UTF8Encoding(false)))
                    { writer.Write(vm.TblDataString); }
                }
                catch (Exception)
                {
                    // 保存に失敗したファイルは無視
                }
            }

            if (this.SavedTileIDList == null)
            { this.SavedTileIDList = new HashSet<Guid>(); }
            this.SavedTileIDList.Add(windowID);

            this.Logger.PrintPop();
        }

        /// <inheritdoc/>
        public void ShutdownProvider(IAppController controller)
        {
            this.Logger = null;
        }

        /// <inheritdoc/>
        public void ShutdownTile(IAppController controller, Guid windowID, UserControl tile)
        { }

        /// <summary>
        /// タイル表示時の引数から初期表示対象の時刻表ファイルのパスを取り出して返す
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        protected string ParseConfig(string parameter)
        {
            var argsDef = new CommandLineOptionDefinition[]
            {
                new CommandLineOptionDefinition()
                {
                    RequireParam = true,
                    AllowMultiple = false,
                    LongSwitch = "file",
                },
            };

            var result = CommandLineUtils.Parse(
                CommandLineUtils.ParseCommandLineArgs(parameter),
                argsDef,
                false);

            if (result.Options.ContainsKey("file"))
            { return result.Options["file"].ElementAt(0); }
            else
            { return null; }
        }

        /// <summary>
        /// タイル情報を生成する
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="tblFilePath">時刻表ファイルを表示する場合、そのパス. 初期表示する場合はnull</param>
        /// <returns></returns>
        protected abstract TileInfo CreateNextTrainTile(
            IAppController controller,
            string tblFilePath);
    }
}
