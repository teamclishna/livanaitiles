﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// 到着駅情報を格納するオブジェクト
    /// </summary>
    public class ArrivalTime
    {
        /// <summary>指定なしの場合の列車種別として辞書に指定するキー</summary>
        public static readonly char TRAIN_ALL = '*';

        /// <summary>無印の場合の列車種別として辞書に指定するキー</summary>
        public static readonly char TRAIN_NOMARK = '-';

        /// <summary>
        /// 到着駅の駅名
        /// </summary>
        public string StationName
        { get; set; }

        /// <summary>
        /// 列車種別と到着時間を格納した辞書.
        /// キーは備考の文字、値はこの駅までの時間. 負の値を指定した場合は通過
        /// </summary>
        public Dictionary<char,int> Arrival
        { get; private set; }

        /// <summary>
        /// 指定した列車種別の到着時間を返す
        /// </summary>
        /// <param name="trainType">列車種別. 無印を指定する場合は TRAIN_NOMARK</param>
        /// <returns>
        /// 指定した列車種別の到着時間.
        /// 指定した種別の設定が存在しない場合は指定なしの到着時間.
        /// その設定も存在しない場合は通過
        /// </returns>
        public int Query(char trainType)
        {
            return (
                this.Arrival.ContainsKey(trainType) ? this.Arrival[trainType] :
                this.Arrival.ContainsKey(TRAIN_ALL) ? this.Arrival[TRAIN_ALL] :
                -1);
        }

        /// <summary>
        /// 指定した列車種別の集合の中で、最初に見つかった種別の到着時間を返す
        /// </summary>
        /// <param name="trainTypes"></param>
        /// <returns></returns>
        public int Query(HashSet<char> trainTypes)
        {
            foreach(var t in trainTypes)
            {
                if(this.Arrival.ContainsKey(t))
                { return this.Arrival[t]; }
            }

            //どの種別に対する定義もなかった場合は無印の到着時間を返す
            //無印の定義がない場合は全列車の定義を返す
            return (
                this.Arrival.ContainsKey(TRAIN_NOMARK) ? this.Arrival[TRAIN_NOMARK] :
                this.Arrival.ContainsKey(TRAIN_ALL) ? this.Arrival[TRAIN_ALL] :
                -1);
        }
            
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ArrivalTime()
        {
            this.Arrival = new Dictionary<char, int>();
        }
    }
}
