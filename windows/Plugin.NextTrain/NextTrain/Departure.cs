﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// 発車予定1件を示すデータ
    /// </summary>
    public class Departure
    {
        /// <summary>
        /// この発射予定の親になる時刻表
        /// </summary>
        public TimeTable Parent { get; set; }

        /// <summary>
        /// この発車予定に含まれる備考のフラグ
        /// </summary>
        public HashSet<char> Notes { get; set; }

        /// <summary>
        /// 発車時刻.
        /// 日付を無視して時刻部分のみ使う
        /// </summary>
        public DateTime DepartureTime { get; set; }

        /// <summary>
        /// この発車予定に含まれる備考の記号を列挙して返す
        /// </summary>
        public string NotesString
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var c in this.Notes)
                { sb.Append(c); }

                return sb.ToString();
            }
        }

        /// <summary>
        /// 画面表示用に備考を連結した文字列
        /// </summary>
        public string NotesViewString
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var note in this.Notes)
                { sb.Append(this.Parent.Parent.Notes[note]).Append(' '); }

                return sb.ToString();
            }
        }

        public override string ToString()
        {
            //発車時刻と備考情報
            return $"[{this.DepartureTime.ToString("HH:mm")} {{{this.NotesViewString}}}]";
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Departure()
        { }
    }
}
