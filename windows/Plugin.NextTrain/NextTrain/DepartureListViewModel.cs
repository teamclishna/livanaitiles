﻿using System.Collections.Generic;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// 時刻表データの1時間ごとの行を示すViewModel
    /// </summary>
    public class DepartureListViewModel
    {
        /// <summary>
        /// この行の「時」を示す値
        /// </summary>
        public int Hour
        { get; set; }

        /// <summary>
        /// この1時間の発車時刻データ
        /// </summary>
        public List<Departure> Deps
        { get; set; }

        public string HourView
        {
            get { return this.Hour.ToString("00"); }
        }

        public List<DepartureViewModel> Departures
        {
            get
            {
                var result = new List<DepartureViewModel>();
                foreach (var dep in this.Deps)
                { result.Add(new DepartureViewModel() { Dep = dep, }); }
                return result;
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DepartureListViewModel()
        { }
    }
}
