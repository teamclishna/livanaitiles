﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// 発車時刻1件を表示するためのViewModel
    /// </summary>
    public class DepartureViewModel
    {
        public Departure Dep
        { get; set; }

        /// <summary>
        /// 発車時刻表示
        /// </summary>
        public string MinuteView
        { get { return this.Dep.DepartureTime.ToString("mm"); } }

        /// <summary>
        /// 備考の記号の列挙
        /// </summary>
        public string NotesString
        { get { return this.Dep.NotesString; } }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public  DepartureViewModel()
        { }
    }
}
