﻿using System;
using System.Text;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// 直近の発車時刻を表示するためのViewModel
    /// </summary>
    public class NextDepartureViewModel
    {
        public Departure Dep { get; set; }

        public string DepartureTime
        {
            get
            {
                //日付が2日(翌日)になっている場合は24時以降として表示する
                var departureTime = this.Dep.DepartureTime;
                return string.Format(
                    "{0:00}:{1:00}",
                    (1 < departureTime.Day ? 24 : 0) + departureTime.Hour,
                    departureTime.Minute);
            }
        }

        public string TrainType
        {
            get
            {
                var notesTable = this.Dep.Parent.Parent;

                //備考情報をすべて連結して返す
                var sb = new StringBuilder();
                foreach (var note in this.Dep.Notes)
                {
                    if (notesTable.Notes.ContainsKey(note))
                    { sb.Append(notesTable.Notes[note]).Append("\n"); }
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// この発車時刻までの残り時間を表示する
        /// </summary>
        public string RemainTime
        {
            get
            {
                //日付を無視した現在時刻からの差を求める
                var now = DateTime.Now;
                var currentTime = new DateTime(1, 1, 1, now.Hour, now.Minute, 0);
                var span = this.Dep.DepartureTime - currentTime;

                var sb = new StringBuilder();
                if (0 < span.Hours)
                { sb.Append(string.Format("{0} Hrs, {1} min.", span.Hours, span.Minutes)); }
                else
                { sb.Append(string.Format("{0} minutes.", span.Minutes)); }

                return sb.ToString();
            }
        }

        public NextDepartureViewModel()
        { }
    }
}
