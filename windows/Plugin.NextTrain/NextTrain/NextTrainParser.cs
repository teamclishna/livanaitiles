﻿using System;
using System.Collections.Generic;
using System.Linq;

using clUtils.log;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// NextTrain形式の文字列データを読み取って時刻表データを生成して返すためのオブジェクト
    /// </summary>
    public class NextTrainParser
    {
        public ILogger Logger { get; set; }

        /// <summary>
        /// 時刻表データのうち、曜日指定で区切られている1つのブロックを示す構造体
        /// </summary>
        public struct TimeTableBlock
        {
            /// <summary>
            /// このブロックを適用する曜日.
            /// 曜日指定が完全に存在しない場合はALLのブロックとして扱う
            /// </summary>
            public List<Weekday> Weekdays;

            /// <summary>
            /// このブロックに含まれるデータ.
            /// </summary>
            public List<string> lines;
        }

        /// <summary>
        /// 文字列データを解析して時刻表データを生成して返す
        /// </summary>
        /// <param name="source">NextTrain形式の時刻表データの文字列</param>
        /// <returns>生成した時刻表データ</returns>
        public TblData Parse(string source)
        { return Parse(source.Split(new char[] { '\r', '\n', })); }

        /// <summary>
        /// 文字列データを解析して時刻表データを生成して返す
        /// </summary>
        /// <param name="source">NextTrain形式の時刻表データを格納した、行単位の文字列配列</param>
        /// <returns>生成した時刻表データ</returns>
        public TblData Parse(string[] source)
        {
            var result = new TblData();

            //コメントと空行を取り除く
            var normalizedSource = IgnoreComments(source);

            //備考データの取得
            var notes = FindNotes(normalizedSource);
            foreach (var key in notes.Keys)
            { result.Notes.Add(key, notes[key]); }

            //Tblデータを曜日ごとのブロックに分割し、ブロックごとに時刻表データのパースを行う
            //パースしたデータはその曜日([XXX] 表記で複数指定可能) すべての値として辞書に格納する
            var weekdayData = FindTimeTableBlocks(normalizedSource);
            foreach (var data in weekdayData)
            {
                var timeTable = ParseTimeTable(data.lines, result);
                foreach (var day in data.Weekdays)
                { result.TimeTables.Add(day, timeTable); }
            }

            //「祝日」を除くすべての曜日のデータが存在していれば「ALL」の時刻表データを削除する
            //このとき、ALLに格納していた到着時刻リストはTblデータグローバルの情報として格納する
            if(CheckContainsAllWeekday(result.TimeTables))
            {
                result.ArrivalTimeList.AddRange(result.TimeTables[Weekday.ALL].ArrivalTimeList);
                result.TimeTables.Remove(Weekday.ALL);
            }

            return result;
        }

        /// <summary>
        /// 入力データから空の行とコメントを除き、前後の空文字を取り除いた配列を返す
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private string[] IgnoreComments(string[] source)
        {
            return source
                .Where((s) => { return (0 < s.Length && s.Trim()[0] != ';'); })
                .Select<string,string>((s)=> { return s.Trim(); })
                .ToArray();
        }

        /// <summary>
        /// 入力データから備考を抜き出して辞書に格納して返す.
        /// 備考は1文字目がアルファベッド、かつ2文字目がコロンの行を対象とし、コロン以降を備考とする
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private Dictionary<char, string> FindNotes(string[] source)
        {
            var result = new Dictionary<char, string>();
            var lines = source
                .Where((s) => { return (2 < s.Length && s[1] == ':'); })
                .Where((s) => { return (('a' <= s[0] && s[0] <= 'z') || 'A' <= s[0] && s[0] <= 'Z'); });
            foreach (var line in lines)
            { result.Add(line[0], line.Substring(2)); }

            return result;
        }

        /// <summary>
        /// 入力データを曜日ごとのブロックに分割して返す
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private List<TimeTableBlock> FindTimeTableBlocks(string[] source)
        {
            var result = new List<TimeTableBlock>();
            TimeTableBlock current = new TimeTableBlock()
            {
                Weekdays = new List<Weekday>() { Weekday.ALL },
                lines = new List<string>(),
            };

            foreach(var line in source)
            {
                if(line[0] == '[')
                {
                    //曜日指定のラベルが出現したら新しい曜日データを生成する
                    result.Add(current);
                    current = new TimeTableBlock()
                    {
                        Weekdays = ParseWeekdayLabels(line),
                        lines = new List<string>(),
                    };
                }
                else
                {
                    //行の内容を格納する
                    current.lines.Add(line);
                }
            }
            //最後に現在走査中だった要素をリストに格納してから返す
            result.Add(current);
            return result;
        }

        /// <summary>
        /// 文字列を分割して曜日ラベルを生成して返す
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private List<Weekday> ParseWeekdayLabels(string source)
        {
            var result = new List<Weekday>();
            var labels = source.Split(new char[] { '[' });
            foreach(var label in labels)
            {
                if (0 < label.IndexOf(']'))
                {
                    Weekday wd;
                    if (Enum.TryParse<Weekday>(label.Substring(0, label.IndexOf(']')), out wd))
                    { result.Add(wd); }
                }
            }

            return result;
        }

        /// <summary>
        /// 時刻表データを生成して返す
        /// </summary>
        /// <param name="source">1ブロック分の時刻表データのもとになる文字列</param>
        /// <param name="parent">親データ</param>
        /// <returns></returns>
        private TimeTable ParseTimeTable(List<string> source, TblData parent)
        {
            var result = new TimeTable() { Parent = parent, };

            //時刻表データの最初に出現する「時」. これより小さい値は24時以降として扱う
            int firstHour = 0;

            foreach (var line in source)
            {
                //シャープで始まる行は時刻表のラベル
                if (line[0] == '#')
                { result.Title = line.Substring(1); }

                //ドル記号で始まる行は到着時間情報
                if (line[0] == '$')
                {
                    var arrival = ParseArrivalTime(line);
                    if (arrival != null)
                    { result.ArrivalTimeList.Add(arrival); }
                }

                //コロンで区切られており、かつコロンの前が数値として解釈できる場合、時刻表データとして解釈する
                int colonIndex = line.IndexOf(':');
                int hour = 0;
                if (0 < colonIndex && int.TryParse(line.Substring(0, colonIndex), out hour))
                {
                    //最初の行より小さい「時」は24時以降として扱う
                    if (firstHour == 0)
                    { firstHour = hour; }
                    else
                    {
                        if(hour<firstHour)
                        { hour += 24; }
                    }

                    //1時間分の時刻データを解釈する
                    var hourDeparture = ParseHour(hour, line.Substring(colonIndex + 1), result);

                    if (!result.Hour.ContainsKey(hour))
                    { result.Hour.Add(hour, hourDeparture); }
                    else
                    { result.Hour[hour].AddRange(hourDeparture); }
                }
            }

            return result;
        }

        /// <summary>
        /// 1時間分の時刻表データの解析を行う
        /// </summary>
        /// <param name="hour">解析する行の「時」を示す数値. 値域は24時以上になってもよい.</param>
        /// <param name="hourLine"></param>
        /// <param name="parent">親データ</param>
        /// <returns></returns>
        private List<Departure> ParseHour(int hour, string hourLine, TimeTable parent)
        {
            var result = new List<Departure>();

            //時刻表データはスペース区切り
            foreach (var value in hourLine.Split(new char[] { ' ' }))
            {
                result.Add(new Departure()
                {
                    Parent = parent,
                    DepartureTime = new DateTime(1, 1, (24 <= hour ? 2 : 1), (hour % 24), ParseMinute(value), 0),
                    Notes = ParseNotes(value),
                });
            }

            return result;
        }

        /// <summary>
        /// 指定した1件の時刻データから備考の部分を取り出してハッシュセットに格納して返す
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private HashSet<char> ParseNotes(string value)
        {
            var result = new HashSet<char>();
            foreach(var c in value.ToCharArray())
            {
                //数字が出現したら終わり
                if('0' <= c && c <= '9')
                { return result; }
                result.Add(c);
            }
            return result;
        }

        /// <summary>
        /// 指定した1件の時刻データから「分」の部分を取り出して返す
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private int ParseMinute(string value)
        {
            int minute = 0;
            foreach (var c in value.ToCharArray())
            {
                //数字以外は無視する
                if(c < '0' || '9' < c)
                { continue; }
                else
                { minute = minute * 10 + (c - '0'); }
            }
            return minute;
        }

        /// <summary>
        /// 到着時間情報を解析してオブジェクトを返す
        /// </summary>
        /// <param name="src">到着時間情報の行</param>
        /// <returns>解析結果のオブジェクト. パースに失敗した場合はnull</returns>
        private ArrivalTime ParseArrivalTime(string src)
        {
            //コロンの手前は駅名情報、後ろに列車種別ごとの到着時間
            var values = src.Substring(1).Split(new char[] { ':' });
            if (2 <= values.Length)
            {
                var result = new ArrivalTime() { StationName = values[0].Trim() };
                var types = values[1].Trim().Split(new char[] { ' ' });

                foreach (var t in types)
                {
                    int n;
                    if(int.TryParse(t, out n))
                    {
                        //数値のみの場合はすべての列車
                        result.Arrival.Add(ArrivalTime.TRAIN_ALL, n);
                    }
                    else if(t == "=")
                    {
                        //イコールのみの場合は無印の列車が通過
                        result.Arrival.Add(ArrivalTime.TRAIN_NOMARK, -1);
                    }
                    else
                    {
                        //数字かイコールが現れるまでを列車種別として取得する
                        var trainTypes= new List<char>();
                        foreach(var train in t.ToCharArray())
                        {
                            if(('0' <= train && train <= '9') || '=' == train)
                            { break; }
                            else
                            { trainTypes.Add(train); }
                        }
                        //列車種別以降を到着時間としてパース
                        int arriveTime = 0;
                        if(t[trainTypes.Count] == '=')
                        { arriveTime = -1; }
                        else
                        { int.TryParse(t.Substring(trainTypes.Count), out arriveTime); }

                        //列車種別ごとに到着時間を格納する
                        //ただし、ハイフンとアスタリスクが現れた場合は無視(仕様上は使われないはずだが)
                        foreach(var train in trainTypes)
                        {
                            if (train == '-' || train == '*')
                            { }
                            else
                            { result.Arrival.Add(train, arriveTime); }
                        }
                    }
                }

                return result;
            }
            else
            { return null; }
        }

        /// <summary>
        /// 時刻表データの中にすべての曜日のデータが格納されているかどうかを確認する
        /// </summary>
        /// <param name="timetables">チェック対象の時刻表データ</param>
        /// <returns>時刻表データの中に日曜～土曜のすべての曜日のデータが含まれていればtrue</returns>
        private bool CheckContainsAllWeekday(Dictionary<Weekday ,TimeTable> timetables)
        {
            foreach(var wd in new Weekday[] {  Weekday.SUN, Weekday.MON, Weekday.TUE, Weekday.WED, Weekday.THU , Weekday.FRI, Weekday.SAT})
            {
                if(!timetables.ContainsKey(wd))
                { return false; }
            }
            return true;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public NextTrainParser()
        { }
    }
}
