﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// Tblフォーマットのデータ
    /// </summary>
    public class TblData
    {
        /// <summary>
        /// 備考.
        /// A～Z, a～z 1文字をキーとしてタイトルを格納する
        /// </summary>
        public Dictionary<char, string> Notes
        { get; private set; }

        /// <summary>
        /// 1日分の時刻表データ.
        /// 曜日をキーとしてデータを格納するが、複数の曜日で同一インスタンスを格納する場合もある(例えば 平日 → 月～金 など)
        /// </summary>
        public Dictionary<Weekday, TimeTable> TimeTables
        { get; private set; }

        /// <summary>
        /// 指定した曜日の時刻表データを取得する
        /// </summary>
        /// <param name="wd"></param>
        /// <returns></returns>
        public TimeTable GetTimeTable(Weekday wd)
        {
            //指定した曜日の時刻表が存在しない場合はラベル無しのデータを返す
            return (this.TimeTables.ContainsKey(wd) ? this.TimeTables[wd] : this.TimeTables[Weekday.ALL]);
        }

        /// <summary>
        /// 備考の一覧をリスト格納して返す
        /// </summary>
        public List<TblNote> NotesList
        {
            get
            {
                var result = new List<TblNote>();
                foreach (var key in this.Notes.Keys.OrderBy((c) => { return c; }))
                { result.Add(new TblNote() { Key = new string(key, 1), Title = this.Notes[key] }); }
                return result;
            }
        }

        /// <summary>
        /// 各駅の到着時間リスト.
        /// 曜日ごとの設定が存在しない場合はここに格納した内容を参照する.
        /// </summary>
        public List<ArrivalTime > ArrivalTimeList
        { get; private set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TblData()
        {
            this.Notes = new Dictionary<char, string>();
            this.TimeTables = new Dictionary<Weekday, TimeTable>();
            this.ArrivalTimeList = new List<ArrivalTime>();
        }
    }

    /// <summary>
    /// 時刻表の備考データを示す構造体
    /// </summary>
    public struct TblNote
    {
        public string Key { get; set; }
        public string Title { get; set; }
    }
}
