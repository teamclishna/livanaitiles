﻿using System;
using System.Collections.Generic;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// 1日分の時刻表データ
    /// </summary>
    public class TimeTable
    {
        /// <summary>
        /// この時刻表データの親になるTblデータ
        /// </summary>
        public TblData Parent { get; set; }

        /// <summary>
        /// この日の時刻表のタイトル
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 1時間ごとの時刻表データ
        /// </summary>
        public Dictionary<int, List<Departure>> Hour { get; private set; }

        /// <summary>
        /// 到着時間リスト
        /// </summary>
        public List<ArrivalTime> ArrivalTimeList { get; private set; }

        /// <summary>
        /// 直近の指定した件数の発車予定を検索して返す
        /// </summary>
        /// <param name="currentTime">検索の起点とする時刻</param>
        /// <param name="itemCount">返す時刻情報の最大数</param>
        /// <returns></returns>
        public List<Departure> GetNextDepartures(DateTime currentTime, int itemCount)
        {
            var result = new List<Departure>();

            //現在時刻より後の発車時刻を丸ごと取得し、先頭から指定件数だけを返す
            //このとき、時刻のみを見るため、日付を強制的に1年1月1日にする。
            var findTime = new DateTime(1, 1, 1, currentTime.Hour, currentTime.Minute, 0);
            foreach (var d in GetDepartures(findTime))
            {
                result.Add(d);
                if (result.Count == itemCount)
                { return result; }
            }

            return result;
        }

        /// <summary>
        /// 指定した時刻より後にあるすべての発車予定を格納して時刻順にソートして返す
        /// </summary>
        /// <param name="currentTime">現在時刻. この時刻より後 (同じ時刻を含む) の発車時刻を返す</param>
        /// <returns></returns>
        private List<Departure> GetDepartures(DateTime currentTime)
        {
            var result = new List<Departure>();
            foreach (var h in this.Hour.Keys)
            { result.AddRange(this.Hour[h].FindAll((d) => { return currentTime <= d.DepartureTime; })); }

            result.Sort((a, b) => { return (a.DepartureTime < b.DepartureTime ? -1 : 1); });
            return result;
        }


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TimeTable()
        {
            this.Hour = new Dictionary<int, List<Departure>>();
            this.ArrivalTimeList = new List<ArrivalTime>();
        }
    }
}
