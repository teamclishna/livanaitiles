﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// 時刻表データを表示するためのViewModel
    /// </summary>
    public class TimeTableViewModel
    {
        /// <summary>
        /// 表示対象のデータ
        /// </summary>
        public TimeTable Data
        { get; set; }

        public string Title
        { get { return (this.Data != null ? this.Data.Title : ""); } }

        /// <summary>
        /// 1時間ごとの発車時刻リスト
        /// </summary>
        public List<DepartureListViewModel> DepartureList
        {
            get
            {
                var result = new List<DepartureListViewModel>();

                if (this.Data != null)
                {
                    //発車時刻を「時」順に取得する
                    var hours = this.Data.Hour.Keys.OrderBy((h) => { return h; });
                    foreach (var h in hours)
                    { result.Add(new DepartureListViewModel() { Hour = h, Deps = this.Data.Hour[h], }); }
                }
                return result;
            }
        }

        /// <summary>
        /// 備考の一覧を記号、その内容の配列に格納して返す
        /// </summary>
        public List<TblNote> NotesList
        { get { return this.Data.Parent.NotesList; } }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TimeTableViewModel()
        { }
    }
}
