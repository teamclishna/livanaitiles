﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.NextTrain.NextTrain
{
    /// <summary>
    /// 曜日データ
    /// </summary>
    public enum Weekday
    {
        SUN,
        MON,
        TUE,
        WED,
        THU,
        FRI,
        SAT,
        /// <summary>
        /// 祝日
        /// </summary>
        HOL,
        /// <summary>
        /// 曜日設定が存在しない場合
        /// </summary>
        ALL,
    }
}
