﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using clUtils.util;
using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.Plugins;

using LivanaiTiles.NextTrain.NextTrain;
using LivanaiTiles.NextTrain.VM;
using LivanaiTiles.NextTrain.Utils;
using LivanaiTiles.Utils;

namespace LivanaiTiles.NextTrain
{
    /// <summary>
    /// 時刻表を表示するタイル (大)
    /// </summary>
    public class NextTrainLargeProvider : AbstractNextTrainProvider
    {
        /// <inheritdoc/>
        public override Guid ProviderID => new Guid("{0F407EC1-9B68-44A6-B35B-482A35A98CCD}");

        /// <inheritdoc/>
        public override string Name => "NextTrain (Large)";

        /// <inheritdoc/>
        protected override TileInfo CreateNextTrainTile(
            IAppController controller,
            string tblFilePath)
        {
            var ctl = new NextTrainLargeTile() { Provider = this, };

            WindowControlUtils.ApplyTileControlStylle(
                new FrameworkElement[] {
                    ctl.TitleLabel,
                    ctl.NextTrainIconLabel,
                    ctl.SettingsButton,
                    ctl.DepartureTime0Label,
                    ctl.DepartureTime1Label,
                    ctl.DepartureTime2Label,
                    ctl.DepartureName0Label,
                    ctl.DepartureName1Label,
                    ctl.DepartureName2Label,
                },
                new SolidColorBrush(controller.Config.ForeGroundColor),
                new SolidColorBrush(Colors.Gray));

            // 時刻表ファイルを読み込む
            string tblDataString = null;
            if (tblFilePath != null)
            {
                try
                {
                    // ファイルを開いて読み込む
                    // エンコーディングは自動判別する
                    tblDataString = NextTrainUtils.LoadTblFile(
                        tblFilePath,
                        NextTrainUtils.DetectFileEncoding(tblFilePath));
                }
                catch (Exception)
                { }
            }

            ctl.DataContext = new NextTrainTileViewModel()
            {
                Parser = new NextTrainParser() { Logger = this.Logger, },
                TblDataString = tblDataString,
            };

            return new TileInfo()
            {
                UseAeroGlass = null,
                WindowTitle = this.Name,
                BackGroundColor = null,
                TileContent = ctl,
                TileHeight = 150,
                TileWidth = 310,
            };
        }
    }
}
