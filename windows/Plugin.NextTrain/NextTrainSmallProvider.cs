﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using clUtils.util;
using clUtils.log;

using LivanaiTiles.Controllers;
using LivanaiTiles.Plugins;

using LivanaiTiles.NextTrain.NextTrain;
using LivanaiTiles.NextTrain.VM;
using LivanaiTiles.NextTrain.Utils;
using LivanaiTiles.Utils;

namespace LivanaiTiles.NextTrain
{
    /// <summary>
    /// 時刻表を表示するタイル (小)
    /// </summary>
    public class NextTrainSmallProvider : AbstractNextTrainProvider
    {
        /// <inheritdoc/>
        public override Guid ProviderID => new Guid("{8DECC2DA-0D0D-4AE0-8DE5-02A8BA4D9078}");

        /// <inheritdoc/>
        public override string Name => "NextTrain (Small)";

        /// <inheritdoc/>
        protected override TileInfo CreateNextTrainTile(
            IAppController controller,
            string tblFilePath)
        {
            var ctl = new NextTrainSmallTile() { Provider = this, };

            WindowControlUtils.ApplyTileControlStylle(
                new FrameworkElement[] {
                    ctl.TitleLabel,
                    ctl.NextTrainIconLabel,
                    ctl.SettingsButton,
                    ctl.DepartureTime0Label,
                    ctl.DepartureTime1Label,
                    ctl.DepartureName0Label,
                    ctl.DepartureName1Label,
                },
                new SolidColorBrush(controller.Config.ForeGroundColor),
                new SolidColorBrush(Colors.Gray));

            // 時刻表ファイルを読み込む
            string tblDataString = null;
            if (tblFilePath != null)
            {
                try
                {
                    // ファイルを開いて読み込む
                    // エンコーディングは自動判別する
                    tblDataString = NextTrainUtils.LoadTblFile(
                        tblFilePath,
                        NextTrainUtils.DetectFileEncoding(tblFilePath));
                }
                catch (Exception)
                { }
            }

            ctl.DataContext = new NextTrainTileViewModel()
            {
                Parser = new NextTrainParser() { Logger = this.Logger, },
                TblDataString = tblDataString,
            };

            return new TileInfo()
            {
                UseAeroGlass = null,
                WindowTitle = this.Name,
                BackGroundColor = null,
                TileContent = ctl,
                TileHeight = 150,
                TileWidth = 150,
            };
        }
    }
}
