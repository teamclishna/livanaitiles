﻿using System;
using System.Windows.Controls;
using System.Windows.Threading;

using LivanaiTiles.Controllers;
using LivanaiTiles.NextTrain.VM;
using LivanaiTiles.NextTrain.Windows;

namespace LivanaiTiles.NextTrain
{
    /// <summary>
    /// NextTrainSmallTile.xaml の相互作用ロジック
    /// </summary>
    public partial class NextTrainSmallTile : UserControl
    {
        /// <summary>
        /// このタイルを生成したプロバイダ
        /// </summary>
        public AbstractNextTrainProvider Provider { get; set; }

        /// <summary>
        /// 一定間隔で表示するフィードを切り替えるためのタイマー
        /// </summary>
        private DispatcherTimer timer;

        public NextTrainSmallTile()
        {
            InitializeComponent();

            this.timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 1000), };
            this.timer.Tick += Timer_Tick;
            this.timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            // 毎分0秒になったらViewModelの再描画チェック
            if (DateTime.Now.Second == 0)
            {
                var vm = this.DataContext as NextTrainTileViewModel;
                if (vm != null)
                { vm.UpdateCurrentTimeTable(DateTime.Now, false); }
            }
        }

        private void SettingsButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // 時刻表ファイルの設定ダイアログを開く
            var vm = this.DataContext as NextTrainTileViewModel;

            var dlg = new TblSettingsWindow()
            {
                DataContext = new TblSettingsWindowViewModel() { TblString = vm.TblDataString, },
            };

            var dlgResult = dlg.ShowDialog();
            if (dlgResult.HasValue && dlgResult.Value)
            {
                // ダイアログで入力した内容を設定する
                var dlgVM = dlg.DataContext as TblSettingsWindowViewModel;
                vm.TblDataString = dlgVM.TblString;
            }
        }
    }
}
