﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.util;

namespace LivanaiTiles.NextTrain.Utils
{
    public static class NextTrainUtils
    {
        /// <summary>
        /// 指定したファイルを開いてエンコーディングを判別する
        /// </summary>
        /// <param name="path">開こうとするファイルのパス</param>
        /// <returns></returns>
        public static Encoding DetectFileEncoding(string path)
        {
            var f = new FileInfo(path);
            var buf = new byte[f.Length];
            var s = new FileStream(path, FileMode.Open, FileAccess.Read);
            s.Read(buf, 0, (int)f.Length);

            // エンコーディングを判別する
            return EncodingUtils.DetectJapanese(buf, new UTF8Encoding(false));
        }

        /// <summary>
        /// 指定したファイルを読み込んでファイル全体の文字列を返す
        /// </summary>
        /// <param name="path">読み込むファイルのフルパス</param>
        /// <param name="encoding">ファイルのエンコーディング</param>
        /// <returns></returns>
        public static string LoadTblFile(
            string path,
            Encoding encoding)
        {
            using (var reader = new StreamReader(
                new FileStream(
                    path,
                    FileMode.Open,
                    FileAccess.Read),
                encoding))
            { return reader.ReadToEnd(); }
        }
    }
}
