﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LivanaiTiles.NextTrain.NextTrain;

namespace LivanaiTiles.NextTrain.VM
{
    /// <summary>
    /// 時刻表表示タイルのViewModel (レイアウト確認用のダミー)
    /// </summary>
    public class NextTrainTileDummyModel : NextTrainTileViewModel
    {
        public NextTrainTileDummyModel() : base()
        {
            this.Title = "Dummy Line (Sunday)";
            this.Departures = new DepartureView[]
            {
                new DepartureView(){ DepartureName = "特急大阪行",  DepartureTime = new DateTime(2020, 1, 1, 12, 34, 0), TransferTime = "12:50 Osaka\n13:45 Tokyo", },
                new DepartureView(){ DepartureName = "急行鹿児島行", DepartureTime = new DateTime(2020, 1,1, 15, 0, 0), TransferTime = null, },
                new DepartureView(){ DepartureName = "夜行",        DepartureTime = new DateTime(2020, 1, 1, 23, 40, 0), TransferTime = "06:00 Sapporo", },
            };
        }
    }
}
