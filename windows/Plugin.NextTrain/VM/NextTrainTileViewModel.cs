﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.gui;

using LivanaiTiles.NextTrain.NextTrain;

namespace LivanaiTiles.NextTrain.VM
{
    /// <summary>
    /// 時刻表表示タイルのViewModel
    /// </summary>
    public class NextTrainTileViewModel : NotifiableViewModel
    {
        /// <summary>
        /// タイルに表示する直近の発車時刻数
        /// </summary>
        private static readonly int DEFAULT_DEPARTURE_COUNT = 3;

        /// <summary>
        /// 到着時刻表示1件の項目.
        /// Departure オブジェクトを元にして行先表示、列車種別などを結合した表示用のオブジェクト
        /// </summary>
        public class DepartureView
        {
            /// <summary>
            /// 発車時刻
            /// </summary>
            public DateTime DepartureTime { get; set; }

            /// <summary>
            /// 発車時刻の文字列表記
            /// </summary>
            public string DepartureTimeString
            { get { return (string.IsNullOrEmpty(this.DepartureName) ? "" : this.DepartureTime.ToString("HH:mm")); } }

            /// <summary>
            /// 行先名
            /// </summary>
            public String DepartureName { get; set; }

            /// <summary>
            /// マウスオーバー時に表示する次の駅の到着時刻
            /// </summary>
            public string TransferTime { get; set; }

            /// <summary>
            /// 空のオブジェクトを生成する.
            /// </summary>
            public static DepartureView Empty
            {
                get
                {
                    return new DepartureView()
                    {
                        DepartureName = "",
                        DepartureTime = DateTime.MinValue,
                        TransferTime = "",
                    };
                }
            }
        }

        #region Fields

        private string _title;
        private DepartureView[] _departures;
        private string _tblString;
        private TblData _timeTable;

        #endregion

        #region Properties
        
        /// <summary>
        /// 表示する時刻表のタイトル表記
        /// </summary>
        public string Title
        {
            get { return this._title; }
            set
            {
                this._title = value;
                OnPropertyChanged(nameof(Title));
            }
        }

        /// <summary>
        /// 直近の発車時刻
        /// </summary>
        public DepartureView[] Departures
        {
            get { return this._departures; }
            set
            {
                this._departures = value;
                OnPropertyChanged(nameof(Departures));
            }
        }

        /// <summary>
        /// 表示する時刻表データの元になる文字列
        /// </summary>
        public string TblDataString
        {
            get { return this._tblString; }
            set
            {
                this._tblString = value;
                OnPropertyChanged(nameof(TblDataString));

                // 時刻表データを解析してパーサに格納する
                if (this.Parser != null)
                {
                    if (value != null)
                    { this.TimeTable = this.Parser.Parse(value); }
                    else
                    {
                        // 時刻表データを空にしたら表示案内も空にして王
                        this.Title = "";
                        this.Departures = new DepartureView[]
                        {
                            DepartureView.Empty,
                            DepartureView.Empty,
                            DepartureView.Empty,
                        };
                    }
                }
                else
                { throw new InvalidOperationException("Parser not specified."); }
            }
        }

        /// <summary>
        /// このタイルで表示対象とする時刻表データ.
        /// TblDataString プロパティを設定した時にその内容をパースしてこのプロパティに格納する.
        /// </summary>
        public TblData TimeTable
        {
            get { return this._timeTable; }
            private set
            {
                this._timeTable = value;
                OnPropertyChanged(nameof(TimeTable));

                // 表示対象の時刻表データを更新する
                UpdateCurrentTimeTable(DateTime.Now, true);
            }
        }

        /// <summary>
        /// 時刻表データの解析に使用するパーサ
        /// </summary>
        public NextTrainParser Parser { get; set; }

        #endregion

        /// <summary>
        /// 保持している時刻表データと現在時刻を元に表示対象の時刻表データを生成する
        /// </summary>
        /// <param name="dt">現在時刻</param>
        /// <param name="forceUpdate">現在時刻が直近の発車時刻を過ぎていなくても再描画を強制する場合はtrue</param>
        public void UpdateCurrentTimeTable(
            DateTime dt,
            bool forceUpdate)
        {
            if(this.TimeTable == null)
            { return; }

            if (!forceUpdate && this.Departures != null && 0 < this.Departures.Length)
            {
                // 現在表示中の直近発車時刻を過ぎていない場合は再描画不要
                // 日付部分は適当な値が格納されているので時刻の時、分部分だけで計算する
                var next = this.Departures[0].DepartureTime;
                if ((dt.Hour * 60 + dt.Minute) <= (next.Hour * 60 + next.Minute))
                { return; }
            }

            // 今日の曜日を確定し、この曜日に対応する時刻表データを取得する
            var currentWeekDay = (Weekday)dt.DayOfWeek;
            var todayTable = this.TimeTable.GetTimeTable(currentWeekDay);

            // 直近の発車時刻データを取得
            var departures = todayTable.GetNextDepartures(dt, DEFAULT_DEPARTURE_COUNT);

            // 表示用の時刻表データに変換
            var viewDepartures = new List<DepartureView>();
            foreach(var departure in departures)
            {
                // 乗り換え時間の取得

                viewDepartures.Add(new DepartureView()
                {
                    DepartureTime = departure.DepartureTime,
                    DepartureName = departure.NotesViewString,
                    TransferTime = CreateArrivalString(departure),
                });
            }
            this.Title = todayTable.Title;
            this.Departures = viewDepartures.ToArray();

            return;
        }

        /// <summary>
        /// 乗り換え時間を示すツールチップ文字列を生成する
        /// </summary>
        /// <param name="dept">到着時刻データ</param>
        /// <returns></returns>
        private static string CreateArrivalString(
            Departure dept)
        {
            var sb = new StringBuilder();

            //この列車の各駅への到着時刻を表示する
            //現在の曜日の時刻表個別に到着時刻が設定されていない場合、ALLの到着時刻データを使用する
            var arrivalList = (
                0 < dept.Parent.ArrivalTimeList.Count ? dept.Parent.ArrivalTimeList :
                dept.Parent.Parent.TimeTables.ContainsKey(Weekday.ALL) ? dept.Parent.Parent.TimeTables[Weekday.ALL].ArrivalTimeList :
                dept.Parent.Parent.ArrivalTimeList);

            if (0 < arrivalList.Count)
            {
                sb.Append('\n');
                foreach (var arr in arrivalList)
                {
                    sb.Append(arr.StationName).Append(": ");
                    if (arr.Query(dept.Notes) < 0)
                    {
                        //通過
                        sb.Append("--:--");
                    }
                    else
                    {
                        //発車時刻から指定した分を足した値を到着予定時刻として表示
                        var arrivedTime = new DateTime(1, 1, 1, dept.DepartureTime.Hour, dept.DepartureTime.Minute, 0);
                        sb.Append(arrivedTime.AddMinutes((double)arr.Query(dept.Notes)).ToString("HH:mm"));
                    }
                    sb.Append('\n');
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public NextTrainTileViewModel()
        {
            // 空の行先情報で初期化しておく
            this.Title = "";
            this.Departures = new DepartureView[]
            {
                DepartureView.Empty,
                DepartureView.Empty,
                DepartureView.Empty,
            };
            this.TimeTable = null;
        }
    }
}
