﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.gui;

using LivanaiTiles.NextTrain.Utils;

namespace LivanaiTiles.NextTrain.VM
{
    /// <summary>
    /// Tblファイルの設定用ウィンドウのViewModel
    /// </summary>
    public class TblSettingsWindowViewModel : NotifiableViewModel
    {
        #region Fields

        private string _tblString;

        #endregion

        #region Properties

        /// <summary>
        /// NextTrain (.tbl) 形式の文字列
        /// </summary>
        public string TblString
        {
            get { return this._tblString; }
            set
            {
                this._tblString = value;
                OnPropertyChanged(nameof(TblString));
            }
        }

        #endregion

        #region Commands

        public RelayCommand OpenAsCommand { get; set; }

        #endregion

        /// <summary>
        /// ファイルを開いて NextTrain 形式ファイルを読み込むコマンド
        /// </summary>
        private void OpenTblFile()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog()
            {
                Multiselect = false,
                FilterIndex = 1,
                Filter = "NextTrain Data File (*.tbl)|*.tbl|Text File (*.txt)|*.txt|All Files (*.*)|*.*",
            };

            var dlgResult = dlg.ShowDialog();
            if (dlgResult.HasValue && dlgResult.Value)
            {
                // ファイルを読み込んでその内容を保存する
                var tblDataString = NextTrainUtils.LoadTblFile(
                    dlg.FileName,
                    NextTrainUtils.DetectFileEncoding(dlg.FileName));

                this.TblString = tblDataString;
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TblSettingsWindowViewModel()
        {
            this.OpenAsCommand = new RelayCommand(x => true, (sender) => { OpenTblFile(); });
        }
    }
}
