﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LivanaiTiles.Rss.Rss
{
    /// <summary>
    /// ニュース記事の比較に使用するコンパレータ.
    /// 重複削除のために等価であることの確認にのみ使う
    /// </summary>
    public class NewsArticleComparer : IEqualityComparer<NewsArticle>
    {
        public bool Equals(NewsArticle x, NewsArticle y)
        { return x.ParmaLink == y.ParmaLink; }

        public int GetHashCode(NewsArticle obj)
        { return obj.ParmaLink.GetHashCode(); }
    }
}
