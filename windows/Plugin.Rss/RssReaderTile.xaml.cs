﻿using System;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Threading;

using LivanaiTiles.Controllers;
using LivanaiTiles.Rss.Rss;
using LivanaiTiles.Rss.Utils;
using LivanaiTiles.Rss.VM;
using LivanaiTiles.Rss.Windows;

namespace LivanaiTiles.Rss
{
    /// <summary>
    /// RssReaderTile.xaml の相互作用ロジック
    /// </summary>
    public partial class RssReaderTile : UserControl
    {
        /// <summary>
        /// このタイルを生成したプロバイダ
        /// </summary>
        public RssReaderProvider provider;

        /// <summary>
        /// フィード再取得までの残り秒数
        /// </summary>
        private int _remainTimeReload;

        /// <summary>
        /// 次のフィードを表示するまでの残り秒数
        /// </summary>
        private int _remainTimeViewChange;

        /// <summary>
        /// 一定間隔で表示するフィードを切り替えるためのタイマー
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// キャスト済みのViewModel.
        /// set はキャスト無しでも出来るのでgetのみ
        /// </summary>
        private RssReaderTileViewModel TypedDataContext
        { get { return this.DataContext as RssReaderTileViewModel; } }

        public RssReaderTile()
        {
            InitializeComponent();

            //
            var dc = new RssReaderTileViewModel()
            {
                Config = null,
                FeedTitle = "",
                Articles = new NewsArticle[0],
                CurrentArticleIndex = 0,
            };
            this.DataContext = dc;

            this.timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 1000), };
            this.timer.Tick += Timer_Tick;
            this.timer.Start();
        }

        /// <summary>
        /// 設定情報を更新する
        /// </summary>
        /// <param name="newConfig">新しい設定情報</param>
        public void SetConfig(FeedConfig newConfig)
        {
            var dc = this.TypedDataContext;
            var oldURL = (dc.Config != null ? dc.Config.URL : "");
            dc.Config = newConfig;

            // URLが変更された場合は記事を再取得する
            if (oldURL != newConfig.URL)
            { ReloadFeed(); }
        }

        /// <summary>
        /// タイマーのコールバック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            var dc = this.TypedDataContext;

            // フィード再取得
            if (dc.Config != null && 0 < dc.Config.FeedInterval)
            {
                this._remainTimeReload--;
                if (this._remainTimeReload == 0)
                { ReloadFeed(); }
            }

            // 次のフィードを表示
            if (dc.Config != null && 0 < dc.Config.FeedRefreshCount)
            {
                this._remainTimeViewChange--;
                if (this._remainTimeViewChange == 0)
                {
                    dc.CurrentArticleIndex++;
                    this._remainTimeViewChange = dc.Config.FeedRefreshCount;
                }
            }
        }

        private void btnSettings_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var dc = this.TypedDataContext;
            var vm = new FeedConfigWindowViewModel()
            {
                Config = (dc.Config != null ? dc.Config : new FeedConfig()),
            };

            // 設定画面ダイアログを表示する
            var wnd = new FeedConfigWindow() { DataContext = vm };
            var result = wnd.ShowDialog();
            if (result.HasValue && result.Value)
            {
                // 更新後の値を反映する
                var dlgDC = wnd.DataContext as FeedConfigWindowViewModel;
                dc.Config = dlgDC.Config;

                // 記事を再取得する
                ReloadFeed();
            }
        }

        /// <summary>
        /// 現在の設定で新しいフィードを取得する
        /// </summary>
        private void ReloadFeed()
        {
            var tile = this;
            var dc = this.TypedDataContext;

            new Thread(() => {
                try
                {
                    var newArticles = FeedReaderUtils.GetFeed(dc.Config.URL, AppControllerProvider.Instance.Logger);
                    var marged = FeedReaderUtils.MargeArticle(dc.Articles, newArticles.Articles, dc.Config.FeedCount);
                    dc.FeedTitle = newArticles.NewsTitle;
                    dc.Articles = marged;

                    tile.Dispatcher.Invoke(new Action(() => {
                        // リロード値の内容で ViewModel を更新する
                        tile.DataContext = dc;
                        // タイマーの残り時間をリセットする
                        tile._remainTimeReload = dc.Config.FeedInterval;
                        tile._remainTimeViewChange = dc.Config.FeedRefreshCount;
                    }));
                }
                catch (Exception)
                {
                    // TODO: エラー処理を入れる
                }
            }).Start();
        }

        /// <summary>
        /// 記事詳細表示のウィンドウを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFeedDescription_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // 現在表示されているのが有効な記事ではない場合は無視
            var showArticle = this.TypedDataContext.CurrentArticle;
            if (showArticle != null && DateTime.MinValue < showArticle.PublishDate)
            {
                // 記事の詳細表示ダイアログを開く
                // この時、詳細表示中の一覧が自動的に変わらないように
                // DataContext のインスタンスそのものではなくコピーを渡す
                var wnd = new FeedDescriptionWindow() { DataContext = this.TypedDataContext.Clone() };
                wnd.Show();
            }
        }
    }
}
