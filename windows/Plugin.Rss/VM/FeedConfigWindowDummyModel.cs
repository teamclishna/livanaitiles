﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.Rss.VM
{
    /// <summary>
    /// 設定ダイアログのViewModel
    /// (レイアウト確認用のダミーオブジェクト)
    /// </summary>
    public class FeedConfigWindowDummyModel : FeedConfigWindowViewModel
    {
        public FeedConfigWindowDummyModel() : base()
        {
            this.Config = new FeedConfig();
            this.Title = "Dummy News Feed Site";
            this.URL = "https://news-site.example.com/feed/dummy.xml";
            this.FeedCount = 20;
            this.ChangeDisplayInterval = 30;
            this.IsInterval60Checked = true;
        }
    }
}
