﻿using System;
using System.Collections.Generic;
using System.Text;

using clUtils.gui;

using LivanaiTiles.Rss;

namespace LivanaiTiles.Rss.VM
{
    /// <summary>
    /// 設定ダイアログのViewModel
    /// </summary>
    public class FeedConfigWindowViewModel : NotifiableViewModel
    {
        #region Fields

        private FeedConfig _conf;
        
        #endregion

        public FeedConfig Config
        {
            get { return this._conf; }
            set
            {
                this._conf = value;
                OnPropertyChanged("Config");
                OnPropertyChanged("Title");
                OnPropertyChanged("URL");
                OnPropertyChanged("FetchInterval");
                OnPropertyChanged("IsInterval30Checked");
                OnPropertyChanged("IsInterval60Checked");
                OnPropertyChanged("IsInterval180Checked");
                OnPropertyChanged("IsInterval360Checked");
                OnPropertyChanged("ChangeDisplayInterval");
                OnPropertyChanged("FeedCount");
            }
        }

        /// <summary>
        /// フィードのタイトル
        /// </summary>
        public string Title
        {
            get { return this._conf.Title; }
            set
            {
                this._conf.Title = value;
                OnPropertyChanged("Title");
            }
        }

        /// <summary>
        /// フィード取得先のURL
        /// </summary>
        public string URL
        {
            get { return this._conf.URL; }
            set
            {
                this._conf.URL = value;
                OnPropertyChanged("URL");
            }
        }

        /// <summary>
        /// フィードの再取得間隔
        /// </summary>
        public int FetchInterval
        {
            get { return this._conf.FeedInterval; }
            set
            {
                this._conf.FeedInterval = value;
                OnPropertyChanged("FetchInterval");
                OnPropertyChanged("IsInterval30Checked");
                OnPropertyChanged("IsInterval60Checked");
                OnPropertyChanged("IsInterval180Checked");
                OnPropertyChanged("IsInterval360Checked");
            }
        }

        /// <summary>
        /// フィードの表示を自動更新するまでの間隔
        /// </summary>
        public int ChangeDisplayInterval
        {
            get { return this._conf.FeedRefreshCount; }
            set
            {
                this._conf.FeedRefreshCount = value;
                OnPropertyChanged("ChangeDisplayInterval");
            }
        }

        /// <summary>
        /// フィードの保存件数
        /// </summary>
        public int FeedCount
        {
            get { return this._conf.FeedCount; }
            set
            {
                this._conf.FeedCount = value;
                OnPropertyChanged("FeedCount");
            }
        }

        /// <summary>
        /// 更新間隔のラジオボタンがチェックされているかどうか（30分）
        /// </summary>
        public bool IsInterval30Checked
        {
            get { return (this._conf.FeedInterval == 30); }
            set
            {
                if(value )
                {
                    this._conf.FeedInterval = 30;
                    UpdateIntervalChecked(30);
                }
            }
        }

        /// <summary>
        /// 更新間隔のラジオボタンがチェックされているかどうか（1時間）
        /// </summary>
        public bool IsInterval60Checked
        {
            get { return (this._conf.FeedInterval == 60); }
            set
            {
                if (value)
                {
                    this._conf.FeedInterval = 60;
                    UpdateIntervalChecked(60);
                }
            }
        }

        /// <summary>
        /// 更新間隔のラジオボタンがチェックされているかどうか（3時間）
        /// </summary>
        public bool IsInterval180Checked
        {
            get { return (this._conf.FeedInterval == 180); }
            set
            {
                if (value)
                {
                    this._conf.FeedInterval = 180;
                    UpdateIntervalChecked(180);
                }
            }
        }

        /// <summary>
        /// 更新間隔のラジオボタンがチェックされているかどうか（6時間）
        /// </summary>
        public bool IsInterval360Checked
        {
            get { return (this._conf.FeedInterval == 360); }
            set
            {
                if (value)
                {
                    this._conf.FeedInterval = 360;
                    UpdateIntervalChecked(360);
                }
            }
        }

        /// <summary>
        /// 更新間隔のラジオボタンがクリックされたときの同期処理
        /// </summary>
        /// <param name="newInterval"></param>
        private void UpdateIntervalChecked(int newInterval)
        {
            this._conf.FeedInterval = newInterval;
            OnPropertyChanged("FetchInterval");
            OnPropertyChanged("IsInterval30Checked");
            OnPropertyChanged("IsInterval60Checked");
            OnPropertyChanged("IsInterval180Checked");
            OnPropertyChanged("IsInterval360Checked");
        }
    }
}
