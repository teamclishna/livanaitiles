﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivanaiTiles.Rss.VM
{
    /// <summary>
    /// RSSフィード表示のタイルを表示するためのViewModel
    /// (レイアウト確認用のダミーモデル)
    /// </summary>
    public class RssReaderTileDummyModel : RssReaderTileViewModel
    {
        public RssReaderTileDummyModel() : base()
        {
            this.Config = new FeedConfig()
            {
                Title = "Dummy News",
                FeedCount = 2,
                FeedInterval = 10,
                FeedRefreshCount = 10,
                URL = "https://example.com/feed.xml",
            };
            this.FeedTitle = "Dummy News";
            this.Articles = new Rss.NewsArticle[]
            {
                new Rss.NewsArticle()
                {
                    Title = "Test News",
                    LinkUrl = "https://example.com/news.html",
                    PublishDate = DateTime.Now,
                    ParmaLink = "https://example.com/news.html",
                    Description = "This is test new article for check layout of news articles."
                },
                new Rss.NewsArticle()
                {
                    Title = "Test News2",
                    LinkUrl = "https://example.com/news.html",
                    PublishDate = DateTime.Now,
                    ParmaLink = "https://example.com/news.html",
                    Description = "This is test new article for check layout of news articles."
                },
            };
            this.CurrentArticleIndex = 0;
        }
    }
}
