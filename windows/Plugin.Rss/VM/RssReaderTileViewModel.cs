﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using Accessibility;
using clUtils.gui;
using LivanaiTiles.Rss;
using LivanaiTiles.Rss.Rss;

namespace LivanaiTiles.Rss.VM
{
    /// <summary>
    /// RSSフィード表示のタイルを表示するためのViewModel
    /// </summary>
    public class RssReaderTileViewModel : NotifiableViewModel
    {
        #region Fields

        private FeedConfig _config;
        private string _feedTitle;
        private NewsArticle[] _articles;
        private int _currentArticleIndex;

        #endregion

        /// <summary>
        /// フィード取得の設定情報
        /// </summary>
        public FeedConfig Config
        {
            get { return this._config; }
            set {
                this._config = value;
                OnPropertyChanged("TileTitle");
            }
        }

        /// <summary>
        /// タイルに表示するこのフィードのタイトル.
        /// 設定画面で明示的に設定した場合はその内容を表示する.
        /// 未設定の場合は現在取得した記事のタイトルを表示する
        /// </summary>
        public string TileTitle
        {
            get
            {
                return (
                    this._config == null ? "" :
                    !string.IsNullOrWhiteSpace(this._config.Title) ? this._config.Title :
                    this._feedTitle != null ? this._feedTitle :
                    "");
            }
        }

        /// <summary>
        /// RSSフィードから取得したフィードのタイトル
        /// </summary>
        public string FeedTitle
        {
            get { return this._feedTitle; }
            set
            {
                this._feedTitle = value;
                OnPropertyChanged("FeedTitle");
                OnPropertyChanged("TileTitle");
            }
        }

        /// <summary>
        /// RSSフィードから取得した記事
        /// </summary>
        public NewsArticle[] Articles
        {
            get { return this._articles; }
            set
            {
                this._articles = value;
                this.CurrentArticleIndex = (
                    (value != null) && (0 < value.Length) ? (this.CurrentArticleIndex % this._articles.Length) :
                    0);
                OnPropertyChanged("Articles");
                OnPropertyChanged("CurrentArticleIndex");
                OnPropertyChanged("CurrentArticle");
            }
        }

        /// <summary>
        /// 表示対象の記事のインデックス.
        /// 取得対象の記事が存在しない場合、記事の件数の範囲外のインデックスを設定した場合は0に設定される.
        /// </summary>
        public int CurrentArticleIndex
        {
            get { return this._currentArticleIndex; }
            set
            {
                if (this._articles == null || this._articles.Length == 0 || value < 0)
                { this._currentArticleIndex = 0; }
                else
                { this._currentArticleIndex = (value % this._articles.Length); }
                OnPropertyChanged("CurrentArticleIndex");
                OnPropertyChanged("CurrentArticle");
            }
        }

        /// <summary>
        /// 現在の表示対象の記事.
        /// 記事が未取得の場合、インデックスが不正な値の場合は空の記事を返す
        /// </summary>
        public NewsArticle CurrentArticle
        {
            get
            {
                if (this._articles == null || this._articles.Length <= this._currentArticleIndex)
                {
                    return new NewsArticle()
                    {
                        Title = "",
                        LinkUrl = "",
                        Description = "",
                        ParmaLink = "",
                        PublishDate = DateTime.MinValue,
                    };
                }
                else
                {
                    return this._articles[this._currentArticleIndex];
                }
            }
        }

        /// <summary>
        /// このインスタンスのコピーを生成して返す
        /// </summary>
        /// <returns></returns>
        public RssReaderTileViewModel Clone()
        {
            return new RssReaderTileViewModel()
            {
                Config = new FeedConfig()
                {
                    FeedCount = this.Config.FeedCount,
                    FeedInterval = this.Config.FeedInterval,
                    FeedRefreshCount = this.Config.FeedRefreshCount,
                    Title = this.Config.Title,
                    URL = this.Config.URL,
                },
                Articles = this.Articles,
                CurrentArticleIndex = this.CurrentArticleIndex,
            };
        }

        public RssReaderTileViewModel()
        {
            this.Articles = new NewsArticle[0];
        }
    }
}
