# Livanai Tiles

## これは何? (What's this?)

![Tiles Sample](./readme_images/livanaitiles_overview.png)

Windows Phone 7 で導入されたタイル風のデスクトップアプリケーションです。
デフォルトで実装しているタイルには

  * カレンダー
  * 時計
  * メールチェック (POP3 / IMAP4 対応, SSL非対応)
  * RSS チェッカ
  * メディアプレイヤー (MP3 / WMA 対応)
  * 時刻表表示 (NextTrain 形式)

の機能があります。タイルはプラグインとして実装しており、新しい機能を持つタイルを実装することができます。

A tile-like desktop application introduced with Windows Phone 7.
The tiles implemented by default are

  * Calendar
  * clock
  * Email check (POP3 / IMAP4. SSL not supported)
  * RSS checker
  * Media player (MP3 / WMA)
  * Timetable (NextTrain file format)

There is a function of.
The tiles are implemented as plugins and you can implement tiles with new functionality.

## 必要な動作環境 (System Requirements)

* Windows 10
* .net 6

## インストール (How to install)

このアプリケーションのインストーラはありません。
LivanaiTIles.exe とフォルダ内にあるすべてのファイルを同一のフォルダにおいて LivanaiTIles.exe を実行します。

There is no installer for this application.
Run LivanaiTIles.exe and all the files in that folder in the same folder.


## アンインストール (How to uninstall)

このアプリケーションのアンインストーラはありません。
インストール手順で説明したフォルダを削除します。

This application does not use an uninstaller.
When deleting an application, delete the files described in the installation procedure.

以下のフォルダにアプリケーションの設定ファイルが保存されます。
設定を残す必要がないのならこのフォルダとサブフォルダを削除します。

If you do not need to leave the application settings, delete this folder and its subfolders.

``%USERPROFILE%\AppData\Roaming\TeamClishnA\LivanaiTiles``


## 使い方 (How to use)

アプリケーションを起動するとタスクトレイにアイコンが追加されます。
アイコンを右クリックし、メニューのセパレータより上の項目をクリックするとタイルが表示されます。

![Icon on TaskTray](./readme_images/tasktray.png)
![Context Menu](./readme_images/contextmenu.png)

「Launch As...」をクリックするとパラメータを指定してタイルを開くことができます。
一部のタイルはパラメータを渡して起動することで、初期状態を設定することができます。

![Launch As Dialog](./readme_images/launch_as.png)

「Settings...」をクリックするとアプリケーションの設定が表示されます。

![Settings](./readme_images/settings.png)

「Save Session」をクリックすると現在開いているタイルの位置と設定を保存します。

「Exit」をクリックするとすべてのタイルを閉じ、アプリケーションを終了します。

When you start the application, an icon will be added to the task tray.
Right-click the icon and click the item above the menu separator to display the tile.

By clicking "Launch As ...", you can specify the parameters and open the tile.
Some tiles can pass configuration items as parameters.

Click "Settings ..." to display the application settings.

Click "Save Session" to save the position and settings of the tile that is currently open.

Click “Exit” to close all tiles and exit the application.


## バージョンアップの履歴 (Recent updated)

### ver 1.2.0

  * 時刻表表示タイルを新規に追加
  * Add new plugin: Timetable that show "NextTrain" format file.
  * About ダイアログで認識しているプラグインの DLL とバージョン情報を表示するよう機能を追加
  * Added display version info that installed plugin dll in About dialog.
  
    ![Show plugin versions](./readme_images/about.png)
  
  * 右クリックメニューから表示しているタイルを閉じる機能を追加
  * Add close tile menu in context menu.
  
    ![Close existing tiles](./readme_images/contextmenu_close.png)
  
  * カレンダーで祝日を設定できるようにした、また、祝日をマウスオーバーすると祝日名を表示するようにした
  * Calendar: Made it possible to set holidays. Changed to display the name when hovering over a holiday.
  
    ![Calendar Tooltip](./readme_images/calendar_tooltip.png)
  
  * 時計表示で日付の書式を C# の書式文字列で設定できるようにした
  * Date and Time: Enabled to format date and day of the week in C # formatting
  
    ![Datetime Format Settings](./readme_images/datetime_format.png)
  
  * RSS タイルで記事をクリックした時に、現在表示中の記事以外もリストから表示できるようにした
  * RSS: Enabled to display a list of loaded articles in the detailed display when an article is clicked.


### ver 1.1.1 (DateTime)

  * 今月末日の求め方に問題があるために 12 月にカレンダーのタイルが起動できない問題を修正した。

### ver 1.1.0

  * メディアプレイヤーで Windows Media Player でエンコーディングした mp3 ファイルを開いた場合に曲名などが文字化けする場合がある問題を修正した
  * メディアプレイヤーで曲送り、曲戻しボタンが押せない状態の時に背景がグレーで塗りつぶされる問題を修正した
  * メール表示で長い表題の途中に不要な空白が入る問題を修正した
  * 時計タイルに中央部分をクリックすることでカウントダウンタイマ、時刻指定のアラームを使用できる機能を追加した
  * タイル右上の設定ボタン (メディアプレイヤーの場合はプレイリスト編集ボタン) をタイルのマウスオーバー時のみ表示する設定を追加した
  * アプリケーションの設定変更後にタイルを再表示するよう修正した

  * Media Player: fix failed decode Japanese title when open mp3 file that encoded by Windows Media Player.
  * Media Player: fix previous/forward button painted background gray when disabled push.
  * Mail: fix insert unnessesary space in long subject.
  * Add setting that display settings button on upper-right corner on tiles only when mouseover.
  * Add reopen tiles when save settings.


## 未実装の機能、既知のバグ (Unimplemented features and known bugs)

メディアプレイヤーのタイルに設定を開く歯車のアイコンがありますが、機能しません。
MediaPlayer の設定画面を実装する予定でしたが、特に設定する内容が無かったので
設定画面が未実装のままアイコンだけ残しています。

Open gear icon in Media Player tile does not work.

I was planning to implement the setting screen of MediaPlayer, 
but since there was nothing to set in particular, 
the setting screen is not implemented and only the icon is left.

一部のタイトル、制作者名が未設定のmp3ファイル、wmaファイルを読み込むとアプリケーションがクラッシュする場合があります。
私が最近聞いている曲では発生していないので、対応せず放置しています。

The application may crash when reading some titles, mp3 files or wma files for which the creator name is not set.
It doesn't occur in the song I've been listening to lately, so I left it without supporting it.

IMAP4 形式のメールボックスをチェックする場合、"INBOX" 名前空間直下を検索します。
着信したメールをサーバ側で自動振り分けしている場合、新着メールを読み取ることができません。

When checking IMAP4 format mailboxes, search directly under the "INBOX" namespace.
If the incoming mail is automatically sorted on the server side, new mail cannot be read.

RSSフィードの配信時刻は取得したXMLから取得していますが、タイムゾーンを考慮していません。

The delivery time of the RSS feed is obtained from the obtained XML, 
but the time zone is not taken into consideration.


## ライセンス (License)

このアプリケーションはシェアウェアです。
アプリケーションを使用するための料金は株式会社ベクター様の「シェアレジ」で支払う事が出来ます。

料金は 500円 です。実際に支払う料金はシェアレジ手数料 (100円) と消費税が加算され、660円です。
料金を支払わないことによる機能の制限はなく、また料金を支払うことによる機能の追加はありません。
また、料金を支払った場合に特に必要な手続きはありません。継続して使用することができます。

Team ClishnA のシェアウェアに関する FAQ は以下の URL を参照してください。

This application is shareware.
The fee for using the application can be paid at "Share Registration" of Vector Co., Ltd.

The fee is 500 JPY. The actual fee to be paid is 660 JPY,
including the share registration fee (100 JPY) and consumption tax.

There are no functional restrictions due to not paying, and no additional features due to paying.
In addition, there is no special procedure required when paying the fee. You can continue to use it.

For FAQs on Team ClishnA shareware, please refer to the following URL.

* [FAQ](https://clishna.info/apps/shareware/)

### 無償バージョンの使用継続について (for Use fee free version)

Ver. 1.2.0 未満のバージョンは無償で使用することが出来ました。
これらのバージョンを使用している場合にシェアウェアであるバージョンに置き換えることは必須ではありません。
古いバージョンの使用を継続することができます。

ver. 1.2.0 未満のバージョンと現在のバージョンはプラグインに互換性がありません。
ただし、ソースコードレベルでは互換性があるので ver. 1,20 未満のバージョンで作成したプラグインを現在のバージョンで使用することができます。

逆に ver. 1.2.0 のプラグインは TileInfo クラスの追加したプロパティをコメントアウトすることで
ver. 1.2.0 未満のバージョンでコンパイルすることができます。

Versions less than ver. 1.2.0 could be used free of charge.
If you are using these versions, it is not mandatory to replace them with shareware versions. You can continue to use the older version.

The version less than ver. 1.2.0 and the current version are not compatible with the plugin.
However, since it is compatible at the source code level, plugins created with versions less than ver. 1.2.0 can be used with the current version.

Conversely, the ver. 1.2.0 plugin can be compiled with a version less than ver. 1,20 by commenting out the properties added by the TileInfo class.

### ソースコードについて (Source codes)

このアプリケーションはソースコードを公開しています。
このソースコードをダウンロードしてあなたはバグを修正したり自分に必要な機能を追加することができます。

このアプリケーションはオープンソースソフトウェア**ではありません**。

あなたはこのアプリケーションを配布、転載することはできません。
あなたが修正したソースコードを配布する場合、修正箇所のみを配布することができます。

You can use this application for free.

This application has released the source code.
You can download the source code and fix bugs or add functions.

This application is **not** open source software.

You cannot distribute or reprint this application.
When you distribute the source code that you modified,
you can distribute only the modified part.

## 作者 (Author)

Team ClishnA

* [InfoClishnA](https://clishna.info/apps/livanaitiles/)
* [Source code](https://bitbucket.org/teamclishna/livanaitiles/)
