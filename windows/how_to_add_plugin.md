# プラグインを追加するには (HOW to Add Plugins)

このアプリケーションで使用するタイルはプラグイン形式のDLLで実装しているため、
新しい機能を追加することができます。

## 必要な開発環境 (Requirements)

  * Visual Studio 2019
  * C#, .net 5.0

## プロジェクトの作成

BitBucket からソリューションをダウンロードします。
LivanaiTiles 本体に加えて clUtiles (https://bitbucket.org/teamclishna/clutilsdotnet/src/master/) も必要です。

ソリューションを開き、追加するプラグインのためのプロジェクトを追加します。
プロジェクトの形式は「クラスライブラリ (.net Core)」にします。

プロジェクトの設定を修正します。

  * SDK タイプを "net5.0-windows" に変更
  * PropertyGroup に UseWPF を追加

```
<Project Sdk="Microsoft.NET.Sdk.WindowsDesktop">

  <PropertyGroup>
    <TargetFramework>net5.0-windows</TargetFramework>
    <UseWPF>true</UseWPF>
  </PropertyGroup>
```

プロジェクトの参照設定を追加し、"LivanaiTiles.Core" を追加します。

## クラスの追加

```LivanaiTiles.Plugin.ITileProvider``` インターフェイスを実装したクラスを作成します。
LivanaiTiles は DLL からこのインターフェイスを実装したクラスを検索し、インスタンスを生成します。
このクラスは引数無しのコンストラクタを呼び出せなければなりません。

このクラスは以下のプロパティ、メソッドを実装します。

### ProviderID

一意のGUIDを返すプロパティです。LivanaiTilesはこのGUIDでプラグインを識別します。

### Name

タスクトレイのアイコンを右クリックした時に表示する表示名を返すプロパティです。
空文字、空白以外を含む任意の文字列を返します。

### InitProvider

プラグインのインスタンスを生成するとこのメソッドが最初に呼び出されます。
ここでプラグインの初期化処理を行います。

初期化に成功した場合は true を返します。

初期化に失敗した場合は false を返します。この場合、LivanalTilesはこのプラグインを無視します。

### ShutdownProvider

アプリケーションが終了するときにこのメソッドが呼び出されます。
プラグインの終了時の処理をここに実装します。

### CreateTile

右クリックメニューからタイルを表示するときにこのメソッドが呼び出されます。
引数の右クリックメニューからプラグイン名を選択した場合、引数 parameter には null が渡されます。
「Launch As...」メニューから起動した場合はダイアログで入力したパラメータが渡されます。

parameter が null の場合、空文字の場合はどちらも「デフォルト設定のタイルを生成する」動作をしなければなりません。

戻り値のTileInfoの詳細は後のセクションで節説明します。

### SaveTileSession

右クリックメニューの「Save Session」を選択した時に呼び出されます。
表示しているタイルごとにこのメソッドが呼び出され、このメソッドでタイル個別の設定を保存します。
設定を保存する場所は次のメソッドを呼び出して返されたフォルダを使用してください。

```DirectoryInfo FileUtils.GetWindowConfigDirectory(this, windowID, true);```

1番目の引数で渡したプラグインの ProvicerID、2番目の引数で渡したタイルの識別子を使用して保存場所のパスを生成します。
3番目の引数でtrueを渡した場合、このフォルダが存在しなければ当たら良く作成します。

### SaveProviderSession

右クリックメニューの「Save Session」を選択した時に呼び出されます。
表示しているタイルの SaveTileSession メソッドが呼び出された後、最後に呼び出されます。

このメソッドでプラグインの設定を保存します。
また、既に閉じたタイルの設定を保存しているフォルダは自動的に削除されないので、
このメソッドで不要なフォルダは削除する必要があります。

### RestoreTile

右クリックメニューの「Save Session」でタイルの情報を保存している場合、次にアプリケーションを起動した時に
このメソッドが呼び出されます。
メソッドの引数で渡される WindowID を元に SaveTileSession で保存した設定を読み込み、
タイルを復元してください。

## タイルのUIを作成する

タイルは以下の規則で作成するUserControlとして作成します。

  * コントロールの幅、高さは150px、または310pxにする。
  * Backgroundは設定しない。または透明 (Transparent) にする。

## TileInfoを設定する

CreateTile、RestoreTileメソッドの戻り値として設定するTileInfoには以下の値を設定します。

### UseAeroGlass

タイルの背景にAeroglassのすりガラス効果を適用する場合、trueを設定します。
すりガラス効果を適用しない場合、またはUserControlで背景色を設定する場合はnullを設定します。
null を設定した場合は "TiledWindow" と表示されます。

### WindowTitle

タイルに設定するタイトルを設定します。
ここで設定した内容は Alt + Tab やタスクマネージャで表示されるウィンドウタイトルとして表示されます。

タイルの背景にAeroglassのすりガラス効果を適用する場合、trueを設定します。
すりガラス効果を適用しない場合、またはUserControlで背景色を設定する場合はnullを設定します。

### BackgroundColor

タイル固有の背景色を設定する場合、このプロパティに背景色を設定します。
アプリケーションの設定で設定した背景色を使用する場合、nullを設定します。

### TileContent

このプラグインが提供するタイルのUserControlを生成して渡します。

### TileHeight, TileWidth

タイルのサイズを設定します。
TileContent に設定した UserControl のサイズを設定します。150、または310を推奨します。

